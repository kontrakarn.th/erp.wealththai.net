import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import Dialog from 'react-dialog'
import Picky from 'react-picky';
import 'react-picky/dist/picky.css'; // Include CSS
import Modal from 'react-awesome-modal';
import RTChart from 'react-rt-chart';
import Select from 'react-select';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import jsPDF from "jspdf";

export default class SearchHeader extends Component {

  constructor(){
    super();
    ////console.log(super());
    this.state = {
      casefilter:[],
      casename:'',
      casestate:'',
      casecode:'',
      casetype:'',
      casestatus:'',
      casechannel:'',
      caseacceptdate:'',
      casetypelist:[],
      hide:0,
      minusorplus:'fa fa-minus',
      casestatuslist:[],
      casechannellist:[],
      fromcreatedday:'',
      fromcreatedmonth:'',
      fromcreatedyear:'',
      tocreatedday:'',
      tocreatedmonth:'',
      tocreatedyear:'',
      fromfinishedday:'',
      fromfinishedmonth:'',
      fromfinishedyear:'',
      tofinishedday:'',
      tofinishedmonth:'',
      tofinishedyear:'',
      day:[],
      month:[],
      year:[],
      structurelist:[],
      structure:'',
      blockarray:[],
      blocklist:[],
      block:'',
      userlist:[],
      user:'',
      partnerblocklist:[],
      partnerblock:'',
      membername:'',
      memberlname:'',
      assetname:'',
      assetrefname:'',
      advisorname:'',
      contactname: '',
      caseSubtypeList:[],
      casesubtype:'',
      clickedpage:1,
      lastpage:0,

    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.clickhideorshow = this.clickhideorshow.bind(this);
    this.filterblockbystructure = this.filterblockbystructure.bind(this);
    this.submitallcase = this.submitallcase.bind(this);
    this.changeCasetype = this.changeCasetype.bind(this);
    this.nextpage = this.nextpage.bind(this);
    this.previouspage = this.previouspage.bind(this);
    this.pagination = this.pagination.bind(this);
    
  }
  componentDidMount() {
    axios.get('/wealththaiinsurance/load/casetype').then(response=>{
      //console.log(response.data)
      this.setState({casetypelist:response.data});

    })
    axios.get('/wealththaiinsurance/all/casestatus').then(response=>{
      //console.log(response.data)
      this.setState({casestatuslist:response.data});

    })
    axios.get('/wealththaiinsurance/load/casechannel').then(response=>{
      this.setState({casechannellist:response.data});
    })
    axios.get('/wealththaiinsurance/load/month').then(response=>{
      this.setState({month:response.data});
    })
    axios.get('/wealththaiinsurance/load/day').then(response=>{
      this.setState({day:response.data});
    })
    axios.get('/wealththaiinsurance/load/year').then(response=>{
      this.setState({year:response.data});
    })
    axios.get('/wealththaiinsurance/get/structure').then(response=>{
      this.setState({structurelist:response.data});
    })
    axios.get('/wealththaiinsurance/report/getblock').then(response=>{
      this.setState({blockarray:response.data});
    })
    axios.get('/wealththaiinsurance/load/alluser').then(response=>{
      this.setState({userlist:response.data});
    })
    axios.get('/wealththaiinsurance/load/partner').then(response=>{
      this.setState({partnerblocklist:response.data});
    })
  }
  handleSubmit(e){
    e.preventDefault();
    axios.post('/wealththaiinsurance/all/searchcasepost',{
      casecode:this.state.casecode,
      casetype:this.state.casetype,
      casename:this.state.casename,
      casestate:this.state.casestate,
      casestatus:this.state.casestatus,
      casechannel:this.state.casechannel,
      caseacceptdatefrom:this.state.fromcreatedday+"/"+this.state.fromcreatedmonth+"/"+this.state.fromcreatedyear,
      caseacceptdateto:this.state.tocreatedday+"/"+this.state.tocreatedmonth+"/"+this.state.tocreatedyear,
      finisheddatefrom:this.state.fromfinishedday+"/"+this.state.fromfinishedmonth+"/"+this.state.fromfinishedyear,
      finisheddateto:this.state.tofinishedday+"/"+this.state.tofinishedmonth+"/"+this.state.tofinishedyear,
      coordinate:this.state.user,
      userblock:this.state.block,
      partnerblock:this.state.partnerblock,
      dayremainingfrom:this.state.inputDayremainingfrom,
      dayremainingto:this.state.inputDayremainingto,
      membername:this.state.membername,
      memberlname:this.state.memberlname,
      assetname:this.state.assetname,
      assetrefname:this.state.assetrefname,
      advisorname:this.state.advisorname,
      contactname:this.state.contactname,
      casesubtype:this.state.casesubtype,

    }).then(res=>{

      //console.log(res.data.data);
      this.setState({
        casefilter:res.data.data,
        lastpage:res.data.last_page

      })
    });

  }
  submitallcase(e)
  {
    e.preventDefault();
    axios.post('/wealththaiinsurance/all/searchcasepost',{
      casecode:'',
      casetype:'',
      casename:'',
      casestate:'',
      casestatus:'',
      casechannel:'',
      caseacceptdate:'',
      coordinate:'',
      userblock:'',
      partnerblock:'',
      inputDayremainingfrom:0,
      inputDayremainingto:0,
      membername:'',
      memberlname:'',
      assetname:'',
      assetrefname:'',
      advisorname:'',

    }).then(res=>{

      //console.log(res.data);
      this.setState({
        casefilter:res.data.data,
        lastpage:res.data
      })
    });

  }
  pagination(e){
  //return  <span>{
  //    text.map(
  //      data =>
  //      <a onClick={(e) => this.nextpage(data)}>{data} &nbsp;</a>
  //    )
  //  }</span>
  if(this.state.lastpage == this.state.clickedpage){
  return <div><a onClick={this.previouspage}>Previous  &nbsp;</a> {this.state.clickedpage} &nbsp;</div>
  }else if(this.state.clickedpage == 1){
    return <div> {this.state.clickedpage} &nbsp; <a onClick={this.nextpage}>Next  &nbsp;</a></div>
  }else{
    return <div><a onClick={this.previouspage}>Previous  &nbsp;</a> {this.state.clickedpage} &nbsp; <a onClick={this.nextpage}>Next  &nbsp;</a></div>
  }

  }
  previouspage(e){
    // e.preventDefault();
   // console.log(e.preventDefault())
    let page = this.state.clickedpage-Number(1)
    if(page <= 0){
       page = 1;
    }
     axios.post('/wealththaiinsurance/all/searchcasepost?page='+page,{
     }).then(res=>{
       //console.log(res.data);
       this.setState({
         casefilter:res.data.data,
         clickedpage:page,
         lastpage:res.data.last_page

       })
     });
 
   }

  nextpage(e){
   // e.preventDefault();
  // console.log(e.preventDefault())
    let page = this.state.clickedpage+Number(1)
    axios.post('/wealththaiinsurance/all/searchcasepost?page='+page,{
    }).then(res=>{
      //console.log(res.data);
      this.setState({
        casefilter:res.data.data,
        clickedpage:page,
        lastpage:res.data.last_page

      })
    });

  }
  changeCasetype(e){
    axios.post('/wealththaiinsurance/caseSubtype',{
      casetype:e.target.value
    }).then(res=>{

      //console.log(res.data);
      this.setState({
        casetype:res.data.casetype,
        caseSubtypeList:res.data.subtype,
      })
    });
  }
  filterblockbystructure(e)
  {
    //console.log(e.target.value);

    let blockarr= this.state.blockarray.filter((block) => {
     return block.structure_id == e.target.value
   })
   this.setState({
     blocklist :blockarr
   })
   //console.log(this.state.blocklist);

 }
clickhideorshow()
{
  if(this.state.hide == 1)
  {
    this.setState({
      hide:0,
      minusorplus:'fa fa-minus'
    })
  }
  else
  {
    this.setState({
      hide:1,
      minusorplus:'fa fa-plus'
    })
  }

}
hideorshow()
{
  if(this.state.hide == 1)
  {
    return
  }
  else
  {
    return <form onSubmit={this.handleSubmit}>
    <div className="column4">
    <table >
    <tr role="row" >
      <th >รหัสงาน &nbsp;</th>
      <td ><input style={{width:'195px'}} type="text" className="form-control" onChange={(e) => this.setState({ casecode: e.target.value })} value={this.state.casecode}/></td>
   </tr>
   <tr role="row" >&nbsp;</tr>
   <tr role="row" >
    <th >ประเภทงาน &nbsp;</th>
    <td ><select className="form-control" style={{width:'195px'}} onChange={this.changeCasetype}><option value="">ไม่เลือก</option>{this.state.casetypelist.map(data => <option value={data.id}>{data.name}</option>)}</select></td>
  </tr>
  <tr role="row" >&nbsp;</tr>
   <tr role="row" >
    <th >ประเภทงานย่อย &nbsp;</th>
    <td ><select className="form-control" style={{width:'195px'}} onChange={(e) => this.setState({ casesubtype: e.target.value })}><option value="">ไม่เลือก</option>{this.state.caseSubtypeList.map(data => <option value={data.id}>{data.name}</option>)}</select></td>
  </tr>
   <tr role="row" >&nbsp;</tr>
   <tr role="row" >
    <th >ชื่องาน &nbsp;</th>
    <td ><input id="name" style={{width:'195px'}} type="text" className="form-control" onChange={(e) => this.setState({ casename: e.target.value })} value={this.state.casename}  /></td>
  </tr>
  <tr role="row" >&nbsp;</tr>
  <tr role="row" >
   <th >สถานะงาน &nbsp;</th>
   <td ><select className="form-control" style={{width:'195px'}} onChange={(e) => this.setState({ casestatus: e.target.value })}><option value="">ไม่เลือก</option>{this.state.casestatuslist.map(data => <option value={data.id}>{data.name}</option>)}</select></td>
 </tr>
 <tr role="row" >&nbsp;</tr>
  <tr role="row" ></tr>
 <tr role="row" >
    <th >ขั้นตอนงาน &nbsp;</th>
    <td ><input id="name" style={{width:'195px'}} type="text" className="form-control" onChange={(e) => this.setState({ casestate: e.target.value })} value={this.state.casestate}  /></td>
  </tr>

    </table>
    </div>
    <div className="column4">
    <table >
 <tr role="row" >
  <th >ตั้งแต่วันที่รับงาน &nbsp;</th>
  <td >
<select onChange={(e) => this.setState({ fromcreatedday: e.target.value })} name="dayex">
  <option value ="">  วัน  </option>
  <option value ="01">  01  </option>
  <option value ="02">  02  </option>
  <option value ="03">  03  </option>
  <option value ="04">  04  </option>
  <option value ="05">  05  </option>
  <option value ="06">  06  </option>
  <option value ="07">  07  </option>
  <option value ="08">  08  </option>
  <option value ="09">  09  </option>          {
    this.state.day.map(
      data =>
      <option value={data}>{data}</option>
    )
    }
  </select>

  &nbsp;

  <select  onChange={(e) => this.setState({ fromcreatedmonth: e.target.value })}>
  <option value ="">  เดือน  </option>
  <option value ="01">  01  </option>
  <option value ="02">  02  </option>
  <option value ="03">  03  </option>
  <option value ="04">  04  </option>
  <option value ="05">  05  </option>
  <option value ="06">  06  </option>
  <option value ="07">  07  </option>
  <option value ="08">  08  </option>
  <option value ="09">  09  </option>
  {
    this.state.month.map(
      data =>
      <option value={data}>{data}</option>
    )
    }
  </select>
  &nbsp;

  <select onChange={(e) => this.setState({ fromcreatedyear: e.target.value })}  >
  <option value ="">  ปี ค.ศ  </option>
  {
    this.state.year.map(
      data =>
      <option value={data}>{data}</option>
    )
    }
  </select></td>
</tr>
<tr role="row" >&nbsp;</tr>
 <tr role="row" >
  <th >ถึงวันที่รับงาน &nbsp;</th>
  <td >
<select onChange={(e) => this.setState({ tocreatedday: e.target.value })} name="dayex">
  <option value ="">  วัน  </option>
  <option value ="01">  01  </option>
  <option value ="02">  02  </option>
  <option value ="03">  03  </option>
  <option value ="04">  04  </option>
  <option value ="05">  05  </option>
  <option value ="06">  06  </option>
  <option value ="07">  07  </option>
  <option value ="08">  08  </option>
  <option value ="09">  09  </option>          {
    this.state.day.map(
      data =>
      <option value={data}>{data}</option>
    )
    }
  </select>

  &nbsp;

  <select  onChange={(e) => this.setState({ tocreatedmonth: e.target.value })}>
  <option value ="">  เดือน  </option>
  <option value ="01">  01  </option>
  <option value ="02">  02  </option>
  <option value ="03">  03  </option>
  <option value ="04">  04  </option>
  <option value ="05">  05  </option>
  <option value ="06">  06  </option>
  <option value ="07">  07  </option>
  <option value ="08">  08  </option>
  <option value ="09">  09  </option>
  {
    this.state.month.map(
      data =>
      <option value={data}>{data}</option>
    )
    }
  </select>
  &nbsp;

  <select onChange={(e) => this.setState({ tocreatedyear: e.target.value })}  >
  <option value ="">  ปี ค.ศ  </option>
  {
    this.state.year.map(
      data =>
      <option value={data}>{data}</option>
    )
    }
  </select></td>
</tr>

<tr role="row" >&nbsp;</tr>
<tr role="row" >
 <th >ตั้งแต่วันทีงานเสร็จสิ้น &nbsp;</th>
 <td >
<select onChange={(e) => this.setState({ fromfinishedday: e.target.value })} name="dayex">
 <option value ="">  วัน  </option>
 <option value ="01">  01  </option>
 <option value ="02">  02  </option>
 <option value ="03">  03  </option>
 <option value ="04">  04  </option>
 <option value ="05">  05  </option>
 <option value ="06">  06  </option>
 <option value ="07">  07  </option>
 <option value ="08">  08  </option>
 <option value ="09">  09  </option>          {
   this.state.day.map(
     data =>
     <option value={data}>{data}</option>
   )
   }
 </select>

 &nbsp;

 <select  onChange={(e) => this.setState({ fromfinishedmonth: e.target.value })}>
 <option value ="">  เดือน  </option>
 <option value ="01">  01  </option>
 <option value ="02">  02  </option>
 <option value ="03">  03  </option>
 <option value ="04">  04  </option>
 <option value ="05">  05  </option>
 <option value ="06">  06  </option>
 <option value ="07">  07  </option>
 <option value ="08">  08  </option>
 <option value ="09">  09  </option>
 {
   this.state.month.map(
     data =>
     <option value={data}>{data}</option>
   )
   }
 </select>
 &nbsp;

 <select onChange={(e) => this.setState({fromfinishedyear: e.target.value })}  >
 <option value ="">  ปี ค.ศ  </option>
 {
   this.state.year.map(
     data =>
     <option value={data}>{data}</option>
   )
   }
 </select></td>
</tr>
<tr role="row" >&nbsp;</tr>
<tr role="row" >
 <th >ถึงวันทีงานเสร็จสิ้น &nbsp;</th>
 <td >
<select onChange={(e) => this.setState({ tofinishedday: e.target.value })} name="dayex">
 <option value ="">  วัน  </option>
 <option value ="01">  01  </option>
 <option value ="02">  02  </option>
 <option value ="03">  03  </option>
 <option value ="04">  04  </option>
 <option value ="05">  05  </option>
 <option value ="06">  06  </option>
 <option value ="07">  07  </option>
 <option value ="08">  08  </option>
 <option value ="09">  09  </option>          {
   this.state.day.map(
     data =>
     <option value={data}>{data}</option>
   )
   }
 </select>

 &nbsp;

 <select  onChange={(e) => this.setState({ tofinishedmonth: e.target.value })}>
 <option value ="">  เดือน  </option>
 <option value ="01">  01  </option>
 <option value ="02">  02  </option>
 <option value ="03">  03  </option>
 <option value ="04">  04  </option>
 <option value ="05">  05  </option>
 <option value ="06">  06  </option>
 <option value ="07">  07  </option>
 <option value ="08">  08  </option>
 <option value ="09">  09  </option>
 {
   this.state.month.map(
     data =>
     <option value={data}>{data}</option>
   )
   }
 </select>
 &nbsp;

 <select onChange={(e) => this.setState({tofinishedyear: e.target.value })}  >
 <option value ="">  ปี ค.ศ  </option>
 {
   this.state.year.map(
     data =>
     <option value={data}>{data}</option>
   )
   }
 </select></td>
</tr>
     <tr role="row" >&nbsp;</tr>
  <tr role="row" >
   <th >จำนวนวันที่คงเหลือ &nbsp;</th>
   <td ><input  style={{width:'75px'}} onChange={(e) => this.setState({ inputDayremainingfrom: e.target.value })} type="number"/>
   &nbsp;&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;&nbsp;
   <input style={{width:'75px'}} onChange={(e) => this.setState({ inputDayremainingto: e.target.value })} type="number"/></td>
 </tr>
    </table>
    </div>
    <div className="column4">
    <table >
    <tr role="row" >
     <th >เส้นทางรับงาน &nbsp;</th>
     <td ><select className="form-control" style={{width:'195px'}} onChange={(e) => this.setState({ casechannel: e.target.value })}><option value="">ไม่เลือก</option>{this.state.casechannellist.map(data => <option value={data.id}>{data.name}</option>)}</select></td>
    </tr>
    <tr role="row" >&nbsp;</tr>
    <tr role="row" >
     <th >Structure</th>
     <td ><select className="form-control" style={{width:'195px'}} onChange={this.filterblockbystructure}><option value="">ไม่เลือก</option>{this.state.structurelist.map(data => <option value={data.id}>{data.name}</option>)}</select></td>
   </tr>
   <tr role="row" >&nbsp;</tr>
    <tr role="row" >
     <th >ผู้แจ้งงาน &nbsp;</th>
     <td ><select className="form-control" style={{width:'195px'}} onChange={(e) => this.setState({ block: e.target.value })}><option value="">ไม่เลือก</option>{this.state.blocklist.map(data => <option value={data.id}>{data.name}</option>)}</select></td>
   </tr>
   <tr role="row" >&nbsp;</tr>
   <tr role="row" >
    <th >ผู้ประสานงาน &nbsp;</th>
    <td ><select className="form-control" style={{width:'195px'}} onChange={(e) => this.setState({ user: e.target.value })}><option value="">ไม่เลือก</option>{this.state.userlist.map(data => <option value={data.id}>{data.firstname}</option>)}</select></td>
  </tr>
  <tr role="row" >&nbsp;</tr>
  <tr role="row" >
   <th >ผู้ให้คำปรึกษา &nbsp;</th>
   <td ><select className="form-control" style={{width:'195px'}} onChange={(e) => this.setState({ partnerblock: e.target.value })}><option value="">ไม่เลือก</option>{this.state.partnerblocklist.map(data => <option value={data.id}>{data.name}</option>)}</select></td>
 </tr>

 
    </table>
    </div>
    <div className="column4">
    <table >
 <tr role="row" >
  <th >ชื่อลูกค้า &nbsp;</th>
  <td ><input id="name" type="text" className="form-control" onChange={(e) => this.setState({ membername: e.target.value })}  /></td>
</tr>
<tr role="row" >&nbsp;</tr>
<tr role="row" >
 <th >นามสกุลลูกค้า &nbsp;</th>
 <td ><input id="name" type="text" className="form-control" onChange={(e) => this.setState({ memberlname: e.target.value })}  /></td>
</tr>
<tr role="row" >&nbsp;</tr>
<tr role="row" >
  <th >ชื่อสินทรัพย์อ้างอิงลูกค้า &nbsp;</th>
  <td ><input id="name" type="text" className="form-control" onChange={(e) => this.setState({ assetname: e.target.value })}  /></td>
</tr>
<tr role="row" >&nbsp;</tr>
<tr role="row" >
<th >ชื่ออ้างอิง &nbsp;</th>
<td ><input id="name" type="text" className="form-control" onChange={(e) => this.setState({ assetrefname: e.target.value })} /></td>
</tr>
<tr role="row" >&nbsp;</tr>
<tr role="row" >
<th >ชื่อผู้แนะนำ</th>
<td ><input id="name" type="text" className="form-control" onChange={(e) => this.setState({ advisorname: e.target.value })} /></td>
</tr>
<tr role="row" >&nbsp;</tr>
<tr role="row" >
<th >ชื่อ-นามสกุล ผู้ติดต่อ</th>
<td ><input id="name" type="text" className="form-control" onChange={(e) => this.setState({ contactname: e.target.value })} /></td>
</tr>
    </table>
    </div>
    <div className="column">
    <button type="button" onClick={this.submitallcase} className="btn btn-default" style={{folat:'right'}}>
      All Cases
    </button>&nbsp;
    <a href="/wealththaiinsurance/all/searchcase"><button type="button"  className="btn btn-warning" style={{folat:'right'}}>
    Clear Search
    </button></a>&nbsp;
      <button type="submit" className="btn btn-primary" style={{folat:'right'}}>
        <span className="glyphicon glyphicon-search" aria-hidden="true"></span>
        Search
      </button>

      </div>
   </form>
  }
}
  remainingName(data){
      let req7 = 100000000000;
      let req8 = 100000000000;
      let req9 = 100000000000;
      let daynotifyremaining = '';
      let datereal = '';
      let daterealname = '';
      if (data.require_value7 == null || data.require_value7 == '' || data.require_value7 == '//') {
          if (data.require_value7 == '//') {

          }
      } else {
          if (data.var_value71 != null) {
             
          } else {
              let explode7 = data.require_value7.split('/');
              let day7 = explode7[0];
              let month7 = explode7[1];
              let year7 = explode7[2];
              let var7merge = year7+month7+day7;
               req7 = var7merge;
          }

      }
      if (data.require_value8 == null || data.require_value8 == '' || data.require_value8 == '//') {
          if (data.require_value8 == '//') {
          }

      } else {
          if (data.var_value72 != null) {

          } else {
            let explode8 = data.require_value8.split('/');
            let day8 = explode8[0];
            let month8 = explode8[1];
            let year8 = explode8[2];
            let var8merge = year8+month8+day8;
             req8 = var8merge;
          }
      }
      if (data.require_value9 == null || data.require_value9 == '' || data.require_value9 == '//') {
          if (data.require_value9 == '//') {
          }

      } else {
          if (data.var_value73 != null) {
          } else {
            let explode9 = data.require_value9.split('/');
            let day9 = explode9[0];
            let month9 = explode9[1];
            let year9 = explode9[2];
            let var9merge = year9+month9+day9;
             req9 = var9merge;
          }
      }
      let findmin = Math.min(req7, req8, req9);
      if (req7 == findmin) {
          datereal = data.require_value7;
          daterealname = data.requirename_var7;
      } else if (req8 == findmin) {
          datereal = data.require_value8;
          daterealname = data.requirename_var8;

      } else if (req9 == findmin) {
          datereal = data.require_value9;
          daterealname = data.requirename_var9;
      } else {
      }
      if (findmin == 100000000000) {
         daterealname = '';
      }
      return daterealname;
  }
  remainingDay(data){
      let req7 = 100000000000;
      let req8 = 100000000000;
      let req9 = 100000000000;
      let daynotifyremaining = '';
      let datereal = '';
      let daterealname = '';
      if (data.require_value7 == null || data.require_value7 == '' || data.require_value7 == '//') {
          if (data.require_value7 == '//') {

          }
      } else {
          if (data.var_value71 != null) {
             
          } else {
              let explode7 = data.require_value7.split('/');
              let day7 = explode7[0];
              let month7 = explode7[1];
              let year7 = explode7[2];
              let var7merge = year7+month7+day7;
               req7 = var7merge;
          }

      }
      if (data.require_value8 == null || data.require_value8 == '' || data.require_value8 == '//') {
          if (data.require_value8 == '//') {
          }

      } else {
          if (data.var_value70 != null) {

          } else {
            let explode8 = data.require_value8.split('/');
            let day8 = explode8[0];
            let month8 = explode8[1];
            let year8 = explode8[2];
            let var8merge = year8+month8+day8;
             req8 = var8merge;
          }
      }
      if (data.require_value9 == null || data.require_value9 == '' || data.require_value9 == '//') {
          if (data.require_value9 == '//') {
          }

      } else {
          if (data.var_value72 != null) {
          } else {
            let explode9 = data.require_value9.split('/');
            let day9 = explode9[0];
            let month9 = explode9[1];
            let year9 = explode9[2];
            let var9merge = year9+month9+day9;
             req9 = var9merge;
          }
      }
      let findmin = Math.min(req7, req8, req9);
      if (req7 == findmin) {
          datereal = data.require_value7;
      } else if (req8 == findmin) {
          datereal = data.require_value8;
      } else if (req9 == findmin) {
           datereal = data.require_value9;
      } else {
      }
      if (findmin == 100000000000) {
         datereal = '';
      }

      let explode = datereal.split('/');


      if (explode.length == 3) {
          var date = new Date();
          let dayre = explode[0];
          let monthre = explode[1];
          let yearre = explode[2];
          let day = date.getDate();
          let month = date.getMonth()+1;
          let year = date.getFullYear();
          let currentdate = month + "/" + day + "/" + year;
          let renewdate = monthre + "/" + dayre + "/" + yearre;
          let datetime1 = moment(renewdate,'M/D/YYYY');
          let datetime2 = moment(currentdate,'M/D/YYYY');
          let interval = datetime1.diff(datetime2, 'days');
          daynotifyremaining = interval;


      }
      if(Number.isInteger(daynotifyremaining) == true){

      if(Math.sign(daynotifyremaining) == 1){
        if(daynotifyremaining <= 10){
          return <span style={{color:'orange'}}><b>{daynotifyremaining}</b></span>;
        }else{
          return <span>{daynotifyremaining}</span>;
        }
      }else{
        return <span style={{color:'red'}}><b>{daynotifyremaining}</b></span>;
      }
    }else{
      return <span>{daynotifyremaining}</span>;
    }
  }
  showfilterdata()
  {
    if(this.state.casefilter.length > 0)
    {
      return <div className="card-body" style={{overflowX:'auto'}}>
        <table id="example2" className="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
          <thead>
          <tr role="row" style={{backgroundColor:'#E3E3E3'}}>
          <th colSpan="7" style={{textAlign:'center'}}>ข้อมูลงาน</th>
          <th colSpan="3" style={{textAlign:'center'}}>บุคคลที่เกี่ยวข้องกับงาน</th>
          <th colSpan="2" style={{textAlign:'center'}}>ข้อมูลลูกค้า</th>
          <th colSpan="2" style={{textAlign:'center'}}>ข้อมูลสินทรัพย์ลูกค้า</th>
          <th colSpan="1" style={{textAlign:'center'}}>ผู้แนะนำ</th>
          </tr>
          <tr role="row" style={{backgroundColor:'#E3E3E3'}}>
          <th>การติดตามงาน</th>
          <th>วันที่คงเหลือ</th>
          <th>รหัสงาน</th>
          <th>ประเภทงาน</th>
          <th>ชื่องาน</th>
          <th>สถานะงาน</th>
          <th>ขั้นตอนงาน</th>
          <th>วันที่รับงาน</th>
          <th>ผู้แจ้งงาน</th>
          <th>ผู้ประสานงาน</th>
          <th>ผู้ให้คำปรึกษา</th>
          <th>ชื่อ</th>
          <th>นามสกุล</th>
          <th>ชื่อ</th>
          <th>ทะเบียนรถ</th>
          <th>ชื่อ</th>

          </tr>
          </thead>
          <tbody>
          {this.state.casefilter.map(data =>
            <tr role="row" className="table-tr" data-url={'/wealththaiinsurance/cases/'+data.id+'/detail/show'}>
            <td>{this.remainingName(data)}</td>
            <td>{this.remainingDay(data)}</td>
            <td>{data.id}</td>
            <td>{data.type_name}</td>
            <td>{data.name}</td>
            <td>{data.status_name}</td>
            <td>{data.state_name}</td>
            <td>{data.case_created_date}</td>
            <td>{data.block_name}</td>
            <td>{data.coordinator_name}</td>
            <td>{data.partner_block_name}</td>
            <td>{data.member_name}</td>
            <td>{data.member_lname}</td>
            <td>{data.asset_name}</td>
            <td>{data.asset_refname}</td>
            <td>{data.advisor_name}</td>

            </tr>
          )}
          </tbody>
          <tfoot>
          <tr role="row" >
          <th>การติดตามงาน</th>
          <th>วันที่คงเหลือ</th>
          <th>รหัสงาน</th>
          <th>ประเภทงาน</th>
          <th>ชื่องาน</th>
          <th>สถานะงาน</th>
          <th>ขั้นตอนงาน</th>
          <th>วันที่รับงาน</th>
          <th>ผู้แจ้งงาน</th>
          <th>ผู้ประสานงาน</th>
          <th>ผู้ให้คำปรึกษา</th>
          <th>ชื่อ</th>
          <th>นามสกุล</th>
          <th>ชื่อ</th>
          <th>ทะเบียนรถ</th>
          <th>ชื่อ</th>
          </tr>
          </tfoot>
          </table>
          {this.pagination()}
            </div>
    }
    else
    {
      return <div className="card-body" style={{overflowX:'auto'}}>
        <table id="example2" className="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
          <thead>
          </thead>
          <tbody>
                        <tr role="row">
                        <td style={{textAlign:'center'}}>No Data Found</td>
                        </tr>
          </tbody>
          </table>
            </div>
    }

  }
    render() {
      return (
        <div>
        <div className="card">
        <div className="card-header">
        <div className="column"><button style={{float:'right',padding:'10px'}} type="button" onClick={this.clickhideorshow} className="btn btn-box-tool" ><span style={{color:'',fontSize:'16px'}}><i className={this.state.minusorplus}></i></span></button></div>
        {this.hideorshow()}
        </div>
       {this.showfilterdata()}

       </div>
       </div>

        );
    }
}

if (document.getElementById('searchheader')) {
  const component = document.getElementById('searchheader');
  const props = Object.assign({}, component.dataset);
    ReactDOM.render(<SearchHeader {...props}/>, component);
}

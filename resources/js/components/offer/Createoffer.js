import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Select from 'react-select';

export default class Createoffer extends Component {

  constructor(){
    super();
    this.state = {
        offertype:[],
        campaign:[],
        campaignresult:'',
        promotion:[],
        proposal:[],
        referalmember:[],
        selectedreferalmember:'',
        referalbranch:[],
        selectedoffertype:[],
        flagcheckbox:'',
        offervaluename1:'',
        offervaluename2:'',
        offervaluename3:'',
        offervaluename4:'',
        offervaluename5:'',
        offervaluename6:'',
        offervaluename7:'',
        offervaluename8:'',
        offervaluename9:'',
        offervaluename10:'',
        offervaluename11:'',
        offervaluename12:'',
        offervaluename13:'',
        offervaluename14:'',
        offervaluename15:'',
        offervaluename16:'',
        offervaluename17:'',
        offervaluename18:'',
        offervaluename19:'',
        offervaluename20:'',
        offervaluename21:'',
        offervaluename22:'',
        offervaluename23:'',
        offervaluename24:'',
        offervaluename25:'',
        offervaluename26:'',
        offervaluename27:'',
        offervaluename28:'',
        offervaluename29:'',
        offervaluename30:'',
        offervaluename31:'',
        offervaluename32:'',
        offervaluename33:'',
        offervaluename34:'',
        offervaluename35:'',
        offervaluename36:'',
        offervaluename37:'',
        offervaluename38:'',
        offervaluename39:'',
        offervaluename40:'',

        offervalue1:'',
        offervalue2:'',
        offervalue3:'',
        offervalue4:'',
        offervalue5:'',
        offervalue6:'',
        offervalue7:'',
        offervalue8:'',
        offervalue9:'',
        offervalue10:'',
        offervalue11:'',
        offervalue12:'',
        offervalue13:'',
        offervalue14:'',
        offervalue15:'',
        offervalue16:'',
        offervalue17:'',
        offervalue18:'',
        offervalue19:'',
        offervalue20:'',
        offervalue21:'',
        offervalue22:'',
        offervalue23:'',
        offervalue24:'',
        offervalue25:'',
        offervalue26:'',
        offervalue27:'',
        offervalue28:'',
        offervalue29:'',
        offervalue30:'',
        offervalue31:'',
        offervalue32:'',
        offervalue33:'',
        offervalue34:'',
        offervalue35:'',
        offervalue36:'',
        offervalue37:'',
        offervalue38:'',
        offervalue39:'',
        offervalue40:'',


        offerdetailname1:'',
        offerdetailname2:'',
        offerdetailname3:'',
        offerdetailname4:'',
        offerdetailname5:'',
        offerdetailname6:'',
        offerdetailname7:'',
        offerdetailname8:'',
        offerdetailname9:'',
        offerdetailname10:'',
        offerdetailname11:'',
        offerdetailname12:'',
        offerdetailname13:'',
        offerdetailname14:'',
        offerdetailname15:'',
        offerdetailname16:'',
        offerdetailname17:'',
        offerdetailname18:'',
        offerdetailname19:'',
        offerdetailname20:'',

        offerdetailvalue1:'',
        offerdetailvalue2:'',
        offerdetailvalue3:'',
        offerdetailvalue4:'',
        offerdetailvalue5:'',
        offerdetailvalue6:'',
        offerdetailvalue7:'',
        offerdetailvalue8:'',
        offerdetailvalue9:'',
        offerdetailvalue10:'',
        offerdetailvalue11:'',
        offerdetailvalue12:'',
        offerdetailvalue13:'',
        offerdetailvalue14:'',
        offerdetailvalue15:'',
        offerdetailvalue16:'',
        offerdetailvalue17:'',
        offerdetailvalue18:'',
        offerdetailvalue19:'',
        offerdetailvalue20:'',

        offerpaymentname1:'',
        offerpaymentname2:'',
        offerpaymentname3:'',
        offerpaymentname4:'',
        offerpaymentname5:'',
        offerpaymentname6:'',
        offerpaymentname7:'',
        offerpaymentname8:'',
        offerpaymentname9:'',
        offerpaymentname10:'',
        offerpaymentname11:'',
        offerpaymentname12:'',
        offerpaymentname13:'',
        offerpaymentname14:'',
        offerpaymentname15:'',
        offerpaymentname16:'',
        offerpaymentname17:'',
        offerpaymentname18:'',
        offerpaymentname19:'',
        offerpaymentname20:'',
        offerpaymentname21:'',
        offerpaymentname22:'',
        offerpaymentname23:'',
        offerpaymentname24:'',
        offerpaymentname25:'',
        offerpaymentname26:'',
        offerpaymentname27:'',
        offerpaymentname28:'',
        offerpaymentname29:'',
        offerpaymentname30:'',
        offerpaymentname31:'',
        offerpaymentname32:'',
        offerpaymentname33:'',
        offerpaymentname34:'',
        offerpaymentname35:'',
        offerpaymentname36:'',
        offerpaymentname37:'',
        offerpaymentname38:'',
        offerpaymentname39:'',
        offerpaymentname40:'',


        offerpaymentvalue1:'',
        offerpaymentvalue2:'',
        offerpaymentvalue3:'',
        offerpaymentvalue4:'',
        offerpaymentvalue5:'',
        offerpaymentvalue5flag:0,
        offerpaymentvalue6:'',
        offerpaymentvalue7:'',
        offerpaymentvalue8:'',
        offerpaymentvalue9:'',
        offerpaymentvalue10:'',
        offerpaymentvalue11:'',
        offerpaymentvalue12:'',
        offerpaymentvalue13:'',
        offerpaymentvalue14:'',
        offerpaymentvalue14method2:'',
        caltodisspecialmethod2:'',
        offerpaymentvalue15:'',
        offerpaymentvalue16method2:'',
        caltodispartnermethod2:'',
        offerpaymentvalue16:'',
        offerpaymentvalue17:'',
        offerpaymentvalue18:'',
        offerpaymentvalue18method2:'',
        caltodisusermethod2:'',
        offerpaymentvalue19:'',
        offerpaymentvalue20:'',
        offerpaymentvalue21:'',
        offerpaymentvalue22:'',
        offerpaymentvalue23:'',
        offerpaymentvalue24:'',
        offerpaymentvalue25:'',
        offerpaymentvalue26:'',
        offerpaymentvalue27:'',
        offerpaymentvalue28:'',
        offerpaymentvalue29:'',
        offerpaymentvalue30:'',
        offerpaymentvalue31:'',
        offerpaymentvalue32:'',
        offerpaymentvalue33:'',
        offerpaymentvalue34:'',
        offerpaymentvalue35:'',
        offerpaymentvalue36:'',
        offerpaymentvalue37:'',
        offerpaymentvalue38:'',
        offerpaymentvalue39:'',
        offerpaymentvalue40:'',


        percentpromotion:'',
        partnermethod:1,
        usermethod:1,
        companymethod:1,
        cat:'',         //Commission after Tax Gross = Comm - Tax Fee 
        catc:'',        //Commission after Tax, Coor  = CAT - Co-or Fee 
        catco:'',       //Commission after Tax, Coor & Other  = CATC - Other Fee
        ncm:'',         //Net Commission after Member  = CATCO-M_Discount
        ncmp:'',        //Net Commission after Member, Partner = NCM - P_quota
        ncmpu:'',       //Net Commission after Member, Partner & User  =NCMP - U_Quota
        ci:'',          //Company Income = NCMPU - O_Discount 
        pquota:'',      //P_quota = P_Discount +  P_comm
        pcom:'',      //P_comm
        uquota:'',      //U_quota = U_Discount + U_comm
        ucom:'',      // U_comm
        odiscount:'',   //O_Discount = Baht
    }
    this.clickoffertype = this.clickoffertype.bind(this);
    this.clickreferalmember = this.clickreferalmember.bind(this);
    this.offpay5checkbox = this.offpay5checkbox.bind(this);    
    this.selectpromotion = this.selectpromotion.bind(this);   
    this.selectcampaign = this.selectcampaign.bind(this);   
    this.partnermethod2 = this.partnermethod2.bind(this);    
    this.usermethod2 = this.usermethod2.bind(this);    
    this.companymethod = this.companymethod.bind(this);    
    this.submitpartnermethod2 = this.submitpartnermethod2.bind(this);    
    this.submitusermethod2 = this.submitusermethod2.bind(this);    
    this.submitspecialmethod2 = this.submitspecialmethod2.bind(this);    

    };

    componentDidMount()
    {
        axios.get('/wealththaiinsurance/get/offertype').then(response=>{
            this.setState({offertype:response.data});
          }).catch(errors=>{
          })

        axios.get('/wealththaiinsurance/get/campaign').then(response=>{
            this.setState({campaign:response.data});
          }).catch(errors=>{
          }) 
          
        axios.get('/wealththaiinsurance/get/promotion').then(response=>{
            this.setState({promotion:response.data});
          }).catch(errors=>{
          })
       
        axios.get('/wealththaiinsurance/get/proposalwherecaseid/'+this.props.caseid).then(response=>{
            this.setState({proposal:response.data});
          }).catch(errors=>{
          })  
      
        axios.get('/wealththaiinsurance/get/membertypeorg').then(response=>{
            this.setState({referalmember:response.data});
          }).catch(errors=>{
          })      
    }

    clickoffertype(e)
    {
        //console.log(e.target.value)
        let result = this.state.offertype.find((offertype) => {
            return offertype.id == e.target.value
          })
          this.setState({   selectedoffertype:result,
                            offervaluename1:result.offer_value_name1,
                            offervaluename2:result.offer_value_name2,
                            offervaluename3:result.offer_value_name3,
                            offervaluename4:result.offer_value_name4,
                            offervaluename5:result.offer_value_name5,
                            offervaluename6:result.offer_value_name6,
                            offervaluename7:result.offer_value_name7,
                            offervaluename8:result.offer_value_name8,
                            offervaluename9:result.offer_value_name9,
                            offervaluename10:result.offer_value_name10,
                            offervaluename11:result.offer_value_name11,
                            offervaluename12:result.offer_value_name12,
                            offervaluename13:result.offer_value_name13,
                            offervaluename14:result.offer_value_name14,
                            offervaluename15:result.offer_value_name15,
                            offervaluename16:result.offer_value_name16,
                            offervaluename17:result.offer_value_name17,
                            offervaluename18:result.offer_value_name18,
                            offervaluename19:result.offer_value_name19,
                            offervaluename20:result.offer_value_name20,
                            offervaluename21:result.offer_value_name21,
                            offervaluename22:result.offer_value_name22,
                            offervaluename23:result.offer_value_name23,
                            offervaluename24:result.offer_value_name24,
                            offervaluename25:result.offer_value_name25,
                            offervaluename26:result.offer_value_name26,
                            offervaluename27:result.offer_value_name27,
                            offervaluename28:result.offer_value_name28,
                            offervaluename29:result.offer_value_name29,
                            offervaluename30:result.offer_value_name30,
                            offervaluename31:result.offer_value_name31,
                            offervaluename32:result.offer_value_name32,
                            offervaluename33:result.offer_value_name33,
                            offervaluename34:result.offer_value_name34,
                            offervaluename35:result.offer_value_name35,
                            offervaluename36:result.offer_value_name36,
                            offervaluename37:result.offer_value_name37,
                            offervaluename38:result.offer_value_name38,
                            offervaluename39:result.offer_value_name39,
                            offervaluename40:result.offer_value_name40,

                            offerdetailname1:result.offer_detail_name1,
                            offerdetailname2:result.offer_detail_name2,
                            offerdetailname3:result.offer_detail_name3,
                            offerdetailname4:result.offer_detail_name4,
                            offerdetailname5:result.offer_detail_name5,
                            offerdetailname6:result.offer_detail_name6,
                            offerdetailname7:result.offer_detail_name7,
                            offerdetailname8:result.offer_detail_name8,
                            offerdetailname9:result.offer_detail_name9,
                            offerdetailname10:result.offer_detail_name10,
                            offerdetailname11:result.offer_detail_name11,
                            offerdetailname12:result.offer_detail_name12,
                            offerdetailname13:result.offer_detail_name13,
                            offerdetailname14:result.offer_detail_name14,
                            offerdetailname15:result.offer_detail_name15,
                            offerdetailname16:result.offer_detail_name16,
                            offerdetailname17:result.offer_detail_name17,
                            offerdetailname18:result.offer_detail_name18,
                            offerdetailname19:result.offer_detail_name19,
                            offerdetailname20:result.offer_detail_name20,


                            offerpaymentname1:result.offer_payment_name1,
                            offerpaymentname2:result.offer_payment_name2,
                            offerpaymentname3:result.offer_payment_name3,
                            offerpaymentname4:result.offer_payment_name4,
                            offerpaymentname5:result.offer_payment_name5,
                            offerpaymentname6:result.offer_payment_name6,
                            offerpaymentname7:result.offer_payment_name7,
                            offerpaymentname8:result.offer_payment_name8,
                            offerpaymentname9:result.offer_payment_name9,
                            offerpaymentname10:result.offer_payment_name10,
                            offerpaymentname11:result.offer_payment_name11,
                            offerpaymentname12:result.offer_payment_name12,
                            offerpaymentname13:result.offer_payment_name13,
                            offerpaymentname14:result.offer_payment_name14,
                            offerpaymentname15:result.offer_payment_name15,
                            offerpaymentname16:result.offer_payment_name16,
                            offerpaymentname17:result.offer_payment_name17,
                            offerpaymentname18:result.offer_payment_name18,
                            offerpaymentname19:result.offer_payment_name19,
                            offerpaymentname20:result.offer_payment_name20,
                            offerpaymentname21:result.offer_payment_name21,
                            offerpaymentname22:result.offer_payment_name22,
                            offerpaymentname23:result.offer_payment_name23,
                            offerpaymentname24:result.offer_payment_name24,
                            offerpaymentname25:result.offer_payment_name25,
                            offerpaymentname26:result.offer_payment_name26,
                            offerpaymentname27:result.offer_payment_name27,
                            offerpaymentname28:result.offer_payment_name28,
                            offerpaymentname29:result.offer_payment_name29,
                            offerpaymentname30:result.offer_payment_name30,
                            offerpaymentname31:result.offer_payment_name31,
                            offerpaymentname32:result.offer_payment_name32,
                            offerpaymentname33:result.offer_payment_name33,
                            offerpaymentname34:result.offer_payment_name34,
                            offerpaymentname35:result.offer_payment_name35,
                            offerpaymentname36:result.offer_payment_name36,
                            offerpaymentname37:result.offer_payment_name37,
                            offerpaymentname38:result.offer_payment_name38,
                            offerpaymentname39:result.offer_payment_name39,
                            offerpaymentname40:result.offer_payment_name40,


                        });
          //console.log(this.state.selectedoffertype)
          
    }
    selectpromotion(e)
    {
        if(e.target.value != 0)
        {
        let result = this.state.promotion.find((promotion) => {
            return promotion.id == e.target.value
          })
          this.setState({percentpromotion:result.percent_promotion});
                      
        }else{
            this.setState({percentpromotion:0});
        }
    }
    selectcampaign(e)
    {
        if(e.target.value != 0)
        {
        let result = this.state.campaign.find((campaign) => {
            return campaign.id == e.target.value
          })

          this.setState({
            campaignresult : result,
          });
        }else{
            this.setState({

                campaignresult:'',
        });
        }
    }
    clickreferalmember(selectedreferalmember)
    {
        axios.get('/wealththaiinsurance/get/branchwhereorg/'+selectedreferalmember.value).then(response=>{
            this.setState({referalbranch:response.data,
                            selectedreferalmember});
          }).catch(errors=>{
          })  
    }
    generalinformation()
    {
        let options = this.state.referalmember.map(function (referalmember) {
            return { value: referalmember.id, label: referalmember.name};
          })
        return <div>
                    <div>
                        <h3 >Offer Value</h3>
                            <div className="column">
                                <label for="name" className="">Offer Name  <span style={{color:'red'}}>*</span></label>
                                <input id="name" type="text" className="form-control" name="name" required autoFocus/>
                            </div>

                            <div className="column">
                                <label for="case_channel " className="">Offer Type <span style={{color:'red'}}>*</span></label>
                                <select  className="form-control condition" name="type_id" onChange={this.clickoffertype} required>
                                    <option value="" >-select-</option>
                                    {this.state.offertype.map(data =>
                                        <option value={data.id} >{data.name}</option>
                                    )}
                                </select>
                            </div>
                            <div className="column">
                                <label for="name" className="">Campaign</label>
                                <select  className="form-control condition" name="campaign_id" onChange={this.selectcampaign} >
                                    <option value="" >-select-</option>
                                    {this.state.campaign.map(data =>
                                        <option value={data.id} >{data.name}</option>
                                    )}
                                </select>                            
                                </div>

                            <div className="column">
                                <label for="name" className="">Promotion</label>
                                <select  className="form-control condition" name="promotion_id" onChange={this.selectpromotion}>
                                    <option value="0" >-select-</option>
                                    {this.state.promotion.map(data =>
                                        <option value={data.id} >{data.name}</option>
                                    )}
                                </select>                            
                                </div>
                            <div className="column">
                                <label for="name" className="">Proposal <span style={{color:'red'}}>*</span></label>
                                <select  className="form-control condition" name="proposal_id" required>
                                    <option value="" >-select-</option>
                                    {this.state.proposal.map(data =>
                                        <option value={data.id} >{data.name}</option>
                                    )}
                                </select>                               
                                </div>
                            <div className="column1">
                            <div className="column">
                                <label for="name" className="">Referal Member <span style={{color:'red'}}>*</span></label>
                                <Select
                                name="ref_member_id"
                                value={this.state.selectedreferalmember}
                                options={options}
                                onChange={this.clickreferalmember}
                                maxMenuHeight={120}
                                required
                                />
                             
                                </div>

                            <div className="column">
                                <label for="case_channel " className="">Referal Branch <span style={{color:'red'}}>*</span></label>
                                <select  className="form-control condition" name="ref_branch_id" required autofocus>
                                <option value="" >-select-</option>
                                    {this.state.referalbranch.map(data =>
                                        <option value={data.id} >{data.name}</option>
                                    )}
                                </select>
                            </div>
                            </div>

                        </div>
                    </div>
    }
    tryclick()
    {
        console.log("Heyy")
    }
    offervalue()
    {
        console.log(this.state.offervalue8)

        if(this.state.selectedoffertype.length !='')
        {
            return <div>
            <div>
                   <h3 >Offer Value</h3>
                   {this.offervalue1()}
                   {this.offervalue2()}
                   {this.offervalue3()}
                   {this.offervalue4()}
                   {this.offervalue5()}
                   {this.offervalue6()}
                   {this.offervalue7()}
                   {this.offervalue8()}
                   {this.offervalue9()}
                   {this.offervalue10()}
                   {this.offervalue11()}
                   {this.offervalue12()}
                   {this.offervalue13()}
                   {this.offervalue14()}
                   {this.offervalue15()}
                   {this.offervalue16()}
                   {this.offervalue17()}
                   {this.offervalue18()}
                   {this.offervalue19()}
                   {this.offervalue20()}
                   {this.offervalue21()}
                   {this.offervalue22()}
                   {this.offervalue23()}
                   {this.offervalue24()}
                   {this.offervalue25()}
                   {this.offervalue26()}
                   {this.offervalue27()}
                   {this.offervalue28()}
                   {this.offervalue29()}
                   {this.offervalue30()}
                   {this.offervalue31()}
                   {this.offervalue32()}
                   {this.offervalue33()}
                   {this.offervalue34()}
                   {this.offervalue35()}
                   {this.offervalue36()}
                   {this.offervalue37()}
                   {this.offervalue38()}
                   {this.offervalue39()}
                   {this.offervalue40()}


                    </div>
                </div>
        }
        else
        {
            return  
        }
    }

    offerdetail()
    {
        if(this.state.selectedoffertype.length !='')
        {
            return <div>
            <div>
                   <h3 >Offer Detail</h3>
                   {this.offerdetail1()}
                   {this.offerdetail2()}
                   {this.offerdetail3()}
                   {this.offerdetail4()}
                   {this.offerdetail5()}
                   {this.offerdetail6()}
                   {this.offerdetail7()}
                   {this.offerdetail8()}
                   {this.offerdetail9()}
                   {this.offerdetail10()}
                   {this.offerdetail11()}
                   {this.offerdetail12()}
                   {this.offerdetail13()}
                   {this.offerdetail14()}
                   {this.offerdetail15()}
                   {this.offerdetail16()}
                   {this.offerdetail17()}
                   {this.offerdetail18()}
                   {this.offerdetail19()}
                   {this.offerdetail20()}

                    </div>
                </div>
        }
        else
        {
            return  
        }
    }

    offerpayment()
    {
         
        if(this.state.selectedoffertype.length !='')
        {
            return <div>
            <div>
                   <h3 >Offer Payment</h3>
                   {this.offerpayment1()}
                   {this.offerpayment2()}
                   {this.offerpayment3()}
                   {this.offerpayment4()}
                   {this.offerpayment5()}
                   {this.offerpayment6()}
                   {this.offerpayment7()}
                   {this.offerpayment8()}
                   {this.offerpayment9()}
                   {this.offerpayment10()}
                   {this.offerpayment11()}
                   {this.offerpayment12()}
                   {this.offerpayment13()}
                   {this.offerpayment14()}
                   {this.offerpayment16()}
                   {this.offerpayment18()}
                   {this.offerpayment20()}
                   {this.offerpayment21()}
                   {this.offerpayment22()}
                   {this.offerpayment23()}
                   {this.offerpayment24()}
                   {this.offerpayment25()}
                   {this.offerpayment26()}
                   {this.offerpayment27()}
                   {this.offerpayment28()}
                   {this.offerpayment29()}
                   {this.offerpayment30()}
                   {this.offerpayment31()}
                   {this.offerpayment32()}
                   {this.offerpayment33()}
                   {this.offerpayment34()}
                   {this.offerpayment35()}
                   {this.offerpayment36()}
                   {this.offerpayment37()}
                   {this.offerpayment38()}
                   {this.offerpayment39()}
                   {this.offerpayment40()}


                    </div>
                </div>
        }
        else
        {
            return  
        }
    }
    offerpayment1()
    {
        if(this.state.campaignresult.offer_payment_value_flag1 ==1 )
        {
            this.state.offerpaymentvalue1 = this.state.campaignresult.offer_payment_value1;
        }
        if(this.state.offerpaymentname1 == '' || this.state.offerpaymentname1 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname1}</label><input  id="offer_payment_value1" type="text" className="form-control" name="offer_payment_value1" onChange={(e) => this.setState({ offerpaymentvalue1: e.target.value })} value={this.state.offerpaymentvalue1} /></div>}
    }
    offerpayment2()
    {
        if(this.state.campaignresult.offer_payment_value_flag2 ==1 )
        {
            this.state.offerpaymentvalue2 = this.state.campaignresult.offer_payment_value2;
        }
        if(this.state.offerpaymentname2 == ''||this.state.offerpaymentname2 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname2}</label><input id="offer_payment_value2" type="text" className="form-control" name="offer_payment_value2" onChange={(e) => this.setState({ offerpaymentvalue2: e.target.value })} value={this.state.offerpaymentvalue2} /></div>}
    }
    offerpayment3()
    {
        if(this.state.campaignresult.offer_payment_value_flag3 ==1 )
        {
            this.state.offerpaymentvalue3 = this.state.campaignresult.offer_payment_value3;
        }
        if(this.state.offerpaymentname3 == '' || this.state.offerpaymentname3 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname3}</label><input id="offer_payment_value3" type="text" className="form-control" name="offer_payment_value3" onChange={(e) => this.setState({ offerpaymentvalue3: e.target.value })} value={this.state.offerpaymentvalue3} /></div>}
    }
    offerpayment4()
    {

        let result = (Number(this.state.offerpaymentvalue1)+Number(this.state.offerpaymentvalue2)+Number(this.state.offerpaymentvalue3)).toFixed(2)
        this.state.offerpaymentvalue4 = result
        if(this.state.campaignresult.offer_payment_value_flag4 ==1 )
        {
            this.state.offerpaymentvalue4 = this.state.campaignresult.offer_payment_value4;
        }
        if(this.state.offerpaymentname4 == '' || this.state.offerpaymentname4 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname4}</label><input readOnly id="offer_payment_value4" type="text" className="form-control" name="offer_payment_value4"  value={this.state.offerpaymentvalue4} /></div>}
    }
    offerpayment5()
    {
        
        if(this.state.offerpaymentname5 == '' ||this.state.offerpaymentname5 == null){return}
        else{
            if(this.state.campaignresult.offer_payment_value_flag5 ==1 )
            {
                this.state.offerpaymentvalue5flag = 1;
            }
            if(this.state.offerpaymentvalue5flag == 0){
                this.state.offerpaymentvalue5 = 0
                return <div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value5" className="checkbox" ><input  type="checkbox" id="checkoff5" onClick={this.offpay5checkbox} />{this.state.offerpaymentname5}</label><input id="offer_payment_value5" type="text" className="form-control" readOnly name="offer_payment_value5" value={this.state.offerpaymentvalue5}  /></div>
            }
            else{
                let result = ((Number(this.state.offerpaymentvalue1)+Number((this.state.offerpaymentvalue2))*1)/100).toFixed(2)
                this.state.offerpaymentvalue5 = result
                if(this.state.campaignresult.offer_payment_value_flag5 ==1 )
                {
                    this.state.offerpaymentvalue5 = this.state.campaignresult.offer_payment_value5;
                }
                return <div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value5" className="checkbox" ><input  type="checkbox" checked="checked" id="checkoff5" onClick={this.offpay5checkbox} />{this.state.offerpaymentname5}</label><input id="offer_payment_value5" type="text" className="form-control" readOnly name="offer_payment_value5" value={this.state.offerpaymentvalue5}  /></div>}
            }
    }
    offpay5checkbox(e)
    {
        if(e.target.checked === true)
        {
            this.setState({offerpaymentvalue5flag:1});
        }
        else
        {
            this.setState({offerpaymentvalue5flag:0});
        }
    }
    offerpayment6()
    {
        if(this.state.campaignresult.offer_payment_value_flag6 ==1 )
        {
            this.state.offerpaymentvalue6 = this.state.campaignresult.offer_payment_value6;
        }
        if(this.state.offerpaymentname6 == '' ||this.state.offerpaymentname6 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname6}</label><select id="offer_payment_value6" className="form-control" onChange={(e) => this.setState({ offerpaymentvalue6: e.target.value })}  name="offer_payment_value6"><option value="0">-เลือกหมวดการคำนวณ-</option><option value="1" selected={this.state.offerpaymentvalue6 == '1' ? 'selected' : ''}>อัตราค่าคอมมิชชั่น</option><option value="2" selected={this.state.offerpaymentvalue6 == '2' ? 'selected' : ''}>ค่าคอมมิชชั่นที่แจ้ง</option><option value="3" selected={this.state.offerpaymentvalue6 == '3' ? 'selected' : ''}>เบี้ยนำจ่ายก่อนหักภาษี ณ ที่จ่าย</option></select></div>}
    }

    offerpayment7()
    {
        if(this.state.campaignresult.offer_payment_value_flag7 ==1 )
        {
            this.state.offerpaymentvalue7 = this.state.campaignresult.offer_payment_value7;
        }
        if(this.state.offerpaymentname7 == '' ||this.state.offerpaymentname7 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname7}</label><input  id="offer_payment_value7" type="text" className="form-control" name="offer_payment_value7" onChange={(e) => this.setState({ offerpaymentvalue7: e.target.value })} value={this.state.offerpaymentvalue7}/></div>}
    }
    offerpayment8()
    {
        let result;
        if(this.state.offerpaymentvalue6 ==  1)
        {
            result = (Number(this.state.offerpaymentvalue1) * Number(this.state.offerpaymentvalue7)/100).toFixed(2)
        }
        else if (this.state.offerpaymentvalue6 ==  2)
        {
            result = Number(this.state.offerpaymentvalue7).toFixed(2)
        }
        else if (this.state.offerpaymentvalue6 ==  3)
        {
            result = (Number(this.state.offerpaymentvalue4) - Number(this.state.offerpaymentvalue7)).toFixed(2)
        }
        else 
        {
            result = (0).toFixed(2)
        }
        this.state.offerpaymentvalue8 = result
        if(this.state.campaignresult.offer_payment_value_flag8 ==1 )
        {
            this.state.offerpaymentvalue8 = this.state.campaignresult.offer_payment_value8;
        }
        if(this.state.offerpaymentname8 == '' ||this.state.offerpaymentname8 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname8}</label><input readOnly id="offer_payment_value8" type="text" className="form-control" name="offer_payment_value8" onChange={(e) => this.setState({ offerpaymentvalue8: e.target.value })} value={this.state.offerpaymentvalue8}  /></div>}
    }
    offerpayment9()
    {
        if(this.state.campaignresult.offer_payment_value_flag9 ==1 )
        {
            this.state.offerpaymentvalue9 = this.state.campaignresult.offer_payment_value9;
        }
       let result = (Number(this.props.taxrate) * Number(this.state.offerpaymentvalue8)/100).toFixed(2)
        this.state.offerpaymentvalue9  =result;
        if(this.state.offerpaymentname9 == '' ||this.state.offerpaymentname9 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname9}</label><input readOnly id="offer_payment_value9" type="text" className="form-control" name="offer_payment_value9" onChange={(e) => this.setState({ offerpaymentvalue9: e.target.value })} value={this.state.offerpaymentvalue9}  /></div>}
    }
    offerpayment10()
    {
        
        this.state.cat = (Number(this.state.offerpaymentvalue8) - Number(this.state.offerpaymentvalue9)).toFixed(2)
        let result = (Number(this.state.cat) * Number(this.props.coorrate)/100).toFixed(2)
        this.state.offerpaymentvalue10  =result;
        if(this.state.campaignresult.offer_payment_value_flag10 ==1 )
        {
            this.state.offerpaymentvalue10 = this.state.campaignresult.offer_payment_value10;
        }
        if(this.state.offerpaymentname10 == '' ||this.state.offerpaymentname10 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname10}</label><input readOnly id="offer_payment_value10" type="text" className="form-control" name="offer_payment_value10"  onChange={(e) => this.setState({ offerpaymentvalue10: e.target.value })} value={this.state.offerpaymentvalue10} /></div>}
    }
    offerpayment11()
    {
        let result = (Number(this.state.cat) * Number(this.props.otherrate)/100).toFixed(2)
        this.state.offerpaymentvalue11  =result;
        if(this.state.campaignresult.offer_payment_value_flag11 ==1 )
        {
            this.state.offerpaymentvalue11 = this.state.campaignresult.offer_payment_value11;
        }
        if(this.state.offerpaymentname11 == '' ||this.state.offerpaymentname11 == null){return}
        else{return <div className="column1"><div className="column"><label for="name" className="">{this.state.offerpaymentname11}</label><input readOnly id="offer_payment_value11" type="text" className="form-control" name="offer_payment_value11" onChange={(e) => this.setState({ offerpaymentvalue11: e.target.value })} value={this.state.offerpaymentvalue11} /></div></div>}
    }
    offerpayment12()
    {
        
        let result = (Number(this.state.percentpromotion)*Number(this.state.offerpaymentvalue1)/100).toFixed(2)
        this.state.offerpaymentvalue12 = result;
        if(this.state.campaignresult.offer_payment_value_flag12 ==1 )
        {
            this.state.offerpaymentvalue12 = this.state.campaignresult.offer_payment_value12;
        }
        if(this.state.offerpaymentname12 == ''  ||this.state.offerpaymentname12 == null){return}
        else{return <div className="column"><label  for="name" className="">{this.state.offerpaymentname12}</label><input readOnly id="offer_payment_value12" type="text" className="form-control" name="offer_payment_value12" onChange={(e) => this.setState({ offerpaymentvalue12: e.target.value })} value={this.state.offerpaymentvalue12} /></div>}
    }
    offerpayment13()
    {
        this.state.catc = (Number(this.state.cat) - Number(this.state.offerpaymentvalue10)).toFixed(2)
        this.state.catco = (Number(this.state.catc) - Number(this.state.offerpaymentvalue11)).toFixed(2)
        let result = (Number(this.props.memberlevel) * Number(this.state.catco)/100).toFixed(2)
        this.state.offerpaymentvalue13 = result
        if(this.state.campaignresult.offer_payment_value_flag13 ==1 )
        {
            this.state.offerpaymentvalue13 = this.state.campaignresult.offer_payment_value13;
        }
        if(this.state.offerpaymentname13 == '' ||this.state.offerpaymentname13 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname13}</label><input readOnly id="offer_payment_value13" type="text" className="form-control" name="offer_payment_value13" value={this.state.offerpaymentvalue13}  /></div>}
    }
    offerpayment14()
    {
        let result;
        if(Number(this.state.offerpaymentvalue12) > Number(this.state.offerpaymentvalue13) )
        {
            result = (Number(this.state.offerpaymentvalue12)+Number(this.state.offerpaymentvalue14)).toFixed(2)
        }
        else
        {
            result = (Number(this.state.offerpaymentvalue13)+Number(this.state.offerpaymentvalue14)).toFixed(2)
        }
        
        if(this.state.offerpaymentname14 == '' ||this.state.offerpaymentname14 == null){return}
        else{if(this.state.companymethod == 2)
            {
                this.state.caltodisspecialmethod2 = ((Number(this.state.offerpaymentvalue4)-Number(result))-Number(this.state.offerpaymentvalue14method2)).toFixed(2)
                //console.log("DFSFSFS",this.state.caltodisspecialmethod2)
                if(this.state.campaignresult.offer_payment_value_flag14 ==1 )
                {
                    this.state.offerpaymentvalue14 = this.state.campaignresult.offer_payment_value14;
                }

                return <div className="column1"><div className="column"><label  >{this.state.offerpaymentname14}</label><input readOnly id="offer_payment_value5" type="text" className="form-control" name="offer_payment_value14" value={this.state.offerpaymentvalue14} onChange={(e) => this.setState({ offerpaymentvalue14: e.target.value })}  /></div>
                <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value5" className="checkbox" ><input readOnly type="checkbox" id="checkoff5" value="1" onClick={this.companymethod}  />เบี้ยจ่ายก่อนหัก</label><div className="input-group date"><input id="offer_payment_value5" type="text" className="form-control" onChange={(e) => this.setState({ offerpaymentvalue14method2: e.target.value })}  value={this.state.offerpaymentvalue14method2}/><div className="input-group-addon"> <button onClick={this.submitspecialmethod2} type="button">คำนวณ</button></div></div></div>{this.offerpayment15()}</div></div>
            }
            else
            {
                if(this.state.campaignresult.offer_payment_value_flag14 ==1 )
                {
                    this.state.offerpaymentvalue14 = this.state.campaignresult.offer_payment_value14;
                }
                if(this.state.partnermethod == 2 || this.state.usermethod == 2)
                {
                    return <div className="column1"><div className="column"><label  >{this.state.offerpaymentname14}</label><input id="offer_payment_value5" type="text" className="form-control" name="offer_payment_value14" value={this.state.offerpaymentvalue14} onChange={(e) => this.setState({ offerpaymentvalue14: e.target.value })}  /></div>
                    <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value5" className="checkbox" >เบี้ยจ่ายก่อนหัก</label><input readOnly id="offer_payment_value5" type="text" className="form-control"  value={this.state.offerpaymentvalue14method2}  /></div>{this.offerpayment15()}</div></div>
                }
                else{
                    return <div className="column1"><div className="column"><label  >{this.state.offerpaymentname14}</label><input id="offer_payment_value5" type="text" className="form-control" name="offer_payment_value14" value={this.state.offerpaymentvalue14} onChange={(e) => this.setState({ offerpaymentvalue14: e.target.value })}  /></div>
                    <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value5" className="checkbox" ><input readOnly type="checkbox" id="checkoff5" value="1" onClick={this.companymethod}  />เบี้ยจ่ายก่อนหัก</label><input readOnly id="offer_payment_value5" type="text" className="form-control"  value={this.state.offerpaymentvalue14method2}  /></div>{this.offerpayment15()}</div></div>
                }

            }
            }
    }
    submitspecialmethod2(){

            this.setState({offerpaymentvalue14:this.state.caltodisspecialmethod2});
    }
    companymethod(e) {
        if(e.target.checked)
        {
            this.setState({companymethod:2,
                           offerpaymentvalue14 :'',
                           partnermethod:1,
                           usermethod:1,
                        });
        }
        else
        {
            this.setState({companymethod:1,
                           offerpaymentvalue14 :'',
                           offerpaymentvalue14method2:'',
                           flagcheckbox:0,
                        });

        }
        console.log(this.state.flagcheckbox)
    }
    offerpayment15()
    {
        let result
        let whatselect =''
        if(Number(this.state.offerpaymentvalue12) > Number(this.state.offerpaymentvalue13) )
        {
            whatselect = '(โปรโมชั่น)'
            result = (Number(this.state.offerpaymentvalue12)+Number(this.state.offerpaymentvalue14)).toFixed(2)
        }
        else
        {
            whatselect = '(ส่วนลดลูกค้า)'
            result = (Number(this.state.offerpaymentvalue13)+Number(this.state.offerpaymentvalue14)).toFixed(2)
        }
        this.state.offerpaymentvalue15 = result
        if(this.state.campaignresult.offer_payment_value_flag15 ==1 )
        {
            this.state.offerpaymentvalue15 = this.state.campaignresult.offer_payment_value15;
        }
        if(this.state.offerpaymentname15 == '' ||this.state.offerpaymentname15 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname15} <span style={{color:'red'}}>{whatselect}</span></label><input readOnly id="offer_payment_value15" type="text" className="form-control" name="offer_payment_value15" value={this.state.offerpaymentvalue15}  /></div>}
    }
    offerpayment16()
    {
       // console.log("pquota",this.state.pquota)
      //  console.log("uquota",this.state.uquota)

        this.state.catco = (Number(this.state.catc) - Number(this.state.offerpaymentvalue11)).toFixed(2)
        this.state.ncm = (Number(this.state.catco) - Number(this.state.offerpaymentvalue15)).toFixed(2)
        let result = (Number(this.props.partnerlevel)*Number(this.state.ncm)/100).toFixed(2)
        let result2 = (Number(result)-Number(this.state.offerpaymentvalue16)).toFixed(2)
        this.state.pcom = result2
        if(Number(result) < 0)
        {
            this.state.pquota = 0
        }
        else
        {
            this.state.pquota = result
        }
        if(Number(this.state.pcom) < 0 )
        {
            this.state.offerpaymentvalue16 =''
            let result2 = (Number(result)-Number(this.state.offerpaymentvalue16)).toFixed(2)
            this.state.pcom = result2
            this.state.pcom = 0.00;
            //alert("คุณให้ส่วนลดเกิน")
        }

        if(this.state.offerpaymentname16 == '' ||this.state.offerpaymentname16 == null){return}
        else{if(this.state.partnermethod == 2)
            {
                 this.state.caltodispartnermethod2 = ((Number(this.state.offerpaymentvalue4)+Number(this.state.offerpaymentvalue15))-Number(this.state.offerpaymentvalue16method2)).toFixed(2)
                 if(this.state.campaignresult.offer_payment_value_flag16 ==1 )
                 {
                     this.state.offerpaymentvalue16 = this.state.campaignresult.offer_payment_value16;
                 }
               return <div><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value16" className="checkbox">{this.state.offerpaymentname16}</label><div className="input-group date"><input id="offer_payment_value16" readOnly onChange={(e) => this.setState({ offerpaymentvalue16: e.target.value })} value={this.state.offerpaymentvalue16} type="text" className="form-control" name="offer_payment_value16"  /><div className="input-group-addon"> <span style={{color:'red'}}>ไม่เกิน {this.state.pquota}</span></div></div></div>
                    <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value16" className="checkbox"><input  type="checkbox" onClick={this.partnermethod2}  />เบี้ยจ่ายก่อนหัก</label><div className="input-group date"><input id="offer_payment_value16" type="text" className="form-control"  onChange={(e) => this.setState({ offerpaymentvalue16method2: e.target.value })} value={this.state.offerpaymentvalue16method2} /> <div className="input-group-addon"> <button onClick={this.submitpartnermethod2} type="button">คำนวณ</button></div></div></div>{this.offerpayment17()}</div></div>
            }
            else
            {
                if(this.state.campaignresult.offer_payment_value_flag16 ==1 )
                {
                    this.state.offerpaymentvalue16 = this.state.campaignresult.offer_payment_value16;
                }
                if(this.state.companymethod == 2 || this.state.usermethod == 2){
                    return <div><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value16" className="checkbox">{this.state.offerpaymentname16}</label><div className="input-group date"><input id="offer_payment_value16" onChange={(e) => this.setState({ offerpaymentvalue16: e.target.value })} value={this.state.offerpaymentvalue16} type="text" className="form-control" name="offer_payment_value16"  /><div className="input-group-addon"> <span style={{color:'red'}}>ไม่เกิน {Number(this.state.pquota).toFixed(2)}</span></div></div></div>
                    <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value16" className="checkbox">เบี้ยจ่ายก่อนหัก</label><input id="offer_payment_value16" readOnly type="text" className="form-control"/></div>{this.offerpayment17()}</div></div>
                }
                else{
                return <div><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value16" className="checkbox">{this.state.offerpaymentname16}</label><div className="input-group date"><input id="offer_payment_value16" onChange={(e) => this.setState({ offerpaymentvalue16: e.target.value })} value={this.state.offerpaymentvalue16} type="text" className="form-control" name="offer_payment_value16"  /><div className="input-group-addon"> <span style={{color:'red'}}>ไม่เกิน {Number(this.state.pquota).toFixed(2)}</span></div></div></div>
                <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value16" className="checkbox"><input  type="checkbox" onClick={this.partnermethod2}  />เบี้ยจ่ายก่อนหัก</label><input id="offer_payment_value16" readOnly type="text" className="form-control"/></div>{this.offerpayment17()}</div></div>
                }    
        }
        }
    }
    submitpartnermethod2(){
        if(Number(this.state.offerpaymentvalue16) < 0)
        {
            this.setState({offerpaymentvalue16:''});
        }
        else
        {
            this.setState({offerpaymentvalue16:this.state.caltodispartnermethod2});
        }

    }
    partnermethod2(e) {
        if(e.target.checked)
        {
            this.setState({partnermethod:2,
                          offerpaymentvalue16:'',
                          companymethod:1,
                          usermethod:1,});
        }
        else
        {
            this.setState({partnermethod:1,
                           offerpaymentvalue16 :'',
                           offerpaymentvalue16method2:''});
        }
    }

    offerpayment17()
    {

                this.state.offerpaymentvalue17 = this.state.pcom;
                if(this.state.campaignresult.offer_payment_value_flag17 ==1 )
                {
                    this.state.offerpaymentvalue17 = this.state.campaignresult.offer_payment_value17;
                }
        if(this.state.offerpaymentname17 == '' ||this.state.offerpaymentname17 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname17}</label><input readOnly id="offer_payment_value17" type="text" className="form-control" name="offer_payment_value17"  value={Number(this.state.offerpaymentvalue17).toFixed(2)}/></div>}
    }
    offerpayment18()
    {
        this.state.ncmp = (Number(this.state.ncm) - Number(this.state.pquota)).toFixed(2)
        let result = (Number(this.props.userlevel)*Number(this.state.ncmp)/100).toFixed(2)
        let result2 = (Number(result)-Number(this.state.offerpaymentvalue18)).toFixed(2)
        this.state.ucom = result2
        if(Number(result) < 0)
        {
            this.state.uquota = 0
        }
        else
        {
            this.state.uquota = result
        }
        if(Number(this.state.ucom) < 0)
        {
            this.state.offerpaymentvalue18 =''
            let result2 = (Number(result)-Number(this.state.offerpaymentvalue18)).toFixed(2)
            this.state.ucom = result2
            this.state.ucom = 0;
            //alert("คุณให้ส่วนลดเกิน")

        }
        if(this.state.offerpaymentname18 == '' ||this.state.offerpaymentname18 == null){return}
        else{if(this.state.usermethod == 2)
            {
                this.state.caltodisusermethod2 = ((Number(this.state.offerpaymentvalue4)+Number(this.state.offerpaymentvalue15)+Number(this.state.offerpaymentvalue17))-Number(this.state.offerpaymentvalue18method2)).toFixed(2)
                if(this.state.campaignresult.offer_payment_value_flag18 ==1 )
                {
                    this.state.offerpaymentvalue18 = this.state.campaignresult.offer_payment_value18;
                }
                return <div><div className="column"><label>{this.state.offerpaymentname18}</label><div className="input-group date"><input id="offer_payment_value18" type="text" className="form-control" name="offer_payment_value18" readOnly   value={this.state.offerpaymentvalue18} onChange={(e) => this.setState({ offerpaymentvalue18: e.target.value })}/><div className="input-group-addon"> <span style={{color:'red'}}>ไม่เกิน {Number(this.state.uquota).toFixed(2)}</span></div></div></div>
                <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value18" className="checkbox"><input readOnly type="checkbox" id="checkoff5" value="1" defaultChecked ="checked" onClick={this.usermethod2} />เบี้ยจ่ายก่อนหัก</label><div className="input-group date"><input id="offer_payment_value18" type="text" className="form-control" onChange={(e) => this.setState({ offerpaymentvalue18method2: e.target.value })} value={this.state.offerpaymentvalue18method2}/><div className="input-group-addon"> <button onClick={this.submitusermethod2} type="button">คำนวณ</button></div></div></div>{this.offerpayment19()}</div></div>
            }
            else
            {
                this.state.caltodisusermethod2 = ((Number(this.state.offerpaymentvalue4)+Number(this.state.offerpaymentvalue15)+Number(this.state.offerpaymentvalue17))-Number(this.state.offerpaymentvalue18method2)).toFixed(2)
                if(this.state.campaignresult.offer_payment_value_flag18 ==1 )
                {
                    this.state.offerpaymentvalue18 = this.state.campaignresult.offer_payment_value18;
                }
                if(this.state.companymethod == 2 || this.statepartnermethod ==2 )
                {
                    return <div><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value18" >{this.state.offerpaymentname18}</label><div className="input-group date"><input id="offer_payment_value18" type="text" className="form-control" name="offer_payment_value18"  value={this.state.offerpaymentvalue18} onChange={(e) => this.setState({ offerpaymentvalue18: e.target.value })}/><div className="input-group-addon"> <span style={{color:'red'}}>ไม่เกิน {this.state.uquota}</span></div></div></div>
                    <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value18" className="checkbox">เบี้ยจ่ายก่อนหัก</label><input readOnly id="offer_payment_value18" type="text" className="form-control" /></div>{this.offerpayment19()}</div></div>
                }
                else
                {
                return <div><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value18" >{this.state.offerpaymentname18}</label><div className="input-group date"><input id="offer_payment_value18" type="text" className="form-control" name="offer_payment_value18"  value={this.state.offerpaymentvalue18} onChange={(e) => this.setState({ offerpaymentvalue18: e.target.value })}/><div className="input-group-addon"> <span style={{color:'red'}}>ไม่เกิน {this.state.uquota}</span></div></div></div>
                    <div className="columnnopad"><div className="column"><label style={{marginTop:'-3px'}}for="offer_payment_value18" className="checkbox"><input  type="checkbox" id="checkoff5" value="1"  onClick={this.usermethod2} />เบี้ยจ่ายก่อนหัก</label><input readOnly id="offer_payment_value18" type="text" className="form-control" /></div>{this.offerpayment19()}</div></div>
                }    
        }
                
            }
    }

    submitusermethod2(){
        if(Number(this.state.offerpaymentvalue18) < 0)
        {
            this.setState({offerpaymentvalue18:''});
        }
        else
        {
            this.setState({offerpaymentvalue18:this.state.caltodisusermethod2});
        }

    }
    usermethod2(e) {
        if(e.target.checked)
        {
            this.setState({usermethod:2,
                           offerpaymentvalue18:'',
                           companymethod:1,
                           partnermethod:1,});
        }
        else
        {
            this.setState({usermethod:1,
                           offerpaymentvalue18 :'',
                           offerpaymentvalue18method2:''});

        }
    }

    offerpayment19()
    {
        if(this.state.campaignresult.offer_payment_value_flag19 ==1 )
        {
            this.state.offerpaymentvalue19 = this.state.campaignresult.offer_payment_value19;
        }
        if(this.state.offerpaymentname19 == '' ||this.state.offerpaymentname19 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname19}</label><input readOnly id="offer_payment_value19" type="text" className="form-control" name="offer_payment_value19" value={Number(this.state.ucom).toFixed(2)}  /></div>}
    }
    offerpayment20()
    {
        if(this.state.campaignresult.offer_payment_value_flag20 ==1 )
        {
            this.state.offerpaymentvalue20 = this.state.campaignresult.offer_payment_value20;
        }
        if(this.state.offerpaymentname20 == '' ||this.state.offerpaymentname20 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname20}</label><input  id="offer_payment_value20" type="text" className="form-control" name="offer_payment_value20"  value={this.state.offerpaymentvalue20} onChange={(e) => this.setState({ offerpaymentvalue20: e.target.value })}/></div>}
    }
    offerpayment21()
    {
       // console.log("cat", this.state.cat)
      //  console.log("catc", this.state.catc)
      //  console.log("catco", this.state.catco)
      //  console.log("ncm", this.state.ncm)
      //  console.log("ncmp",  this.state.ncmp)
     //   console.log("ncmpu",  this.state.ncmpu)
        this.state.ncmpu = (Number(this.state.ncmp) - Number(this.state.uquota)).toFixed(2)
        this.state.odiscount = Number(this.state.offerpaymentvalue20).toFixed(2);
        this.state.ci = this.state.ncmpu-this.state.odiscount;
        this.state.offerpaymentvalue21 = this.state.ci;
        if(this.state.campaignresult.offer_payment_value_flag21 ==1 )
        {
            this.state.offerpaymentvalue21 = this.state.campaignresult.offer_payment_value21;
        }
        if(this.state.offerpaymentname21 == '' ||this.state.offerpaymentname21 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname21}</label><input readOnly id="offer_payment_value21" type="text" className="form-control" name="offer_payment_value21" value={this.state.offerpaymentvalue21} /></div>}
    }
    offerpayment22()
    {
        if(this.state.campaignresult.offer_payment_value_flag22 ==1 )
        {
            this.state.offerpaymentvalue22 = this.state.campaignresult.offer_payment_value22;
        }
        if(this.state.offerpaymentname22 == '' ||this.state.offerpaymentname22 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname22}</label><input id="offer_payment_value22" type="text" className="form-control" name="offer_payment_value22"  /></div>}
    }
    offerpayment23()
    {
        if(this.state.campaignresult.offer_payment_value_flag23 ==1 )
        {
            this.state.offerpaymentvalue23 = this.state.campaignresult.offer_payment_value23;
        }
        if(this.state.offerpaymentname23 == '' ||this.state.offerpaymentname23 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname23}</label><input id="offer_payment_value23" type="text" className="form-control" name="offer_payment_value23"  /></div>}
    }
    offerpayment24()
    {
        if(this.state.campaignresult.offer_payment_value_flag24 ==1 )
        {
            this.state.offerpaymentvalue24 = this.state.campaignresult.offer_payment_value24;
        }
        if(this.state.offerpaymentname24 == '' ||this.state.offerpaymentname24 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname24}</label><input id="offer_payment_value24" type="text" className="form-control" name="offer_payment_value24"  /></div>}
    }
    offerpayment25()
    {
        if(this.state.campaignresult.offer_payment_value_flag25 ==1 )
        {
            this.state.offerpaymentvalue25 = this.state.campaignresult.offer_payment_value25;
        }
        if(this.state.offerpaymentname25 == '' ||this.state.offerpaymentname25 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname25}</label><input id="offer_payment_value25" type="text" className="form-control" name="offer_payment_value25"  /></div>}
    }
    offerpayment26()
    {
        if(this.state.campaignresult.offer_payment_value_flag26 ==1 )
        {
            this.state.offerpaymentvalue26 = this.state.campaignresult.offer_payment_value26;
        }
        if(this.state.offerpaymentname26 == '' ||this.state.offerpaymentname26 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname26}</label><input id="offer_payment_value26" type="text" className="form-control" name="offer_payment_value26"  /></div>}
    }
    offerpayment27()
    {
        if(this.state.campaignresult.offer_payment_value_flag27 ==1 )
        {
            this.state.offerpaymentvalue27 = this.state.campaignresult.offer_payment_value27;
        }
        if(this.state.offerpaymentname27 == '' ||this.state.offerpaymentname27 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname27}</label><input id="offer_payment_value27" type="text" className="form-control" name="offer_payment_value27"  /></div>}
    }
    offerpayment28()
    {
        if(this.state.campaignresult.offer_payment_value_flag28 ==1 )
        {
            this.state.offerpaymentvalue28 = this.state.campaignresult.offer_payment_value28;
        }
        if(this.state.offerpaymentname28 == '' ||this.state.offerpaymentname28 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname28}</label><input id="offer_payment_value28" type="text" className="form-control" name="offer_payment_value28"  /></div>}
    }
    offerpayment29()
    {
        if(this.state.campaignresult.offer_payment_value_flag29 ==1 )
        {
            this.state.offerpaymentvalue29 = this.state.campaignresult.offer_payment_value29;
        }
        if(this.state.offerpaymentname29 == '' ||this.state.offerpaymentname29 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname29}</label><input id="offer_payment_value29" type="text" className="form-control" name="offer_payment_value29"  /></div>}
    }
    offerpayment30()
    {
        if(this.state.campaignresult.offer_payment_value_flag30 ==1 )
        {
            this.state.offerpaymentvalue30 = this.state.campaignresult.offer_payment_value30;
        }
        if(this.state.offerpaymentname30 == '' ||this.state.offerpaymentname30 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname30}</label><input id="offer_payment_value30" type="text" className="form-control" name="offer_payment_value30"  /></div>}
    }

    offerpayment31()
    {
        if(this.state.campaignresult.offer_payment_value_flag31 ==1 )
        {
            this.state.offerpaymentvalue31 = this.state.campaignresult.offer_payment_value31;
        }
        if(this.state.offerpaymentname31 == '' ||this.state.offerpaymentname31 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname31}</label><input id="offer_payment_value31" type="text" className="form-control" name="offer_payment_value31"  /></div>}
    }

    offerpayment32()
    {
        if(this.state.campaignresult.offer_payment_value_flag32 ==1 )
        {
            this.state.offerpaymentvalue32 = this.state.campaignresult.offer_payment_value32;
        }
        if(this.state.offerpaymentname32 == '' ||this.state.offerpaymentname32 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname32}</label><input id="offer_payment_value32" type="text" className="form-control" name="offer_payment_value32"  /></div>}
    }
    offerpayment33()
    {
        if(this.state.campaignresult.offer_payment_value_flag33 ==1 )
        {
            this.state.offerpaymentvalue33 = this.state.campaignresult.offer_payment_value33;
        }
        if(this.state.offerpaymentname33 == '' ||this.state.offerpaymentname33 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname33}</label><input id="offer_payment_value33" type="text" className="form-control" name="offer_payment_value33"  /></div>}
    }
    offerpayment34()
    {
        if(this.state.campaignresult.offer_payment_value_flag34 ==1 )
        {
            this.state.offerpaymentvalue34 = this.state.campaignresult.offer_payment_value34;
        }
        if(this.state.offerpaymentname34 == '' ||this.state.offerpaymentname34 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname34}</label><input id="offer_payment_value34" type="text" className="form-control" name="offer_payment_value34"  /></div>}
    }
    offerpayment35()
    {
        if(this.state.campaignresult.offer_payment_value_flag35 ==1 )
        {
            this.state.offerpaymentvalue35= this.state.campaignresult.offer_payment_value35;
        }
        if(this.state.offerpaymentname35 == '' ||this.state.offerpaymentname35 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname35}</label><input id="offer_payment_value35" type="text" className="form-control" name="offer_payment_value35"  /></div>}
    }
    offerpayment36()
    {
        if(this.state.campaignresult.offer_payment_value_flag36 ==1 )
        {
            this.state.offerpaymentvalue36= this.state.campaignresult.offer_payment_value36;
        }
        if(this.state.offerpaymentname36 == '' ||this.state.offerpaymentname36 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname36}</label><input id="offer_payment_value36" type="text" className="form-control" name="offer_payment_value36"  /></div>}
    }
    offerpayment37()
    {
        if(this.state.campaignresult.offer_payment_value_flag37 ==1 )
        {
            this.state.offerpaymentvalue37= this.state.campaignresult.offer_payment_value37;
        }
        if(this.state.offerpaymentname37 == '' ||this.state.offerpaymentname37 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname37}</label><input id="offer_payment_value37" type="text" className="form-control" name="offer_payment_value37"  /></div>}
    }
    offerpayment38()
    {
        if(this.state.campaignresult.offer_payment_value_flag38 ==1 )
        {
            this.state.offerpaymentvalue38= this.state.campaignresult.offer_payment_value38;
        }
        if(this.state.offerpaymentname38 == '' ||this.state.offerpaymentname38 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname38}</label><input id="offer_payment_value38" type="text" className="form-control" name="offer_payment_value38"  /></div>}
    }
    offerpayment39()
    {
        if(this.state.campaignresult.offer_payment_value_flag39 ==1 )
        {
            this.state.offerpaymentvalue39= this.state.campaignresult.offer_payment_value39;
        }
        if(this.state.offerpaymentname39 == '' ||this.state.offerpaymentname39 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname39}</label><input id="offer_payment_value39" type="text" className="form-control" name="offer_payment_value39"  /></div>}
    }
    offerpayment40()
    {
        if(this.state.campaignresult.offer_payment_value_flag40 ==1 )
        {
            this.state.offerpaymentvalue40= this.state.campaignresult.offer_payment_value40;
        }
        if(this.state.offerpaymentname40 == '' ||this.state.offerpaymentname40 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerpaymentname40}</label><input id="offer_payment_value40" type="text" className="form-control" name="offer_payment_value40"  /></div>}
    }

    
    offerdetail1()
    {
        if(this.state.campaignresult.offer_detail_value_flag1 ==1 )
        {
            this.state.offerdetailvalue1= this.state.campaignresult.offer_detail_value1;
        }
        if(this.state.offerdetailname1 == '' || this.state.offerdetailname1 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname1}</label><input id="offer_detail_value1" type="text" className="form-control" name="offer_detail_value1" value={this.state.offerdetailvalue1} onChange={(e) => this.setState({ offerdetailvalue1: e.target.value })} /></div>}
    }
    offerdetail2()
    {
        if(this.state.campaignresult.offer_detail_value_flag2 ==1 )
        {
            this.state.offerdetailvalue2= this.state.campaignresult.offer_detail_value2;
        }
        if(this.state.offerdetailname2 == ''||this.state.offerdetailname2 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname2}</label><input id="offer_detail_value2" type="text" className="form-control" name="offer_detail_value2"  value={this.state.offerdetailvalue2} onChange={(e) => this.setState({ offerdetailvalue2: e.target.value })}/></div>}
    }
    offerdetail3()
    {
        if(this.state.campaignresult.offer_detail_value_flag3 ==1 )
        {
            this.state.offerdetailvalue3= this.state.campaignresult.offer_detail_value3;
        }
        if(this.state.offerdetailname3 == '' || this.state.offerdetailname3 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname3}</label><input id="offer_detail_value3" type="text" className="form-control" name="offer_detail_value3"  value={this.state.offerdetailvalue3} onChange={(e) => this.setState({ offerdetailvalue3: e.target.value })}/></div>}
    }
    offerdetail4()
    {
        if(this.state.campaignresult.offer_detail_value_flag4 ==1 )
        {
            this.state.offerdetailvalue4= this.state.campaignresult.offer_detail_value4;
        }
        if(this.state.offerdetailname4 == '' || this.state.offerdetailname4 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname4}</label><input id="offer_detail_value4" type="text" className="form-control" name="offer_detail_value4"  value={this.state.offerdetailvalue4} onChange={(e) => this.setState({ offerdetailvalue4: e.target.value })}/></div>}
    }
    offerdetail5()
    {
        if(this.state.campaignresult.offer_detail_value_flag5 ==1 )
        {
            this.state.offerdetailvalue5= this.state.campaignresult.offer_detail_value5;
        }
        if(this.state.offerdetailname5 == '' ||this.state.offerdetailname5 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname5}</label><input id="offer_detail_value5" type="text" className="form-control" name="offer_detail_value5"  value={this.state.offerdetailvalue5} onChange={(e) => this.setState({ offerdetailvalue5: e.target.value })}/></div>}
    }
    offerdetail6()
    {
        if(this.state.campaignresult.offer_detail_value_flag6 ==1 )
        {
            this.state.offerdetailvalue6= this.state.campaignresult.offer_detail_value6;
        }
        if(this.state.offerdetailname6 == '' ||this.state.offerdetailname6 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname6}</label><input id="offer_detail_value6" type="text" className="form-control" name="offer_detail_value6"  value={this.state.offerdetailvalue6} onChange={(e) => this.setState({ offerdetailvalue6: e.target.value })}/></div>}
    }
    offerdetail7()
    {
        if(this.state.campaignresult.offer_detail_value_flag7 ==1 )
        {
            this.state.offerdetailvalue7= this.state.campaignresult.offer_detail_value7;
        }
        if(this.state.offerdetailname7 == '' ||this.state.offerdetailname7 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname7}</label><input id="offer_detail_value7" type="text" className="form-control" name="offer_detail_value7"  value={this.state.offerdetailvalue7} onChange={(e) => this.setState({ offerdetailvalue7: e.target.value })}/></div>}
    }
    offerdetail8()
    {
        if(this.state.campaignresult.offer_detail_value_flag8 ==1 )
        {
            this.state.offerdetailvalue8= this.state.campaignresult.offer_detail_value8;
        }
        if(this.state.offerdetailname8 == '' ||this.state.offerdetailname8 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname8}</label><input id="offer_detail_value8" type="text" className="form-control" name="offer_detail_value8"  value={this.state.offerdetailvalue8} onChange={(e) => this.setState({ offerdetailvalue8: e.target.value })}/></div>}
    }
    offerdetail9()
    {
        if(this.state.campaignresult.offer_detail_value_flag9 ==1 )
        {
            this.state.offerdetailvalue9= this.state.campaignresult.offer_detail_value9;
        }
        if(this.state.offerdetailname9 == '' ||this.state.offerdetailname9 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname9}</label><input id="offer_detail_value1" type="text" className="form-control" name="offer_detail_value9"  value={this.state.offerdetailvalue9} onChange={(e) => this.setState({ offerdetailvalue9: e.target.value })}/></div>}
    }
    offerdetail10()
    {
        if(this.state.campaignresult.offer_detail_value_flag10 ==1 )
        {
            this.state.offerdetailvalue10= this.state.campaignresult.offer_detail_value10;
        }
        if(this.state.offerdetailname10 == '' ||this.state.offerdetailname10 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname10}</label><input id="offer_detail_value1" type="text" className="form-control" name="offer_detail_value10"  value={this.state.offerdetailvalue10} onChange={(e) => this.setState({ offerdetailvalue10: e.target.value })}/></div>}
    }
    offerdetail11()
    {
        if(this.state.campaignresult.offer_detail_value_flag11 ==1 )
        {
            this.state.offerdetailvalue11= this.state.campaignresult.offer_detail_value11;
        }
        if(this.state.offerdetailname11 == '' ||this.state.offerdetailname11 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname11}</label><input id="offer_detail_value11" type="text" className="form-control" name="offer_detail_value11"  value={this.state.offerdetailvalue11} onChange={(e) => this.setState({ offerdetailvalue11: e.target.value })}/></div>}
    }
    offerdetail12()
    {
        if(this.state.campaignresult.offer_detail_value_flag12 ==1 )
        {
            this.state.offerdetailvalue12= this.state.campaignresult.offer_detail_value12;
        }
        if(this.state.offerdetailname12 == ''  ||this.state.offerdetailname12 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname12}</label><input id="offer_detail_value12" type="text" className="form-control" name="offer_detail_value12"  value={this.state.offerdetailvalue12} onChange={(e) => this.setState({ offerdetailvalue12: e.target.value })}/></div>}
    }
    offerdetail13()
    {
        if(this.state.campaignresult.offer_detail_value_flag13 ==1 )
        {
            this.state.offerdetailvalue13= this.state.campaignresult.offer_detail_value13;
        }
        if(this.state.offerdetailname13 == '' ||this.state.offerdetailname13 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname13}</label><input id="offer_detail_value13" type="text" className="form-control" name="offer_detail_value13"  value={this.state.offerdetailvalue13} onChange={(e) => this.setState({ offerdetailvalue13: e.target.value })}/></div>}
    }
    offerdetail14()
    {
        if(this.state.campaignresult.offer_detail_value_flag14 ==1 )
        {
            this.state.offerdetailvalue14= this.state.campaignresult.offer_detail_value14;
        }
        if(this.state.offerdetailname14 == '' ||this.state.offerdetailname14 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname14}</label><input id="offer_detail_value14" type="text" className="form-control" name="offer_detail_value14"  value={this.state.offerdetailvalue14} onChange={(e) => this.setState({ offerdetailvalue14: e.target.value })}/></div>}
    }
    offerdetail15()
    {
        if(this.state.campaignresult.offer_detail_value_flag15 ==1 )
        {
            this.state.offerdetailvalue15= this.state.campaignresult.offer_detail_value15;
        }
        if(this.state.offerdetailname15 == '' ||this.state.offerdetailname15 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname15}</label><input id="offer_detail_value15" type="text" className="form-control" name="offer_detail_value15" value={this.state.offerdetailvalue15} onChange={(e) => this.setState({ offerdetailvalue15: e.target.value })} /></div>}
    }
    offerdetail16()
    {
        if(this.state.campaignresult.offer_detail_value_flag16 ==1 )
        {
            this.state.offerdetailvalue16= this.state.campaignresult.offer_detail_value16;
        }
        if(this.state.offerdetailname16 == '' ||this.state.offerdetailname16 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname16}</label><input id="offer_detail_value16" type="text" className="form-control" name="offer_detail_value16"  value={this.state.offerdetailvalue16} onChange={(e) => this.setState({ offerdetailvalue16: e.target.value })}/></div>}
    }
    offerdetail17()
    {
        if(this.state.campaignresult.offer_detail_value_flag17 ==1 )
        {
            this.state.offerdetailvalue17= this.state.campaignresult.offer_detail_value17;
        }
        if(this.state.offerdetailname17 == '' ||this.state.offerdetailname17 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname17}</label><input id="offer_detail_value17" type="text" className="form-control" name="offer_detail_value17" value={this.state.offerdetailvalue17} onChange={(e) => this.setState({ offerdetailvalue17: e.target.value })} /></div>}
    }
    offerdetail18()
    {
        if(this.state.campaignresult.offer_detail_value_flag18 ==1 )
        {
            this.state.offerdetailvalue18= this.state.campaignresult.offer_detail_value18;
        }
        if(this.state.offerdetailname18 == '' ||this.state.offerdetailname18 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname18}</label><input id="offer_detail_value18" type="text" className="form-control" name="offer_detail_value18" value={this.state.offerdetailvalue18} onChange={(e) => this.setState({ offerdetailvalue18: e.target.value })} /></div>}
    }
    offerdetail19()
    {
        if(this.state.campaignresult.offer_detail_value_flag19 ==1 )
        {
            this.state.offerdetailvalue19= this.state.campaignresult.offer_detail_value19;
        }
        if(this.state.offerdetailname19 == '' ||this.state.offerdetailname19 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname19}</label><input id="offer_detail_value19" type="text" className="form-control" name="offer_detail_value19" value={this.state.offerdetailvalue19} onChange={(e) => this.setState({ offerdetailvalue19: e.target.value })} /></div>}
    }
    offerdetail20()
    {
        if(this.state.campaignresult.offer_detail_value_flag20 ==1 )
        {
            this.state.offerdetailvalue20= this.state.campaignresult.offer_detail_value20;
        }
        if(this.state.offerdetailname20 == '' ||this.state.offerdetailname20 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offerdetailname20}</label><input id="offer_detail_value20" type="text" className="form-control" name="offer_detail_value20" value={this.state.offerdetailvalue20} onChange={(e) => this.setState({ offerdetailvalue20: e.target.value })} /></div>}
    }


    offervalue1()
    {
        if(this.state.campaignresult.offer_value_flag1 ==1 )
        {
            this.state.offervalue1= this.state.campaignresult.offer_value1;
        }
        if(this.state.offervaluename1 == '' || this.state.offervaluename1 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename1}</label><input id="offer_value1" type="text" className="form-control" name="offer_value1" value={this.state.offervalue1} onChange={(e) => this.setState({ offervalue1: e.target.value })}  /></div>}
    }
    offervalue2()
    {
        if(this.state.campaignresult.offer_value_flag1 ==1 )
        {
            this.state.offervalue1= this.state.campaignresult.offer_value1;
        }
        if(this.state.offervaluename2 == ''||this.state.offervaluename2 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename2}</label><input id="offer_value2" type="text" className="form-control" name="offer_value2"  value={this.state.offervalue2} onChange={(e) => this.setState({ offervalue2: e.target.value })}/></div>}
    }
    offervalue3()
    {
        if(this.state.campaignresult.offer_value_flag3 ==1 )
        {
            this.state.offervalue3= this.state.campaignresult.offer_value3;
        }
        if(this.state.offervaluename3 == '' || this.state.offervaluename3 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename3}</label><input id="offer_value3" type="text" className="form-control" name="offer_value3" value={this.state.offervalue3} onChange={(e) => this.setState({ offervalue3: e.target.value })} /></div>}
    }
    offervalue4()
    {
        if(this.state.campaignresult.offer_value_flag4 ==1 )
        {
            this.state.offervalue4= this.state.campaignresult.offer_value4;
        }
        if(this.state.offervaluename4 == '' || this.state.offervaluename4 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename4}</label><input id="offer_value4" type="text" className="form-control" name="offer_value4"  value={this.state.offervalue4} onChange={(e) => this.setState({ offervalue4: e.target.value })}/></div>}
    }
    offervalue5()
    {
        if(this.state.campaignresult.offer_value_flag5 ==1 )
        {
            this.state.offervalue5= this.state.campaignresult.offer_value5;
        }
        if(this.state.offervaluename5 == '' ||this.state.offervaluename5 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename5}</label><input id="offer_value5" type="text" className="form-control" name="offer_value5"  value={this.state.offervalue5} onChange={(e) => this.setState({ offervalue5: e.target.value })}/></div>}
    }
    offervalue6()
    {
        if(this.state.campaignresult.offer_value_flag6 ==1 )
        {
            this.state.offervalue6= this.state.campaignresult.offer_value6;
        }
        if(this.state.offervaluename6 == '' ||this.state.offervaluename6 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename6}</label><input id="offer_value6" type="text" className="form-control" name="offer_value6"  value={this.state.offervalue6} onChange={(e) => this.setState({ offervalue6: e.target.value })}/></div>}
    }
    offervalue7()
    {
        if(this.state.campaignresult.offer_value_flag7 ==1 )
        {
            this.state.offervalue7= this.state.campaignresult.offer_value7;
        }
        if(this.state.offervaluename7 == '' ||this.state.offervaluename7 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename7}</label><input id="offer_value7" type="text" className="form-control" name="offer_value7"  value={this.state.offervalue7} onChange={(e) => this.setState({ offervalue7: e.target.value })}/></div>}
    }
    offervalue8()
    {
        if(this.state.campaignresult.offer_value_flag8 ==1 )
        {
            this.state.offervalue8= this.state.campaignresult.offer_value8;
        }
        if(this.state.offervaluename8 == '' ||this.state.offervaluename8 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename8}</label><input id="offer_value8" type="text" className="form-control" name="offer_value8"  value={this.state.offervalue8} onChange={(e) => this.setState({ offervalue8: e.target.value })}/></div>}
    }
    offervalue9()
    {
        if(this.state.campaignresult.offer_value_flag9 ==1 )
        {
            this.state.offervalue9= this.state.campaignresult.offer_value9;
        }
        if(this.state.offervaluename9 == '' ||this.state.offervaluename9 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename9}</label><input id="offer_value1" type="text" className="form-control" name="offer_value9"  value={this.state.offervalue9} onChange={(e) => this.setState({ offervalue9: e.target.value })}/></div>}
    }
    offervalue10()
    {
        if(this.state.campaignresult.offer_value_flag10 ==1 )
        {
            this.state.offervalue10= this.state.campaignresult.offer_value10;
        }
        if(this.state.offervaluename10 == '' ||this.state.offervaluename10 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename10}</label><input id="offer_value1" type="text" className="form-control" name="offer_value10"  value={this.state.offervalue10} onChange={(e) => this.setState({ offervalue10: e.target.value })}/></div>}
    }
    offervalue11()
    {
        if(this.state.campaignresult.offer_value_flag11 ==1 )
        {
            this.state.offervalue11= this.state.campaignresult.offer_value11;
        }
        if(this.state.offervaluename11 == '' ||this.state.offervaluename11 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename11}</label><input id="offer_value11" type="text" className="form-control" name="offer_value11" value={this.state.offervalue11} onChange={(e) => this.setState({ offervalue11: e.target.value })} /></div>}
    }
    offervalue12()
    {
        if(this.state.campaignresult.offer_value_flag12 ==1 )
        {
            this.state.offervalue12= this.state.campaignresult.offer_value12;
        }
        if(this.state.offervaluename12 == ''  ||this.state.offervaluename12 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename12}</label><input id="offer_value12" type="text" className="form-control" name="offer_value12"  value={this.state.offervalue12} onChange={(e) => this.setState({ offervalue12: e.target.value })}/></div>}
    }
    offervalue13()
    {
        if(this.state.campaignresult.offer_value_flag13 ==1 )
        {
            this.state.offervalue13= this.state.campaignresult.offer_value13;
        }
        if(this.state.offervaluename13 == '' ||this.state.offervaluename13 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename13}</label><input id="offer_value13" type="text" className="form-control" name="offer_value13"  value={this.state.offervalue13} onChange={(e) => this.setState({ offervalue13: e.target.value })}/></div>}
    }
    offervalue14()
    {
        if(this.state.campaignresult.offer_value_flag14 ==1 )
        {
            this.state.offervalue14= this.state.campaignresult.offer_value14;
        }
        if(this.state.offervaluename14 == '' ||this.state.offervaluename14 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename14}</label><input id="offer_value14" type="text" className="form-control" name="offer_value14"  value={this.state.offervalue14} onChange={(e) => this.setState({ offervalue14: e.target.value })}/></div>}
    }
    offervalue15()
    {
        if(this.state.campaignresult.offer_value_flag15 ==1 )
        {
            this.state.offervalue15= this.state.campaignresult.offer_value15;
        }
        if(this.state.offervaluename15 == '' ||this.state.offervaluename15 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename15}</label><input id="offer_value15" type="text" className="form-control" name="offer_value15"  value={this.state.offervalue15} onChange={(e) => this.setState({ offervalue15: e.target.value })}/></div>}
    }
    offervalue16()
    {
        if(this.state.campaignresult.offer_value_flag16 ==1 )
        {
            this.state.offervalue16= this.state.campaignresult.offer_value16;
        }
        if(this.state.offervaluename16 == '' ||this.state.offervaluename16 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename16}</label><input id="offer_value16" type="text" className="form-control" name="offer_value16"  value={this.state.offervalue16} onChange={(e) => this.setState({ offervalue16: e.target.value })}/></div>}
    }
    offervalue17()
    {
        if(this.state.campaignresult.offer_value_flag17 ==1 )
        {
            this.state.offervalue17= this.state.campaignresult.offer_value17;
        }
        if(this.state.offervaluename17 == '' ||this.state.offervaluename17 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename17}</label><input id="offer_value17" type="text" className="form-control" name="offer_value17"  value={this.state.offervalue17} onChange={(e) => this.setState({ offervalue17: e.target.value })}/></div>}
    }
    offervalue18()
    {
        if(this.state.campaignresult.offer_value_flag18 ==1 )
        {
            this.state.offervalue18= this.state.campaignresult.offer_value18;
        }
        if(this.state.offervaluename18 == '' ||this.state.offervaluename18 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename18}</label><input id="offer_value18" type="text" className="form-control" name="offer_value18" value={this.state.offervalue18} onChange={(e) => this.setState({ offervalue18: e.target.value })} /></div>}
    }
    offervalue19()
    {
        if(this.state.campaignresult.offer_value_flag19 ==1 )
        {
            this.state.offervalue19= this.state.campaignresult.offer_value19;
        }
        if(this.state.offervaluename19 == '' ||this.state.offervaluename19 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename19}</label><input id="offer_value19" type="text" className="form-control" name="offer_value19" value={this.state.offervalue19} onChange={(e) => this.setState({ offervalue19: e.target.value })} /></div>}
    }
    offervalue20()
    {
        if(this.state.campaignresult.offer_value_flag20 ==1 )
        {
            this.state.offervalue20= this.state.campaignresult.offer_value20;
        }
        if(this.state.offervaluename20 == '' ||this.state.offervaluename20 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename20}</label><input id="offer_value20" type="text" className="form-control" name="offer_value20" value={this.state.offervalue20} onChange={(e) => this.setState({ offervalue20: e.target.value })} /></div>}
    }

    offervalue21()
    {
        if(this.state.campaignresult.offer_value_flag21 ==1 )
        {
            this.state.offervalue21= this.state.campaignresult.offer_value21;
        }
        if(this.state.offervaluename21 == '' ||this.state.offervaluename21 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename21}</label><input id="offer_value21" type="text" className="form-control" name="offer_value21" value={this.state.offervalue21} onChange={(e) => this.setState({ offervalue21: e.target.value })} /></div>}
    }
    offervalue22()
    {
        if(this.state.campaignresult.offer_value_flag22 ==1 )
        {
            this.state.offervalue22= this.state.campaignresult.offer_value22;
        }
        if(this.state.offervaluename22 == '' ||this.state.offervaluename22 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename22}</label><input id="offer_value22" type="text" className="form-control" name="offer_value22" value={this.state.offervalue22} onChange={(e) => this.setState({ offervalue22: e.target.value })} /></div>}
    }
    offervalue23()
    {
        if(this.state.campaignresult.offer_value_flag23 ==1 )
        {
            this.state.offervalue23= this.state.campaignresult.offer_value23;
        }
        if(this.state.offervaluename23 == '' ||this.state.offervaluename23 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename23}</label><input id="offer_value23" type="text" className="form-control" name="offer_value23" value={this.state.offervalue23} onChange={(e) => this.setState({ offervalue23: e.target.value })} /></div>}
    }
    offervalue24()
    {
        if(this.state.campaignresult.offer_value_flag24 ==1 )
        {
            this.state.offervalue24= this.state.campaignresult.offer_value24;
        }
        if(this.state.offervaluename24 == '' ||this.state.offervaluename24 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename24}</label><input id="offer_value24" type="text" className="form-control" name="offer_value24" value={this.state.offervalue24} onChange={(e) => this.setState({ offervalue24: e.target.value })} /></div>}
    }
    offervalue25()
    {
        if(this.state.campaignresult.offer_value_flag25 ==1 )
        {
            this.state.offervalue25= this.state.campaignresult.offer_value25;
        }
        if(this.state.offervaluename25 == '' ||this.state.offervaluename25 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename25}</label><input id="offer_value25" type="text" className="form-control" name="offer_value25" value={this.state.offervalue25} onChange={(e) => this.setState({ offervalue25: e.target.value })} /></div>}
    }
    offervalue26()
    {
        if(this.state.campaignresult.offer_value_flag26 ==1 )
        {
            this.state.offervalue26= this.state.campaignresult.offer_value26;
        }
        if(this.state.offervaluename26 == '' ||this.state.offervaluename26 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename26}</label><input id="offer_value26" type="text" className="form-control" name="offer_value26" value={this.state.offervalue26} onChange={(e) => this.setState({ offervalue26: e.target.value })} /></div>}
    }
    offervalue27()
    {
        if(this.state.campaignresult.offer_value_flag27 ==1 )
        {
            this.state.offervalue27= this.state.campaignresult.offer_value27;
        }
        if(this.state.offervaluename27 == '' ||this.state.offervaluename27 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename27}</label><input id="offer_value27" type="text" className="form-control" name="offer_value27" value={this.state.offervalue27} onChange={(e) => this.setState({ offervalue27: e.target.value })} /></div>}
    }
    offervalue28()
    {
        if(this.state.campaignresult.offer_value_flag28 ==1 )
        {
            this.state.offervalue28= this.state.campaignresult.offer_value28;
        }
        if(this.state.offervaluename28 == '' ||this.state.offervaluename28 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename28}</label><input id="offer_value28" type="text" className="form-control" name="offer_value28" value={this.state.offervalue28} onChange={(e) => this.setState({ offervalue28: e.target.value })} /></div>}
    }
    offervalue29()
    {
        if(this.state.campaignresult.offer_value_flag29 ==1 )
        {
            this.state.offervalue29= this.state.campaignresult.offer_value29;
        }
        if(this.state.offervaluename29 == '' ||this.state.offervaluename29 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename29}</label><input id="offer_value29" type="text" className="form-control" name="offer_value29" value={this.state.offervalue29} onChange={(e) => this.setState({ offervalue29: e.target.value })} /></div>}
    }
    offervalue30()
    {
        if(this.state.campaignresult.offer_value_flag30 ==1 )
        {
            this.state.offervalue30= this.state.campaignresult.offer_value30;
        }
        if(this.state.offervaluename30 == '' ||this.state.offervaluename30 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename30}</label><input id="offer_value30" type="text" className="form-control" name="offer_value30" value={this.state.offervalue30} onChange={(e) => this.setState({ offervalue30: e.target.value })} /></div>}
    }
    offervalue31()
    {
        if(this.state.campaignresult.offer_value_flag31 ==1 )
        {
            this.state.offervalue31= this.state.campaignresult.offer_value31;
        }
        if(this.state.offervaluename31 == '' ||this.state.offervaluename31 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename31}</label><input id="offer_value31" type="text" className="form-control" name="offer_value31" value={this.state.offervalue31} onChange={(e) => this.setState({ offervalue31: e.target.value })} /></div>}
    }
    offervalue32()
    {
        if(this.state.campaignresult.offer_value_flag32 ==1 )
        {
            this.state.offervalue32= this.state.campaignresult.offer_value32;
        }
        if(this.state.offervaluename32 == '' ||this.state.offervaluename32 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename32}</label><input id="offer_value32" type="text" className="form-control" name="offer_value32" value={this.state.offervalue32} onChange={(e) => this.setState({ offervalue32: e.target.value })} /></div>}
    }
    offervalue33()
    {
        if(this.state.campaignresult.offer_value_flag33 ==1 )
        {
            this.state.offervalue33= this.state.campaignresult.offer_value33;
        }
        if(this.state.offervaluename33 == '' ||this.state.offervaluename33 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename33}</label><input id="offer_value33" type="text" className="form-control" name="offer_value33" value={this.state.offervalue33} onChange={(e) => this.setState({ offervalue33: e.target.value })} /></div>}
    }
    offervalue34()
    {
        if(this.state.campaignresult.offer_value_flag34 ==1 )
        {
            this.state.offervalue34= this.state.campaignresult.offer_value34;
        }
        if(this.state.offervaluename34 == '' ||this.state.offervaluename34 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename34}</label><input id="offer_value34" type="text" className="form-control" name="offer_value34" value={this.state.offervalue34} onChange={(e) => this.setState({ offervalue34: e.target.value })} /></div>}
    }
    offervalue35()
    {
        if(this.state.campaignresult.offer_value_flag35 ==1 )
        {
            this.state.offervalue35= this.state.campaignresult.offer_value35;
        }
        if(this.state.offervaluename35 == '' ||this.state.offervaluename35 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename35}</label><input id="offer_value35" type="text" className="form-control" name="offer_value35" value={this.state.offervalue35} onChange={(e) => this.setState({ offervalue35: e.target.value })} /></div>}
    }
    offervalue36()
    {
        if(this.state.campaignresult.offer_value_flag36 ==1 )
        {
            this.state.offervalue36= this.state.campaignresult.offer_value36;
        }
        if(this.state.offervaluename36 == '' ||this.state.offervaluename36 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename36}</label><input id="offer_value36" type="text" className="form-control" name="offer_value36" value={this.state.offervalue36} onChange={(e) => this.setState({ offervalue36: e.target.value })} /></div>}
    }
    offervalue37()
    {
        if(this.state.campaignresult.offer_value_flag37 ==1 )
        {
            this.state.offervalue37= this.state.campaignresult.offer_value37;
        }
        if(this.state.offervaluename37 == '' ||this.state.offervaluename37 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename37}</label><input id="offer_value37" type="text" className="form-control" name="offer_value37" value={this.state.offervalue37} onChange={(e) => this.setState({ offervalue37: e.target.value })} /></div>}
    }
    offervalue38()
    {
        if(this.state.campaignresult.offer_value_flag38 ==1 )
        {
            this.state.offervalue38= this.state.campaignresult.offer_value38;
        }
        if(this.state.offervaluename38 == '' ||this.state.offervaluename38 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename38}</label><input id="offer_value38" type="text" className="form-control" name="offer_value38" value={this.state.offervalue38} onChange={(e) => this.setState({ offervalue38: e.target.value })} /></div>}
    }
    offervalue39()
    {
        if(this.state.campaignresult.offer_value_flag39 ==1 )
        {
            this.state.offervalue39= this.state.campaignresult.offer_value39;
        }
        if(this.state.offervaluename39 == '' ||this.state.offervaluename39 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename39}</label><input id="offer_value39" type="text" className="form-control" name="offer_value39" value={this.state.offervalue39} onChange={(e) => this.setState({ offervalue39: e.target.value })} /></div>}
    }
    offervalue40()
    {
        if(this.state.campaignresult.offer_value_flag40 ==1 )
        {
            this.state.offervalue40= this.state.campaignresult.offer_value40;
        }
        if(this.state.offervaluename40 == '' ||this.state.offervaluename40 == null){return}
        else{return <div className="column"><label for="name" className="">{this.state.offervaluename40}</label><input id="offer_value40" type="text" className="form-control" name="offer_value40" value={this.state.offervalue40} onChange={(e) => this.setState({ offervalue40: e.target.value })} /></div>}
    }



    lastshow()
    {
        if(this.state.selectedoffertype.length !='')
        {
                let alldiscount = (Number(this.state.offerpaymentvalue15)+Number(this.state.offerpaymentvalue16)+Number(this.state.offerpaymentvalue18)+Number(this.state.offerpaymentvalue20)).toFixed(2)
                let allmemberpaybefore = (Number(this.state.offerpaymentvalue4)-Number(alldiscount)).toFixed(2)
                let allmemberpayafter = (Number(allmemberpaybefore)-Number(this.state.offerpaymentvalue5)).toFixed(2)
                let allpartnerpayafter = (Number(allmemberpayafter)-Number(this.state.offerpaymentvalue17)).toFixed(2)
                let alluserpayafter = (Number(allmemberpayafter)-Number(this.state.ucom)).toFixed(2)
                let paytocompany = (Number(this.state.offerpaymentvalue4)-Number(this.state.offerpaymentvalue8)-Number(this.state.offerpaymentvalue5)).toFixed(2)

               return <div style={{overflowX:'auto'}}>
                <table style={{border: 'solid 3px #00325d'}} className="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                    <tr role="row"  >
                    <th style={{backgroundColor: '#00325d',color:'white'}}>หัวข้อ</th>
                    <th style={{backgroundColor: '#00325d',color:'white'}}>จำนวน</th>
                    </tr>
            
                </thead>
                <tbody>
                <tr role="row" className="odd">
                    <th>เบี้ยรวมหน้าตั๋ว</th>
                    <td><input className="form-control" readOnly value={this.state.offerpaymentvalue4} /></td>
                </tr>
                <tr role="row" className="odd">
                    <th>ยอดหัก ณ ที่จ่าย * (ถ้ามีค่า)</th>
                    <td><input className="form-control" readOnly value={this.state.offerpaymentvalue5} /></td>
                </tr>
                <tr role="row" className="odd">
                    <th>ส่วนลดพิเศษทั้งหมด </th>
                    <td><input className="form-control" readOnly value={alldiscount} /></td>
                </tr>
                <tr role="row" className="odd">
                    <th>ค่าใช้จ่ายสุทธิที่ลูกค้าต้องจ่ายก่อนหัก ณ ที่จ่าย   (Customer)</th>
                    <td><input className="form-control" readOnly value={allmemberpaybefore} /></td>
                </tr>
                <tr role="row" className="odd">
                    <th>ค่าใช้จ่ายสุทธิที่ลูกค้าต้องจ่ายหลังหัก ณ ที่จ่าย   (Customer)</th>
                    <td><input className="form-control" readOnly value={allmemberpayafter} /> </td>
                </tr>
                <tr role="row" className="odd">
                    <th>ค่าใช้จ่ายสุทธิที่ให้คำปรึกษา/แนะนำ ต้องจ่ายให้แก่บริษัท  (Partner) </th>
                    <td><input className="form-control" readOnly value={allpartnerpayafter}/ ></td>
                </tr>
                <tr role="row" className="odd">
                    <th>ค่าใช้จ่ายสุทธิที่ผู้ให้บริการ ต้องจ่ายให้แก่บริษัท  (User) /(เบี้ยที่ผู้แจ้งงานต้องโอนเดิม)</th>
                    <td><input className="form-control" readOnly value={alluserpayafter} /></td>
                </tr>
                <tr role="row" className="odd">
                    <th>ค่าใช้จ่ายสุทธิที่บริษัทต้องโอนไปบริษัทประกัน (Company)</th>
                    <td><input className="form-control" readOnly value={paytocompany} /></td>
                </tr>
                </tbody>
            </table>
            </div>
        }
        else
        {
            return 
        }
    }
    
      render() {
        return (<div>
                     <div className="column1">{this.generalinformation()}</div>
                     <div className="column1">{this.offervalue()}</div>
                     <div className="column1">{this.offerdetail()}</div>
                     <div className="column1">{this.offerpayment()}</div>
                     <div className="column1">{this.lastshow()}</div>


                </div>
        )
      }
    }
if (document.getElementById('createoffer')) {
  const component = document.getElementById('createoffer');
  const props = Object.assign({}, component.dataset);
    ReactDOM.render(<Createoffer {...props}/>, component);
}

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import Dialog from 'react-dialog'
import Picky from 'react-picky';
import 'react-picky/dist/picky.css'; // Include CSS
import Modal from 'react-awesome-modal';
import RTChart from 'react-rt-chart';
import Select from 'react-select';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import jsPDF from "jspdf";

export default class TrackingCase extends Component {

  constructor(){
    super();
    //console.log(super());
    this.state = {
        day:[],
        month:[],
        year:[],
      showall:'box collapsed-box',
      daynextnotify:'',
      monthnextnotify:'',
      yearnextnotify:'',
      flagnextnotify:'',
      defaultvarvaluenextnotify:'',
      flagtopicnotify:'',
      defaultvarvaluetopicnotify:'',
      daynotifyremaining:'',
    };

    this.opennextnotify= this.opennextnotify.bind(this);
    this.closenextnotify= this.closenextnotify.bind(this);
    this.handleChangenextnotify= this.handleChangenextnotify.bind(this);
    this.handleSubmitnextnotify= this.handleSubmitnextnotify.bind(this);
    this.opentopicnotify= this.opentopicnotify.bind(this);
    this.closetopicnotify= this.closetopicnotify.bind(this);
    this.handleChangetopicnotify= this.handleChangetopicnotify.bind(this);
    this.handleSubmittopicnotify= this.handleSubmittopicnotify.bind(this);
  }
  componentDidMount() {
    axios.get('/wealththaiinsurance/load/day').then(response=>{
        this.setState({day:response.data});
      })
      axios.get('/wealththaiinsurance/load/month').then(response=>{
        this.setState({month:response.data});
      })
      axios.get('/wealththaiinsurance/load/year').then(response=>{
        this.setState({year:response.data});
      })
      this.setState({defaultvarvaluenextnotify:this.props.nextnotify,
                     defaultvarvaluetopicnotify:this.props.topicnotify,
                     daynotifyremaining:this.props.daynotifyremaining});

  }
  daynotifyremainingdatereal()
  {
    if(this.props.daynotifyremainingdatereal <= 7 && this.props.daynotifyremainingdatereal > 3)
    {
      return <span style={{color:'orange',fontSize:'14px'}}><br/><b>{this.props.daynotifyremainingdatereal} </b></span>;
    }
    else if(this.props.daynotifyremainingdatereal < 3)
    {
      return <span style={{color:'red',fontSize:'14px'}}><br/><b>{this.props.daynotifyremainingdatereal} </b></span>;
    }
    else
    {
      return <span><br/> {this.props.daynotifyremainingdatereal} </span>;
    }
  }
  daynotiremaining()
  {
      if(this.state.daynotifyremaining <= 7 && this.state.daynotifyremaining > 3)
      {
        return <span style={{color:'orange',fontSize:'14px'}}><b>{this.state.daynotifyremaining} </b></span>;
      }
      else if(this.state.daynotifyremaining < 3)
      {
        return <span style={{color:'red',fontSize:'14px'}}><b>{this.state.daynotifyremaining} </b></span>;
      }
      else
      {
        return <span>{this.state.daynotifyremaining} </span>;
      }
  }
  handleSubmittopicnotify(e){
    e.preventDefault();
    axios.post('/wealththaiinsurance/update/somecase?topicnotify',{
      id:this.props.id,
      topicnotify:this.state.topicnotify,
    }).then(res=>{
      //console.log(res.data);
      this.setState({
        defaultvarvaluetopicnotify:this.state.topicnotify,
        flagtopicnotify:0,
      })
    });
  }

  handleChangetopicnotify(e){
    //console.log(e.target.value);
    this.setState({
      topicnotify:e.target.value,
    })
  }
  opentopicnotify()
  {
    this.setState({
      flagtopicnotify:1
    })
  }
  closetopicnotify()
  {
    this.setState({
      flagtopicnotify:0
    })
  }
  topicnotify()
  {
    if(this.state.flagtopicnotify == 1)
    {
      return  <div> <form onSubmit={this.handleSubmittopicnotify}><input onChange={this.handleChangetopicnotify} value={this.state.topicnotify} class="form-control"/>&nbsp;&nbsp;&nbsp;<br/><button type="submit"  class="btn btn-box-tool" ><span style={{color:'green'}}>บันทึก</span></button><button type="button" onClick={this.closetopicnotify}class="btn btn-box-tool" ><span style={{color:'red'}}>ยกเลิก</span></button></form></div>
    }
    else
    {
      return <div ><span style={{float:'left'}}>{this.state.defaultvarvaluetopicnotify}</span> &nbsp;&nbsp;&nbsp;<span  style={{float:'right',color:'orange'}}  onClick={this.opentopicnotify}>แก้ไข</span></div>
    }
  }
  
  handleSubmitnextnotify(e){
    e.preventDefault();
    axios.post('/wealththaiinsurance/update/somecase?nextnotify',{
      id:this.props.id,
      nextnotify:this.state.daynextnotify+'/'+this.state.monthnextnotify+'/'+this.state.yearnextnotify,
    }).then(res=>{

      //console.log(res.data);
      this.setState({
        defaultvarvaluenextnotify:this.state.daynextnotify+'/'+this.state.monthnextnotify+'/'+this.state.yearnextnotify,
        flagnextnotify:0,
        daynotifyremaining:res.data,

      })
    });
  }
  handleChangenextnotify(e){
    //console.log(e.target.value);
    this.setState({
      nextnotify:e.target.value,
      
    })
  }
  opennextnotify()
  {
    this.setState({
      flagnextnotify:1
    })
  }
  closenextnotify()
  {
    this.setState({
      flagnextnotify:0
    })
  }
  nextnotify(data)
  {
    if(this.state.flagnextnotify == 1)
    {
      return  <div> <form onSubmit={this.handleSubmitnextnotify}>
      <select onChange={(e) => this.setState({ daynextnotify: e.target.value })} name="dayex">
      <option value ="">  วัน  </option>
      <option value ="01">  01  </option>
      <option value ="02">  02  </option>
      <option value ="03">  03  </option>
      <option value ="04">  04  </option>
      <option value ="05">  05  </option>
      <option value ="06">  06  </option>
      <option value ="07">  07  </option>
      <option value ="08">  08  </option>
      <option value ="09">  09  </option>          {
        this.state.day.map(
          data =>
          <option value={data}>{data}</option>
        )
        }
        </select>

        <select  onChange={(e) => this.setState({ monthnextnotify: e.target.value })}>
        <option value ="">  เดือน  </option>
        <option value ="01">  01  </option>
        <option value ="02">  02  </option>
        <option value ="03">  03  </option>
        <option value ="04">  04  </option>
        <option value ="05">  05  </option>
        <option value ="06">  06  </option>
        <option value ="07">  07  </option>
        <option value ="08">  08  </option>
        <option value ="09">  09  </option>
        {
          this.state.month.map(
            data =>
            <option value={data}>{data}</option>
          )
          }
        </select>
      <select onChange={(e) => this.setState({ yearnextnotify: e.target.value })}  >
      <option value ="">  ปี ค.ศ  </option>
      {
        this.state.year.map(
          data =>
          <option value={data}>{data}</option>
        )
      }
      </select>&nbsp;&nbsp;&nbsp;<button type="submit"  class="btn btn-box-tool" ><span style={{color:'green'}}>บันทึก</span></button><button type="button" onClick={this.closenextnotify}class="btn btn-box-tool" ><span style={{color:'red'}}>ยกเลิก</span></button></form></div>
    }
    else
    {
      return <div ><span style={{float:'left'}}>{this.state.defaultvarvaluenextnotify}</span> &nbsp;&nbsp;&nbsp;<span  style={{float:'right',color:'orange'}}  onClick={this.opennextnotify}>แก้ไข</span></div>
    }
  }
  showdetail()
  {

      return <div className="column" id="casedetail">
          <div className={this.state.showall} style={{backgroundColor:'#F5F5F5'}}>
          <div className="box-header">
          <div style={{overflowX:'auto'}}>
            <table>
            <tbody>
              <tr>
              <td style={{padding:'10px'}}><b>การติดตามงาน</b></td>
              <td  style={{padding:'10px'}}>เวลาคงเหลือ {this.daynotiremaining()} วัน</td>
              <td  style={{padding:'10px'}}><b>วันที่ต้องติดตามงาน</b></td>
              <td  style={{padding:'10px'}}  >{this.nextnotify()}</td>
              <td style={{padding:'10px'}} ><b>หัวข้อ</b></td>
              <td style={{padding:'10px'}} >{this.topicnotify()}</td>

              </tr>  
              </tbody>
            </table>
          </div>
          </div>
          </div>
          <div className={this.state.showall} style={{backgroundColor:'#F5F5F5'}}>
          <div className="box-header">
          <div style={{overflowX:'auto'}}>
            <table>
            <tbody>
              <tr>
              <td style={{padding:'10px'}}><b>การติดตามงานอัตโนมัติ</b></td>
              <td  style={{padding:'10px'}}>เวลาคงเหลือ {this.daynotifyremainingdatereal()} วัน</td>
              <td  style={{padding:'10px'}}><b>วันที่ครบกำหนดการแจ้งเตือน</b></td>
              <td  style={{padding:'10px'}}  >{this.props.datereal}</td>
              <td style={{padding:'10px'}} ><b>หัวข้อ</b></td>
              <td style={{padding:'10px'}} >{this.props.daterealname}</td>

              </tr>  
              </tbody>
            </table>
          </div>
          </div>
          </div>
          </div>
    

  }
    render() {
      return (
          <div style={{height:'20px'}}>{this.showdetail()}</div>          
        );
    }
}

if (document.getElementById('trackingcase')) {
  const component = document.getElementById('trackingcase');
  const props = Object.assign({}, component.dataset);
    ReactDOM.render(<TrackingCase {...props}/>, component);
}

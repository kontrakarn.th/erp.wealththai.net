

        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title"> Path Lists</h3>
        
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
              <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                  <tr>
                    <th>Path Name</th>
                    <th>From Stage</th>
                    <th>To Stage</th>
                    <th>Path Connection</th>
                    <th>Priority</th>
                    <th>Path Condition</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($pathList) <= 0)
                  <td colspan="6" style="color:red;text-align:center">-None-</td>
                  @else
                    @foreach($pathList as $index => $data)
                    <tr>
                      <td>{{++$index}}. {{$data->name}}</td>
                      
                      <!-- check if from stage or to stage empty -->
                      @if(empty($data->from_stage))
                        <td></td>
                      @else
                        <td>{{$data->fromstage->name}}</td>
                      @endif
                      @if(empty($data->to_stage))
                        <td></td>
                      @else
                        <td>{{$data->tostage->name}}</td>
                    @endif
                      <!-- end check if from stage or to stage empty -->

                    <td> {{$data->path_connection}}</td>
                    <td> {{$data->path_priority}}</td>
                    <td>
                      @foreach($pathCondition as $subindex => $subdata)
                        <p> 
                          {{++$subindex}}. <a href="/admin/pathcondition/{{$subdata->id}}/edit">{{$subdata->name}} </a>
                        </p>
                      @endforeach
                    </td>
                    <td> {{$data->description}}</td>

                    </tr>
                    @endforeach
                  @endif
                  </tbody>
                <tfoot>
                  <tr>
                    <th>Path Name</th>
                    <th>From Stage</th>
                    <th>To Stage</th>
                    <th>Path Connection</th>
                    <th>Priority</th>
                    <th>Path Condition</th>
                    <th>Description</th>
                  </tr>
                </tfoot>
              </table>

          </div>
          <!-- /.box-body -->

        </div>


     
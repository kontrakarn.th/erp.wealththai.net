

        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title"> Action Lists</h3>
        
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="column">
              <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                  <tr>
                  <th style="background-color:#A8DC66">Enter Action Lists <a href="/admin/stageaction/create?stage={{$stageDetail->id}}&actime=enter&blink=/admin/stage/detail/{{$stageDetail->id}}"><i class="fa fa-plus" style="float:right"></i></a></th>
                  </tr>
                  <tr>
                    <th width="50%" >Entering</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($enterActionList) <= 0)
                  <td style="color:red">-None-</td>
                  @else
                    @foreach($enterActionList as $index => $data)
                    <tr>
                      <td>{{++$index}}. {{$data->name}}</td>
                    </tr>
                    @endforeach
                  @endif
                  </tbody>
                <tfoot>
                  <tr>
                    <th>Entering</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <div class="column">
              <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                <thead>
                  <tr>
                    <th style="background-color:#F26E8A">Exit Action Lists <a href="/admin/stageaction/create?stage={{$stageDetail->id}}&actime=exit&blink=/admin/stage/detail/{{$stageDetail->id}}"><i class="fa fa-plus" style="float:right"></i></a></th>
                  </tr>
                  <tr>
                    <th width="50%">Exiting</th>
                  </tr>
                </thead>
                <tbody>
                  @if(count($exitActionList) <= 0)
                  <td style="color:red">-None- </td>
                  @else
                  @foreach($exitActionList as $index => $data)
                  <tr>
                    <td>{{++$index}}. {{$data->name}}</td>
                  </tr>
                  @endforeach
                  @endif
                  </tbody>
                <tfoot>
                  <tr>
                    <th>Exiting</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
          <!-- /.box-body -->

        </div>


     
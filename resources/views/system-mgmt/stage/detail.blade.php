@extends('system-mgmt.stage.base')
@section('action-content')
    <!-- Main content -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
    /* Create two equal columns that floats next to each other */
    .column {
      float: left;
      width: 50%;
      padding: 10px;
    }
    @media screen and (max-width: 600px) {
      .column {
        width: 100%;
      }
    }
    </style>

    <section class="content">

      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
        <h3 class="box-title">{{$stageDetail->name}} </h3>
        </div>
        
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>

    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">


      <div style="padding:10px">
        <table >
          <thead>
          <tr>
          <th width="30%">Stage ID</th>
          <td >{{$stageDetail->id}}</td>
          </tr>
          
          <tr>
          <th width="30%">Stage Name</th>
          <td>{{$stageDetail->name}}</td>
          </tr>
          <tr>
          <th width="30%">Process Name</th>
          <td>{{$stageDetail->Process->name}}</td>
          </tr>
          <tr>
          <th width="30%">End Stage Flag</th>
          @if($stageDetail->end_stage_flag == 1)
          <td>Yes</td>
          @else
          <td>No</td>
          @endif
          </tr>
          <tr>
          <th width="30%">Stage Description </th>
          @if($stageDetail->description == NULL)
          <td style="color:red"> -None- </td>
          @else
          <td>{{$stageDetail->description}}</td>
          @endif
          </tr>
        </thead>
          </table>
        </div>

        @include('system-mgmt.stage.detail_action')
        @include('system-mgmt.stage.detail_path')


      <br />
     
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
        </div>
        <div class="col-sm-7">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
</div>
    </section>

    @endsection

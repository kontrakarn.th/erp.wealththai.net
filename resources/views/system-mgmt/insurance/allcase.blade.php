@extends('system-mgmt.insurance.base')
@section('action-content')


    <style>
            tr:hover {
            cursor: pointer
        }
    div.dataTables_wrapper  div.dataTables_filter {
    margin-top:30px;
  width: 100%;
  float: none;
  text-align: left;
}
</style>
    <section class="content">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div  >
        <div class="container">

        </div>


          <div style="background-color:blue"></div>

    </div>
  </div>
  <div class="box">
  <div class="box-header">
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>

    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">


          <div style="overflow-x:auto" >
          <div>   
            
          <table style="width:2000px;" id="datatable" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info">
                    <thead >
                        
                        <tr>
                        <th colspan="17" >
                        <label  class="checkbox-inline">
                        @if($checkBoxflag == 1)
                          <input  checked="true"  type="checkbox" value="/wealththaiinsurance/all/cases" onClick=" window.location = this.value"/>
                        @else
                          <input    type="checkbox" value="/wealththaiinsurance/all/casesdata" onClick="if (this.checked) { window.location = this.value; }"/>

                        @endif
                        ดูงานทั้งหมดที่มีสิทธิ  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                        </label>
                        <a href="/wealththaiinsurance/all/casesdata?filter=late"><i style="color:#FE9292" class="fa fa-stop"> <b>เลยวันที่ติดตามงานแล้ว</b></i></a>
                        <a href="/wealththaiinsurance/all/casesdata?filter=remaining"><i style="color:#EBC527" class="fa fa-stop"> <b>ใกล้ถึงวันที่ติดตามงานแล้ว</b></i></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{$paginateUrl}}">รีเซ็ต</a>

                        </th>
                        </tr>

                        <tr style="background-color:#C9EEFF;">
                          <th style="display:none"></th>
                          <th scope="col" >No.</th>
                          <th scope="col" >การติดตามงาน</th>
                          <th scope="col" >วันที่คงเหลือ</th>
                          <th scope="col" width="10">รหัส</th>
                          <th scope="col" >ประเภทงาน</th>
                          <th scope="col" >ชื่องาน</th>
                          <th scope="col">ขั้นตอน</th>
                          <th scope="col">งานต่ออายุ</th>
                          <th scope="col" >สถานะ</th>
                          <th scope="col" >ช่องทางรับงาน</th>
                          <th scope="col" >วันที่รับงาน</th>
                          <th scope="col" >ผู้แจ้งงาน</th>
                          <th scope="col" >ผู้ประสานงาน</th>
                          <th scope="col" >ผู้ให้คำปรึกษา</th>
                          <th scope="col">ชื่อ - นามสกุล</th>
                          <th scope="col" >สถานะลูกค้า</th>
                          <th scope="col" width="20">ชื่อสินทรัพย์</th>
                          <th scope="col" >ทะเบียน</th>
                        </tr>
                       
                      </thead>
                      <tbody>
                    @foreach($cases as $index =>$data)
                    @php
                    $req7 = 100000000000;
        $req8 = 100000000000;
        $req9 = 100000000000;
        $datereal = '';
        $daterealname = '';
        if ($data->require_value7 == null || $data->require_value7 == '' || $data->require_value7 == '//') {
            if ($data->require_value7 == '//') {

            }
        } else {
            if ($data->var_value71 != null) {

            } else {
                $explode7 = explode('/', $data->require_value7);
                $day7 = $explode7[0];
                $month7 = $explode7[1];
                $year7 = $explode7[2];
                $var7merge = $year7 . $month7 . $day7;
                $req7 = $var7merge;
            }

        }
        if ($data->require_value8 == null || $data->require_value8 == '' || $data->require_value8 == '//') {
            if ($data->require_value8 == '//') {
            }

        } else {
            if ($data->var_value70 != null) {

            } else {
                $explode8 = explode('/', $data->require_value8);
                $day8 = $explode8[0];
                $month8 = $explode8[1];
                $year8 = $explode8[2];
                $var8merge = $year8 . $month8 . $day8;
                $req8 = $var8merge;
            }
        }
        if ($data->require_value9 == null || $data->require_value9 == '' || $data->require_value9 == '//') {
            if ($data->require_value9 == '//') {
            }

        } else {
            if ($data->var_value72 != null) {
            } else {
                $explode9 = explode('/', $data->require_value9);
                $day9 = $explode9[0];
                $month9 = $explode9[1];
                $year9 = $explode9[2];
                $var9merge = $year9 . $month9 . $day9;
                $req9 = $var9merge;
            }
        }
        $findmin = min($req7, $req8, $req9);
        if ($req7 == $findmin) {
            $datereal = $data->require_value7;
            $daterealname = $data->casetype->requirename_var7;
        } elseif ($req8 == $findmin) {
            $datereal = $data->require_value8;
            $daterealname = $data->casetype->requirename_var8;
        } elseif ($req9 == $findmin) {
            $datereal = $data->require_value9;
            $daterealname = $data->casetype->requirename_var9;
        } else {
        }
        if ($findmin == 100000000000) {
            $datereal = '';
            $daterealname = '';
        }

        $daynotifyremaining = '';
        $explode = explode('/', $datereal);
        if (count($explode) == 3) {
            $dayre = $explode[0];
            $monthre = $explode[1];
            $yearre = $explode[2];
            $day = date("d");
            $month = date("m");
            $year = date("Y");
            $currentdate = $year . "-" . $month . "-" . $day;
            $renewdate = $yearre . "-" . $monthre . "-" . $dayre;
            $datetime1 = date_create($renewdate);
            $datetime2 = date_create($currentdate);
            $interval = date_diff($datetime2, $datetime1);
            if ($interval->format('%R') == '+') {
                $daynotifyremaining = $interval->format('%a');
            } else {
                $daynotifyremaining = $interval->format('%R%a');
            }
        }
                    @endphp
                    @if($daynotifyremaining < 0)
                    <tr class="table-tr" style="background-color:#FE9292" data-url="/wealththaiinsurance/cases/{{$data -> id}}/detail/show">
                    @elseif($daynotifyremaining <= 10 && $daynotifyremaining >=1 )
                    <tr class="table-tr" style="background-color:#EBC527" data-url="/wealththaiinsurance/cases/{{$data -> id}}/detail/show">
                    @else
                    <tr class="table-tr" data-url="/wealththaiinsurance/cases/{{$data -> id}}/detail/show">
                    @endif
                    <td style="display:none"></td>
                    <td >{{++$index}}</td>
                    @if(empty($daynotifyremaining))
                    <td> - </td>
                    <td> - </td>
                    @else
                    <td>{{$daterealname}}</td>
                    <td>{{$daynotifyremaining}} </td>
                    @endif
                      <td>{{$data -> id}}</td>
                      <td>{{$data -> CaseType -> name}}</td>
                      <td>{{$data -> name}}</td>
                      @if(empty($data->stage))
                      <td></td>
                      @else
                      <td>{{$data -> Stage -> name}}</td>
                      @endif
                      <td>{{$data -> renew_case_id}}</td>
                      @if(empty($data->case_status))
                      <td></td>
                      @else
                      <td>{{$data -> CaseStatus -> name}}</td>
                      @endif
                      <td>{{$data -> CaseChannel -> name}}</td>
                      <td>{{$data -> case_created_date}}</td>
                      @if(empty($data->service_user_block_id))
                      <td></td>
                      @else
                      <td>{{$data -> Block -> name}}</td>
                      @endif
                      @if(empty($data->coordinate_user_block_id))
                      <td></td>
                      @else
                      <td>{{$data -> coordiantor -> firstname}}</td>
                      @endif
                      @if(empty($data->consult_partner_block_id))
                      <td></td>
                      @else
                      <td>{{$data -> partner_block -> name}}</td>
                      @endif
                      @if(empty($data->member_case_owner))
                      <td></td>
                      <td></td>
                      @else
                      <td>{{$data -> Person -> name}} {{$data -> Person -> lname}}</td>

                        @if(empty($data-> Person -> status))
                        <td></td>
                        @else
                        <td>{{$data -> Person -> memberstatus-> name}}</td>
                        @endif

                      @endif
                      
                      @if(empty($data->referal_asset))
                      <td></td>
                      <td></td>
                      @else
                      <td>{{$data -> Asset -> name}}</td>
                      <td>{{$data -> Asset -> ref_name}}</td>
                      @endif
                      
                    </tr>
                    @endforeach
                </tbody>
                  </table>
                  <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to
                      @if(!in_array($paginateQuantity,[5,10,20,30,40,50,100,200,300]))
                             {{$paginateQuantity}}
                      @else 
                    <select onchange="location = this.value;">
                        <option value="{{$paginateUrl}}/pagination/5/page" {{$paginateQuantity == 5? 'selected' : ''}}>5</option>
                        <option value="{{$paginateUrl}}/pagination/10/page" {{$paginateQuantity == 10? 'selected' : ''}}>10</option>
                        <option value="{{$paginateUrl}}/pagination/20/page" {{$paginateQuantity == 20? 'selected' : ''}}>20</option>
                        <option value="{{$paginateUrl}}/pagination/30/page" {{$paginateQuantity == 30? 'selected' : ''}}>30</option>
                        <option value="{{$paginateUrl}}/pagination/40/page" {{$paginateQuantity == 40? 'selected' : ''}}>40</option>
                        <option value="{{$paginateUrl}}/pagination/50/page" {{$paginateQuantity == 50? 'selected' : ''}}>50</option>
                        <option value="{{$paginateUrl}}/pagination/100/page" {{$paginateQuantity == 100? 'selected' : ''}}>100</option>
                        <option value="{{$paginateUrl}}/pagination/200/page" {{$paginateQuantity == 200? 'selected' : ''}}>200</option>
                        <option value="{{$paginateUrl}}/pagination/300/page" {{$paginateQuantity == 300? 'selected' : ''}}>300</option>

                    </select>
                    @endif
                    of {{count($cases)}} entries</div>

                  
                  <br/>
                  {{$cases -> links()}}

                  </div>

        </div>
      </div>

    </div>
  </div>
  <!-- /.box-body -->
</div>

    </section>

@endsection
@push('scripts')

    <script type="text/javascript">
        $(document).ready(function() {

            exportFile('#datatable', 'cases_lists');

            function exportFile(element, file_name) {

                $(element).DataTable( {
                    dom: 'Bfrt',
                    "paging":true,

                    "columnDefs": [
                    /*{ "orderable": false, "targets": 1 },
                    { "orderable": false, "targets": 3 },
                    { "orderable": false, "targets": 7 },
                    { "orderable": false, "targets": 8 }*/

                    ],
                    "pageLength": 5000,
                    buttons: [

                        {
                            text: 'กลับไปหน้าหลัก',
                            className:'btn btn-primary btn-margin',
                            action: function ( e, dt, button, config ) {
                            window.location = '/wealththaiinsurance/all/case/stage';
                        }
                        },
                        {
                            text: 'ไปหน้าค้นหาข้อมูล',
                            className:'btn btn-warning btn-margin',
                            action: function ( e, dt, button, config ) {
                            window.location = '/wealththaiinsurance/all/searchcase';
                        }
                        },
                        
                        /* {
                            extend: 'copyHtml5',
                            className:'btn btn-default btn-margin',
                            bom: true,
                            title: file_name
                        },*/
                        {
                            text: 'Export CSV',
                            extend: 'csvHtml5',
                            className:'btn btn-default btn-margin',
                            bom: true,
                            title: file_name
                        },


                    ],

                });
            }

        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {


            $('#master').on('click', function(e) {
             if($(this).is(':checked',true))
             {
                $(".sub_chk").prop('checked', true);
             } else {
                $(".sub_chk").prop('checked',false);
             }
            });


            $('.delete_all').on('click', function(e) {


                var allVals = [];
                $(".sub_chk:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });


                if(allVals.length <=0)
                {
                    alert("Please select row.");
                }  else {


                    var check = confirm("Are you sure you want to delete this row?");
                    if(check == true){


                        var join_selected_values = allVals.join(",");


                        $.ajax({
                            url: $(this).data('url'),
                            type: 'DELETE',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: 'ids='+join_selected_values,
                            success: function (data) {
                                if (data['success']) {
                                    $(".sub_chk:checked").each(function() {
                                        $(this).parents("tr").remove();
                                    });
                                    alert(data['success']);
                                    location.href = "/allpatient";

                                } else if (data['error']) {
                                    alert(data['error']);
                                } else {
                                    alert('Whoops Something went wrong!!');
                                }
                            },
                            error: function (data) {
                                alert(data.responseText);
                            }
                        });


                      $.each(allVals, function( index, value ) {
                          $('table tr').filter("[data-row-id='" + value + "']").remove();
                      });
                    }
                }
            });


            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                onConfirm: function (event, element) {
                    element.trigger('confirm');
                }
            });


            $(document).on('confirm', function (e) {
                var ele = e.target;
                e.preventDefault();


                $.ajax({
                    url: ele.href,
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (data) {
                        if (data['success']) {
                            $("#" + data['tr']).slideUp("slow");
                            alert(data['success']);
                        } else if (data['error']) {
                            alert(data['error']);
                        } else {
                            alert('Whoops Something went wrong!!');
                        }
                    },
                    error: function (data) {
                        alert(data.responseText);
                    }
                });


                return false;
            });


        });

    </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>

$(function() {
  $('table.table').on("click", "tr.table-tr", function() {
    window.location = $(this).data("url");
    //alert($(this).data("url"));
  });
});
</script>
    @endpush

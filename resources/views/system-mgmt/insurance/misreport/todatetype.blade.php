
@if($dateType == 1)

<div class="card-header">
    <b>ถึงวันที่</b>
</div>
<div class="card-body">
    <select class="form-control" name="toDay" required>
        <option value=""> วัน </option>
        <option value="01" {{$toDay == "01" ? 'selected' : ''}}> 01 </option>
        <option value="02" {{$toDay == "02" ? 'selected' : ''}}> 02 </option>
        <option value="03" {{$toDay == "03" ? 'selected' : ''}}> 03 </option>
        <option value="04" {{$toDay == "04" ? 'selected' : ''}}> 04 </option>
        <option value="05" {{$toDay == "05" ? 'selected' : ''}}> 05 </option>
        <option value="06" {{$toDay == "06" ? 'selected' : ''}}> 06 </option>
        <option value="07" {{$toDay == "07" ? 'selected' : ''}}> 07 </option>
        <option value="08" {{$toDay == "08" ? 'selected' : ''}}> 08 </option>
        <option value="09" {{$toDay == "09" ? 'selected' : ''}}> 09 </option>
        @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$toDay == $i ? 'selected' : ''}}> {{$i}} </option>
            @endfor
    </select>
    <select class="form-control" name="toMonth" required>
        <option value=""> เดือน </option>
        <option value="01" {{$toMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
        <option value="02" {{$toMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
        <option value="03" {{$toMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
        <option value="04" {{$toMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
        <option value="05" {{$toMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
        <option value="06" {{$toMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
        <option value="07" {{$toMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
        <option value="08" {{$toMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
        <option value="09" {{$toMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
        <option value="10" {{$toMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
        <option value="11" {{$toMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
        <option value="12" {{$toMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
    </select>
    <select class="form-control" name="toYear" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> ปี ค.ศ </option>
        @for($i=$currentYear;$i>=1900;$i--)
        <option value="{{$i}}" {{$toYear == $i ? 'selected' : ''}}>{{$i}}</option>
        @endfor
    </select><br/><br/>&nbsp;
</div>
@elseif($dateType == 2)

<div class="card-header">
    <b>ถึงเดือนที่</b>
</div>
<div class="card-body">
    <select class="form-control" name="toMonth" required>
        <option value=""> เดือน </option>
        <option value="01" {{$toMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
        <option value="02" {{$toMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
        <option value="03" {{$toMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
        <option value="04" {{$toMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
        <option value="05" {{$toMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
        <option value="06" {{$toMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
        <option value="07" {{$toMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
        <option value="08" {{$toMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
        <option value="09" {{$toMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
        <option value="10" {{$toMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
        <option value="11" {{$toMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
        <option value="12" {{$toMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
    </select>
    <select class="form-control" name="toYear" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> ปี ค.ศ </option>
        @for($i=$currentYear;$i>=1900;$i--)
        <option value="{{$i}}" {{$toYear == $i ? 'selected' : ''}}>{{$i}}</option>
        @endfor
    </select><br/><br/>&nbsp;
</div>
@elseif($dateType == 3)
<div class="card-header">
    <b>ถึงไตรมาสที่</b>
</div>
<div class="card-body">
    
    <select class="form-control" name="toMonth" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> โปรดเลือก </option>    
        <option value="1" {{$toMonth == "1" ? 'selected' : ''}}> ไตรมาสที่ 1 </option>    
        <option value="2" {{$toMonth == "2" ? 'selected' : ''}}> ไตรมาสที่ 2 </option>    
        <option value="3" {{$toMonth == "3" ? 'selected' : ''}}> ไตรมาสที่ 3 </option>    
        <option value="4" {{$toMonth == "4" ? 'selected' : ''}}> ไตรมาสที่ 4 </option>    
       
    </select>
    <select class="form-control" name="toYear" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> ปี ค.ศ </option>
        @for($i=$currentYear;$i>=1900;$i--)
        <option value="{{$i}}" {{$toYear == $i ? 'selected' : ''}}>{{$i}}</option>
        @endfor
    </select><br/><br/>&nbsp;
</div>
@elseif($dateType == 4)
<div class="card-header">
    <b>ปี</b>
</div>
<div class="card-body">
   
    <select class="form-control" name="fromYear" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> ปี ค.ศ </option>
        @for($i=$currentYear;$i>=1900;$i--)
        <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
        @endfor
    </select><br/><br/>&nbsp;
</div>

@elseif($dateType == 5)
<div class="card-header">
    <b>ถึงวันที่</b>
</div>
<div class="card-body">
   
    <select class="form-control" name="toYear" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> ปี ค.ศ </option>
        @for($i=$currentYear;$i>=1900;$i--)
        <option value="{{$i}}" {{$toYear == $i ? 'selected' : ''}}>{{$i}}</option>
        @endfor
    </select><br/><br/>&nbsp;
</div>
@else

<div class="card-header">
    <b>ถึงวันที่</b>
</div>
<div class="card-body">
    <select class="form-control" name="toDay" required>
        <option value=""> วัน </option>
        <option value="01" {{$toDay == "01" ? 'selected' : ''}}> 01 </option>
        <option value="02" {{$toDay == "02" ? 'selected' : ''}}> 02 </option>
        <option value="03" {{$toDay == "03" ? 'selected' : ''}}> 03 </option>
        <option value="04" {{$toDay == "04" ? 'selected' : ''}}> 04 </option>
        <option value="05" {{$toDay == "05" ? 'selected' : ''}}> 05 </option>
        <option value="06" {{$toDay == "06" ? 'selected' : ''}}> 06 </option>
        <option value="07" {{$toDay == "07" ? 'selected' : ''}}> 07 </option>
        <option value="08" {{$toDay == "08" ? 'selected' : ''}}> 08 </option>
        <option value="09" {{$toDay == "09" ? 'selected' : ''}}> 09 </option>
        @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$toDay == $i ? 'selected' : ''}}> {{$i}} </option>
            @endfor
    </select>
    <select class="form-control" name="toMonth" required>
        <option value=""> เดือน </option>
        <option value="01" {{$toMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
        <option value="02" {{$toMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
        <option value="03" {{$toMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
        <option value="04" {{$toMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
        <option value="05" {{$toMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
        <option value="06" {{$toMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
        <option value="07" {{$toMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
        <option value="08" {{$toMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
        <option value="09" {{$toMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
        <option value="10" {{$toMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
        <option value="11" {{$toMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
        <option value="12" {{$toMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
    </select>
    <select class="form-control" name="toYear" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> ปี ค.ศ </option>
        @for($i=$currentYear;$i>=1900;$i--)
        <option value="{{$i}}" {{$toYear == $i ? 'selected' : ''}}>{{$i}}</option>
        @endfor
    </select><br/><br/>&nbsp;
</div>
@endif
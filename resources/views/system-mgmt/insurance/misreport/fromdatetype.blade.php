@if($dateType == 1)
<div class="card-header">
    <b>จากวันที่</b>
</div>
<div class="card-body">
    <select class="form-control" name="fromDay" required>
    <option value=""> วัน </option>
    <option value="01" {{$fromDay == "01" ? 'selected' : ''}}> 01 </option>
    <option value="02" {{$fromDay == "02" ? 'selected' : ''}}> 02 </option>
    <option value="03" {{$fromDay == "03" ? 'selected' : ''}}> 03 </option>
    <option value="04" {{$fromDay == "04" ? 'selected' : ''}}> 04 </option>
    <option value="05" {{$fromDay == "05" ? 'selected' : ''}}> 05 </option>
    <option value="06" {{$fromDay == "06" ? 'selected' : ''}}> 06 </option>
    <option value="07" {{$fromDay == "07" ? 'selected' : ''}}> 07 </option>
    <option value="08" {{$fromDay == "08" ? 'selected' : ''}}> 08 </option>
    <option value="09" {{$fromDay == "09" ? 'selected' : ''}}> 09 </option>
    @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$fromDay == $i ? 'selected' : ''}}> {{$i}} </option>
        @endfor
</select>
<select class="form-control" name="fromMonth" required>
    <option value=""> เดือน </option>
    <option value="01" {{$fromMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
    <option value="02" {{$fromMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
    <option value="03" {{$fromMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
    <option value="04" {{$fromMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
    <option value="05" {{$fromMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
    <option value="06" {{$fromMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
    <option value="07" {{$fromMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
    <option value="08" {{$fromMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
    <option value="09" {{$fromMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
    <option value="10" {{$fromMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
    <option value="11" {{$fromMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
    <option value="12" {{$fromMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
</select>
<select class="form-control" name="fromYear" required>
    @php
    $currentYear = date("Y");
    @endphp
    <option value=""> ปี ค.ศ </option>
    @for($i=$currentYear;$i>=1900;$i--)
    <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
    @endfor
</select><br/><br/>&nbsp;

@elseif($dateType == 2)

<div class="card-header">
    <b>จากเดือนที่</b>
</div>
<div class="card-body">

<select class="form-control" name="fromMonth" required>
    <option value=""> เดือน </option>
    <option value="01" {{$fromMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
    <option value="02" {{$fromMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
    <option value="03" {{$fromMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
    <option value="04" {{$fromMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
    <option value="05" {{$fromMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
    <option value="06" {{$fromMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
    <option value="07" {{$fromMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
    <option value="08" {{$fromMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
    <option value="09" {{$fromMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
    <option value="10" {{$fromMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
    <option value="11" {{$fromMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
    <option value="12" {{$fromMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
</select>
<select class="form-control" name="fromYear" required>
    @php
    $currentYear = date("Y");
    @endphp
    <option value=""> ปี ค.ศ </option>
    @for($i=$currentYear;$i>=1900;$i--)
    <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
    @endfor
</select><br/><br/>&nbsp;

@elseif($dateType == 3)

<div class="card-header">
    <b>จากไตรมาสที่</b>
</div>
<div class="card-body">
    <select class="form-control" name="fromMonth" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> โปรดเลือก </option>    
        <option value="1" {{$fromMonth == "1" ? 'selected' : ''}}> ไตรมาสที่ 1 </option>    
        <option value="2" {{$fromMonth == "2" ? 'selected' : ''}}> ไตรมาสที่ 2 </option>    
        <option value="3" {{$fromMonth == "3" ? 'selected' : ''}}> ไตรมาสที่ 3 </option>    
        <option value="4" {{$fromMonth == "4" ? 'selected' : ''}}> ไตรมาสที่ 4 </option>       
       
    </select>
    <select class="form-control" name="fromYear" required>
        @php
        $currentYear = date("Y");
        @endphp
        <option value=""> ปี ค.ศ </option>
        @for($i=$currentYear;$i>=1900;$i--)
        <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
        @endfor
    </select><br/><br/>&nbsp;
@elseif($dateType == 4)

<div class="card-header">
    <b>จากปี</b>
</div>
<div class="card-body">
<select class="form-control" name="fromMonth" required>
    <option value=""> เดือน </option>
    <option value="1" {{$fromMonth == "1" ? 'selected' : ''}}> เดือนที่ 1-6 </option>    
    <option value="2" {{$fromMonth == "2" ? 'selected' : ''}}> เดือนที่ 7-12 </option>    
</select><br/><br/>&nbsp;

@elseif($dateType == 5)

<div class="card-header">
    <b>จากปี</b>
</div>
<div class="card-body">
<select class="form-control" name="fromYear" required>
    @php
    $currentYear = date("Y");
    @endphp
    <option value=""> ปี ค.ศ </option>
    @for($i=$currentYear;$i>=1900;$i--)
    <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
    @endfor
</select><br/><br/>&nbsp;

@else

<div class="card-header">
    <b>จากวันที่</b>
</div>
<div class="card-body">
    <select class="form-control" name="fromDay" required>
    <option value=""> วัน </option>
    <option value="01" {{$fromDay == "01" ? 'selected' : ''}}> 01 </option>
    <option value="02" {{$fromDay == "02" ? 'selected' : ''}}> 02 </option>
    <option value="03" {{$fromDay == "03" ? 'selected' : ''}}> 03 </option>
    <option value="04" {{$fromDay == "04" ? 'selected' : ''}}> 04 </option>
    <option value="05" {{$fromDay == "05" ? 'selected' : ''}}> 05 </option>
    <option value="06" {{$fromDay == "06" ? 'selected' : ''}}> 06 </option>
    <option value="07" {{$fromDay == "07" ? 'selected' : ''}}> 07 </option>
    <option value="08" {{$fromDay == "08" ? 'selected' : ''}}> 08 </option>
    <option value="09" {{$fromDay == "09" ? 'selected' : ''}}> 09 </option>
    @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$fromDay == $i ? 'selected' : ''}}> {{$i}} </option>
        @endfor
</select>
<select class="form-control" name="fromMonth" required>
    <option value=""> เดือน </option>
    <option value="01" {{$fromMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
    <option value="02" {{$fromMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
    <option value="03" {{$fromMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
    <option value="04" {{$fromMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
    <option value="05" {{$fromMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
    <option value="06" {{$fromMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
    <option value="07" {{$fromMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
    <option value="08" {{$fromMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
    <option value="09" {{$fromMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
    <option value="10" {{$fromMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
    <option value="11" {{$fromMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
    <option value="12" {{$fromMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
</select>
<select class="form-control" name="fromYear" required>
    @php
    $currentYear = date("Y");
    @endphp
    <option value=""> ปี ค.ศ </option>
    @for($i=$currentYear;$i>=1900;$i--)
    <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
    @endfor
</select><br/><br/>&nbsp;
@endif
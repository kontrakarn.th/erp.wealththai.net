@extends('system-mgmt.insurance.misreport.base')
@section('action-content')
    <!-- Main content -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />

    <script src="//fb.me/react-0.14.3.js"></script>
<script src="//fb.me/react-dom-0.14.3.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.1.0/jspdf.plugin.autotable.js"></script>


    <style>

</style>
    <section class="content">
  <div class="box">
  <div class="box-header">
    <h3>Liquidity Asset report</h3>

  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>

    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <form method="POST" action="/wealththaiinsurance/report/liquidityasset/search">
            {{ csrf_field() }}
          <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>จากวันที่สร้างงาน</b>
                </div>
                <div class="card-body">
                    <select class="form-control" name="fromDay" >
                    <option value=""> วัน </option>
                    <option value="01" {{$fromDay == "01" ? 'selected' : ''}}> 01 </option>
                    <option value="02" {{$fromDay == "02" ? 'selected' : ''}}> 02 </option>
                    <option value="03" {{$fromDay == "03" ? 'selected' : ''}}> 03 </option>
                    <option value="04" {{$fromDay == "04" ? 'selected' : ''}}> 04 </option>
                    <option value="05" {{$fromDay == "05" ? 'selected' : ''}}> 05 </option>
                    <option value="06" {{$fromDay == "06" ? 'selected' : ''}}> 06 </option>
                    <option value="07" {{$fromDay == "07" ? 'selected' : ''}}> 07 </option>
                    <option value="08" {{$fromDay == "08" ? 'selected' : ''}}> 08 </option>
                    <option value="09" {{$fromDay == "09" ? 'selected' : ''}}> 09 </option>
                    @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$fromDay == $i ? 'selected' : ''}}> {{$i}} </option>
                        @endfor
                </select>
                <select class="form-control" name="fromMonth" >
                    <option value=""> เดือน </option>
                    <option value="01" {{$fromMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
                    <option value="02" {{$fromMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
                    <option value="03" {{$fromMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
                    <option value="04" {{$fromMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
                    <option value="05" {{$fromMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
                    <option value="06" {{$fromMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
                    <option value="07" {{$fromMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
                    <option value="08" {{$fromMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
                    <option value="09" {{$fromMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
                    <option value="10" {{$fromMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
                    <option value="11" {{$fromMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
                    <option value="12" {{$fromMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
                </select>
                <select class="form-control" name="fromYear" >
                    @php
                    $currentYear = date("Y");
                    @endphp
                    <option value=""> ปี ค.ศ </option>
                    @for($i=$currentYear;$i>=1900;$i--)
                    <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
                    @endfor
                </select><br/><br/>&nbsp;
                </div>
            </div>
        </div>
        <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>ถึงวันที่สร้างงาน</b>
                </div>
                <div class="card-body">
                    <select class="form-control" name="toDay" >
                        <option value=""> วัน </option>
                        <option value="01" {{$toDay == "01" ? 'selected' : ''}}> 01 </option>
                        <option value="02" {{$toDay == "02" ? 'selected' : ''}}> 02 </option>
                        <option value="03" {{$toDay == "03" ? 'selected' : ''}}> 03 </option>
                        <option value="04" {{$toDay == "04" ? 'selected' : ''}}> 04 </option>
                        <option value="05" {{$toDay == "05" ? 'selected' : ''}}> 05 </option>
                        <option value="06" {{$toDay == "06" ? 'selected' : ''}}> 06 </option>
                        <option value="07" {{$toDay == "07" ? 'selected' : ''}}> 07 </option>
                        <option value="08" {{$toDay == "08" ? 'selected' : ''}}> 08 </option>
                        <option value="09" {{$toDay == "09" ? 'selected' : ''}}> 09 </option>
                        @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$toDay == $i ? 'selected' : ''}}> {{$i}} </option>
                            @endfor
                    </select>
                    <select class="form-control" name="toMonth" >
                        <option value=""> เดือน </option>
                        <option value="01" {{$toMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
                        <option value="02" {{$toMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
                        <option value="03" {{$toMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
                        <option value="04" {{$toMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
                        <option value="05" {{$toMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
                        <option value="06" {{$toMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
                        <option value="07" {{$toMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
                        <option value="08" {{$toMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
                        <option value="09" {{$toMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
                        <option value="10" {{$toMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
                        <option value="11" {{$toMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
                        <option value="12" {{$toMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
                    </select>
                    <select class="form-control" name="toYear" >
                        @php
                        $currentYear = date("Y");
                        @endphp
                        <option value=""> ปี ค.ศ </option>
                        @for($i=$currentYear;$i>=1900;$i--)
                        <option value="{{$i}}" {{$toYear == $i ? 'selected' : ''}}>{{$i}}</option>
                        @endfor
                    </select><br/><br/>&nbsp;
                </div>
            </div>
        </div>

        <div class="column4">
          <div class="card">
              <div class="card-header">
                  <b>เลือก Structure</b>
              </div>
              <div class="card-body">
                  <select class="form-control structureList" id="autowidth" name="structureId" >
                      <option value="0">ทั้งหมด</option>
                      @foreach ($structure as $data)
                        <option value="{{$data->id}}" {{$structureId == $data->id ? 'selected' : ''}}>{{$data->name}}</option>  
                      @endforeach
                  </select><br/><br/>&nbsp;
                 
              </div>
          </div>
      </div>

        <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>เลือก Block </b>
                </div>
                <div class="card-body">
                    <select class="form-control blockList" id="autowidth" name="blockid" >
                        @if(empty($blockinDropdown) || $blockinDropdown == "0")
                        <option value="0">ทั้งหมด</option>
                        @else
                        <option value="{{$blockinDropdown->id}}">{{$blockinDropdown->name}}</option>
                        @endif
                    </select><br/><br/>
                    ดูทุก Block ที่อยู่ภายใต้ Block นี <select name="underblock" ><option value="0" {{$underBlock == 0 ? 'selected' : ''}}>ไม่</option><option value="1" {{$underBlock == 1 ? 'selected' : ''}}>ใช่</option></select>
                  
                   
                </div>
            </div>
        </div>
        <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>จากวันที่คุ้มครอง</b>
                </div>
                <div class="card-body">
                    <select class="form-control" name="fromDayvalid" >
                    <option value=""> วัน </option>
                    <option value="01" {{$fromDayvalid == "01" ? 'selected' : ''}}> 01 </option>
                    <option value="02" {{$fromDayvalid == "02" ? 'selected' : ''}}> 02 </option>
                    <option value="03" {{$fromDayvalid == "03" ? 'selected' : ''}}> 03 </option>
                    <option value="04" {{$fromDayvalid == "04" ? 'selected' : ''}}> 04 </option>
                    <option value="05" {{$fromDayvalid == "05" ? 'selected' : ''}}> 05 </option>
                    <option value="06" {{$fromDayvalid == "06" ? 'selected' : ''}}> 06 </option>
                    <option value="07" {{$fromDayvalid == "07" ? 'selected' : ''}}> 07 </option>
                    <option value="08" {{$fromDayvalid == "08" ? 'selected' : ''}}> 08 </option>
                    <option value="09" {{$fromDayvalid == "09" ? 'selected' : ''}}> 09 </option>
                    @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$fromDayvalid == $i ? 'selected' : ''}}> {{$i}} </option>
                        @endfor
                </select>
                <select class="form-control" name="fromMonthvalid" >
                    <option value=""> เดือน </option>
                    <option value="01" {{$fromMonthvalid == "01" ? 'selected' : ''}}> ม.ค. </option>
                    <option value="02" {{$fromMonthvalid == "02" ? 'selected' : ''}}> ก.พ. </option>
                    <option value="03" {{$fromMonthvalid == "03" ? 'selected' : ''}}> มี.ค. </option>
                    <option value="04" {{$fromMonthvalid == "04" ? 'selected' : ''}}> เม.ย. </option>
                    <option value="05" {{$fromMonthvalid == "05" ? 'selected' : ''}}> พ.ค. </option>
                    <option value="06" {{$fromMonthvalid == "06" ? 'selected' : ''}}> มิ.ย. </option>
                    <option value="07" {{$fromMonthvalid == "07" ? 'selected' : ''}}> ก.ค. </option>
                    <option value="08" {{$fromMonthvalid == "08" ? 'selected' : ''}}> ส.ค. </option>
                    <option value="09" {{$fromMonthvalid == "09" ? 'selected' : ''}}> ก.ย. </option>
                    <option value="10" {{$fromMonthvalid == "10" ? 'selected' : ''}}> ต.ค. </option>
                    <option value="11" {{$fromMonthvalid == "11" ? 'selected' : ''}}> พ.ย. </option>
                    <option value="12" {{$fromMonthvalid == "12" ? 'selected' : ''}}> ธ.ค. </option>
                </select>
                <select class="form-control" name="fromYearvalid" >
                    @php
                    $currentYear = date("Y");
                    @endphp
                    <option value=""> ปี ค.ศ </option>
                    @for($i=$currentYear;$i>=1900;$i--)
                    <option value="{{$i}}" {{$fromYearvalid == $i ? 'selected' : ''}}>{{$i}}</option>
                    @endfor
                </select><br/><br/>&nbsp;
                </div>
            </div>
        </div>
        <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>ถึงวันที่คุ้มครอง</b>
                </div>
                <div class="card-body">
                    <select class="form-control" name="toDayvalid" >
                        <option value=""> วัน </option>
                        <option value="01" {{$toDayvalid == "01" ? 'selected' : ''}}> 01 </option>
                        <option value="02" {{$toDayvalid == "02" ? 'selected' : ''}}> 02 </option>
                        <option value="03" {{$toDayvalid == "03" ? 'selected' : ''}}> 03 </option>
                        <option value="04" {{$toDayvalid == "04" ? 'selected' : ''}}> 04 </option>
                        <option value="05" {{$toDayvalid == "05" ? 'selected' : ''}}> 05 </option>
                        <option value="06" {{$toDayvalid == "06" ? 'selected' : ''}}> 06 </option>
                        <option value="07" {{$toDayvalid == "07" ? 'selected' : ''}}> 07 </option>
                        <option value="08" {{$toDayvalid == "08" ? 'selected' : ''}}> 08 </option>
                        <option value="09" {{$toDayvalid == "09" ? 'selected' : ''}}> 09 </option>
                        @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$toDayvalid == $i ? 'selected' : ''}}> {{$i}} </option>
                            @endfor
                    </select>
                    <select class="form-control" name="toMonthvalid" >
                        <option value=""> เดือน </option>
                        <option value="01" {{$toMonthvalid == "01" ? 'selected' : ''}}> ม.ค. </option>
                        <option value="02" {{$toMonthvalid == "02" ? 'selected' : ''}}> ก.พ. </option>
                        <option value="03" {{$toMonthvalid == "03" ? 'selected' : ''}}> มี.ค. </option>
                        <option value="04" {{$toMonthvalid == "04" ? 'selected' : ''}}> เม.ย. </option>
                        <option value="05" {{$toMonthvalid == "05" ? 'selected' : ''}}> พ.ค. </option>
                        <option value="06" {{$toMonthvalid == "06" ? 'selected' : ''}}> มิ.ย. </option>
                        <option value="07" {{$toMonthvalid == "07" ? 'selected' : ''}}> ก.ค. </option>
                        <option value="08" {{$toMonthvalid == "08" ? 'selected' : ''}}> ส.ค. </option>
                        <option value="09" {{$toMonthvalid == "09" ? 'selected' : ''}}> ก.ย. </option>
                        <option value="10" {{$toMonthvalid == "10" ? 'selected' : ''}}> ต.ค. </option>
                        <option value="11" {{$toMonthvalid == "11" ? 'selected' : ''}}> พ.ย. </option>
                        <option value="12" {{$toMonthvalid == "12" ? 'selected' : ''}}> ธ.ค. </option>
                    </select>
                    <select class="form-control" name="toYearvalid" >
                        @php
                        $currentYear = date("Y");
                        @endphp
                        <option value=""> ปี ค.ศ </option>
                        @for($i=$currentYear;$i>=1900;$i--)
                        <option value="{{$i}}" {{$toYearvalid == $i ? 'selected' : ''}}>{{$i}}</option>
                        @endfor
                    </select><br/><br/>&nbsp;
                </div>
            </div>
        </div>

        <div class="column4">
          <div class="card">
              <div class="card-header">
                  <b>Asset Category</b>
              </div>
              <div class="card-body">
                  <select class="form-control assetcatList" id="autowidth" name="assetcatID" >
                      <option value="0">ทั้งหมด</option>
                      @foreach ($assetCategory as $data)
                        <option value="{{$data->id}}" {{$assetcatID == $data->id ? 'selected' : ''}}>{{$data->name}}</option>  
                      @endforeach
                  </select><br/><br/>&nbsp;
                 
              </div>
          </div>
      </div>

        <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>Asset Type </b>
                </div>
                <div class="card-body">
                    <select class="form-control assettypeList" id="autowidth" name="assettypeid" >
                        <option value="0">ทั้งหมด</option>
                        @foreach ($assetType as $data)
                          <option value="{{$data->id}}" {{$assettypeID == $data->id ? 'selected' : ''}}>{{$data->la_nla_type}} {{$data->nla_sub_type}}</option>  
                        @endforeach
                    </select><br/><br/>&nbsp;
                  
                   
                </div>
            </div>
        </div>
        <button style="float:right" type="submit" class="btn btn-primary">
          <span class="fa fa-search" aria-hidden="true"></span>
          ค้นหา
      </button>
      <br/>&nbsp;
      <br/>
      <br/>

      </form>

      <table id="datatable" class="table table-bordered table-hover" role="grid"
      aria-describedby="example2_info">
      <thead>
          <tr></tr>
          <th scope="col" >No.</th>
          <th scope="col" >ประเภทสินทรัพย์</th>
          <th scope="col" >ประเภทย่อย สินทรัพย์ </th>
          <th scope="col" >ชื่อ สินทรัพย์ </th>
          <th scope="col" >ชื่ออ้างอิงสินทรัพย์	</th>
          <th scope="col" >เริ่มคุ้มครองตั้งแต่	</th>
          <th scope="col" >คุ้มครองถึง	</th>
          <th scope="col" >Portfolio Name	</th>
          <th scope="col" >ชื่อลูกค้า</th>
          <th scope="col" >สินทรัพย์ที่คุ้มครอง</th>
          <th scope="col" >ชื่ออ้างอิงสินทรัพย์ที่คุ้มครอง</th>

          </tr>
      </thead>
      @if(empty($caseList))
      @else
      <tbody>
          @php
          $customerCount = 0;
          @endphp
          @foreach($caseList as $index => $data)
          @php
          $findLiquidAsset = \App\Cases::where('id',$data->case_id)->value('referal_asset');
          $findLiquidAsset = \App\Asset::where('id',$findLiquidAsset)->first();
          $findAsset = \App\Asset::where('id',$data->asset_id)->first();
          @endphp
          <tr>
              <td>{{++$index}}</td>
              @if(empty($findAsset))
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              @else
              <td>{{$findAsset->assettype->la_nla_type}}</td>
              <td>{{$findAsset->assettype->nla_sub_type}}</td>
              <td>{{$findAsset->name}}</td>
              <td>{{$findAsset->ref_name}}</td>

              <td>{{$findAsset->valid_from}}</td>
              <td>{{$findAsset->valid_to}}</td>
              <td>{{$findAsset->portfolio->type}}</td>
              <td>{{$findAsset->portfolio->person->name}} {{$findAsset->portfolio->person->lname}}</td>

              @endif
              <td>{{$findLiquidAsset->name}}</td>
              <td>{{$findLiquidAsset->ref_name}}</td>

          </tr>
          @endforeach   
      </tbody>
      <tfoot>

          <tr>
              <th></th>
              <th style="background-color:#F4D789">Total Cases <span
                style="float:right">{{number_format($caseList->count())}}</span></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th> </th>
              <th> </th>
              <th> </th>
              <th> </th>
              <th> </th>

          </tr>
      </tfoot>
      @endif
  </table>


        </div>
      </div>

    </div>
  </div>
  <!-- /.box-body -->
</div>

    </section>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

          $(".name").select2({
                placeholder: "กรุณาเลือก",
                allowClear: true
            });
    </script>
@endsection

@extends('system-mgmt.insurance.misreport.base')
@section('action-content')
    <!-- Main content -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />

    <script src="//fb.me/react-0.14.3.js"></script>
<script src="//fb.me/react-dom-0.14.3.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.1.0/jspdf.plugin.autotable.js"></script>

<style>
    div.dataTables_wrapper div.dataTables_filter {
        margin-top: 30px;
        width: 100%;
        float: none;
        text-align: left;
    }
</style>
    <section class="content">
  <div class="box">
  <div class="box-header">
    <h3>User Perfomance report</h3>

  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>

    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <form method="POST" action="/wealththaiinsurance/report/userperfomance/search">
            {{ csrf_field() }}
            <div class="column5">
              <div class="card">
                  <div class="card-header">
                      <b>เลือกรูปแบบวันที่</b>
                  </div>
                  <div class="card-body">
                      <select class="form-control" id="datetype" name="datetype" onchange="changeDatetype()" required> 
                      <option value="">โปรดเลือก</option>
                      <option value="1" {{$dateType == "1" ? 'selected' : ''}}>รายวัน</option>
                      <option value="2" {{$dateType == "2" ? 'selected' : ''}}>รายเดือน</option>
                      <option value="3" {{$dateType == "3" ? 'selected' : ''}}>รายไตรมาส</option>
                      <option value="4" {{$dateType == "4" ? 'selected' : ''}}>ครึ่งปี</option>
                      <option value="5" {{$dateType == "5" ? 'selected' : ''}}>รายปี</option>
                  </select><br/><br/>&nbsp;
                  
                  </div>
              </div>
          </div>
          
          <div class="column5">
            <div class="card fromtype" id="fromtype">
                
                @include('system-mgmt.insurance.misreport.fromdatetype')

                </div>
            </div>
        </div>
        <div class="column5" id="cardtotype">
            <div class="card toType" id="toType">
                @include('system-mgmt.insurance.misreport.todatetype')
            </div>
        </div>

        <div class="column5">
          <div class="card">
              <div class="card-header">
                  <b>เลือก Structure</b>
              </div>
              <div class="card-body">
                  <select class="form-control structureuser" id="autowidth" name="structureId" required>
                      <option value="0">ทั้งหมด</option>
                      @foreach ($structure as $data)
                        <option value="{{$data->id}}" {{$structureId == $data->id ? 'selected' : ''}}>{{$data->name}}</option>  
                      @endforeach
                  </select><br/><br/>&nbsp;
                 
              </div>
          </div>
      </div>

        <div class="column5">
            <div class="card">
                <div class="card-header">
                    <b>เลือก User </b>
                </div>
                <div class="card-body">
                    <select class="form-control userList" id="autowidth" name="blockid" >
                        @if(empty($blockinDropdown))
                        <option value="0">ทั้งหมด</option>
                        @else
                        <option value="{{$blockinDropdown->id}}">{{$blockinDropdown->firstname}} {{$blockinDropdown->lastname}}</option>
                        @endif
                    </select><br/><br/>
                    ดูทุก User ที่อยู่ภายใต้ User นี <select name="underblock" ><option value="0" {{$underBlock == 0 ? 'selected' : ''}}>ไม่</option><option value="1" {{$underBlock == 1 ? 'selected' : ''}}>ใช่</option></select>
                  
                   
                </div>
            </div>
        </div>

        <div class="column5" id="blankdiv" style="display:none;">
            <br/><br/><br/><br/><br/> &nbsp;
        </div>

        <button style="float:right" type="submit" class="btn btn-primary btn-margin">
          <span class="fa fa-search" aria-hidden="true"></span>
          ค้นหา
      </button>
      <a href="/wealththaiinsurance/report/userperfomance" style="float:right"  class="btn btn-default btn-margin">
        <span class="fa fa-mail-reply" aria-hidden="true"></span>
        กลับ
    </a>
      </form>
      <table id="datatable" class="table table-bordered table-hover" role="grid"
      aria-describedby="example2_info">
      <thead>
          <tr></tr>
          <th scope="col" width="10">No.</th>
          <th scope="col" width="30">Structure Name</th>
          <th scope="col" width="10">Block Name</th>
          <th scope="col" width="10">Belong To </th>
          <th scope="col" width="10">User Name </th>
          <th scope="col" width="30">Block Commission</th>
          <th scope="col" width="30">% Income</th>
          </tr>
      </thead>
      @if(empty($blockList))
      @else
      <tbody>

          @foreach($blockList as $index => $data)
          @php
          $underBlock = \App\Block::where('under_block',$data->id)->value('name');
          $userinBlock = \App\User_auth::where('block_id',$data->id)->pluck('user_id')->toArray();
          $user = \App\User::whereIn('id',$userinBlock)->get(['id','firstname','lastname']);
          $findCase = \App\Cases::where('service_user_block_id',$data->id)->where('case_status',2)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->pluck('id')->toArray();
          $findConfirmOffer = \App\Casemiddledata::whereIn('case_id',$findCase)->pluck('offer_id')->toArray();
          $findSumofOffer = \App\Offer::whereIn('id',$findConfirmOffer)->sum('offer_payment_value19');
          @endphp
          <tr>
              <td>{{++$index}}</td>
              <td>{{$data->structure->name}}</td>
              <td>{{$data->name}}</td>
              @if(!empty($data->under_block))
              <td>{{$data->belongtoblock->name}}</td>
              @else
              <td></td>
              @endif
              <td>@foreach($user as $u)<p>{{$u->firstname}} {{$u->lastname}},</p>@endforeach</td>
              @if($findSumofOffer != 0)
              <td>{{number_format($findSumofOffer,2,'.',',')}}</td>
              <td>{{number_format(($findSumofOffer / $sumTotalfee)*100,2,'.','')}} %</td>
              @else
              <td>0.00</td>
              <td>0.00 %</td>
              @endif

          </tr>
          @endforeach
      </tbody>
      <tfoot>

          <tr>
              <th></th>
              <th></th>
              <th style="background-color:#F4D789">Total Block <span
                      style="float:right">{{number_format($blockList->count())}}</span></th>
              <th></th>
              <th></th>
              <th style="background-color:#F4D789">Total Commission <span
                      style="float:right">{{number_format($sumTotalfee,2,'.',',')}}</span></th>
              <th> 100 % </th>
          </tr>
      </tfoot>
      @endif
  </table>

        </div>
      </div>

    </div>
  </div>
  <!-- /.box-body -->
</div>

    </section>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

          $(".name").select2({
                placeholder: "กรุณาเลือก",
                allowClear: true
            });
    </script>
@endsection
@push('scripts')
<script>
    function changeDatetype() {
    var x = document.getElementById("datetype").value;
    //document.getElementById("demo").innerHTML = "You selected: " + x;
    var fromDay = {{$fromDay}}
    var fromMonth = {{$fromMonth}}
    var fromYear = {{$fromYear}}
    var toDay = {{$toDay}}
    var toMonth = {{$toMonth}}
    var toYear = {{$toYear}}

    var currentYear = new Date();
    var currentYear = currentYear.getFullYear();
    var fromtemplate = " ";
    var totemplate = " ";
    var totype = document.getElementById("cardtotype");
    var blankdiv = document.getElementById("blankdiv");

        switch(x) {
            case "1":
                totype.style.display ="block";
                blankdiv.style.display ="none";

                fromtemplate += '<div class="card-header">';
                fromtemplate += '<b>จากวันที่</b></div>';
                fromtemplate += '<div class="card-body">';
                fromtemplate += '<select class="form-control" name="fromDay" required>';
                fromtemplate += '<option value=""> วัน </option>';    
                fromtemplate += '<option value="01" {{$fromDay == "01" ? 'selected' : ''}}> 01 </option>';    
                fromtemplate += '<option value="02" {{$fromDay == "02" ? 'selected' : ''}}> 02 </option>';    
                fromtemplate += '<option value="03" {{$fromDay == "03" ? 'selected' : ''}}> 03 </option>';    
                fromtemplate += '<option value="04" {{$fromDay == "04" ? 'selected' : ''}}> 04 </option>';    
                fromtemplate += '<option value="05" {{$fromDay == "05" ? 'selected' : ''}}> 05 </option>';        
                fromtemplate += '<option value="06" {{$fromDay == "06" ? 'selected' : ''}}> 06 </option>';    
                fromtemplate += '<option value="07" {{$fromDay == "07" ? 'selected' : ''}}> 07 </option>';    
                fromtemplate += '<option value="08" {{$fromDay == "08" ? 'selected' : ''}}> 08 </option>';    
                fromtemplate += '<option value="09" {{$fromDay == "09" ? 'selected' : ''}}> 09 </option>';    
                for(i = 10;i<=31;i++){
                    if(i == fromDay){
                        fromtemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        fromtemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                fromtemplate += '</select> ';    
                fromtemplate += '<select class="form-control" name="fromMonth" required>';    
                fromtemplate += '<option value=""> เดือน </option>';    
                fromtemplate += '<option value="01" {{$fromMonth == "01" ? 'selected' : ''}}> ม.ค. </option>';    
                fromtemplate += '<option value="02" {{$fromMonth == "02" ? 'selected' : ''}}> ก.พ. </option>';    
                fromtemplate += '<option value="03" {{$fromMonth == "03" ? 'selected' : ''}}> มี.ค. </option>';    
                fromtemplate += '<option value="04" {{$fromMonth == "04" ? 'selected' : ''}}> เม.ย. </option>';    
                fromtemplate += '<option value="05" {{$fromMonth == "05" ? 'selected' : ''}}> พ.ค. </option>';    
                fromtemplate += '<option value="06" {{$fromMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>';    
                fromtemplate += '<option value="07" {{$fromMonth == "07" ? 'selected' : ''}}> ก.ค. </option>';    
                fromtemplate += '<option value="08" {{$fromMonth == "08" ? 'selected' : ''}}> ส.ค. </option>';    
                fromtemplate += '<option value="09" {{$fromMonth == "09" ? 'selected' : ''}}> ก.ย. </option>';    
                fromtemplate += '<option value="10" {{$fromMonth == "10" ? 'selected' : ''}}> ต.ค. </option>';    
                fromtemplate += '<option value="11 {{$fromMonth ==  "11" ? 'selected' : ''}}> พ.ย. </option>';    
                fromtemplate += '<option value="12" {{$fromMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>';    
                fromtemplate += '</select> ';    
                fromtemplate += '<select class="form-control" name="fromYear" required>';    
                fromtemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == fromYear){
                        fromtemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        fromtemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                fromtemplate += '</select><br/><br/>&nbsp;';    
                
                totemplate += '<div class="card-header">';
                totemplate += '<b>ถึงวันที่</b></div>';
                totemplate += '<div class="card-body">';
                totemplate += '<select class="form-control" name="toDay" required>';
                totemplate += '<option value=""> วัน </option>';    
                totemplate += '<option value="01" {{$toDay == "01" ? 'selected' : ''}}> 01 </option>';    
                totemplate += '<option value="02" {{$toDay == "02" ? 'selected' : ''}}> 02 </option>';    
                totemplate += '<option value="03" {{$toDay == "03" ? 'selected' : ''}}> 03 </option>';    
                totemplate += '<option value="04" {{$toDay == "04" ? 'selected' : ''}}> 04 </option>';    
                totemplate += '<option value="05" {{$toDay == "05" ? 'selected' : ''}}> 05 </option>';        
                totemplate += '<option value="06" {{$toDay == "06" ? 'selected' : ''}}> 06 </option>';    
                totemplate += '<option value="07" {{$toDay == "07" ? 'selected' : ''}}> 07 </option>';    
                totemplate += '<option value="08" {{$toDay == "08" ? 'selected' : ''}}> 08 </option>';    
                totemplate += '<option value="09" {{$toDay == "09" ? 'selected' : ''}}> 09 </option>';    
                for(i = 10;i<=31;i++){
                    if(i == toDay){
                        totemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        totemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                totemplate += '</select> ';    
                totemplate += '<select class="form-control" name="toMonth" required>';    
                totemplate += '<option value=""> เดือน </option>';    
                totemplate += '<option value="01" {{$toMonth == "01" ? 'selected' : ''}}> ม.ค. </option>';    
                totemplate += '<option value="02" {{$toMonth == "02" ? 'selected' : ''}}> ก.พ. </option>';    
                totemplate += '<option value="03" {{$toMonth == "03" ? 'selected' : ''}}> มี.ค. </option>';    
                totemplate += '<option value="04" {{$toMonth == "04" ? 'selected' : ''}}> เม.ย. </option>';    
                totemplate += '<option value="05" {{$toMonth == "05" ? 'selected' : ''}}> พ.ค. </option>';    
                totemplate += '<option value="06" {{$toMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>';    
                totemplate += '<option value="07" {{$toMonth == "07" ? 'selected' : ''}}> ก.ค. </option>';    
                totemplate += '<option value="08" {{$toMonth == "08" ? 'selected' : ''}}> ส.ค. </option>';    
                totemplate += '<option value="09" {{$toMonth == "09" ? 'selected' : ''}}> ก.ย. </option>';    
                totemplate += '<option value="10" {{$toMonth == "10" ? 'selected' : ''}}> ต.ค. </option>';    
                totemplate += '<option value="11 {{$toMonth ==  "11" ? 'selected' : ''}}> พ.ย. </option>';    
                totemplate += '<option value="12" {{$toMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>';    
                totemplate += '</select> ';    
                totemplate += '<select class="form-control" name="toYear" required>';    
                totemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == toYear){
                        totemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        totemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                totemplate += '</select><br/><br/>&nbsp;';    
            break;
            case "2":
                totype.style.display ="block";
                blankdiv.style.display ="none";

                fromtemplate += '<div class="card-header">';
                fromtemplate += '<b>จากเดือนที่</b></div>';
                fromtemplate += '<div class="card-body">';
                fromtemplate += '<select class="form-control" name="fromMonth" required>';    
                fromtemplate += '<option value=""> เดือน </option>';    
                fromtemplate += '<option value="01" {{$fromMonth == "01" ? 'selected' : ''}}> ม.ค. </option>';    
                fromtemplate += '<option value="02" {{$fromMonth == "02" ? 'selected' : ''}}> ก.พ. </option>';    
                fromtemplate += '<option value="03" {{$fromMonth == "03" ? 'selected' : ''}}> มี.ค. </option>';    
                fromtemplate += '<option value="04" {{$fromMonth == "04" ? 'selected' : ''}}> เม.ย. </option>';    
                fromtemplate += '<option value="05" {{$fromMonth == "05" ? 'selected' : ''}}> พ.ค. </option>';    
                fromtemplate += '<option value="06" {{$fromMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>';    
                fromtemplate += '<option value="07" {{$fromMonth == "07" ? 'selected' : ''}}> ก.ค. </option>';    
                fromtemplate += '<option value="08" {{$fromMonth == "08" ? 'selected' : ''}}> ส.ค. </option>';    
                fromtemplate += '<option value="09" {{$fromMonth == "09" ? 'selected' : ''}}> ก.ย. </option>';    
                fromtemplate += '<option value="10" {{$fromMonth == "10" ? 'selected' : ''}}> ต.ค. </option>';    
                fromtemplate += '<option value="11 {{$fromMonth ==  "11" ? 'selected' : ''}}> พ.ย. </option>';    
                fromtemplate += '<option value="12" {{$fromMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>';    
                fromtemplate += '</select> ';    
                fromtemplate += '<select class="form-control" name="fromYear" required>';    
                fromtemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == fromYear){
                        fromtemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        fromtemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                fromtemplate += '</select><br/><br/>&nbsp;';    

                totemplate += '<div class="card-header">';
                totemplate += '<b>ถึงเดือนที่</b></div>';
                totemplate += '<div class="card-body">';
                totemplate += '<select class="form-control" name="toMonth" required>';    
                totemplate += '<option value=""> เดือน </option>';    
                totemplate += '<option value="01" {{$toMonth == "01" ? 'selected' : ''}}> ม.ค. </option>';    
                totemplate += '<option value="02" {{$toMonth == "02" ? 'selected' : ''}}> ก.พ. </option>';    
                totemplate += '<option value="03" {{$toMonth == "03" ? 'selected' : ''}}> มี.ค. </option>';    
                totemplate += '<option value="04" {{$toMonth == "04" ? 'selected' : ''}}> เม.ย. </option>';    
                totemplate += '<option value="05" {{$toMonth == "05" ? 'selected' : ''}}> พ.ค. </option>';    
                totemplate += '<option value="06" {{$toMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>';    
                totemplate += '<option value="07" {{$toMonth == "07" ? 'selected' : ''}}> ก.ค. </option>';    
                totemplate += '<option value="08" {{$toMonth == "08" ? 'selected' : ''}}> ส.ค. </option>';    
                totemplate += '<option value="09" {{$toMonth == "09" ? 'selected' : ''}}> ก.ย. </option>';    
                totemplate += '<option value="10" {{$toMonth == "10" ? 'selected' : ''}}> ต.ค. </option>';    
                totemplate += '<option value="11 {{$toMonth ==  "11" ? 'selected' : ''}}> พ.ย. </option>';    
                totemplate += '<option value="12" {{$toMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>';    
                totemplate += '</select> ';    
                totemplate += '<select class="form-control" name="toYear" required>';    
                totemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == toYear){
                        totemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        totemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                totemplate += '</select><br/><br/>&nbsp;';    

            break;
            case "3":
                totype.style.display ="block";
                blankdiv.style.display ="none";

                fromtemplate += '<div class="card-header">';
                fromtemplate += '<b>จากไตรมาสที่</b></div>';
                fromtemplate += '<div class="card-body">';
                fromtemplate += '<select class="form-control" name="fromMonth" required>';    
                fromtemplate += '<option value=""> โปรดเลือก </option>';    
                fromtemplate += '<option value="1"> ไตรมาสที่ 1 </option>';    
                fromtemplate += '<option value="2"> ไตรมาสที่ 2 </option>';    
                fromtemplate += '<option value="3"> ไตรมาสที่ 3 </option>';    
                fromtemplate += '<option value="4"> ไตรมาสที่ 4 </option>';    
                fromtemplate += '</select> ';    
                fromtemplate += '<select class="form-control" name="fromYear" required>';    
                fromtemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == fromYear){
                        fromtemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        fromtemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                fromtemplate += '</select><br/><br/>&nbsp;'; 

                totemplate += '<div class="card-header">';
                totemplate += '<b>ถึงไตรมาสที่</b></div>';
                totemplate += '<div class="card-body">';
                totemplate += '<select class="form-control" name="toMonth" required>';    
                totemplate += '<option value=""> โปรดเลือก </option>';    
                totemplate += '<option value="1"> ไตรมาสที่ 1 </option>';    
                totemplate += '<option value="2"> ไตรมาสที่ 2 </option>';    
                totemplate += '<option value="3"> ไตรมาสที่ 3 </option>';    
                totemplate += '<option value="4"> ไตรมาสที่ 4 </option>';    
                totemplate += '</select> ';    
                totemplate += '<select class="form-control" name="toYear" required>';    
                totemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == toYear){
                        totemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        totemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                totemplate += '</select><br/><br/>&nbsp;'; 
            break;
            case "4":
               // totype.style.display ="none";
               // blankdiv.style.display ="block";

                fromtemplate += '<div class="card-header">';
                fromtemplate += '<b>จากเดือนที่</b></div>';
                fromtemplate += '<div class="card-body">';
                fromtemplate += '<select class="form-control" name="fromMonth" required>';    
                fromtemplate += '<option value=""> โปรดเลือก </option>';    
                fromtemplate += '<option value="1"> เดือนที่ 1-6 </option>';    
                fromtemplate += '<option value="2"> เดือนที่ 7-12 </option>';    
                fromtemplate += '</select><br/><br/>&nbsp;';    


                totemplate += '<div class="card-header">';
                totemplate += '<b>ปี</b></div>';
                totemplate += '<div class="card-body">';
                totemplate += '<select class="form-control" name="fromYear" required>';    
                totemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == toYear){
                        totemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        totemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                totemplate += '</select><br/><br/>&nbsp;';    

                
            break;
            case "5":
                totype.style.display ="block";
                blankdiv.style.display ="none";

                fromtemplate += '<div class="card-header">';
                fromtemplate += '<b>จากปี</b></div>';
                fromtemplate += '<div class="card-body">';
                fromtemplate += '<select class="form-control" name="fromYear" required>';    
                fromtemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == fromYear){
                        fromtemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        fromtemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                fromtemplate += '</select><br/><br/>&nbsp;';   

                totemplate += '<div class="card-header">';
                totemplate += '<b>ถึงปี</b></div>';
                totemplate += '<div class="card-body">';
                totemplate += '<select class="form-control" name="toYear" required>';    
                totemplate += '<option value=""> ปี ค.ศ </option>';    
                for(i = currentYear;i>=1900;i--){
                    if(i == toYear){
                        totemplate += '<option value="'+i+'" selected> '+i+' </option>';    
                    }else{
                        totemplate += '<option value="'+i+'"> '+i+' </option>';    
                    }
                }
                totemplate += '</select><br/><br/>&nbsp;';    
            break;
            default:
            // code block
        }
    $('.fromtype').html(" ");
    $('.fromtype').append(fromtemplate);
    $('.totype').html(" ");
    $('.totype').append(totemplate);

    //alert(fromDay);
  }
</script>


<script type="text/javascript">
    $(document).ready(function () {

        exportFile('#datatable', 'User_Performance_Report');

        function exportFile(element, file_name) {

            $(element).DataTable({
                dom: 'Bfrt',
                "paging": true,

                "pageLength": 5000,
                buttons: [

                    /*{
                        text: 'กลับไปหน้าหลัก',
                        className: 'btn btn-primary btn-margin',
                        action: function(e, dt, button, config) {
                            window.location = '/wealththaiinsurance/report/cordinator';
                        }
                    },*/

                    /* {
                        extend: 'copyHtml5',
                        className:'btn btn-default btn-margin',
                        bom: true,
                        title: file_name
                    }, {
                        text: 'Export PDF',
                        extend: 'pdfHtml5',
                        className: 'btn btn-default btn-margin',
                        bom: true,
                        title: file_name,
                        footer: true,
                        charset: "utf-8",
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    },*/
                    {
                        text: 'Export CSV',
                        extend: 'csvHtml5',
                        className: 'btn btn-default btn-margin',
                        bom: true,
                        title: file_name,
                        footer: true

                    },
                   

                ],

            });
        }

    });
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var dateType = {{$dateType}};

        switch(dateType) {
            case 1:

            break;
            case 2:

            break;
            case 3:

            break;
            case 4:

            break;
            case 5:
       //         var data = google.visualization.arrayToDataTable(ar);
        //        var options = {'title':'My Average Day', 'width':1500, 'height':400};

// Display the chart inside the <div> element with id="piechart"
var chart = new google.visualization.ColumnChart(document.getElementById('piechart'));
chart.draw(data, options);
            break;
            default:
            
        }
      
  }
  </script>
@endpush


@extends('system-mgmt.insurance.misreport.base')
@section('action-content')
    <!-- Main content -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />

    <script src="//fb.me/react-0.14.3.js"></script>
<script src="//fb.me/react-dom-0.14.3.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.1.0/jspdf.plugin.autotable.js"></script>


<style>
  div.dataTables_wrapper div.dataTables_filter {
        margin-top: 30px;
        width: 100%;
        float: none;
        text-align: left;
    }
</style>
    <section class="content">
  <div class="box">
  <div class="box-header">
    <h3>Case Cancel report</h3>

  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>

    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <form method="POST" action="/wealththaiinsurance/report/casecancelreportsearch">
            {{ csrf_field() }}
          <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>จากวันที่</b>
                </div>
                <div class="card-body">
                    <select class="form-control" name="fromDay" required>
                    <option value=""> วัน </option>
                    <option value="01" {{$fromDay == "01" ? 'selected' : ''}}> 01 </option>
                    <option value="02" {{$fromDay == "02" ? 'selected' : ''}}> 02 </option>
                    <option value="03" {{$fromDay == "03" ? 'selected' : ''}}> 03 </option>
                    <option value="04" {{$fromDay == "04" ? 'selected' : ''}}> 04 </option>
                    <option value="05" {{$fromDay == "05" ? 'selected' : ''}}> 05 </option>
                    <option value="06" {{$fromDay == "06" ? 'selected' : ''}}> 06 </option>
                    <option value="07" {{$fromDay == "07" ? 'selected' : ''}}> 07 </option>
                    <option value="08" {{$fromDay == "08" ? 'selected' : ''}}> 08 </option>
                    <option value="09" {{$fromDay == "09" ? 'selected' : ''}}> 09 </option>
                    @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$fromDay == $i ? 'selected' : ''}}> {{$i}} </option>
                        @endfor
                </select>
                <select class="form-control" name="fromMonth" required>
                    <option value=""> เดือน </option>
                    <option value="01" {{$fromMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
                    <option value="02" {{$fromMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
                    <option value="03" {{$fromMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
                    <option value="04" {{$fromMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
                    <option value="05" {{$fromMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
                    <option value="06" {{$fromMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
                    <option value="07" {{$fromMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
                    <option value="08" {{$fromMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
                    <option value="09" {{$fromMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
                    <option value="10" {{$fromMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
                    <option value="11" {{$fromMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
                    <option value="12" {{$fromMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
                </select>
                <select class="form-control" name="fromYear" required>
                    @php
                    $currentYear = date("Y");
                    @endphp
                    <option value=""> ปี ค.ศ </option>
                    @for($i=$currentYear;$i>=1900;$i--)
                    <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
                    @endfor
                </select><br/><br/>&nbsp;
                </div>
            </div>
        </div>
        <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>ถึงวันที่</b>
                </div>
                <div class="card-body">
                    <select class="form-control" name="toDay" required>
                        <option value=""> วัน </option>
                        <option value="01" {{$toDay == "01" ? 'selected' : ''}}> 01 </option>
                        <option value="02" {{$toDay == "02" ? 'selected' : ''}}> 02 </option>
                        <option value="03" {{$toDay == "03" ? 'selected' : ''}}> 03 </option>
                        <option value="04" {{$toDay == "04" ? 'selected' : ''}}> 04 </option>
                        <option value="05" {{$toDay == "05" ? 'selected' : ''}}> 05 </option>
                        <option value="06" {{$toDay == "06" ? 'selected' : ''}}> 06 </option>
                        <option value="07" {{$toDay == "07" ? 'selected' : ''}}> 07 </option>
                        <option value="08" {{$toDay == "08" ? 'selected' : ''}}> 08 </option>
                        <option value="09" {{$toDay == "09" ? 'selected' : ''}}> 09 </option>
                        @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$toDay == $i ? 'selected' : ''}}> {{$i}} </option>
                            @endfor
                    </select>
                    <select class="form-control" name="toMonth" required>
                        <option value=""> เดือน </option>
                        <option value="01" {{$toMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
                        <option value="02" {{$toMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
                        <option value="03" {{$toMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
                        <option value="04" {{$toMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
                        <option value="05" {{$toMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
                        <option value="06" {{$toMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
                        <option value="07" {{$toMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
                        <option value="08" {{$toMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
                        <option value="09" {{$toMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
                        <option value="10" {{$toMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
                        <option value="11" {{$toMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
                        <option value="12" {{$toMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
                    </select>
                    <select class="form-control" name="toYear" required>
                        @php
                        $currentYear = date("Y");
                        @endphp
                        <option value=""> ปี ค.ศ </option>
                        @for($i=$currentYear;$i>=1900;$i--)
                        <option value="{{$i}}" {{$toYear == $i ? 'selected' : ''}}>{{$i}}</option>
                        @endfor
                    </select><br/><br/>&nbsp;
                </div>
            </div>
        </div>

        <div class="column4">
          <div class="card">
              <div class="card-header">
                  <b>เลือก Structure</b>
              </div>
              <div class="card-body">
                  <select class="form-control structureList" id="autowidth" name="structureId" required>
                    <option value="0">ทั้งหมด</option>
                    @foreach ($structure as $data)
                        <option value="{{$data->id}}" {{$structureId == $data->id ? 'selected' : ''}}>{{$data->name}}</option>  
                      @endforeach
                  </select><br/><br/>&nbsp;
                 
              </div>
          </div>
      </div>

        <div class="column4">
            <div class="card">
                <div class="card-header">
                    <b>เลือก Block </b>
                </div>
                <div class="card-body">
                    <select class="form-control blockList" id="autowidth" name="blockid" required>
                        @if(empty($blockinDropdown))
                        <option value="0">ทั้งหมด</option>
                        @else
                        <option value="{{$blockinDropdown->id}}">{{$blockinDropdown->name}}</option>
                        @endif
                    </select><br/><br/>
                    ดูทุก Block ที่อยู่ภายใต้ Block นี <select name="underblock" ><option value="0" {{$underBlock == 0 ? 'selected' : ''}}>ไม่</option><option value="1" {{$underBlock == 1 ? 'selected' : ''}}>ใช่</option></select>
                  
                   
                </div>
            </div>
        </div>
        <button style="float:right" type="submit" class="btn btn-primary">
          <span class="fa fa-search" aria-hidden="true"></span>
          ค้นหา
      </button>
      </form>

      <table id="datatable" class="table table-bordered table-hover" role="grid"
      aria-describedby="example2_info">
      <thead>
          <tr></tr>
          <th scope="col" >No.</th>
          <th scope="col" >รหัสงาน</th>
          <th scope="col" >ชื่องาน</th>
          <th scope="col" >วันที่ยกเลิกงาน </th>
          <th scope="col" >สถานะงานล่าสุดก่อนยกเลิก </th>
          <th scope="col" >บันทึกการยกเลิกงาน	</th>
          <th scope="col" >เส้นทางรับงาน</th>
          <th scope="col" >ชื่อลูกค้า</th>
          <th scope="col" >ชื่อผู้แจ้งงาน</th>
          <th scope="col" >ชื่อผู้ประสานงาน</th>
          <th scope="col" >ชื่อผู้ให้คำปรึกษา/ผู้ให้คำแนะนำ</th>

          </tr>
      </thead>
      @if(empty($caseList))
      @else
      <tbody>
          @php
                    $customerCount = 0;
          @endphp
          @foreach($caseList as $index => $data)
          @php
          $findCaselog = \App\Case_log::where('case_id',$data->id)->Orderby('id','DESC')->first();
          @endphp
          <tr>
              <td>{{++$index}}</td>
              <td>{{$data->id}}</td>
              <td>{{$data->name}}</td>
              @if(empty($findCaselog))
              <td></td>
              <td></td>
              <td></td>
              @else
              <td>{{$findCaselog->date_time}}</td>
              <td>{{$findCaselog->movefromstage->name}}</td>
              <td>{{$findCaselog->description}}</td>
            @endif
              <td>{{$data->casechannel->name}}</td>
              <td>{{$data->person->name}} {{$data->person->lname}}</td>
              <td>{{$data->block->name}} </td>
              <td>{{$data->coordiantor->firstname}} {{$data->coordiantor->lastname}} </td>
              <td>{{$data->partner_block->name}}</td>


          </tr>
          @endforeach   
      </tbody>
      <tfoot>

          <tr>
              <th></th>
              <th style="background-color:#F4D789">Total Cases <span
                style="float:right">{{number_format($caseList->count())}}</span></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th> </th>
              <th> </th>
              <th> </th>
              <th> </th>
              <th> </th>

          </tr>
      </tfoot>
      @endif
  </table>
        
        </div>
      </div>

    </div>
  </div>
  <!-- /.box-body -->
</div>

    </section>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

          $(".name").select2({
                placeholder: "กรุณาเลือก",
                allowClear: true
            });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>


    $(function() {
      $('table.table').on("click", "tr.table-tr", function() {
        window.location = $(this).data("url");
        //alert($(this).data("url"));
      });
    });
    </script>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {

        exportFile('#datatable', 'Cases_Cancel_Report');

        function exportFile(element, file_name) {

            $(element).DataTable({
                dom: 'Bfrt',
                "paging": true,

                "pageLength": 5000,
                buttons: [

                    /*{
                        text: 'กลับไปหน้าหลัก',
                        className: 'btn btn-primary btn-margin',
                        action: function(e, dt, button, config) {
                            window.location = '/wealththaiinsurance/report/cordinator';
                        }
                    },*/

                    /* {
                        extend: 'copyHtml5',
                        className:'btn btn-default btn-margin',
                        bom: true,
                        title: file_name
                    },                    {
                        text: 'Export PDF',
                        extend: 'pdfHtml5',
                        className: 'btn btn-default btn-margin',
                        bom: true,
                        title: file_name,
                        footer: true,
                        charset: "utf-8",
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    },*/
                    {
                        text: 'Export CSV',
                        extend: 'csvHtml5',
                        className: 'btn btn-default btn-margin',
                        bom: true,
                        title: file_name,
                        footer: true

                    },


                ],

            });
        }

    });
</script>
@endpush
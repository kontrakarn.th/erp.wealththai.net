@extends('system-mgmt.insurance.misreport.base')
@section('action-content')
<style>
  div.dataTables_wrapper div.dataTables_filter {
      margin-top: 30px;
      width: 100%;
      float: none;
      text-align: left;
  }
  .column3 {
      float: left;
      text-align:center;

      width: 33.33%;
      padding: 10px;

     /* Should be removed. Only for demonstration */
    }
    @media screen and (max-width: 1110px) {
      .column3 {
        width: 100%;
      }
    }
</style>
    <!-- Main content -->
   
    <section class="content">
  <div class="box">
  <div class="box-header">
    <h3>Management Report (Co-Ordinator)</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>

    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">

        <form method="POST" action="/wealththaiinsurance/report/managementreportbycoor/search">
          {{ csrf_field() }}
        <div class="column3">
          <div class="card">
              <div class="card-header">
                  <b>จากวันที่</b>
              </div>
              <div class="card-body">
                  <select class="form-control" name="fromDay" required>
                  <option value=""> วัน </option>
                  <option value="01" {{$fromDay == "01" ? 'selected' : ''}}> 01 </option>
                  <option value="02" {{$fromDay == "02" ? 'selected' : ''}}> 02 </option>
                  <option value="03" {{$fromDay == "03" ? 'selected' : ''}}> 03 </option>
                  <option value="04" {{$fromDay == "04" ? 'selected' : ''}}> 04 </option>
                  <option value="05" {{$fromDay == "05" ? 'selected' : ''}}> 05 </option>
                  <option value="06" {{$fromDay == "06" ? 'selected' : ''}}> 06 </option>
                  <option value="07" {{$fromDay == "07" ? 'selected' : ''}}> 07 </option>
                  <option value="08" {{$fromDay == "08" ? 'selected' : ''}}> 08 </option>
                  <option value="09" {{$fromDay == "09" ? 'selected' : ''}}> 09 </option>
                  @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$fromDay == $i ? 'selected' : ''}}> {{$i}} </option>
                      @endfor
              </select>
              <select class="form-control" name="fromMonth" required>
                  <option value=""> เดือน </option>
                  <option value="01" {{$fromMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
                  <option value="02" {{$fromMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
                  <option value="03" {{$fromMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
                  <option value="04" {{$fromMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
                  <option value="05" {{$fromMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
                  <option value="06" {{$fromMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
                  <option value="07" {{$fromMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
                  <option value="08" {{$fromMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
                  <option value="09" {{$fromMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
                  <option value="10" {{$fromMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
                  <option value="11" {{$fromMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
                  <option value="12" {{$fromMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
              </select>
              <select class="form-control" name="fromYear" required>
                  @php
                  $currentYear = date("Y");
                  @endphp
                  <option value=""> ปี ค.ศ </option>
                  @for($i=$currentYear;$i>=1900;$i--)
                  <option value="{{$i}}" {{$fromYear == $i ? 'selected' : ''}}>{{$i}}</option>
                  @endfor
              </select><br/><br/>&nbsp;
              </div>
          </div>
      </div>
      <div class="column3">
          <div class="card">
              <div class="card-header">
                  <b>ถึงวันที่</b>
              </div>
              <div class="card-body">
                  <select class="form-control" name="toDay" required>
                      <option value=""> วัน </option>
                      <option value="01" {{$toDay == "01" ? 'selected' : ''}}> 01 </option>
                      <option value="02" {{$toDay == "02" ? 'selected' : ''}}> 02 </option>
                      <option value="03" {{$toDay == "03" ? 'selected' : ''}}> 03 </option>
                      <option value="04" {{$toDay == "04" ? 'selected' : ''}}> 04 </option>
                      <option value="05" {{$toDay == "05" ? 'selected' : ''}}> 05 </option>
                      <option value="06" {{$toDay == "06" ? 'selected' : ''}}> 06 </option>
                      <option value="07" {{$toDay == "07" ? 'selected' : ''}}> 07 </option>
                      <option value="08" {{$toDay == "08" ? 'selected' : ''}}> 08 </option>
                      <option value="09" {{$toDay == "09" ? 'selected' : ''}}> 09 </option>
                      @for($i=10;$i<=31;$i++) <option value="{{$i}}" {{$toDay == $i ? 'selected' : ''}}> {{$i}} </option>
                          @endfor
                  </select>
                  <select class="form-control" name="toMonth" required>
                      <option value=""> เดือน </option>
                      <option value="01" {{$toMonth == "01" ? 'selected' : ''}}> ม.ค. </option>
                      <option value="02" {{$toMonth == "02" ? 'selected' : ''}}> ก.พ. </option>
                      <option value="03" {{$toMonth == "03" ? 'selected' : ''}}> มี.ค. </option>
                      <option value="04" {{$toMonth == "04" ? 'selected' : ''}}> เม.ย. </option>
                      <option value="05" {{$toMonth == "05" ? 'selected' : ''}}> พ.ค. </option>
                      <option value="06" {{$toMonth == "06" ? 'selected' : ''}}> มิ.ย. </option>
                      <option value="07" {{$toMonth == "07" ? 'selected' : ''}}> ก.ค. </option>
                      <option value="08" {{$toMonth == "08" ? 'selected' : ''}}> ส.ค. </option>
                      <option value="09" {{$toMonth == "09" ? 'selected' : ''}}> ก.ย. </option>
                      <option value="10" {{$toMonth == "10" ? 'selected' : ''}}> ต.ค. </option>
                      <option value="11" {{$toMonth == "11" ? 'selected' : ''}}> พ.ย. </option>
                      <option value="12" {{$toMonth == "12" ? 'selected' : ''}}> ธ.ค. </option>
                  </select>
                  <select class="form-control" name="toYear" required>
                      @php
                      $currentYear = date("Y");
                      @endphp
                      <option value=""> ปี ค.ศ </option>
                      @for($i=$currentYear;$i>=1900;$i--)
                      <option value="{{$i}}" {{$toYear == $i ? 'selected' : ''}}>{{$i}}</option>
                      @endfor
                  </select><br/><br/>&nbsp;
              </div>
          </div>
      </div>

      <div class="column3">
        <div class="card">
            <div class="card-header">
                <b>เลือก Cor-Ordinator</b>
            </div>
            <div class="card-body">
                <select class="form-control" id="autowidth" name="coordinatorID" required>
                    <option value="0">ทั้งหมด</option>
                    @foreach ($coorDinator as $data)
                        <option value="{{$data->id}}" {{$coordinateIndropdown == $data->id ? 'selected' : ''}}>{{$data->firstname}} {{$data->lastname}}</option>  
                    @endforeach
                </select><br/><br/>&nbsp;
               
            </div>
        </div>
    </div>

      
      <button style="float:right" type="submit" class="btn btn-primary">
        <span class="fa fa-search" aria-hidden="true"></span>
        ค้นหา
    </button>
    </form>

      <table id="datatable" class="table table-bordered table-hover" role="grid" 
      aria-describedby="example2_info">
      <thead>
          <tr></tr>
          <th scope="col" >Co-Ordinator Name</th>
          <th scope="col" >Number Cases</th>
          <th scope="col" >% Cases</th>
          <th scope="col" >Co-Ordinator Fee</th>
          <th scope="col" >% Co-Ordinator Fee</th>
          </tr>
      </thead>
      @if(empty($blockList))
      @else
      <tbody>

          @foreach($blockList as $index => $data)
          @php
          $underBlock = \App\Block::where('under_block',$data->id)->value('name');
          $userinBlock = \App\User_auth::where('block_id',$data->id)->pluck('user_id')->toArray();
          $user = \App\User::whereIn('id',$userinBlock)->get(['id','firstname','lastname']);
          $findCase = \App\Cases::where('coordinate_user_block_id',$data->id)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status',2)->pluck('id')->toArray();
          $findConfirmOffer = \App\Casemiddledata::whereIn('case_id',$findCase)->pluck('offer_id')->toArray();
          $findSumofOffer = \App\Offer::whereIn('id',$findConfirmOffer)->sum('offer_payment_value19');
          $coorDinatorFee = \App\Offer::whereIn('id',$findConfirmOffer)->sum('offer_payment_value10');
          $taxFee = \App\Offer::whereIn('id',$findConfirmOffer)->sum('offer_payment_value9');
          $otherFee = \App\Offer::whereIn('id',$findConfirmOffer)->sum('offer_payment_value11');
          $companyIncome = \App\Offer::whereIn('id',$findConfirmOffer)->sum('offer_payment_value21');

          @endphp
          <tr>
              <td style="max-width: 150px; word-wrap: break-word;">{{$data->firstname}} {{$data->lastname}}</td>
              <td>{{number_format(count($findCase))}}</td>
              @if(count($findCase) == 0)
              <td>0.00%</td>
              @else
              <td>{{number_format((count($findCase) / $sumTotalcase)*100,2,'.','')}} %</td>
              @endif
              <td>{{number_format($coorDinatorFee,2,'.','')}}</td>
              @if($coorDinatorFee == 0)
              <td>0.00%</td>
              @else
              <td>{{number_format(($coorDinatorFee / $sumTotalnetfee)*100,2,'.','')}} %</td>
              @endif
              


          </tr>
          @endforeach
      </tbody>
      <tfoot>

          <tr>
              <th style="max-width: 150px;"></th>
              <th style="background-color:#F4D789">Total <span style="float:right">{{number_format($blockList->count())}}</span></th>
              <th> 100 % </th>
              <th style="background-color:#F4D789">Total <span style="float:right">{{number_format($sumcoorDinatorFee,2,'.','')}}</span></th>
              <th> 100 % </th>

          </tr>
      </tfoot>
      @endif
  </table>

        </div>
      </div>

    </div>
  </div>
  <!-- /.box-body -->
</div>

    </section>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

          $(".name").select2({
                placeholder: "กรุณาเลือก",
                allowClear: true
            });
    </script>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {

        exportFile('#datatable', 'Management_report(By Co-Ordinator)');

        function exportFile(element, file_name) {

            $(element).DataTable({
                dom: 'Bfrt',
                "paging": true,

                "pageLength": 5000,
                buttons: [

                    /*{
                        text: 'กลับไปหน้าหลัก',
                        className: 'btn btn-primary btn-margin',
                        action: function(e, dt, button, config) {
                            window.location = '/wealththaiinsurance/report/cordinator';
                        }
                    },*/

                    /* {
                        extend: 'copyHtml5',
                        className:'btn btn-default btn-margin',
                        bom: true,
                        title: file_name
                    },
                   
                    {
                        text: 'Export PDF',
                        extend: 'pdfHtml5',
                        className: 'btn btn-default btn-margin',
                        bom: true,
                        title: file_name,
                        footer: true,
                        charset: "utf-8",
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    },*/
                    {
                        text: 'Export CSV',
                        extend: 'csvHtml5',
                        className: 'btn btn-default btn-margin',
                        bom: true,
                        title: file_name,
                        footer: true

                    },
                ],

            });
        }

    });
</script>
@endpush
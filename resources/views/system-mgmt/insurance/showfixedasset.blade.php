@extends('system-mgmt.insurance.base')
@section('action-content')
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
        </div>

    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">


    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
            @if($flag == 1)
            <a class="btn btn-primary" href="/wealththaiinsurance/show/allfixasset">Back</a>
            @else
            <a class="btn btn-primary" href="/wealththaiinsurance/show/fixasset">Back</a>
            @endif
            <table class="table table-bordered table-hover" style="width:100%">
                <th style="background-color:#00325d;color:white;">
                  Topic
                </th>
                <th style="background-color:#00325d;color:white;">
                  Details
                </th>
                </tr>
  
                <tr>
                  <th width="50%"><p>Asset Name</p></th>
                  <td >{{$asset->name}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Asset Type</p></th>
                  <td >{{$asset->asset_type_name}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Sub Type</p></th>
                  <td >{{$asset->asset_subtype_name}} </td>
    
                </tr>
                  @if($asset->assettype->ref_name_head != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_name_head}}</p></th>
                  <td >{{$asset->ref_name}} </td>
    
                </tr>
                @endif
    
                <tr>
                  <th width="50%"><p>Portfolio</p></th>
                  <td >{{$asset->port_name}} </td>
    
                </tr>
    
    
    
                @if($asset->assettype->ref_num_head1 != NULL)
              <tr>
                <th width="50%"><p>{{$asset->assettype->ref_num_head1}}</p></th>
                <td >{{$asset->ref_number1}} </td>
    
    
              </tr>
                @endif
    
                @if($asset->assettype->ref_num_head2 != NULL)
              <tr>
                <th width="50%"><p>{{$asset->assettype->ref_num_head2}}</p></th>
                <td >{{$asset->ref_number2}} </td>
    
    
              </tr>
                @endif
                @if($asset->assettype->ref_num_head3 != NULL)
              <tr>
                <th width="50%"><p>{{$asset->assettype->ref_num_head3}}</p></th>
                <td >{{$asset->ref_number3}} </td>
    
    
              </tr>
                @endif
    
    
                @if($asset->assettype->ref_info_head1 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head1}}</p></th>
                  <td >{{$asset->ref_info1}} </td>
    
                </tr>
                @endif
                  @if($asset->assettype->ref_info_head2 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head2}}</p></th>
                  <td >{{$asset->ref_info2}} </td>
                  @endif
                </tr>
                @if($asset->assettype->ref_info_head3 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head3}}</p></th>
                  <td >{{$asset->ref_info3}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head4 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head4}}</p></th>
                  <td >{{$asset->ref_info4}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head5 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head5}}</p></th>
                  <td >{{$asset->ref_info5}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head6 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head6}}</p></th>
                  <td >{{$asset->ref_info6}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head7 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head7}}</p></th>
                  <td >{{$asset->ref_info7}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head8 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head8}}</p></th>
                  <td >{{$asset->ref_info8}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head9 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head9}}</p></th>
                  <td >{{$asset->ref_info9}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head10 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head10}}</p></th>
                  <td >{{$asset->ref_info10}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head11 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head11}}</p></th>
                  <td >{{$asset->ref_info11}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head12 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head12}}</p></th>
                  <td >{{$asset->ref_info12}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head13 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head13}}</p></th>
                  <td >{{$asset->ref_info13}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head14 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head14}}</p></th>
                  <td >{{$asset->ref_info14}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head15 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head15}}</p></th>
                  <td >{{$asset->ref_info15}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head16 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head16}}</p></th>
                  <td >{{$asset->ref_info16}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head17 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head17}}</p></th>
                  <td >{{$asset->ref_info17}} </td>
    
                </tr>
                @endif
                @if($asset->assettype->ref_info_head18 != NULL)
                <tr>
                  <th width="50%"><p>{{$asset->assettype->ref_info_head18}}</p></th>
                  <td >{{$asset->ref_info18}} </td>
    
                </tr>
                @endif
                <tr>
                  <th width="50%"><p>Link Underlying</p></th>
                  <td >{{$asset->link_underlying}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Amount</p></th>
                  <td >{{$asset->amount}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Value</p></th>
                  <td >{{$asset->value}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Cost</p></th>
                  <td >{{$asset->cost}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Referal Asset</p></th>
                  <td >{{$asset->ref_to_asset}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Valid from</p></th>
                  <td >{{$asset->valid_from}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Valid to</p></th>
                  <td >{{$asset->valid_to}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Link To More</p></th>
                  <td >{{$asset->link_to_more}} </td>
    
                </tr>
    
    
    
    
    
                <tr>
                  <th width="50%"><p>Last Modified</p></th>
                  <td >{{$asset->last_modified_date}} </td>
    
                </tr>
    
                <tr>
                  <th width="50%"><p>Note</p></th>
                  <td >{{$asset->note}} </td>
    
                </tr>
    
                @foreach($fileasset as $key => $a)
                <tr>
                  <th width="50%"><p>File {{++$key}}</p></th>
                  <?php
                              $filecatuser = \App\FileCat::where('id',$a->file_cat_id)->value('user_view');
                              $filecatmiddle = \App\FileCat::where('id',$a->file_cat_id)->value('middle_view');
                  ?>
                    <td >  @if($filecatuser == 1 || $filecatmiddle == 1)<a style="font-size:16px" href="{{ URL::to('SecurityBroke/showfile',[$a->id])}}" >{{$a->file_public_name}}</a>    
                    </td >
                    @else
                    @endif
    
                </tr>
                @endforeach

    
    
              <th style="background-color:#00325d;color:white;">
                Topic
              </th>
              <th style="background-color:#00325d;color:white;">
                Details
              </th>
    
    
    
            </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
        </div>
        <div class="col-sm-7">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
    </section>
    <!-- /.content -->
@endsection
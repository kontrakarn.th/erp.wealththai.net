@extends('layouts.app-template')
@section('content')

  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Wealththai Insurance
      </h1>

    </section>
    @yield('action-content')
    <!-- /.content -->
  </div>
@endsection


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('change', '.structureList', function () {
            //  console.log("hmm its change");

            var cat_id = $(this).val();
            //console.log(department_id);
            var div = $(this).parent();
            var op = " ";
            $.ajax({
                type: 'get',
                url: '{!!URL::to('findBlockbyStructure')!!}',
                data: {
                    'id': cat_id
                },

                success: function (data) {
                    op += '<option value="" >-กรุณาเลือกทีม-</option>';
                    for (var i = 0; i < data.length; i++) {
                        op += '<option value="' + data[i].id + '">' + data[i].name +
                            '</option>';
                    }
                    $('.blockList').html(" ");
                    $('.blockList').append(op);
                },
                error: function () {

                }
            });
        });
    });

    $(document).ready(function () {
        $(document).on('change', '.structureuser', function () {
            //  console.log("hmm its change");

            var cat_id = $(this).val();
            //console.log(department_id);
            var div = $(this).parent();
            var op = " ";
            $.ajax({
                type: 'get',
                url: '{!!URL::to('findUserbyStructure')!!}',
                data: {
                    'id': cat_id
                },

                success: function (data) {
                    op += '<option value="" >-กรุณาเลือกทีม-</option>';
                    for (var i = 0; i < data.length; i++) {
                        op += '<option value="' + data[i].id + '">' + data[i].firstname +' ' + data[i].lastname 
                            '</option>';
                    }
                    $('.userList').html(" ");
                    $('.userList').append(op);
                },
                error: function () {

                }
            });
        });
    });
</script>


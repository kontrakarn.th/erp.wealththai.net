@extends('system-mgmt.insurance.base')
@section('action-content')

    <!-- Main content -->
    <section class="content" >
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
        </div>

    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
          @if($flag == 1)
          <div class="col-sm-8"><a class="btn btn-default " href="/wealththaiinsurance/show/fixasset">ดูเฉพาะที่มีสิทธิ</a></div>
          @else
          <div class="col-sm-8"><a class="btn btn-default " href="/wealththaiinsurance/show/allfixasset">ดูที่มีสิทธิทั้งหมด</a></div>
          @endif
        
        <div class="col-sm-6"></div>
      </div>
      
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">

          <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th  width="10px" >No.</th>
                <th >La/Nla</th>
                <th  >Type</th>
                <th  >Name</th>
                <th  >Customer</th>
                <th >Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($fixedasset as $index => $ass)
                <tr role="row" class="odd">
                    <td>{{ ++$index }}</td>
                    <td>{{ $ass->assettype->la_nla}}</td>
                  <td>{{ $ass->assettype->la_nla_type }}</td>
                  <td>{{ $ass->name}}</td>
                  <td>{{ $ass->portfolio->person->name}} {{ $ass->portfolio->person->lname}}</td>
                  <td>
                  @if($flag == 1)
                  <a class="btn btn-info btn-margin" href="/wealththaiinsurance/show/detailallfixasset/{{$ass->id}}">Details</a>
                  @else
                  <a class="btn btn-info btn-margin" href="/wealththaiinsurance/show/detailfixasset/{{$ass->id}}">Details</a>
                  @endif
                   @php
                   $casesrelate = \App\Cases::where('referal_asset',$ass->id)->get();
                   @endphp
                  <a class="btn btn-info btn-margin" href="/SecurityBroke/per/{{$ass->portfolio->member_id}}">Member Infomation</a>
                  <button type="button" class="btn btn-warning btn-margin" data-toggle="modal" data-target="#showreralteasset{{$ass->id}}">Related Cases</button>
                  <div class="modal" id="showreralteasset{{$ass->id}}" >
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Related Cases</h4>
                          </div>
                          <div class="modal-body">
                            @foreach($casesrelate as $casre)
                            <p><a href="/wealththaiinsurance/cases/{{$casre->id}}/detail/show">{{$casre->name}}</a> <span style="color:black">Created Date:  {{$casre->case_created_date}}</span></p>
                            @endforeach
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    </td>
              </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr role="row">
                    <th  width="10px" >No.</th>
                    <th >La/Nla</th>
                    <th  >Type</th>
                    <th  >Name</th>
                    <th  >Customer</th>
                    <th >Action</th>
                  </tr>
            </tfoot>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
          <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to {{count($fixedasset)}} of {{count($fixedasset)}} entries</div>
        </div>
        <div class="col-sm-7">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            {{ $fixedasset->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
    </section>
    <!-- /.content -->
@endsection
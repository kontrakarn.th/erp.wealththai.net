
<style>
.form-group{
    float:left;
    width:25%;
}
@media screen and (max-width: 1500px) {
    .form-group {
        width: 100%;
    }
}
</style>
<div class="column">

    <div class="card">
        <div class="card-header" style="background-color:#D6FBFF">การอนุญาต</div>
        <div class="card-body">
            <div>
            <div class="column">

                <div class="form-group" style="padding:20px">
                    <div class="col-lg-10">
                        <table class="table" id="publicIdTable">
                            <thead>
                                <tr>
                                    <th>PublicID</th>
                                    <th><a href="javascript:;" style="font-size:20px;color:#00325d" id="add_public_id"><i  class="fa fa-plus"></i</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 0; @endphp
                                @if(count($publicIDauth) <= 0)
                                <tr>
                                    <input type="hidden" name="public_id[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control" id="filtertype" name="public_id_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($caseauthPublicID as $data)
                                            <option value="{{$data -> id}}">{{$data -> public_name}}</option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td></td>
                                </tr>
                                @else
                                @foreach($publicIDauth as $pid)
                                <tr>
                                    <input type="hidden" name="public_id[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="public_id_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($caseauthPublicID as $data)
                                            <option value="{{$data -> id}}" {{$data->id == $pid->id ? 'selected' : ''}}>{{$data -> public_name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><a href="javascript:;" style="font-size:20px;color:red" data-public-id=""><i  class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                @endif
                                
                                @php $i++; @endphp
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="form-group" style="padding:20px">
                    <div class="col-lg-10">
                        <table class="table" id="blockPartnerTable">
                            <thead>
                                <tr>
                                    <th>Block Partner</th>
                                    <th><a href="javascript:;" style="font-size:20px;color:#00325d" id="add_block_partner"><i  class="fa fa-plus"></i</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 0; @endphp
                                @if(count($blockPartnerauth) <= 0)
                                <tr>
                                    <input type="hidden" name="block_partner[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="block_partner_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($blockPartner as $data)
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td></td>
                                </tr>
                                @else
                                @foreach($blockPartnerauth as $pid)
                                <tr>
                                    <input type="hidden" name="block_partner[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="block_partner_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($blockPartner as $data)
                                            <option value="{{$data -> id}}" {{$data->id == $pid->id ? 'selected' : ''}}>{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><a href="javascript:;" style="font-size:20px;color:red" data-public-id=""><i  class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                @endif
                                @php $i++; @endphp
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="form-group" style="padding:20px">
                    <div class="col-lg-10">
                        <table class="table" id="blockUserTable">
                            <thead>
                                <tr>
                                    <th>Block User</th>
                                    <th><a href="javascript:;" style="font-size:20px;color:#00325d" id="add_block_user"><i  class="fa fa-plus"></i</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 0; @endphp
                                @if(count($blockUserauth) <= 0)
                                <tr>
                                    <input type="hidden" name="block_user[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="block_user_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($blockUser as $data)
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td></td>
                                </tr>
                                @else
                                @foreach($blockUserauth as $pid)
                                <tr>
                                    <input type="hidden" name="block_user[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="block_user_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($blockUser as $data)
                                            <option value="{{$data -> id}}" {{$data->id == $pid->id ? 'selected' : ''}}>{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><a href="javascript:;" style="font-size:20px;color:red" data-public-id=""><i  class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                @endif
                                @php $i++; @endphp
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="form-group" style="padding:20px">
                    <div class="col-lg-10">
                        <table class="table" id="guildMemberTable">
                            <thead>
                                <tr>
                                    <th>Guild Member</th>
                                    <th><a href="javascript:;" style="font-size:20px;color:#00325d" id="add_guild_member"><i  class="fa fa-plus"></i</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 0; @endphp
                                @if(count($guildMemberauth) <= 0)
                                <tr>
                                    <input type="hidden" name="guild_member[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="guild_member_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($guildMember as $data)
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td></td>
                                </tr>
                                @else
                                @foreach($guildMemberauth as $pid)
                                <tr>
                                    <input type="hidden" name="guild_member[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="guild_member_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($guildMember as $data)
                                            <option value="{{$data -> id}}" {{$data->id == $pid->id ? 'selected' : ''}}>{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><a href="javascript:;" style="font-size:20px;color:red" data-public-id=""><i  class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                @endif
                                @php $i++; @endphp
                            </tbody>
                        </table>

                    </div>
                </div>
                </div>

                <div class="column">
                <div class="form-group" style="padding:20px">
                    <div class="col-lg-10">
                        <table class="table" id="groupMemberTable">
                            <thead>
                                <tr>
                                    <th>Group Member</th>
                                    <th><a href="javascript:;" style="font-size:20px;color:#00325d" id="add_group_member"><i  class="fa fa-plus"></i</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 0; @endphp
                                @if(count($groupMemberauth) <= 0)
                                <tr>
                                    <input type="hidden" name="group_member[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="group_member_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($groupMember as $data)
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td></td>
                                </tr>
                                @else
                                @foreach($groupMemberauth as $pid)
                                <tr>
                                    <input type="hidden" name="group_member[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="group_member_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($groupMember as $data)
                                            <option value="{{$data -> id}}" {{$data->id == $pid->id ? 'selected' : ''}}>{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><a href="javascript:;" style="font-size:20px;color:red" data-public-id=""><i  class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                @endif
                                @php $i++; @endphp
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="form-group" style="padding:20px">
                    <div class="col-lg-10">
                        <table class="table" id="groupPidTable">
                            <thead>
                                <tr>
                                    <th>Group PID</th>
                                    <th><a href="javascript:;" style="font-size:20px;color:#00325d" id="add_group_pid"><i  class="fa fa-plus"></i</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 0; @endphp
                                @if(count($groupPidauth) <= 0)
                                <tr>
                                    <input type="hidden" name="group_pid[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="group_pid_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($groupPid as $data)
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td></td>
                                </tr>
                                @else
                                @foreach($groupPidauth as $pid)
                                <tr>
                                    <input type="hidden" name="group_pid[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="group_pid_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($groupPid as $data)
                                            <option value="{{$data -> id}}" {{$data->id == $pid->id ? 'selected' : ''}}>{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><a href="javascript:;" style="font-size:20px;color:red" data-public-id=""><i  class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                @endif
                                @php $i++; @endphp
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="form-group" style="padding:20px">
                    <div class="col-lg-10">
                        <table class="table" id="groupPartnerTable">
                            <thead>
                                <tr>
                                    <th>Group Partner</th>
                                    <th><a href="javascript:;" style="font-size:20px;color:#00325d" id="add_group_partner"><i  class="fa fa-plus"></i></a></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 0; @endphp
                                @if(count($groupPartnerauth) <= 0)
                                <tr>
                                    <input type="hidden" name="group_partner[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="group_partner_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($groupPartner as $data)
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>

                                    <td></td>
                                </tr>
                                @else
                                @foreach($groupPartnerauth as $pid)
                                <tr>
                                    <input type="hidden" name="group_partner[{{ $i }}]" value="">
                                    <td>
                                        <select style="width:190px" class="form-control filtertype" name="group_partner_index[]">
                                            <option value="">-เลือก-</option>
                                            @foreach($groupPartner as $data)
                                            <option value="{{$data -> id}}" {{$data->id == $pid->id ? 'selected' : ''}}>{{$data -> name}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td><a href="javascript:;" style="font-size:20px;color:red" data-public-id=""><i  class="fa fa-trash"></i></a></td>
                                </tr>
                                @endforeach
                                @endif
                                @php $i++; @endphp
                            </tbody>
                        </table>

                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>





<script>
$(document).ready(function() {
    $('#add_public_id').on('click', function() {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#publicIdTable tbody').append($('#template_public_id').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

    $('body').on('click', '[data-public-id]', function() {
        var public_id = $(this).attr('data-public-id');
        console.log(public_id)
        if (public_id == '') {
            $(this).parent().parent().remove();
            return true;
        }


    });
});

$(document).ready(function() {
    $('#add_block_partner').on('click', function() {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#blockPartnerTable tbody').append($('#template_block_partner').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

    $('body').on('click', '[data-block-partner]', function() {
        var block_partner = $(this).attr('data-block-partner');
        console.log(block_partner)
        if (block_partner == '') {
            $(this).parent().parent().remove();
            return true;
        }
    });
});

$(document).ready(function() {
    $('#add_block_user').on('click', function() {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#blockUserTable tbody').append($('#template_block_user').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

    $('body').on('click', '[data-block-user]', function() {
        var block_user = $(this).attr('data-block-user');
        console.log(block_user)
        if (block_user == '') {
            $(this).parent().parent().remove();
            return true;
        }
    });
});

$(document).ready(function() {
    $('#add_guild_member').on('click', function() {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#guildMemberTable tbody').append($('#template_guild_member').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

    $('body').on('click', '[data-guild-member]', function() {
        var guild_member = $(this).attr('data-guild-member');
        console.log(guild_member)
        if (guild_member == '') {
            $(this).parent().parent().remove();
            return true;
        }
    });
});


$(document).ready(function() {
    $('#add_group_member').on('click', function() {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#groupMemberTable tbody').append($('#template_group_member').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

    $('body').on('click', '[data-group-member]', function() {
        var group_member = $(this).attr('data-group-member');
        console.log(group_member)
        if (group_member == '') {
            $(this).parent().parent().remove();
            return true;
        }
    });
});

$(document).ready(function() {
    $('#add_group_pid').on('click', function() {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#groupPidTable tbody').append($('#template_group_pid').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

    $('body').on('click', '[data-group-pid]', function() {
        var group_pid = $(this).attr('data-group-pid');
        console.log(group_pid)
        if (group_pid == '') {
            $(this).parent().parent().remove();
            return true;
        }
    });
});


$(document).ready(function() {
    $('#add_group_partner').on('click', function() {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#groupPartnerTable tbody').append($('#template_group_partner').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

    $('body').on('click', '[data-group-partner]', function() {
        var group_partner = $(this).attr('data-group-partner');
        console.log(group_partner)
        if (group_partner == '') {
            $(this).parent().parent().remove();
            return true;
        }
    });
});


</script>
<script id="template_public_id" type="text/html">
<tr>
    <input type="hidden" name="public_id[@{{i}}]" value="new">
    <td>
        <select style="width:190px" class="form-control" id="filtertype" name="public_id_index[]" required>
            <option value="">-เลือก-</option>
            @foreach($caseauthPublicID as $data)
            <option value="{{$data -> id}}">{{$data -> public_name}}</option>
            @endforeach
        </select>
    </td>

    <td><a href="javascript:;" style="font-size:20px;color:red" data-public-id=""><i  class="fa fa-trash"></i></a></td>
</tr>
</script>

<script id="template_block_partner" type="text/html">
<tr>
    <input type="hidden" name="block_partner[@{{i}}]" value="new">
    <td>
        <select style="width:190px" class="form-control filtertype" name="block_partner_index[]" required>
            <option value="">-เลือก-</option>
            @foreach($blockPartner as $data)
            <option value="{{$data -> id}}">{{$data -> name}}</option>
            @endforeach
        </select>
    </td>

    <td><a href="javascript:;" style="font-size:20px;color:red" data-block-partner=""><i  class="fa fa-trash"></i></a></td>
</tr>
</script>

<script id="template_block_user" type="text/html">
<tr>
    <input type="hidden" name="block_user[@{{i}}]" value="new">
    <td>
        <select style="width:190px" class="form-control filtertype" name="block_user_index[]" required>
            <option value="">-เลือก-</option>
            @foreach($blockUser as $data)
            <option value="{{$data -> id}}">{{$data -> name}}</option>
            @endforeach
        </select>
    </td>

    <td><a href="javascript:;" style="font-size:20px;color:red" data-block-user=""><i  class="fa fa-trash"></i></a></td>
</tr>
</script>

<script id="template_guild_member" type="text/html">
<tr>
    <input type="hidden" name="guild_member[@{{i}}]" value="new">
    <td>
        <select style="width:190px" class="form-control filtertype" name="guild_member_index[]" required>
            <option value="">-เลือก-</option>
            @foreach($guildMember as $data)
            <option value="{{$data -> id}}">{{$data -> name}}</option>
            @endforeach
        </select>
    </td>

    <td><a href="javascript:;" style="font-size:20px;color:red" data-guild-member=""><i  class="fa fa-trash"></i></a></td>
</tr>
</script>

<script id="template_group_member" type="text/html">
<tr>
    <input type="hidden" name="group_member[@{{i}}]" value="new">
    <td>
        <select style="width:190px" class="form-control filtertype" name="group_member_index[]" required>
            <option value="">-เลือก-</option>
            @foreach($groupMember as $data)
            <option value="{{$data -> id}}">{{$data -> name}}</option>
            @endforeach
        </select>
    </td>

    <td><a href="javascript:;" style="font-size:20px;color:red" data-group-member=""><i  class="fa fa-trash"></i></a></td>
</tr>
</script>

<script id="template_group_pid" type="text/html">
<tr>
    <input type="hidden" name="group_pid[@{{i}}]" value="new">
    <td>
        <select style="width:190px" class="form-control " name="group_pid_index[]" required>
            <option value="">-เลือก-</option>
            @foreach($groupPid as $data)
            <option value="{{$data -> id}}">{{$data -> name}}</option>
            @endforeach
        </select>
    </td>

    <td><a href="javascript:;" style="font-size:20px;color:red" data-group-pid=""><i  class="fa fa-trash"></i></a></td>
</tr>
</script>

<script id="template_group_partner" type="text/html">
<tr>
    <input type="hidden" name="group_partner[@{{i}}]" value="new">
    <td>
        <select style="width:190px" class="form-control filtertype" name="group_partner_index[]" required>
            <option value="">-เลือก-</option>
            @foreach($groupPartner as $data)
            <option value="{{$data -> id}}">{{$data -> name}}</option>
            @endforeach
        </select>
    </td>

    <td><a href="javascript:;" style="font-size:20px;color:red" data-group-partner=""><i  class="fa fa-trash"></i></a></td>
</tr>
</script>


    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

          $("#filtertype").select2({
                placeholder: "Select",
                allowClear: true,
                
            });
 
          $("#filtertype2").select2({
                placeholder: "Select",
                allowClear: true,
                
            });
    </script>
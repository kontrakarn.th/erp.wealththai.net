<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
          <div class="tab-pane active" role="tabpanel" id="step1">
          <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery" >
          <p style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;กรุณากรอกช่องที่มีเครื่องหมาย * ให้ครบ</p>
                        <div class="column">
                            <div class="card">
                                <div class="card-header" style="background-color:#D6FBFF">Step 1 ผู้แจ้งงานและผู้ประสานงาน</div>

                                    <div class="card-body">
                                    <div class="column2">
                                    <table >
                                    <tr>

                                    <th style="width:200px">ประเภทของใบงาน <b style="color:red">*</b></th>
                                    <td><div style="width:190px">

                                    <select style="width:190px" class="form-control caseCat" id="casecategory" name="casecategory" required>
                                    <option value="">โปรดเลือก</option>
                                    @foreach($caseCategory as $data)
                                        @if(empty($case))
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                        @else
                                        <option value="{{$data->id}}"{{$data->id == $case->CaseType->case_cat_id ? 'selected' : ''}}>{{$data -> name}}</option>
                                        @endif
                                    @endforeach
                                    </select>
                                    </div></td>
                                    </tr>
                                    <tr><th></th><td>&nbsp;</td></tr>
                                    <tr>
                                    <th >ชนิดของใบงาน <b style="color:red">*</b></th>
                                    <td ><div style="width:190px">
                                    <select style="width:190px" class="form-control caseType findcasetype finddefaultuser finddefaultpartner finddefaultcoor" id="casetype" name="casetype" required>
                                    <option value="">โปรดเลือก</option>
                                    @if(empty($case))
                                    @else
                                        @foreach($caseType as $data)
                                            <option value="{{$data->id}}" {{$data->id == $case->type_id ? 'selected' : ''}}>{{$data->name}}</option>
                                        @endforeach
                                    @endif
                                    </select>
                                    </div></td>
                                    </tr>
                                    <tr><th></th><td>&nbsp;</td></tr>
                                    <tr>
                                    <th >ชนิดย่อย ของใบงาน <b style="color:red">*</b></th>
                                    <td ><div style="width:190px">
                                    <select style="width:190px" class="form-control casesubtype" id="casesubtype" name="casesubtype" required>
                                    <option value="">โปรดเลือก</option>
                                    @if(empty($case))
                                    @else
                                    @foreach($caseSubType as $data)
                                            <option value="{{$data->id}}" {{$data->id == $case->sub_type_id ? 'selected' : ''}}>{{$data->name}}</option>
                                        @endforeach
                                    @endif
                                      </select>
                                    </div></td>
                                    </tr>
                                    <tr><th></th><td>&nbsp;</td></tr>
                                    <tr>
                                    <th >ชื่อใบงาน <b style="color:red">*</b></th>
                                    <td ><div style="width:190px">
                                    @if(empty($case))
                                    <input style="width:190px" id="name" name="name" class="form-control" required />
                                    @else
                                    <input style="width:190px" id="name" name="name" class="form-control" value="{{$case->name}}" required />
                                    @endif
                                    </div></td>
                                    </tr>


                                    </table>
                                    </div>
                                    <div class="column2">
                                    <table >

                                    <tr>
                                    <th style="width:200px">ผู้แจ้งงาน <b style="color:red">*</b></th>
                                    <td ><div >

                                      <select style="width:190px" class="selectwidthauto form-control defaultuser" id="user" name="user" required>
                                      @if(empty($case))
                                      <option value="" >-เลือกชนิดของใบงานก่อน-</option>
                                      @foreach($blockUser as $data)
                                      <option>{{$data->name}}</option>
                                      @endforeach
                                      @else
                                      @foreach($blockUser as $data)
                                      <option value="{{$data->id}}" {{$data->id == $case->service_user_block_id ? 'selected' : ''}}>{{$data->name}}</option>
                                      @endforeach
                                      @endif
                                      </select>

                                    </div></td>
                                    </tr>
                                    <tr><th></th><td>&nbsp;</td></tr>
                                    <tr>
                                    <th style="width:200px">ผู้ประสานงาน <b style="color:red">*</b></th>
                                    <td ><div >
                                      <select style="width:190px" class="selectwidthauto form-control defaultcoor" id="coor" name="coor" >
                                      @if(empty($case))
                                      <option value="" >-เลือกชนิดของใบงานก่อน-</option>
                                      @else
                                      @foreach($coordinatorLists as $data)
                                      <option value="{{$data->id}}" {{$data->id == $case->coordinate_user_block_id ? 'selected' : ''}}>{{$data->firstname}} {{$data->lastname}}</option>
                                      @endforeach
                                      @endif
                                      </select>

                                    </div></td>
                                    </tr>
                                    <tr><th></th><td>&nbsp;</td></tr>
                                    <tr>
                                    <th style="width:200px">ผู้ให้คำปรึกษา <b style="color:red">*</b></th>
                                    <td ><div >
                                      <select style="width:190px" class="selectwidthauto form-control defaultpartner" id="partner" name="partner">
                                      @if(empty($case))
                                      <option value="" >-เลือกชนิดของใบงานก่อน-</option>
                                      @else
                                      @foreach($blockPartner as $data)
                                      <option value="{{$data->id}}" {{$data->id == $case->consult_partner_block_id ? 'selected' : ''}}>{{$data->name}}</option>
                                      @endforeach
                                      @endif
                                      </select>
                                    </div></td>
                                    </tr>
                                    <tr><th></th><td>&nbsp;</td></tr>
                                    <tr>
                                    <th style="width:200px">เส้นทางรับงาน <b style="color:red">*</b></th>
                                    <td ><div >
                                      <select  style="width:190px" class="selectwidthauto form-control" id="channel" name="channel" required>
                                        <option value="" >-Select-</option>
                                        @foreach($caseChannel as $data)
                                        @if(empty($case))
                                        <option value="{{$data -> id}}">{{$data -> name}}</option>
                                        @else
                                        <option value="{{$data->id}}"{{$data->id == $case->case_channel ? 'selected' : ''}}>{{$data -> name}}</option>
                                        @endif
                                        @endforeach
                                      </select>

                                    </div></td>
                                    </tr>

                                    </table>
                                    </div>
                                    </div>

                                    </div>
                                    </div>

                                    @include('system-mgmt.insurance.create.stepOne.createauth')
                                    <a  href="{{Request::url()}}" class="btn btn-default btn-margin" id="reset">Reset <i class="icon-reload-alt position-right"></i></a>
                                    @if(empty($case))
                                    <input type="hidden" name="caseid" value="new">
                                    <button type="button" id="btn-save-draft" class="btn btn-primary btn-margin" onclick="save()">Next Step <i class="icon-arrow-right14 position-right"></i></button>
                                    @else
                                    <input type="hidden" name="caseid" value="{{$case->id}}">
                                    <button type="button" id="btn-save-draft" class="btn  btn-warning btn-margin" onclick="saveExit()">Save and Exit <i class="icon-arrow-right14 position-right"></i></button>
                                    <button type="button" id="btn-save-draft" class="btn btn-primary btn-margin" onclick="save()">Next Step <i class="icon-arrow-right14 position-right"></i></button>
                                    @endif
                                    <input type="hidden" name="step" value="1">
                                    </form>

          </div>





    <script>
$(document).ready(function () {

    $('#add_menu').on('click', function () {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#menuTable tbody').append($('#template_menu').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

});
</script>
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

          $(".name").select2({
                placeholder: "Select",
                //allowClear: true
            });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('change','.caseCat',function(){
            //  console.log("hmm its change");

                var cat_id=$(this).val();
                //console.log(department_id);
                var div=$(this).parent();
                var op=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findCasetypeBycaseCat')!!}',
                    data:{'id':cat_id},

                    success:function(data){
                      op+='<option value="" >-เลือกชนิดของใบงาน-</option>';
                      for(var i=0; i<data.length;i++){
                        op+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                      }
                      $('.caseType').html(" ");
                      $('.caseType').append(op);
                    },
                    error:function(){

                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.findcasetype',function(){
            //  console.log("hmm its change");

                var type_id=$(this).val();
                //console.log(department_id);
                var div=$(this).parent();
                var casesubtype=" ";
                var defaultuser=" ";

                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findCaseType')!!}',
                    data:{'id':type_id},

                    success:function(data){
                        casesubtype+='<option value="" >-เลือกชนิดย่อย ของใบงาน-</option>';

                      for(var i=0; i<data.length;i++){
                        casesubtype+='<option value="'+data[i].case_sub_type_id+'">'+data[i].case_sub_type_name+'</option>';

                      }
                      $('.casesubtype').html(" ");

                      $('.casesubtype').append(casesubtype);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>

<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultuser',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var casesubtype=" ";
                var defaultuser=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultUserBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultuser+='<option>-เลือก-</option>';
                        for(var i=0; i<data.default_block.length;i++){
                        defaultuser+='<option value="'+data.default_block[i].id+'" selected>'+data.default_block[i].name+'</option>';
                        }
                      for(var i=0; i<data.another_block.length;i++){
                        defaultuser+='<option value="'+data.another_block[i].id+'">'+data.another_block[i].name+'</option>';
                      }
                      $('.defaultuser').html(" ");
                      $('.defaultuser').append(defaultuser);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>

<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultpartner',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var defaultpartner=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultPartnerBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultpartner+='<option>-เลือก-</option>';
                        for(var i=0; i<data.default_block.length;i++){
                            defaultpartner+='<option value="'+data.default_block[i].id+'" selected>'+data.default_block[i].name+'</option>';
                        }
                      for(var i=0; i<data.another_block.length;i++){
                        defaultpartner+='<option value="'+data.another_block[i].id+'">'+data.another_block[i].name+'</option>';
                      }
                      $('.defaultpartner').html(" ");
                      $('.defaultpartner').append(defaultpartner);
                    },
                    error:function(){
                    }
                });
            });
        });
    </script>
<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultcoor',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var defaultcoor=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultCoorBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultcoor+='<option value="">-เลือกผู้ประสานงาน-</option>';

                      for(var i=0; i<data.length;i++){
                        defaultcoor+='<option value="'+data[i].id+'">'+data[i].firstname+' '+data[i].lastname+'</option>';

                      }
                      $('.defaultcoor').html(" ");
                      $('.defaultcoor').append(defaultcoor);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>


<script>

function save() {

            var msg = '';

           /* if($('#casecategory').val().trim() == '')
            msg = 'กรุณาเลือก ประเภทของใบงาน';

            else if($('#casetype').val().trim() == '')
            msg = 'กรุณาเลือก ชนิดของใบงาน';

            else if($('#casesubtype').val().trim() == '')
            msg = 'กรุณาเลือก ชนิดย่อยของใบงาน';

            else if($('#name').val().trim() == '')
            msg = 'กรุณากรอก ชื่อใบงาน';

            else if($('#user').val().trim() == '')
            msg = 'กรุณาเลือก ผู้แจ้งงาน';

            else if($('#channel').val().trim() == '')
            msg = 'กรุณาเลือก เส้นทางรับงาน';*/

            if(msg != ''){
                swal({
                    title: msg,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
            }else{
                swal({
                       // title: "กำลังโหลด",
                       // showLoaderOnConfirm: false,
                      //  type: "loading"
                        width: '80px',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        icon: 'success',
                        html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                    });
                saveCreate();
              //  swal.close();

            }
            return false;
        }

        function saveExit() {

        var msg = '';

        /* if($('#casecategory').val().trim() == '')
        msg = 'กรุณาเลือก ประเภทของใบงาน';

        else if($('#casetype').val().trim() == '')
        msg = 'กรุณาเลือก ชนิดของใบงาน';

        else if($('#casesubtype').val().trim() == '')
        msg = 'กรุณาเลือก ชนิดย่อยของใบงาน';

        else if($('#name').val().trim() == '')
        msg = 'กรุณากรอก ชื่อใบงาน';

        else if($('#user').val().trim() == '')
        msg = 'กรุณาเลือก ผู้แจ้งงาน';

        else if($('#channel').val().trim() == '')
        msg = 'กรุณาเลือก เส้นทางรับงาน';*/

        if(msg != ''){
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        }else{
            swal({
                // title: "กำลังโหลด",
                // showLoaderOnConfirm: false,
                //  type: "loading"
                    width: '80px',
                    allowOutsideClick: false,
                    showCancelButton: false,
                    showConfirmButton: false,
                    icon: 'success',
                    html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                });
            saveAndExit();
        //  swal.close();

        }
        return false;
        }
        function saveCreate() {
        var formData = new FormData($('#form-data')[0]);
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
            type: 'POST',
            url: "{{ route('cases.store') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                        allowOutsideClick: false,
                    }).then(function () {
                        window.location.href = "/wealththaiinsurance/"+data.case_id+"/case/steptwo";
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        allowOutsideClick: false,
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
        }

        function saveAndExit() {
        var formData = new FormData($('#form-data')[0]);
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
        $.ajax({
            type: 'POST',
            url: "{{ route('cases.store') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                        allowOutsideClick: false,
                    }).then(function () {
                        window.location.href = "/wealththaiinsurance/cases/"+data.case_id+"/detail/show";
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        allowOutsideClick: false,
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
        }
</script>
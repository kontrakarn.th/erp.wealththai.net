<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
          <div class="tab-pane active" role="tabpanel" id="step1">
          <p style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;กรุณากรอกช่องที่มีเครื่องหมาย * ให้ครบ</p>
                        <div class="column">
                            <div class="card">
                                <div class="card-header" style="background-color:#D6FBFF">Step 3 รายละเอียดงาน</div>

                                    <div class="card-body">
                                    <div class="column2">
                                        <form id="form-data-new-asset" name="form-data-new-asset" method="post" class="form-horizontal form-validate-jquery" >
                                    <table >


                                    <tr>

                                        <th >เลือก Portfolio  <b style="color:red">*</b></th>
                                        <td>
                                            <div style=" ">
        
                                                <select style=" " class="form-control" id="newassetPortfolio" name="newassetPortfolio" required>

                                                    <option value="">โปรดเลือก</option>
                                                    @foreach($portfolioNewasset as $data)
                                                    @if(empty($case))
                                                        <option value="{{$data -> id}}">{{$data -> type}}</option>
                                                    @else
                                                    <option value="{{$data->id}}"{{$data->id == $memberSelectedAssetPort ? 'selected' : ''}}>{{$data -> type}}</option>
                                                    @endif
                                                @endforeach
                                                </select>&nbsp;
                                                <span  onclick="openCreatePortfolioForm()" style="display:inline;"><a>เพิ่ม</a></span>
                                            </div>
                                        </td>
                                    </tr>

                                    </table>
                                    <a href="{{'/wealththaiinsurance/'.$case->id.'/case/steptwo'}}" class="btn btn-default " id="reset">Back <i
                                        class="icon-reload-alt position-right"></i></a>
                                <button type="button" id="btn-save-draft" class="btn btn-primary" onclick="saveassetPortfolio()">Submit <i class="icon-arrow-right14 position-right"></i></button>
                                    <input type="hidden" name="step" value="3">
                                <input type="hidden" name="caseid"value="{{$case->id}}">


                                </form>
                                    </div>
                                    
                                    </div>

                                    </div>
                                    </div>
                                    
                                    <div id="formcreateport" style="display:none">
                                        @include('system-mgmt.insurance.create.stepThree.createportfolio')
                                    </div>

                                   
          </div>

            @if(empty($memberSelectedAssetPort))
            @else
            @include('system-mgmt.insurance.create.stepThree.createrequirement')
            @endif



    <script>

function openCreatePortfolioForm() {
  var form = document.getElementById("formcreateport");
  if (form.style.display == "none") {
    form.style.display = "block";
  } else {
    form.style.display = "none";
  }
}

</script>

   


    <script>
$(document).ready(function () {

    $('#add_menu').on('click', function () {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#menuTable tbody').append($('#template_menu').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

});
</script>
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

          $(".name").select2({
                placeholder: "Select",
                //allowClear: true
            });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('change','.caseCat',function(){
            //  console.log("hmm its change");

                var cat_id=$(this).val();
                //console.log(department_id);
                var div=$(this).parent();
                var op=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findCasetypeBycaseCat')!!}',
                    data:{'id':cat_id},

                    success:function(data){
                      op+='<option value="" >-เลือกชนิดของใบงาน-</option>';
                      for(var i=0; i<data.length;i++){
                        op+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                      }
                      $('.caseType').html(" ");
                      $('.caseType').append(op);
                    },
                    error:function(){

                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.findcasetype',function(){
            //  console.log("hmm its change");

                var type_id=$(this).val();
                //console.log(department_id);
                var div=$(this).parent();
                var casesubtype=" ";
                var defaultuser=" ";

                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findCaseType')!!}',
                    data:{'id':type_id},

                    success:function(data){
                        casesubtype+='<option value="" >-เลือกชนิดย่อย ของใบงาน-</option>';

                      for(var i=0; i<data.length;i++){
                        casesubtype+='<option value="'+data[i].case_sub_type_id+'">'+data[i].case_sub_type_name+'</option>';

                      }
                      $('.casesubtype').html(" ");

                      $('.casesubtype').append(casesubtype);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>

<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultuser',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var casesubtype=" ";
                var defaultuser=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultUserBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultuser+='<option value="" >-เลือกผู้แจ้งงาน-</option>';

                      for(var i=0; i<data.length;i++){
                        defaultuser+='<option value="'+data[i].id+'">'+data[i].name+'</option>';

                      }
                      $('.defaultuser').html(" ");
                      $('.defaultuser').append(defaultuser);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>

<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultpartner',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var defaultpartner=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultPartnerBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultpartner+='<option value="" >-เลือกผู้ให้คำปรึกษา-</option>';

                      for(var i=0; i<data.length;i++){
                        defaultpartner+='<option value="'+data[i].id+'">'+data[i].name+'</option>';

                      }
                      $('.defaultpartner').html(" ");
                      $('.defaultpartner').append(defaultpartner);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>
<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultcoor',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var defaultcoor=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultCoorBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultcoor+='<option value="" >-เลือกผู้ประสานงาน-</option>';

                      for(var i=0; i<data.length;i++){
                        defaultcoor+='<option value="'+data[i].id+'">'+data[i].firstname+' '+data[i].lastname+'</option>';

                      }
                      $('.defaultcoor').html(" ");
                      $('.defaultcoor').append(defaultcoor);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>


<script>

function saveassetPortfolio() {
            var msg = '';

           


            if($('#newassetPortfolio').val().trim() == '')
            msg = 'กรุณาเลือก Portfolio';

            
            if(msg != ''){
                swal({
                    title: msg,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
            }else{
                swal({
                       // title: "กำลังโหลด",
                       // showLoaderOnConfirm: false,
                      //  type: "loading"
                        width: '80px',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        icon: 'success',
                        html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                    });
                saveCreateassetPortfolio();
                //swal.close();

            }
            return false;
        }

        function saveCreateassetPortfolio() {
        var formData = new FormData($('#form-data-new-asset')[0]);
        $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
        $.ajax({
            type: 'POST',
            url: "{{ route('cases.store') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                        allowOutsideClick: false,
                    }).then(function () {
                        window.location.href = "/wealththaiinsurance/"+data.case_id+"/case/stepthree";
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        allowOutsideClick: false,
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
        }
</script>
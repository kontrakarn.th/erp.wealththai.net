<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<div class="tab-pane active" role="tabpanel" id="step1">
    <form id="form-data-case-asset" name="form-data-case-asset" method="post"
        class="form-horizontal form-validate-jquery">

        <div class="column">
            <div class="card">
                <div class="card-header" style="background-color:#D6FBFF">ข้อมูลทั่วไป</div>

                <div class="card-body">
                    <div class="column2">
                        <table>
                            @for($i=1;$i<=10;$i++) @php $requireName="requirename_var" .$i;$requireValue="require_value" .$i; @endphp 
                                @if($i == 1)
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">
                                        <textarea style="width:190px" class="form-control" id="{{$requireValue}}" name="{{$requireValue}}" >{{$case->$requireValue}}</textarea>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td>&nbsp;</td>
                                    </tr>
                                @elseif($i == 2)
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">
                                            <div class="input-group date">
                                                <input class="form-control" id="{{$requireValue}}" name="{{$requireValue}}" value="{{$case->$requireValue}}" />
                                                <div class="input-group-addon">บาท </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @elseif($i == 3)
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">

                                            <label class="checkbox-inline"> <input class="subject-list memtype" 
                                                    type="checkbox"   id="{{$requireValue}}" name="{{$requireValue}}" {{  $case->$requireValue =="ไม่ทำ" ? 'checked' : '' }}  value="ไม่ทำ" onclick="nodoAct()"/>
                                                &nbsp; &nbsp; &nbsp; &nbsp; ไม่ทำ </label>

                                            <label class="checkbox-inline"> <input class="subject-list memtype"
                                                   onclick="doAct()" type="checkbox" {{  $case->$requireValue =="ทำ" ? 'checked' : '' }} id="{{$requireValue}}" name="{{$requireValue}}" value="ทำ" />
                                                &nbsp; &nbsp; &nbsp; ทำ </label> 
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @if($case->$requireValue == "ทำ")
                                <tr id="doact" style="display:">
                                @else
                                <tr id="doact" style="display:none">
                                @endif
                                    <th style="width:220px">{{$case->CaseType->requirename_var7}}</th>
                                    <td>
                                        <div style="width: 190px">
                                            <div style="display: inline-block">
                                                <select  id="require_value7Day" name="require_value7Day">
                                                    <option value=""> วัน </option>
                                                    <option value="01" {{  $require7Day =="01" ? 'selected' : '' }}> 01 </option>
                                                    <option value="02" {{  $require7Day =="02" ? 'selected' : '' }}> 02 </option>
                                                    <option value="03" {{  $require7Day =="03" ? 'selected' : '' }}> 03 </option>
                                                    <option value="04" {{  $require7Day =="04" ? 'selected' : '' }}> 04 </option>
                                                    <option value="05" {{  $require7Day =="05" ? 'selected' : '' }}> 05 </option>
                                                    <option value="06" {{  $require7Day =="06" ? 'selected' : '' }}> 06 </option>
                                                    <option value="07" {{  $require7Day =="07" ? 'selected' : '' }}> 07 </option>
                                                    <option value="08" {{  $require7Day =="08" ? 'selected' : '' }}> 08 </option>
                                                    <option value="09" {{  $require7Day =="09" ? 'selected' : '' }}> 09 </option>
                                                    @for($i2=10;$i2<=31;$i2++)
                                                    <option value="{{$i2}}" {{  $require7Day ==$i2 ? 'selected' : '' }}> {{$i2}} </option>
                                                    @endfor
                                                </select>
            
                                            </div>&nbsp;
                                            <div style="display: inline-block">
            
                                                <select  id="require_value7Month" name="require_value7Month">
                                                    <option value=""> เดือน </option>
                                                    <option value="01" {{  $require7Month =="01" ? 'selected' : '' }}> ม.ค. </option>
                                                    <option value="02" {{  $require7Month =="02" ? 'selected' : '' }}> ก.พ. </option>
                                                    <option value="03" {{  $require7Month =="03" ? 'selected' : '' }}> มี.ค. </option>
                                                    <option value="04" {{  $require7Month =="04" ? 'selected' : '' }}> เม.ย. </option>
                                                    <option value="05" {{  $require7Month =="05" ? 'selected' : '' }}> พ.ค. </option>
                                                    <option value="06" {{  $require7Month =="06" ? 'selected' : '' }}> มิ.ย. </option>
                                                    <option value="07" {{  $require7Month =="07" ? 'selected' : '' }}> ก.ค. </option>
                                                    <option value="08" {{  $require7Month =="08" ? 'selected' : '' }}> ส.ค. </option>
                                                    <option value="09" {{  $require7Month =="09" ? 'selected' : '' }}> ก.ย. </option>
                                                    <option value="10" {{  $require7Month =="10" ? 'selected' : '' }}> ต.ค. </option>
                                                    <option value="11" {{  $require7Month =="11" ? 'selected' : '' }}> พ.ย. </option>
                                                    <option value="12" {{  $require7Month =="12" ? 'selected' : '' }}> ธ.ค. </option>
            
                                                </select>
                                            </div>&nbsp;
                                            <div style="display: inline-block">
                                                @php
                                                    $currentYear = date("Y");
                                                @endphp
                                                <select  id="require_value7Year" name="require_value7Year">
                                                    <option value=""> ปี ค.ศ </option>
                                                    @for($i2=$currentYear+50;$i2>=1900;$i2--)
                                                    <option value="{{$i2}}" {{  $require7Year ==$i2 ? 'selected' : '' }}>{{$i2}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @if($case->$requireValue == "ทำ")
                                <tr id="doact" style="display:">
                                @else
                                <tr id="doactspace" style="display:none">
                                @endif
                                <th></th>
                                <td>&nbsp;</td>
                                </tr>
                                @elseif($i== 4)
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">
                                            <select style="width:190px" onchange="doInsurance()" class="form-control" id="{{$requireValue}}" name="{{$requireValue}}">
                                                <option value="ไม่ทำ" {{  $case->$requireValue =="ไม่ทำ" ? 'selected' : '' }}>ไม่ทำ</option>
                                                <option value="ชั้น1"  {{  $case->$requireValue =="ชั้น1" ? 'selected' : '' }}>ชั้น1</option>
                                                <option value="ชั้น2+" {{  $case->$requireValue =="ชั้น2+" ? 'selected' : '' }}>ชั้น2+</option>
                                                <option value="ชั้น2"  {{  $case->$requireValue =="ชั้น2" ? 'selected' : '' }}>ชั้น2</option>
                                                <option value="ชั้น3+" {{  $case->$requireValue =="ชั้น3+" ? 'selected' : '' }}>ชั้น3+</option>
                                                <option value="ชั้น3"  {{  $case->$requireValue =="ชั้น3" ? 'selected' : '' }}>ชั้น3</option>
                                                <option value="อื่นๆ"  {{  $case->$requireValue =="อื่นๆ" ? 'selected' : '' }}>อื่นๆ</option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @elseif($i== 5)
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">
                                            <select style="width:190px" class="form-control" id="{{$requireValue}}" name="{{$requireValue}}">
                                                <option value="ไม่ระบุ" {{  $case->$requireValue =="ไม่ระบุ" ? 'selected' : '' }}>ไม่ระบุ</option>
                                                <option value="อู่" {{  $case->$requireValue =="อู่" ? 'selected' : '' }}>อู่</option>
                                                <option value="ห้าง"  {{  $case->$requireValue =="ห้าง" ? 'selected' : '' }}>ห้าง</option>
                                                
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @if($case->require_value4 != "ไม่ทำ" && !empty($case->require_value4))
                                <tr id="doinsurance" style="display:">
                                @else
                                <tr id="doinsurance" style="display:none">
                                @endif
                                
                                    <th style="width:220px">{{$case->CaseType->requirename_var8}}</th>
                                    <td>
                                        <div style="width: 190px">
                                            <div style="display: inline-block">
                                                <select  id="require_value8Day" name="require_value8Day">
                                                    <option value=""> วัน </option>
                                                    <option value="01" {{  $require8Day =="01" ? 'selected' : '' }}> 01 </option>
                                                    <option value="02" {{  $require8Day =="02" ? 'selected' : '' }}> 02 </option>
                                                    <option value="03" {{  $require8Day =="03" ? 'selected' : '' }}> 03 </option>
                                                    <option value="04" {{  $require8Day =="04" ? 'selected' : '' }}> 04 </option>
                                                    <option value="05" {{  $require8Day =="05" ? 'selected' : '' }}> 05 </option>
                                                    <option value="06" {{  $require8Day =="06" ? 'selected' : '' }}> 06 </option>
                                                    <option value="07" {{  $require8Day =="07" ? 'selected' : '' }}> 07 </option>
                                                    <option value="08" {{  $require8Day =="08" ? 'selected' : '' }}> 08 </option>
                                                    <option value="09" {{  $require8Day =="09" ? 'selected' : '' }}> 09 </option>
                                                    @for($i2=10;$i2<=31;$i2++)
                                                    <option value="{{$i2}}" {{  $require8Day ==$i2 ? 'selected' : '' }}> {{$i2}} </option>
                                                    @endfor
                                                </select>
            
                                            </div>&nbsp;
                                            <div style="display: inline-block">
            
                                                <select  id="require_value8Month" name="require_value8Month">
                                                    <option value=""> เดือน </option>
                                                    <option value="01" {{  $require8Month =="01" ? 'selected' : '' }}> ม.ค. </option>
                                                    <option value="02" {{  $require8Month =="02" ? 'selected' : '' }}> ก.พ. </option>
                                                    <option value="03" {{  $require8Month =="03" ? 'selected' : '' }}> มี.ค. </option>
                                                    <option value="04" {{  $require8Month =="04" ? 'selected' : '' }}> เม.ย. </option>
                                                    <option value="05" {{  $require8Month =="05" ? 'selected' : '' }}> พ.ค. </option>
                                                    <option value="06" {{  $require8Month =="06" ? 'selected' : '' }}> มิ.ย. </option>
                                                    <option value="07" {{  $require8Month =="07" ? 'selected' : '' }}> ก.ค. </option>
                                                    <option value="08" {{  $require8Month =="08" ? 'selected' : '' }}> ส.ค. </option>
                                                    <option value="09" {{  $require8Month =="09" ? 'selected' : '' }}> ก.ย. </option>
                                                    <option value="10" {{  $require8Month =="10" ? 'selected' : '' }}> ต.ค. </option>
                                                    <option value="11" {{  $require8Month =="11" ? 'selected' : '' }}> พ.ย. </option>
                                                    <option value="12" {{  $require8Month =="12" ? 'selected' : '' }}> ธ.ค. </option>
            
                                                </select>
                                            </div>&nbsp;
                                            <div style="display: inline-block">
                                                @php
                                                    $currentYear = date("Y");
                                                @endphp
                                                <select  id="require_value8Year" name="require_value8Year">
                                                    <option value=""> ปี ค.ศ </option>
                                                    @for($i2=$currentYear+50;$i2>=1900;$i2--)
                                                    <option value="{{$i2}}" {{  $require8Year ==$i2 ? 'selected' : '' }}>{{$i2}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @if($case->require_value4 != "ไม่ทำ" && !empty($case->require_value4))
                                <tr id="doinsurancespace" style="display:">
                                @else
                                <tr id="doinsurancespace" style="display:none">
                                @endif
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @elseif($i == 6)
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">

                                            <label class="checkbox-inline"> <input class="subject-list2 memtype"
                                                    type="checkbox" {{  $case->$requireValue =="ไม่ทำ" ? 'checked' : '' }} value="ไม่ทำ" onclick="nodoTax()" id="{{$requireValue}}" name="{{$requireValue}}"/>
                                                &nbsp; &nbsp; &nbsp; &nbsp; ไม่ทำ </label>

                                            <label class="checkbox-inline"> <input class="subject-list2 memtype"
                                                   onclick="doTax()" type="checkbox" {{  $case->$requireValue =="ทำ" ? 'checked' : '' }} value="ทำ" id="{{$requireValue}}" name="{{$requireValue}}" />
                                                &nbsp; &nbsp; &nbsp; ทำ </label> 
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @if($case->$requireValue =="ทำ")
                                <tr id="dotax" style="display:">
                                @else
                                <tr id="dotax" style="display:none">
                                @endif
                                    <th style="width:220px">{{$case->CaseType->requirename_var9}}</th>
                                    <td>
                                        <div style="width: 190px">
                                            <div style="display: inline-block">
                                                <select  id="require_value9Day" name="require_value9Day">
                                                    <option value=""> วัน </option>
                                                    <option value="01" {{  $require9Day =="01" ? 'selected' : '' }}> 01 </option>
                                                    <option value="02" {{  $require9Day =="02" ? 'selected' : '' }}> 02 </option>
                                                    <option value="03" {{  $require9Day =="03" ? 'selected' : '' }}> 03 </option>
                                                    <option value="04" {{  $require9Day =="04" ? 'selected' : '' }}> 04 </option>
                                                    <option value="05" {{  $require9Day =="05" ? 'selected' : '' }}> 05 </option>
                                                    <option value="06" {{  $require9Day =="06" ? 'selected' : '' }}> 06 </option>
                                                    <option value="07" {{  $require9Day =="07" ? 'selected' : '' }}> 07 </option>
                                                    <option value="08" {{  $require9Day =="08" ? 'selected' : '' }}> 08 </option>
                                                    <option value="09" {{  $require9Day =="09" ? 'selected' : '' }}> 09 </option>
                                                    @for($i2=10;$i2<=31;$i2++)
                                                    <option value="{{$i2}}" {{  $require9Day ==$i2 ? 'selected' : '' }}> {{$i2}} </option>
                                                    @endfor
                                                </select>
            
                                            </div>&nbsp;
                                            <div style="display: inline-block">
            
                                                <select  id="require_value9Month" name="require_value9Month">
                                                    <option value=""> เดือน </option>
                                                    <option value="01" {{  $require9Month =="01" ? 'selected' : '' }}> ม.ค. </option>
                                                    <option value="02" {{  $require9Month =="02" ? 'selected' : '' }}> ก.พ. </option>
                                                    <option value="03" {{  $require9Month =="03" ? 'selected' : '' }}> มี.ค. </option>
                                                    <option value="04" {{  $require9Month =="04" ? 'selected' : '' }}> เม.ย. </option>
                                                    <option value="05" {{  $require9Month =="05" ? 'selected' : '' }}> พ.ค. </option>
                                                    <option value="06" {{  $require9Month =="06" ? 'selected' : '' }}> มิ.ย. </option>
                                                    <option value="07" {{  $require9Month =="07" ? 'selected' : '' }}> ก.ค. </option>
                                                    <option value="08" {{  $require9Month =="08" ? 'selected' : '' }}> ส.ค. </option>
                                                    <option value="09" {{  $require9Month =="09" ? 'selected' : '' }}> ก.ย. </option>
                                                    <option value="10" {{  $require9Month =="10" ? 'selected' : '' }}> ต.ค. </option>
                                                    <option value="11" {{  $require9Month =="11" ? 'selected' : '' }}> พ.ย. </option>
                                                    <option value="12" {{  $require9Month =="12" ? 'selected' : '' }}> ธ.ค. </option>
                                                </select>
                                            </div>&nbsp;
                                            <div style="display: inline-block">
                                                @php
                                                    $currentYear = date("Y");
                                                @endphp
                                                <select  id="require_value9Year" name="require_value9Year">
                                                    <option value=""> ปี ค.ศ </option>
                                                    @for($i2=$currentYear+50;$i2>=1900;$i2--)
                                                    <option value="{{$i2}}" {{  $require9Year ==$i2 ? 'selected' : '' }}>{{$i2}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @if($case->$requireValue =="ทำ")
                                <tr id="dotaxspace" style="display:">
                                @else
                                <tr id="dotaxspace" style="display:none">
                                @endif
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @elseif($i == 7)
                                @elseif($i == 8)
                                @elseif($i == 9)
                                @elseif($i == 10)
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">
                                            <textarea style="width:190px;height:190px" class="form-control" id="{{$requireValue}}" name="{{$requireValue}}" >{{$case->$requireValue}}</textarea>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td>&nbsp;</td>
                                    </tr>
                                
                                @else
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">
                                        <input style="width:190px" class="form-control" id="{{$requireValue}}" name="{{$requireValue}}" value="{{$case->$requireValue}}" />
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td>&nbsp;</td>
                                    </tr>
                                @endif
                                
                                @endfor

                        </table>



                    </div>

                    <div class="column2">
                        <table>
                            @for($i=11;$i<=20;$i++) @php $requireName="requirename_var" .$i;$requireValue="require_value" .$i; @endphp 
                            @if($i == 14)
                                <tr>
                                    <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                    <td>
                                        <div style="width:190px">
                                        <select style="width:190px" class="form-control" id="{{$requireValue}}" name="{{$requireValue}}" value="{{$case->$requireValue}}" >
                                            <option value="ยอดเต็ม">ยอดเต็ม</option>
                                            <option value="3เดือน">3เดือน</option>
                                            <option value="4เดือน">4เดือน</option>
                                            <option value="6เดือน">6เดือน</option>

                                        </select>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td>&nbsp;</td>
                                    </tr>
                            @else
                                <tr>
                                <th style="width:220px">{{$case->CaseType->$requireName}}</th>
                                <td>
                                    <div style="width:190px">
                                        <input style="width:190px" class="form-control" id="{{$requireValue}}" name="{{$requireValue}}" value="{{$case->$requireValue}}"/>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                            @endif
                            @endfor

                        </table>
                    </div>

                </div>
                
            </div>


        </div>
        <div class="column">
            <div class="card">
                <div class="card-header" style="background-color:#D6FBFF">ผู้รับผลประโยชน์</div>
                <div class="card-body">
                    <div class="column2">
                        <table>
                            @for($i=54;$i<=61;$i++) @php $varName="var_name" .$i;$varValue="var_value" .$i; @endphp 
                                <tr>
                                <th style="width:220px">{{$case->CaseType->$varName}}</th>
                                <td>
                                    <div style="width:190px">
                                        <input style="width:190px" class="form-control" id="{{$varValue}}" name="{{$varValue}}" value="{{$case->$varValue}}"/>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @endfor

                        </table>
                    </div>
                    <div class="column2">
                        <table>
                            @for($i=62;$i<=69;$i++) @php $varName="var_name" .$i;$varValue="var_value" .$i; @endphp 
                                <tr>
                                <th style="width:220px">{{$case->CaseType->$varName}}</th>
                                <td>
                                    <div style="width:190px">
                                        <input style="width:190px" class="form-control" id="{{$varValue}}" name="{{$varValue}}"  value="{{$case->$varValue}}"/>
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>&nbsp;</td>
                                </tr>
                                @endfor

                        </table>
                    </div>
                </div>
            </div>
        </div>
        <a href="{{'/wealththaiinsurance/'.$case->id.'/case/steptwo'}}" class="btn btn-default btn-margin" id="reset">Back <i
            class="icon-reload-alt position-right"></i></a>
        <button type="button" id="btn-save-draft" class="btn btn-primary btn-margin"
        onclick="saveCase()">Submit <i
            class="icon-arrow-right14 position-right"></i></button>
    <input type="hidden" name="step" value="3.1">
    <input type="hidden" name="caseid" value="{{$case->id}}">
    </form>





    <script>
     $('.subject-list2').on('change', function() {
      $('.subject-list2').not(this).prop('checked', false);
      });
$(document).ready(function () {

    $('#add_menu').on('click', function () {
        var next_menu_index = parseInt($('#next_menu_index').val());
        $('#menuTable tbody').append($('#template_menu').text().replace(/@{{i}}/g, next_menu_index));
        $('#next_menu_index').val(next_menu_index + 1);
    });

});
</script>
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">

          $(".name").select2({
                placeholder: "Select",
                //allowClear: true
            });
    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script type="text/javascript">


        $(document).ready(function(){
            $(document).on('change','.caseCat',function(){
            //  console.log("hmm its change");

                var cat_id=$(this).val();
                //console.log(department_id);
                var div=$(this).parent();
                var op=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findCasetypeBycaseCat')!!}',
                    data:{'id':cat_id},

                    success:function(data){
                      op+='<option value="" >-เลือกชนิดของใบงาน-</option>';
                      for(var i=0; i<data.length;i++){
                        op+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                      }
                      $('.caseType').html(" ");
                      $('.caseType').append(op);
                    },
                    error:function(){

                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.findcasetype',function(){
            //  console.log("hmm its change");

                var type_id=$(this).val();
                //console.log(department_id);
                var div=$(this).parent();
                var casesubtype=" ";
                var defaultuser=" ";

                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findCaseType')!!}',
                    data:{'id':type_id},

                    success:function(data){
                        casesubtype+='<option value="" >-เลือกชนิดย่อย ของใบงาน-</option>';

                      for(var i=0; i<data.length;i++){
                        casesubtype+='<option value="'+data[i].case_sub_type_id+'">'+data[i].case_sub_type_name+'</option>';

                      }
                      $('.casesubtype').html(" ");

                      $('.casesubtype').append(casesubtype);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>

<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultuser',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var casesubtype=" ";
                var defaultuser=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultUserBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultuser+='<option value="" >-เลือกผู้แจ้งงาน-</option>';

                      for(var i=0; i<data.length;i++){
                        defaultuser+='<option value="'+data[i].id+'">'+data[i].name+'</option>';

                      }
                      $('.defaultuser').html(" ");
                      $('.defaultuser').append(defaultuser);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>

<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultpartner',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var defaultpartner=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultPartnerBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultpartner+='<option value="" >-เลือกผู้ให้คำปรึกษา-</option>';

                      for(var i=0; i<data.length;i++){
                        defaultpartner+='<option value="'+data[i].id+'">'+data[i].name+'</option>';

                      }
                      $('.defaultpartner').html(" ");
                      $('.defaultpartner').append(defaultpartner);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>
<script type="text/javascript">
    
        $(document).ready(function(){
            $(document).on('change','.finddefaultcoor',function(){
                var type_id=$(this).val();
                var div=$(this).parent();
                var defaultcoor=" ";
                $.ajax({
                    type:'get',
                    url:'{!!URL::to('findDefaultCoorBlockByCaseType')!!}',
                    data:{'id':type_id},
                    success:function(data){
                        defaultcoor+='<option value="" >-เลือกผู้ประสานงาน-</option>';

                      for(var i=0; i<data.length;i++){
                        defaultcoor+='<option value="'+data[i].id+'">'+data[i].firstname+' '+data[i].lastname+'</option>';

                      }
                      $('.defaultcoor').html(" ");
                      $('.defaultcoor').append(defaultcoor);

                    },
                    error:function(){

                    }
                });
            });
        });
    </script>


<script>



function doInsurance() {
    console.log("hmmmmm")
  var act = document.getElementById("doinsurance");
  if (act.style.display == "none") {
    act.style.display = "";
  } 
  var doinsurancespace = document.getElementById("doinsurancespace");
  if (doinsurancespace.style.display == "none") {
    doinsurancespace.style.display = "";
  } 
  var requirevalue4 = document.getElementById("require_value4");
  if(requirevalue4.value =="ไม่ทำ"){
    doinsurance.style.display = "none";

    doinsurancespace.style.display = "none";
  }



}

function nodoTax() {
  var act = document.getElementById("dotax");
    act.style.display = "none";

  var doactspace = document.getElementById("dotaxspace");
    doactspace.style.display = "none";

}

function doTax() {
    console.log("hmmmmm")
  var tax = document.getElementById("dotax");
  if (tax.style.display == "none") {
    tax.style.display = "";
  } 
  var dotaxspace = document.getElementById("dotaxspace");
  if (dotaxspace.style.display == "none") {
    dotaxspace.style.display = "";
  } 

}



function nodoAct() {
  var act = document.getElementById("doact");
    act.style.display = "none";

  var doactspace = document.getElementById("doactspace");
    doactspace.style.display = "none";

}

function doAct() {
    console.log("hmmmmm")
  var act = document.getElementById("doact");
  if (act.style.display == "none") {
    act.style.display = "";
  } 
  var doactspace = document.getElementById("doactspace");
  if (doactspace.style.display == "none") {
    doactspace.style.display = "";
  } 

}
function saveCase() {
            var msg = '';
            if(msg != ''){
                swal({
                    title: msg,
                    confirmButtonColor: "#EF5350",
                    allowOutsideClick: false,
                    type: "info"
                });
            }else{
                swal({
                        width: '80px',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        icon: 'success',
                        html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                    });
                saveCreatecase();
                //swal.close();

            }
            return false;
        }

        function saveCreatecase() {
        var formData = new FormData($('#form-data-case-asset')[0]);
        $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
        $.ajax({
            type: 'POST',
            url: "{{ route('cases.store') }}",
            data: formData,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if(data.status){
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                        allowOutsideClick: false,
                    }).then(function () {
                        window.location.href = "/wealththaiinsurance/all/cases";
                    }, function (dismiss) {});
                }else{
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        allowOutsideClick: false,
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
        }
</script>
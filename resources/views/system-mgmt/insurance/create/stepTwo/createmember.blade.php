<form id="form-data-member" name="form-data-member" method="post" class="form-horizontal form-validate-jquery">
    <div class="column">
    <div class="column2">
        <div class="card">
            <div class="card-header">ข้อมูลทั่วไป</div>
            <div class="card-body">
                <table style="width:100%">
                    
                    <tr>
                        <th>คำนำหน้า <b style="color:red">*</b></th>
                        <td>
                            <div style="width: 190px">
                                <select style="width: 190px" class="form-control " id="prefix" name="prefix">
                                    <option value="">โปรดเลือก</option>
                                    <option value="นาย(Mr.)">นาย(Mr.)</option>
                                    <option value="นางสาว(Ms.)">นางสาว(Ms.)</option>
                                    <option value="นาง(Mrs.)">นาง(Mrs.)</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ชื่อ <b style="color:red">*</b></th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberName" name="memberName" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>นามสกุล <b style="color:red">*</b></th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberLname" name="memberLname"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ชื่อเล่น</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberNickname" name="memberNickname"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>อีเมล <b style="color:red">*</b></th>
                        <td>
                            <div style="width: 190px;display: flex">
                                <input class="form-control" id="memberEmail" name="memberEmail"/>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>เบอร์โทรศัพท์</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberMobile" name="memberMobile"/>
                            </div>
                        </td>
                    </tr>


                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>วันเดือนปีเกิด</th>
                        <td>
                            <div style="width: 190px">
                                <div style="display: inline-block">

                                    <select  id="memberDayBirth" name="memberDayBirth">
                                        <option value=""> วัน </option>
                                        <option value="01"> 01 </option>
                                        <option value="02"> 02 </option>
                                        <option value="03"> 03 </option>
                                        <option value="04"> 04 </option>
                                        <option value="05"> 05 </option>
                                        <option value="06"> 06 </option>
                                        <option value="07"> 07 </option>
                                        <option value="08"> 08 </option>
                                        <option value="09"> 09 </option>
                                        @for($i=10;$i<=31;$i++)
                                        <option value="{{$i}}"> {{$i}} </option>
                                        @endfor
                                    </select>

                                </div>&nbsp;
                                <div style="display: inline-block">

                                    <select  id="memberMonthBirth" name="memberMonthBirth">
                                        <option value=""> เดือน </option>
                                        <option value="01"> ม.ค. </option>
                                        <option value="02"> ก.พ. </option>
                                        <option value="03"> มี.ค. </option>
                                        <option value="04"> เม.ย. </option>
                                        <option value="05"> พ.ค. </option>
                                        <option value="06"> มิ.ย. </option>
                                        <option value="07"> ก.ค. </option>
                                        <option value="08"> ส.ค. </option>
                                        <option value="09"> ก.ย. </option>
                                        <option value="10"> ต.ค. </option>
                                        <option value="11"> พ.ย. </option>
                                        <option value="12"> ธ.ค. </option>

                                    </select>
                                </div>&nbsp;
                                <div style="display: inline-block">
                                    @php
                                        $currentYear = date("Y");
                                    @endphp
                                    <select  id="memberYearBirth" name="memberYearBirth">
                                        <option value=""> ปี ค.ศ </option>
                                        @for($i=$currentYear;$i>=1900;$i--)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>



                </table>
            </div>
        </div>
    </div>
</div>
<div class="column">
    <div class="column2">
        <div class="card">
            <div class="card-header">ที่อยู่ตามบัตร</div>
            <div class="card-body">
                <table style="width:100%">
                    <tr>
                        <th>บ้านเลขที่</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" name="memberSaveAdd1" id="memberSaveAdd1"/>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ซอย</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" name="memberSaveAdd1Alley" id="memberSaveAdd1Alley"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ถนน</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" name="memberSaveAdd1Road" id="memberSaveAdd1Road"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ประเทศ</th>
                        <td>
                            <div style="width: 190px">

                                <select style="width: 190px" class="form-control selectwidthauto country" id="memberSaveAdd1Country" name="memberSaveAdd1Country">
                                    <option value="">โปรดเลือก</option>
                                    @foreach($countryList as $data)
                                <option value="{{$data -> id}}">{{$data -> name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>จังหวัด</th>
                        <td>
                            <div style="width: 190px">
                                <select style="width: 190px" class="form-control selectwidthauto pro prodis" id="memberSaveAdd1Province" name="memberSaveAdd1Province">
                                    <option value="">โปรดเลือก</option>
                                    @foreach($provinceList as $data)
                                    <option value="{{$data -> id}}">{{$data -> name_in_thai}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>เขต/อำเภอ</th>
                        <td>
                            <div style="width: 190px">

                                <select style="width: 190px" class="form-control selectwidthauto dis dissub" id="memberSaveAdd1District" name="memberSaveAdd1District" >
                                    <option value="">โปรดเลือก</option>

                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>แขวงตำบล</th>
                        <td>
                            <div style="width: 190px">
                                <select style="width: 190px" class="form-control selectwidthauto subdis" id="memberSaveAdd1Subdistrict" name="memberSaveAdd1Subdistrict">
                                    <option value="">โปรดเลือก</option>

                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>รหัสไปรษณีย์</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberSaveAdd1Postcode" name="memberSaveAdd1Postcode"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>โทร</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberSaveAdd1Tel" name="memberSaveAdd1Tel"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>แฟกซ์</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberSaveAdd1Fax" name="memberSaveAdd1Fax"/>
                            </div>
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div>

    <div class="column2">
        <div class="card">
            <div class="card-header">ที่อยู่จัดส่งเอกสาร <span style="float:right;display:inline"><a onclick="closeCreateformadd2Member()">ที่เดียวกับที่อยู่ตามบัตร</a> หรือ <a onclick="openCreateformadd2Member()">สร้างใหม่</a>  </span></div>
            <div class="card-body" id="formadd2Member" >
                <table style="width:100%">
                    <tr>
                        <th>บ้านเลขที่</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" name="memberSaveAdd2" id="memberSaveAdd2"/>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ซอย</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" name="memberSaveAdd2Alley" id="memberSaveAdd2Alley"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ถนน</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" name="memberSaveAdd2Road" id="memberSaveAdd2Road"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ประเทศ</th>
                        <td>
                            <div style="width: 190px">

                                <select style="width: 190px" class="form-control selectwidthauto country2" id="memberSaveAdd2Country" name="memberSaveAdd2Country">
                                    <option value="">โปรดเลือก</option>
                                    @foreach($countryList as $data)
                                <option value="{{$data -> id}}">{{$data -> name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>จังหวัด</th>
                        <td>
                            <div style="width: 190px">
                                <select style="width: 190px" class="form-control selectwidthauto pro2 prodis2" id="memberSaveAdd2Province" name="memberSaveAdd2Province">
                                    <option value="">โปรดเลือก</option>
                                    @foreach($provinceList as $data)
                                    <option value="{{$data -> id}}">{{$data -> name_in_thai}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>เขต/อำเภอ</th>
                        <td>
                            <div style="width: 190px">

                                <select style="width: 190px" class="form-control selectwidthauto dis2 dissub2" id="memberSaveAdd2District" name="memberSaveAdd2District" >
                                    <option value="">โปรดเลือก</option>

                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>แขวงตำบล</th>
                        <td>
                            <div style="width: 190px">
                                <select style="width: 190px" class="form-control selectwidthauto subdis2" id="memberSaveAdd2Subdistrict" name="memberSaveAdd2Subdistrict">
                                    <option value="">โปรดเลือก</option>

                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>รหัสไปรษณีย์</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberSaveAdd2Postcode" name="memberSaveAdd2Postcode"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>โทร</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberSaveAdd2Tel" name="memberSaveAdd2Tel"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>แฟกซ์</th>
                        <td>
                            <div style="width: 190px">
                                <input class="form-control" id="memberSaveAdd2Fax" name="memberSaveAdd2Fax"/>
                            </div>
                        </td>
                    </tr>


                </table>
            </div>
            <div class="card-body" id="formadd1Toadd2Member" style="display:none;text-align:center" >
                <span style="text-align:center;color:red"> * Note: ระบบจะทำการบันทึกที่อยู่ในการจัดส่งเอกสารเป็นที่เดียวกับที่อยู่ตามบัตร </span>
            </div>
            <input type="hidden" id="addflag" name="add2Flag" value="new"/>

        </div>
    </div>
</div>

    <input type="hidden" name="step" value="2.2">
    <input type="hidden" name="caseid" value="{{$case->id}}">
    <a href="URL::current" class="btn btn-default btn-margin" id="reset">Back <i
            class="icon-reload-alt position-right"></i></a>

    <button type="button" id="btn-save-draft" class="btn btn-primary btn-margin" onclick="saveMember()">Submit <i
            class="icon-arrow-right14 position-right"></i></button>

</form>

<script>
    function openCreateformadd2Member() {
        var form = document.getElementById("formadd2Member");
  if (form.style.display === "none") {
    form.style.display = "block";
  }else{
    form.style.display = "block";
  }

  var add1toadd2 = document.getElementById("formadd1Toadd2Member");
  if (add1toadd2.style.display === "block") {
    add1toadd2.style.display = "none";
  }
  document.getElementById("addflag").value="new";
}

function closeCreateformadd2Member() {
    var form = document.getElementById("formadd2Member");

    form.style.display = "none";
   

  var add1toadd2 = document.getElementById("formadd1Toadd2Member");

  if (add1toadd2.style.display === "none") {
    add1toadd2.style.display = "block";
  }
  document.getElementById("addflag").value="sameAdd1";


 

}


    function saveMember() {
        var msg = '';

        if ($('#prefix').val().trim() == '')
            msg = 'กรุณาเลือก คำนำหน้า';

        else if ($('#memberName').val().trim() == '')
            msg = 'กรุณากรอก ชื่อ';

        else if ($('#memberLname').val().trim() == '')
            msg = 'กรุณากรอก นามสกุล';

        

        else if ($('#memberEmail').val().trim() == '')
            msg = 'กรุณากรอก อีเมล';

       

        if (msg != '') {
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        } else {
            swal({
                        width: '80px',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        icon: 'success',
                        html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                    });
            saveCreateMember();
            //swal.close();

        }
        return false;
    }

    function saveCreateMember() {
        var formData = new FormData($('#form-data-member')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: "{{ route('cases.store') }}",
            data: formData,
            dataType: 'json',
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if (data.status) {
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                        allowOutsideClick: false,
                    }).then(function () {
                        window.location.href = "/wealththaiinsurance/" + data.case_id +"/case/steptwo";
                    }, function (dismiss) {});
                } else {
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        allowOutsideClick: false,
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }
</script>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.country', function() {
        //  //console.log("hmm its change");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findProvince')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {

                op += '<option value="0" selected disabled>-เลือกจังหวัด-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';

                }
                $('.pro').html(" ");
                $('.pro').append(op);

            },
            error: function() {

            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.prodis', function() {
          //console.log("hmm its prodis");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findDistrict')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {

                op += '<option value="0" selected disabled>-เลือกเขต-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';

                }
                $('.dis').html(" ");
                $('.dis').append(op);

            },
            error: function() {

            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.dissub', function() {
          //console.log("hmm its dissub");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        var op2 = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findSubdistrict')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {





                op += '<option value="0" selected disabled>-เลือกแขวง-</option>';

                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';
                    
                }
                $('.subdis').html(" ");
                $('.subdis').append(op);
                

            },
            error: function() {

            }
        });
    });
});
</script>

<!-- /.content -->

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.country2', function() {
          //console.log("hmm its country2");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findProvince')!!}',
            data: {
                'id': department_id
            },
            success: function(data) {
                op += '<option value="0" selected disabled>-เลือกจังหวัด-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';
                }
                $('.pro2').html(" ");
                $('.pro2').append(op);

            },
            error: function() {
            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.prodis2', function() {
          //console.log("hmm its prodis2");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findDistrict')!!}',
            data: {
                'id': department_id
            },
            success: function(data) {
                op += '<option value="0" selected disabled>-เลือกเขต-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';

                }
                $('.dis2').html(" ");
                $('.dis2').append(op);

            },
            error: function() {

            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.dissub2', function() {
          //console.log("hmm its dissub2");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        var op2 = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findSubdistrict')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {
                op += '<option value="0" selected disabled>-เลือกแขวง-</option>';

                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';
                   
                }
                $('.subdis2').html(" ");
                $('.subdis2').append(op);
               

            },
            error: function() {

            }
        });
    });
});
</script>

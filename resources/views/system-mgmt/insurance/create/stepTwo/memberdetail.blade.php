<style>
.form-group {
    float: left;
    width: 25%;
}

@media screen and (max-width: 1500px) {
    .form-group {
        width: 100%;
    }
}
</style>


<div class="column">
    <div class="card">
        <div class="card-header">ข้อมุล ทั่วไป</div>
        <div class="card-body">

            <div class="">
                <table>
                    <tr>
                        <th>ชื่อ - นามสกุล </th>
                        <td>{{$memberDetail -> name}} {{$memberDetail -> lname}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ที่อยู่ตามบัตร </th>
                        <td>{{$memberCardAddress}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ที่อยู่จัดส่งเอกสาร </th>
                        <td>{{$memberDocumentAddress}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>เบอร์โทรศัพท์ </th>
                        <td>{{$memberDetail -> mobile}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>อีเมล </th>
                        <td>{{$memberDetail -> email}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>แฟกซ์ </th>
                        <td>{{$memberDetail -> add2_fax}}</td>
                    </tr>


                </table>
            </div>
        </div>
    </div>

 
</div>
<div class="column2">
    <div class="card">
        <div class="card-header">ข้อมุล เอกสาร ผู้เอาประกัน
            <a style="float:right;color:green" href="/SecurityBroke/member/uploadfile/{{$memberDetail -> id}}/xxx/CG3CG/Member_Attachment_{{$memberDetail -> id}}" target="_blank" ><i class="fa fa-plus"></i> เพิ่มเอกสาร</a>
        </div>
        <div class="card-body">

            <div class="">
                <table>
                    @if(count($memberFile) <= 0)
                    <tr>
                        <th style="color:red">** ไม่มีเอกสาร</th>
                        <td> </td>
                    </tr>
                    @else
                    @foreach($memberFile as $file)
                    <tr>
                        <th>{{$file -> filecat -> name}} </th>
                        <td><a href="/SecurityBroke/showfile/{{$file -> id}}" target="_blank">{{$file -> file_public_name}}</a> </td>

                    </tr>
                    @endforeach

                    @endif



                </table>
            </div>
        </div>
    </div>
</div>
<div class="column2">
    <div class="card">
        <div class="card-header">ข้อมุล ผู้แนะนำ</div>
        <div class="card-body">

            <div class="">
            <table>
            @if(!empty($memberAdvisorDetail))

                    <tr>
                        <th>ชื่อ - นามสกุล </th>
                        <td>{{$memberAdvisorDetail -> name}} {{$memberAdvisorDetail -> lname}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ที่อยู่ตามบัตร </th>
                        <td>{{$memberAdvisorCardAddress}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>ที่อยู่จัดส่งเอกสาร </th>
                        <td>{{$memberAdvisorDocumentAddress}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>เบอร์โทรศัพท์ </th>
                        <td>{{$memberAdvisorDetail -> mobile}}</td>
                    </tr>
                    <tr>  
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>อีเมล </th>
                        <td>{{$memberAdvisorDetail -> email}}</td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <th>แฟกซ์ </th>
                        <td>{{$memberAdvisorDetail -> add2_fax}}</td>
                    </tr>
                    @else
                    @if(count($memberFile) <= 0)
                    <tr>
                    <th style="color:red">** ไม่มีผู้แนะนำ</th>
                        <td>&nbsp;</td>
                    </tr>
                    @else
                    @foreach($memberFile as $key => $file)
                    <tr>
                    @if($key == 0)
                    <th style="color:red">** ไม่มีผู้แนะนำ</th>
                    @else
                    <th> </th>
                    @endif
                        <td>&nbsp;</td>
                    </tr>
                   
                    @endforeach
                    @endif
                    @endif

                </table>
            </div>
        </div>
    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<div class="tab-pane active" role="tabpanel" id="step1">
    <p style="color:red">&nbsp;&nbsp;&nbsp;&nbsp;กรุณากรอกช่องที่มีเครื่องหมาย * ให้ครบ</p>
    <div class="column">
        <div class="card">
            <div class="card-header" style="background-color:#D6FBFF">Step 2 ผู้เอาประกัน</div>

            <div class="card-body">
                <form id="form-data" name="form-data" method="post" class="form-horizontal form-validate-jquery">
                    <div class="column2">
                        <table>
                            <tr>

                                <th style="width:200px">ผู้เอาประกันภัย <b style="color:red">*</b></th>
                                <td>
                                    <div style="width:400px">

                                        <select style="width:190px" class="form-control memberowner" id="memberowner"
                                            name="memberowner" required>
                                            <option value="">โปรดเลือก</option>
                                            @foreach($memberOwner as $data)
                                            @if(empty($case))
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                            @else
                                            <option value="{{$data->id}}"
                                                {{$data->id == $case->member_case_owner ? 'selected' : ''}}>
                                                {{$data -> name}} {{$data -> lname}}</option>
                                            @endif
                                            @endforeach
                                        </select>&nbsp;
                                        <span  onclick="openCreateForm()" style="display:inline;"><a>เพิ่ม</a></span>

                                    </div>
                                </td>
                            </tr>



                        </table>
                    </div>

                    <div class="column2">
                        <table>

                            <tr>

                                <th style="width:200px">ผู้แนะนำ <b style="color:red">*</b></th>
                                <td>
                                    <div style="width:400px">

                                        <select style="width:190px" class="form-control memberowner" id="memberadvisor"
                                            name="memberadvisor">
                                            <option value="">โปรดเลือก</option>
                                            @foreach($memberAdvisorPid as $data)
                                            @if(empty($case -> member_case_owner))
                                            <option value="{{$data -> id}}">{{$data -> public_name}}</option>
                                            @else
                                            <option value="{{$data->id}}"
                                                {{$data->id == $memberDetail->ref_member_pid ? 'selected' : ''}}>
                                                {{$data -> public_name}}</option>
                                            @endif
                                            @endforeach
                                        </select>&nbsp;
                                        <span  onclick="openCreateAdvisorForm()" style="display:inline;"><a>เพิ่ม</a></span>
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <a href="{{'/wealththaiinsurance/'.$case->id.'/case'}}" class="btn btn-default btn-margin" id="reset">Back <i
                            class="icon-reload-alt position-right"></i></a>

                    <button type="button" id="btn-save-draft" class="btn btn-primary btn-margin" onclick="save()">Submit <i class="icon-arrow-right14 position-right"></i></button>

                    <input type="hidden" name="step" value="2">
                    <input type="hidden" name="caseid" value="{{$case->id}}">

                </form>
            </div>

        </div>

    </div>

    <div id="createadvisors" style="display: none;">
        <div class="column">
            <div class="card">
                <div class="card-header">เพิ่ม ผู้แนะนำ  <span style="float:right" onclick="openCreateAdvisorForm()" style="display:inline;"><a><i class="fa fa-minus"></i></a></span>
                </div>
        
                <div class="card-body">
                    
                    <div>
                        <div class="column">
                                  @include('system-mgmt.insurance.create.stepTwo.createadvisor')                    
                        
                        </div>
                    </div>

                </div>
            </div>
        
        </div>

    </div>


    <div id="createmember" style="display: none;">
        <div class="column">
            <div class="card">
                <div class="card-header" style="background-color:#F0F8FF">เพิ่ม ผู้เอาประกัน  <span style="float:right" onclick="openCreateForm()" style="display:inline;"><a><i class="fa fa-minus"></i></a></span>
                </div>
        
                <div class="card-body">
                    <button onclick="openNormalmemberForm()" class="btn btn-default btn-margin">บุคคลธรรมดา</button>
                    <button onclick="openOrgmemberForm()" class="btn btn-default btn-margin">นิติบุคคล</button>
                    <div id="NormalMemberForm" style="display: none;">
                        <div class="column">
                                  @include('system-mgmt.insurance.create.stepTwo.createmember')                    
                        
                        </div>
                    </div>
                    <div id="OrgMemberForm" style="display: none;">
                        <div class="column">
                                  @include('system-mgmt.insurance.create.stepTwo.createorg')                    
                        
                        </div>
                    </div>
                </div>
            </div>
        
        </div>

    </div>
    <div id="createadvisor" style="display: none;">
        <div class="column">
            <div class="card">
                <div class="card-header" style="background-color:#F0F8FF">เพิ่ม ผู้เอาประกัน  <span style="float:right" onclick="openCreateForm()" style="display:inline;"><a><i class="fa fa-minus"></i></a></span>
                </div>
        
                <div class="card-body">
                    <button onclick="openNormaladvisorForm()" class="btn btn-default btn-margin">บุคคลธรรมดา</button>
                    <button onclick="openOrgadvisorForm()" class="btn btn-default btn-margin">นิติบุคคล</button>
                    <div id="NormalAdvisorForm" style="display: none;">
                        <div class="column">
                                  @include('system-mgmt.insurance.create.stepTwo.createadvisor')                    
                        
                        </div>
                    </div>
                    <div id="OrgAdvisorForm" style="display: none;">
                        <div class="column">
                                  @include('system-mgmt.insurance.create.stepTwo.createorgadvisor')                    
                        
                        </div>
                    </div>
                </div>
            </div>
        
        </div>

    </div>
    @if(empty($case))
    <input type="hidden" name="caseid" value="new">
    @else
    
        @if(!empty($case->member_case_owner))
            @include('system-mgmt.insurance.create.stepTwo.memberdetail')
            @include('system-mgmt.insurance.create.stepTwo.assetcreate')
            @include('system-mgmt.insurance.create.stepTwo.assetdetail')

        @else
        
        @endif
        @if(empty($memberSelectedAsset))
        @else
        <a href="{{'/wealththaiinsurance/'.$case->id.'/case'}}" class="btn btn-default btn-margin" id="reset">Back <i
            class="icon-reload-alt position-right"></i></a>
        @if(!empty($case))
             <a href="/wealththaiinsurance/cases/{{$case->id}}/detail/show" id="btn-save-draft" class="btn  btn-warning btn-margin" >Save and Exit <i class="icon-arrow-right14 position-right"></i></a>
        @else
        @endif
        <a href="/wealththaiinsurance/{{$case->id}}/case/stepthree" id="btn-save-draft" class="btn btn-primary btn-margin" >Next Step <i class="icon-arrow-right14 position-right"></i></a>
        @endif
    <input type="hidden" name="caseid" value="{{$case->id}}">
    @endif
</div>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
$(".memberowner").select2({
    placeholder: "Select",
    allowClear: true
});
</script>



<script>
function save() {
    var msg = '';
    if (msg != '') {
        swal({
            title: msg,
            confirmButtonColor: "#EF5350",
            allowOutsideClick: false,
            type: "info"
        });
    } else {
        swal({
                        width: '80px',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        icon: 'success',
                        html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                    });
        saveCreate();
        //swal.close();
    }
    return false;
}

function saveCreate() {
    var formData = new FormData($('#form-data')[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: "{{ route('cases.store') }}",
        data: formData,
        dataType: 'json',
        processData: false, // tell jQuery not to process the data
        contentType: false, // tell jQuery not to set contentType
        success: function(data) {
            status_submit = false;
            if (data.status) {
                swal({
                    title: data.message,
                    confirmButtonColor: "#66BB6A",
                    type: "success",
                    confirmButtonText: "ตกลง",
                }).then(function() {
                    window.location.href = "/wealththaiinsurance/" + data.case_id + "/case/steptwo";
                }, function(dismiss) {});
            } else {
                swal({
                    title: data.message,
                    confirmButtonColor: "#EF5350",
                    type: "error"
                }).then(function(dismiss) {});
            }
        },
        error: function(xhr, type) {
            status_submit = false;
        }
    });
}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $('#showcreatemember').click(function() {
      $('.createmember').toggle("");
    });
});

function openCreateAdvisorForm() {
  var form = document.getElementById("createadvisor");
  if (form.style.display === "none") {
    form.style.display = "block";
  } else {
    form.style.display = "none";
  }



}
function openCreateForm() {
  var form = document.getElementById("createmember");
  if (form.style.display === "none") {
    form.style.display = "block";
  } else {
    form.style.display = "none";
  }



}
function openNormaladvisorForm() {
    console.log("hi")
  var org = document.getElementById("OrgAdvisorForm");
  if (org.style.display != "none") {
    org.style.display = "none";
  } 

  var normal = document.getElementById("NormalAdvisorForm");
  if (normal.style.display === "none") {
    normal.style.display = "block";
  } else {
    normal.style.display = "none";
  }



}

function openOrgadvisorForm() {
  var normal = document.getElementById("NormalAdvisorForm");
  if (normal.style.display != "none") {
    normal.style.display = "none";
  } 

  var org = document.getElementById("OrgAdvisorForm");
  if (org.style.display === "none") {
    org.style.display = "block";
  } else {
    org.style.display = "none";
  }

}

function openNormalmemberForm() {
    console.log("hi")
  var org = document.getElementById("OrgMemberForm");
  if (org.style.display != "none") {
    org.style.display = "none";
  } 

  var normal = document.getElementById("NormalMemberForm");
  if (normal.style.display === "none") {
    normal.style.display = "block";
  } else {
    normal.style.display = "none";
  }



}

function openOrgmemberForm() {
  var normal = document.getElementById("NormalMemberForm");
  if (normal.style.display != "none") {
    normal.style.display = "none";
  } 

  var org = document.getElementById("OrgMemberForm");
  if (org.style.display === "none") {
    org.style.display = "block";
  } else {
    org.style.display = "none";
  }

}


$(document).ready(function(){
    $('#showNormalMemberForm').click(function() {
      $('.NormalMemberForm').toggle("");
    //  $('.OrgMemberForm').toggle("");
    });
});

$(document).ready(function(){
    $('#showOrgMemberForm').click(function() {
        $('.NormalMemberForm').toggle("");
        $('.OrgMemberForm').toggle("");

    });
});
</script>
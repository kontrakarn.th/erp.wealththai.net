<style>
.form-group {
    float: left;
    width: 25%;
}

@media screen and (max-width: 1500px) {
    .form-group {
        width: 100%;
    }
}
</style>


<div class="column">
    <div class="card">
        <div class="card-header">สินทรัพย์</div>
        <div class="card-body">
        <form id="form-data-asset" name="form-data-asset" method="post" class="form-horizontal form-validate-jquery">

            <div class="">
            <table>
                            <tr>

                                <th style="width:200px">สินทรัพย์ผู้เอาประกันภัย <b style="color:red">*</b></th>
                                <td>
                                    <div style="width:400px">

                                        <select style="width:190px" class="form-control memberowner" id="asset"
                                            name="asset" required>
                                            <option value="">โปรดเลือก</option>
                                            @foreach($memberAsset as $data)
                                            @if(empty($case))
                                            <option value="{{$data -> id}}">{{$data -> name}}</option>
                                            @else
                                            <option value="{{$data->id}}"
                                                {{$data->id == $case->referal_asset ? 'selected' : ''}}>
                                                {{$data -> name}} </option>
                                            @endif
                                            @endforeach
                                        </select>&nbsp;
                                        <span  onclick="openCreateFormAsset()" style="display:inline;"><a>เพิ่ม</a></span>
                                    </div>
                                </td>
                            </tr>

                        </table>
                        
            </div>
            <input type="hidden" name="step" value="2.6">
            <input type="hidden" name="caseid" value="{{$case->id}}">
            <button type="button"  class="btn btn-primary btn-margin" onclick="saveAsset()">Select <i class="icon-arrow-right14 position-right"></i></button>


            </form>
        </div>
    </div>


</div>

<div id="createasset" style="display: none;">
    <div class="column">
        <div class="card">
            <div class="card-header" style="background-color:#F0F8FF">เพิ่ม สินทรัพย์  <span style="float:right" onclick="openCreateFormAsset()" style="display:inline;"><a><i class="fa fa-minus"></i></a></span>
            </div>
    
            <div class="card-body">
                
                <div>
                    <div class="column">
                              @include('system-mgmt.insurance.create.stepTwo.createasset')                    
                    
                    </div>
                </div>

            </div>
        </div>
    
    </div>

</div>




<script>

function openCreateFormAsset() {
  var form = document.getElementById("createasset");
  if (form.style.display === "none") {
    form.style.display = "block";
  } else {
    form.style.display = "none";
  }



}


function saveAsset() {
    var msg = '';
    if (msg != '') {
        swal({
            title: msg,
            confirmButtonColor: "#EF5350",
            allowOutsideClick: false,
            type: "info"
        });
    } else {
        swal({
                        width: '80px',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        icon: 'success',
                        html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                    });
        saveCreateAsset();
        //swal.close();

    }
    return false;
}

function saveCreateAsset() {
    var formData = new FormData($('#form-data-asset')[0]);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: 'POST',
        url: "{{ route('cases.store') }}",
        data: formData,
        dataType: 'json',
        processData: false, // tell jQuery not to process the data
        contentType: false, // tell jQuery not to set contentType
        success: function(data) {
            status_submit = false;
            if (data.status) {
                swal({
                    title: data.message,
                    confirmButtonColor: "#66BB6A",
                    type: "success",
                    confirmButtonText: "ตกลง",
                    allowOutsideClick: false,
                }).then(function() {
                    window.location.href = "/wealththaiinsurance/" + data.case_id + "/case/steptwo";
                }, function(dismiss) {});
            } else {
                swal({
                    title: data.message,
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    allowOutsideClick: false,
                }).then(function(dismiss) {});
            }
        },
        error: function(xhr, type) {
            status_submit = false;
        }
    });
}
</script>



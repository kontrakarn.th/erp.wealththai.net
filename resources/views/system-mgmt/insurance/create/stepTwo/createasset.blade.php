<form id="form-data-newasset" name="form-data-newasset" method="post" class="form-horizontal form-validate-jquery">
    <div class="column">
        <div class="column3">
            <div class="card" style="border:none">
                <div class="card-body">
            <table style="width:100%">
                <tr>
                    <th>เลือกประเภททรัพย์สิน <b style="color:red">*</b></th>
                    <td>
                        <div style="width:190px">
                            <select style="width:190px" class="form-control assettype assettypeissuer" name="assettype">
                                <option value="">โปรดเลือก</option>
                                @foreach($assetType as $data)
                                <option value="{{$data->id}}">{{$data->la_nla_type}}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </div>
    </div>
    <div id="assetform" style="display:none" >
    <div class="column" >
    <div class="tableform">

    </div>
    <div class="column">
       <div class="card">
            <div class="card-body">
                <div class="column3">
                    <table>
                        <tr>
                            <th style="width:230px">Amount</th>
                            <td>
                                <div style="width:190px"><input class="form-control issuer " id="assetTransactionAmount" name="assetTransactionAmount" />
                                </div>
                            </td>
                        </tr>
                    </table><br />
                    <table>
                        <tr>
                            <th style="width:230px">Estimate Market Value</th>
                            <td>
                                <div style="width:190px"><input class="form-control issuer " id="assetTransactionEstvalue" name="assetTransactionEstvalue" />
                                </div>
                            </td>
                        </tr>
                    </table><br />
                </div>
                <div class="column3">
                    <table>
                        <tr>
                            <th style="width:230px">Cost</th>
                            <td>
                                <div style="width:190px"><input class="form-control issuer "  id="assetTransactionCost" name="assetTransactionCost"  />
                                </div>
                            </td>
                        </tr>
                    </table><br />
                    <table>
                        <tr>
                            <th style="width:230px">Note</th>
                            <td>
                                <div style="width:190px"><input class="form-control issuer " name="assetTransactionNote" id="assetTransactionNote" />
                                </div>
                            </td>
                        </tr>
                    </table><br />
                </div>
                <div class="column3">
                    <table>
                        <tr><th style="width:230px;">Valid From</th>
                            <td><div style="width:190px">                                    
                            <div style="display: inline-block">
                            <select  id="assetTransactionValidfromday" name="assetTransactionValidfromday">
                                <option value=""> วัน </option>
                                <option value="01"> 01 </option>
                                <option value="02"> 02 </option>
                                <option value="03"> 03 </option>
                                <option value="04"> 04 </option>
                                <option value="05"> 05 </option>
                                <option value="06"> 06 </option>
                                <option value="07"> 07 </option>
                                <option value="08"> 08 </option>
                                <option value="09"> 09 </option>
                                @for($i=10;$i<=31;$i++)
                                <option value="{{$i}}"> {{$i}} </option>
                                @endfor
                            </select>

                        </div>&nbsp;
                        <div style="display: inline-block">

                            <select  id="assetTransactionValidfrommonth" name="assetTransactionValidfrommonth">
                                <option value=""> เดือน </option>
                                <option value="01"> ม.ค. </option>
                                <option value="02"> ก.พ. </option>
                                <option value="03"> มี.ค. </option>
                                <option value="04"> เม.ย. </option>
                                <option value="05"> พ.ค. </option>
                                <option value="06"> มิ.ย. </option>
                                <option value="07"> ก.ค. </option>
                                <option value="08"> ส.ค. </option>
                                <option value="09"> ก.ย. </option>
                                <option value="10"> ต.ค. </option>
                                <option value="11"> พ.ย. </option>
                                <option value="12"> ธ.ค. </option>

                            </select>
                        </div>&nbsp;
                        <div style="display: inline-block">
                            @php
                                $currentYear = date("Y");
                            @endphp
                            <select  id="assetTransactionValidfromyear" name="assetTransactionValidfromyear">
                                <option value=""> ปี ค.ศ </option>
                                @for($i=$currentYear;$i>=1900;$i--)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>
                        </div></td>
                        </tr>
                        </table><br/>
                        <table>
                            <tr><th style="width:230px;">Valid To</th>
                            <td><div style="width:190px">                                    
                                <div style="display: inline-block">
                                    <select  id="assetTransactionValidtoday" name="assetTransactionValidtoday">
                                    <option value=""> วัน </option>
                                    <option value="01"> 01 </option>
                                    <option value="02"> 02 </option>
                                    <option value="03"> 03 </option>
                                    <option value="04"> 04 </option>
                                    <option value="05"> 05 </option>
                                    <option value="06"> 06 </option>
                                    <option value="07"> 07 </option>
                                    <option value="08"> 08 </option>
                                    <option value="09"> 09 </option>
                                    @for($i=10;$i<=31;$i++)
                                    <option value="{{$i}}"> {{$i}} </option>
                                    @endfor
                                </select>
    
                            </div>&nbsp;
                            <div style="display: inline-block">
    
                                <select  id="assetTransactionValidtomonth" name="assetTransactionValidtomonth">
                                    <option value=""> เดือน </option>
                                    <option value="01"> ม.ค. </option>
                                    <option value="02"> ก.พ. </option>
                                    <option value="03"> มี.ค. </option>
                                    <option value="04"> เม.ย. </option>
                                    <option value="05"> พ.ค. </option>
                                    <option value="06"> มิ.ย. </option>
                                    <option value="07"> ก.ค. </option>
                                    <option value="08"> ส.ค. </option>
                                    <option value="09"> ก.ย. </option>
                                    <option value="10"> ต.ค. </option>
                                    <option value="11"> พ.ย. </option>
                                    <option value="12"> ธ.ค. </option>
    
                                </select>
                            </div>&nbsp;
                            <div style="display: inline-block">
                                @php
                                    $currentYear = date("Y");
                                @endphp
                                <select  id="assetTransactionValidtoyear" name="assetTransactionValidtoyear">
                                    <option value=""> ปี ค.ศ </option>
                                    @for($i=$currentYear;$i>=1900;$i--)
                                    <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            </div></td>
                            </tr>
                            </table><br/>
                </div>
            </div>
        </div>

    </div>

    </div>
    </div>
    <input type="hidden" name="step" value="2.5">
    <input type="hidden" name="caseid" value="{{$case->id}}">
    <a href="URL::current" class="btn btn-default btn-margin" id="reset">Back <i
            class="icon-reload-alt position-right"></i></a>

    <button type="button" id="btn-save-draft" class="btn btn-primary btn-margin" onclick="saveNewAsset()">Submit <i
            class="icon-arrow-right14 position-right"></i></button>

</form>

<script>

    function saveNewAsset() {
        var msg = '';

        if (msg != '') {
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        } else {
            swal({
                        width: '80px',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        icon: 'success',
                        html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                    });
            saveCreateNewAsset();
            //swal.close();

        }
        return false;
    }

    function saveCreateNewAsset() {
        var formData = new FormData($('#form-data-newasset')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: "{{ route('cases.store') }}",
            data: formData,
            dataType: 'json',
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if (data.status) {
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                        allowOutsideClick: false,
                    }).then(function () {
                        window.location.href = "/wealththaiinsurance/" + data.case_id +"/case/steptwo";
                    }, function (dismiss) {});
                } else {
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        allowOutsideClick: false,
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }
</script>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    
    $(document).on('change', '.assettype', function() {
          //console.log("hmm its dissub2");
          var form = document.getElementById("assetform");

          if (form.style.display === "none") {
            form.style.display = "block";
          }
          
        var department_id = $(this).val();
        //console.log(department_id);
        var tableform = " ";
        var i;
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findAssetHeaderForm')!!}',
            data: {
                'id': department_id
            },
            success: function(data) {
                if(data === "nodata"){
                    var form = document.getElementById("assetform");

                    if (form.style.display === "block") {
                         form.style.display = "none";
                        }
                }
                
                
                tableform += '<div class="column3 tableform" >';
                tableform += '<div class="card">';
                tableform += '<div class="card-header">กรุณากรอกตามแบบฟอร์ม</div>';
                tableform += '<div class="card-body">';
                tableform += '<table>';
                tableform += '<tr><th style="width:240px">ยี่ห้อ / แบรนด์</th>'
                tableform += '<td><div><select style="width:195px" class="form-control issuer" name="assetIssued" id="assetIssued">'
                tableform += '<option>โปรดเลือก</option>'
                tableform += '</select>'
                tableform += '</div></td>'
                tableform += '</tr>'
                tableform += '</table><br/>';
                tableform += '<table>';
                tableform += '<tr><th style="width:240px">ชื่อ</th>'
                tableform += '<td><div style="width:190px"><input class="form-control" name="assetName" id="assetName"/></div></td>'
                tableform += '</tr>'
                tableform += '</table><br/>';
                tableform += '<table>';
                tableform += '<tr><th style="width:240px">'+data.ref_name_head+'</th>'
                tableform += '<td><div style="width:190px"><input class="form-control" name="assetRefname" id="assetRefname"/></div></td>'
                tableform += '</tr>'
                tableform += '</table><br/>';
                for(i = 1;i<=3;i++){
                    var str1 = "ref_num_head";
                    var str2 = i;
                    var res = str1.concat(str2);
                    var str3 = "ref_number";
                    var numname = str3.concat(str2);
                tableform += '<table>';
                    if(data[res] == null || data[res] == "ไม่ต้องระบุ")
                    {
                        tableform += '<tr><th style="width:240px">ไม่ต้องระบุ</th>'
                        tableform += '<td><div style="width:190px"><input class="form-control" name="'+numname+'" readonly id="'+numname+'"/></div></td>'
                    }else{
                        tableform += '<tr><th style="width:240px">'+data[res]+'</th>'
                        tableform += '<td><div style="width:190px"><input class="form-control" name="'+numname+'" id="'+numname+'"/></div></td>'
                    }
                tableform += '</tr>'
                tableform += '</table><br/>';
                }
                for(i = 1;i<=18;i++){ 
                    var str1 = "ref_info_head";
                    var str2 = i;
                    var res = str1.concat(str2);
                    var str3 = "ref_info";
                    var infoname = str3.concat(str2);
                if(i == 3){
                    tableform += '</div>';
                    tableform += '</div>';
                    tableform += '</div>';
                    tableform += '<div class="column3 tableform" >';
                    tableform += '<div class="card">';
                    tableform += '<div class="card-header">กรุณากรอกตามแบบฟอร์ม</div>';
                    tableform += '<div class="card-body">';
                }
                if(i == 11){
                    tableform += '</div>';
                    tableform += '</div>';
                    tableform += '</div>';
                    tableform += '<div class="column3 tableform" >';
                    tableform += '<div class="card">';
                    tableform += '<div class="card-header">กรุณากรอกตามแบบฟอร์ม</div>';
                    tableform += '<div class="card-body">';
                }
                
                tableform += '<table>';
                    if(data[res] == null || data[res] == "ไม่ต้องระบุ")
                    {
                        tableform += '<tr><th style="width:240px">ไม่ต้องระบุ</th>'
                        tableform += '<td><div style="width:190px"><input class="form-control" name="'+infoname+'" readonly id="'+infoname+'"/></div></td>'
                    }else{
                        tableform += '<tr><th style="width:240px">'+data[res]+'</th>'
                        tableform += '<td><div style="width:190px"><input class="form-control" name="'+infoname+'"  id="'+infoname+'"/></div></td>'
                    }
                tableform += '</tr>'
                tableform += '</table><br/>';
                }
                tableform += '</div>';
                tableform += '</div>';
                tableform += '</div>';
                $('.tableform').html(" ");
                $('.tableform').append(tableform);
               

            },
            error: function() {

            }
        });
    });
});
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.assettypeissuer', function() {
        //  //console.log("hmm its change");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('AssetTypeIssuer')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {

                op += '<option>-เลือก-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name +
                        '</option>';

                }
                $('.issuer').html(" ");
                $('.issuer').append(op);

            },
            error: function() {

            }
        });
    });
});
</script>
<form id="form-data-org" name="form-data-org" method="post" class="form-horizontal form-validate-jquery">
<div class="column2">
    <div class="card">
        <div class="card-header">ข้อมูลทั่วไป</div>
        <div class="card-body">
            <table style="width: 100%">

                
                <tr>
                    <th>ชื่อบริษัท <b style="color:red">*</b></th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveName" name="orgSaveName" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>ประเภทนิติบุคคล <b style="color:red">*</b></th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSavePrefix" name="orgSavePrefix" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>เลขที่นิติบุคคล <b style="color:red">*</b></th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveIdcard" name="orgSaveIdcard" />
                        </div>
                        <div style="color:red"></div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>วันที่จดทะเบียนจัดตั้งบริษัท</th>
                    <td>
                        <div style="width: 190px">
                            <div style="display: inline-block">

                                <select id="orgSaveDayBirth" name="orgSaveDayBirth">
                                    <option value=""> วัน </option>
                                    <option value="01"> 01 </option>
                                    <option value="02"> 02 </option>
                                    <option value="03"> 03 </option>
                                    <option value="04"> 04 </option>
                                    <option value="05"> 05 </option>
                                    <option value="06"> 06 </option>
                                    <option value="07"> 07 </option>
                                    <option value="08"> 08 </option>
                                    <option value="09"> 09 </option>
                                    @for($i=10;$i<=31;$i++) <option value="{{$i}}"> {{$i}} </option>
                                    @endfor
                                </select>

                            </div>&nbsp;
                            <div style="display: inline-block">

                                <select id="orgSaveMonthBirth" name="orgSaveMonthBirth">
                                    <option value=""> เดือน </option>
                                    <option value="01"> ม.ค. </option>
                                    <option value="02"> ก.พ. </option>
                                    <option value="03"> มี.ค. </option>
                                    <option value="04"> เม.ย. </option>
                                    <option value="05"> พ.ค. </option>
                                    <option value="06"> มิ.ย. </option>
                                    <option value="07"> ก.ค. </option>
                                    <option value="08"> ส.ค. </option>
                                    <option value="09"> ก.ย. </option>
                                    <option value="10"> ต.ค. </option>
                                    <option value="11"> พ.ย. </option>
                                    <option value="12"> ธ.ค. </option>

                                </select>
                            </div>&nbsp;
                            <div style="display: inline-block">
                                @php
                                $currentYear = date("Y");
                                @endphp
                                <select id="orgSaveYearBirth" name="orgSaveYearBirth">
                                    <option value=""> ปี ค.ศ </option>
                                    @for($i=$currentYear;$i>=1900;$i--)
                                    <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>บริษัทสัญชาติ</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveNation" name="orgSaveNation" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>ประเภทธุรกิจ </th>
                    <td>
                        <div style="width: 190px">
                            <input value="" class="form-control" id="orgSaveType" name="orgSaveType" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>เบอร์โทรออฟฟิศสำนักงาน</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveMobile" name="orgSaveMobile" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>อีเมลล์ติดต่อ</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveContactEmail" name="orgSaveContactEmail" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>อีเมลล์เข้าใช้งานระบบ <b style="color:red">*</b></th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveEmail" name="orgSaveEmail" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                
            </table>
        </div>
    </div>
</div>

<div class="column2">
    <div class="card">
        <div class="card-header">ที่ตั้งบริษัท</div>
        <div class="card-body">
            <table style="width: 100%">
                <tr>
                    <th>บ้านเลขที่</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveAdd2" name="orgSaveAdd2" />
                        </div>
                    </td>
                </tr>

                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>ซอย</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveAdd2Alley" name="orgSaveAdd2Alley" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>ถนน</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveAdd2Road" name="orgSaveAdd2Road" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>ประเทศ</th>
                    <td>
                        <div style="width: 190px">
                            <select style="width: 190px" class="form-control selectwidthauto country2"
                                id="orgSaveAdd2Country" name="orgSaveAdd2Country">
                                <option value="">โปรดเลือก</option>
                                @foreach($countryList as $data)
                                <option value="{{$data -> id}}">{{$data -> name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>จังหวัด</th>
                    <td>
                        <div style="width: 190px">
                            <select style="width: 190px" class="form-control selectwidthauto pro2 prodis2"
                                id="orgSaveAdd2Province" name="orgSaveAdd2Province">
                                <option value="">โปรดเลือก</option>
                                @foreach($provinceList as $data)
                                <option value="{{$data -> id}}">{{$data -> name_in_thai}}</option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>เขต/อำเภอ</th>
                    <td>
                        <div style="width: 190px">
                            <select style="width: 190px" class="form-control selectwidthauto dis2 dissub2"
                                id="orgSaveAdd2District" name="orgSaveAdd2District">
                                <option value="">โปรดเลือก</option>

                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>แขวงตำบล</th>
                    <td>
                        <div style="width: 190px">
                            <select style="width: 190px" class="form-control selectwidthauto subdis2"
                                id="orgSaveAdd2Subistrict" name="orgSaveAdd2Subistrict">
                                <option value="">โปรดเลือก</option>

                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>รหัสไปรษณีย์</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveAdd2Postcode" name="orgSaveAdd2Postcode" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>โทร</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveAdd2Tel" name="orgSaveAdd2Tel" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th>แฟกซ์</th>
                    <td>
                        <div style="width: 190px">
                            <input class="form-control" id="orgSaveAdd2Fax" name="orgSaveAdd2Fax" />
                        </div>
                    </td>
                </tr>

            </table>
        </div>
    </div>
</div>
<input type="hidden" name="step" value="2.4">
<input type="hidden" name="caseid" value="{{$case->id}}">
<a href="URL::current" class="btn btn-default btn-margin" id="reset">Back <i
        class="icon-reload-alt position-right"></i></a>

<button type="button" id="btn-save-draft" class="btn btn-primary btn-margin" onclick="saveOrg()">Submit <i
        class="icon-arrow-right14 position-right"></i></button>
</form>

<script>
    function saveOrg() {
        var msg = '';

        if ($('#orgSaveName').val().trim() == '')
            msg = 'กรุณากรอก ชื่อบริษัท ';

        else if ($('#orgSavePrefix').val().trim() == '')
            msg = 'กรุณากรอก ประเภทนิติบุคคล';

        else if ($('#orgSaveIdcard').val().trim() == '')
            msg = 'กรุณากรอก เลขที่นิติบุคคล';

        

        else if ($('#orgSaveEmail').val().trim() == '')
            msg = 'กรุณากรอก อีเมล';

        if (msg != '') {
            swal({
                title: msg,
                confirmButtonColor: "#EF5350",
                allowOutsideClick: false,
                type: "info"
            });
        } else {
            swal({
                        width: '80px',
                        allowOutsideClick: false,
                        showCancelButton: false,
                        showConfirmButton: false,
                        icon: 'success',
                        html: '<i class="fa fa-spinner fa-pulse" style="font-size:30px"></i>'
                    });
            saveCreateOrg();
            //swal.close();

        }
        return false;
    }

    function saveCreateOrg() {
        var formData = new FormData($('#form-data-org')[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: "{{ route('cases.store') }}",
            data: formData,
            dataType: 'json',
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            success: function (data) {
                status_submit = false;
                if (data.status) {
                    swal({
                        title: data.message,
                        confirmButtonColor: "#66BB6A",
                        type: "success",
                        confirmButtonText: "ตกลง",
                        allowOutsideClick: false,
                    }).then(function () {
                        window.location.href = "/wealththaiinsurance/" + data.case_id +"/case/steptwo";
                    }, function (dismiss) {});
                } else {
                    swal({
                        title: data.message,
                        confirmButtonColor: "#EF5350",
                        type: "error",
                        allowOutsideClick: false,
                    }).then(function (dismiss) {});
                }
            },
            error: function (xhr, type) {
                status_submit = false;
            }
        });
    }
</script>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.country', function() {
        //  //console.log("hmm its change");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findProvince')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {

                op += '<option value="0" selected disabled>-เลือกจังหวัด-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';

                }
                $('.pro').html(" ");
                $('.pro').append(op);

            },
            error: function() {

            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.prodis', function() {
          //console.log("hmm its prodis");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findDistrict')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {

                op += '<option value="0" selected disabled>-เลือกเขต-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';

                }
                $('.dis').html(" ");
                $('.dis').append(op);

            },
            error: function() {

            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.dissub', function() {
          //console.log("hmm its dissub");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        var op2 = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findSubdistrict')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {





                op += '<option value="0" selected disabled>-เลือกแขวง-</option>';

                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';
                    
                }
                $('.subdis').html(" ");
                $('.subdis').append(op);
                

            },
            error: function() {

            }
        });
    });
});
</script>

<!-- /.content -->

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.country2', function() {
          //console.log("hmm its country2");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findProvince')!!}',
            data: {
                'id': department_id
            },
            success: function(data) {
                op += '<option value="0" selected disabled>-เลือกจังหวัด-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';
                }
                $('.pro2').html(" ");
                $('.pro2').append(op);

            },
            error: function() {
            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.prodis2', function() {
          //console.log("hmm its prodis2");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findDistrict')!!}',
            data: {
                'id': department_id
            },
            success: function(data) {
                op += '<option value="0" selected disabled>-เลือกเขต-</option>';
                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';

                }
                $('.dis2').html(" ");
                $('.dis2').append(op);

            },
            error: function() {

            }
        });
    });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    $(document).on('change', '.dissub2', function() {
          //console.log("hmm its dissub2");

        var department_id = $(this).val();
        //console.log(department_id);
        var div = $(this).parent();
        var op = " ";
        var op2 = " ";
        $.ajax({
            type: 'get',
            url: '{!!URL::to('findSubdistrict')!!}',
            data: {
                'id': department_id
            },

            success: function(data) {
                op += '<option value="0" selected disabled>-เลือกแขวง-</option>';

                for (var i = 0; i < data.length; i++) {
                    op += '<option value="' + data[i].id + '">' + data[i].name_in_thai +
                        '</option>';
                   
                }
                $('.subdis2').html(" ");
                $('.subdis2').append(op);
               

            },
            error: function() {

            }
        });
    });
});
</script>

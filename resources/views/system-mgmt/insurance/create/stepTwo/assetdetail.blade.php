<style>
.form-group {
    float: left;
    width: 25%;
}

@media screen and (max-width: 1500px) {
    .form-group {
        width: 100%;
    }
}
</style>

<div class="column">
    <div class="card">
        <div class="card-header">ข้อมุล สินทรัพย์</div>
        <div class="card-body">

            <div class="column2" >
                <table>
                    @if(empty($memberSelectedAsset))
                    <tr>
                        <th style="color:red">**ยังไม่ได้เลือกสินทรัพย์</th>
                        <td> </td>
                    </tr>
                    @else
                    <tr>
                        <th>ชื่อสินทรัพย์</th>
                        <td>{{$memberSelectedAsset -> name}}</td>
                    </tr>
                    <tr>
                        <th>ประเภทสินทรัพย์</th>
                        <td>{{$memberSelectedAsset -> assettype -> la_nla_type}} </td>
                    </tr>
                    <tr>
                        <th>ประเภทย่อยสินทรัพย์</th>
                        <td>{{$memberSelectedAsset -> assettype -> nla_sub_type}} </td>
                    </tr>
                    <tr>
                        <th>แฟ้มสินทรัพย์</th>
                        <td>{{$memberSelectedAsset -> portfolio -> type}} </td>
                    </tr>
                    <tr>
                        <th>{{$memberSelectedAsset -> assettype -> ref_name_head}}</th>
                        <td>{{$memberSelectedAsset -> ref_name}} </td>
                    </tr>
                    @for($i=1;$i<=3;$i++)
                    @php
                        $refnumhead =  "ref_num_head".$i;
                        $refnum =  "ref_number".$i;
                    @endphp
                    @if(empty($memberSelectedAsset -> $refnum))
                    @else
                    <tr>
                        <th>{{$memberSelectedAsset -> assettype -> $refnumhead }}</th>
                        <td>{{$memberSelectedAsset -> $refnum}} </td>
                    </tr>
                    @endif
                    @endfor

                    @for($i=1;$i<=18;$i++)
                    @php
                        $refinfohead =  "ref_info_head".$i;
                        $refinfo =  "ref_info".$i;
                    @endphp
                    @if(empty($memberSelectedAsset -> $refinfo))
                    @else
                    <tr>
                        <th>{{$memberSelectedAsset -> assettype -> $refinfohead }}</th>
                        <td>{{$memberSelectedAsset -> $refinfo}} </td>
                    </tr>
                    @endif
                    @endfor

                    @endif

                   

                </table>
            </div>
            @if(empty($memberSelectedAsset))
            @else
            <div class="column2" >
                <table>
                    <tr>
                        <th>เอกสารสินทรัพย์</th>
                        <td><a style="color:green" href="/SecurityBroke/asset/uploadfile/{{$memberSelectedAsset -> port_id}}/xxx/CG2CG/Asset_Attachment_{{$memberSelectedAsset -> port_id}}" target="_blank"><i class="fa fa-plus"></i> เพิ่มเอกสาร</a></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td>&nbsp;</td>
                    </tr>
                    @if(count($memberSelectedAssetFile) <= 0)
                    <tr>
                        <th></th>
                        <th>ไม่มีเอกสาร</th>
                    </tr>
                    @else
                        @foreach($memberSelectedAssetFile as $file)
                        <tr>
                            <th>{{$file-> filecat -> name}}</th>
                            <td><a href="/SecurityBroke/showfile/{{$file -> id}}" target="_blank">{{$file -> file_public_name}}</a> </td>
                        </tr>
                        @endforeach
                    @endif
                </table>
            </div>
            @endif
        </div>
    </div>
</div>







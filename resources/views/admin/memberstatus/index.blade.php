@extends('admin.memberstatus.base')
@section('action-content')
<style>

  div.dataTables_wrapper div.dataTables_filter {
        margin-top: 30px;
        width: 100%;
        float: none;
        text-align: left;
    }
</style>
    <!-- Main content -->
    <section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">List of Member Status</h3>
        </div>
        <div class="col-sm-4">
          <a class="btn btn-primary" href="{{ route('memberstatus.create') }}">Add new</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>





    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
			<div style="overflow-x:auto;">
        <table id="datatable" class="table table-bordered table-hover" role="grid" aria-describedby="example2_info">
        <thead>
              <tr role="row">
                <th width="10">No.</th>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($memberStatus as $index=>$data)
                <tr role="row" class="odd">
                  <td>{{++$index}}</td>
                  <td>{{ $data->name }}</td>
                  <td>
                    <form class="row" method="POST" action="{{ route('memberstatus.destroy', ['id' => $data->id]) }}" onsubmit = "return confirm('Are you sure?')">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       
                        <a href="{{ route('memberstatus.edit', ['id' => $data->id]) }}" class="btn btn-warning col-sm-3 col-xs-5 btn-margin">
                        Update
                        </a>
                        <button type="submit" class="btn btn-danger col-sm-3 col-xs-5 btn-margin">
                          Delete
                        </button>
                    </form>
                  </td>
              </tr>
            @endforeach
            </tbody>
            <tfoot>
              <tr>
                <th>No.</th>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
			</div>
        </div>
      </div>

    </div>
  </div>
  <!-- /.box-body -->
</div>
    </section>
    <!-- /.content -->

@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {

        exportFile('#datatable', 'User_Ranking_Report_Total_Cases');

        function exportFile(element, file_name) {

            $(element).DataTable({
                dom: 'Bfrt',
                "paging": true,

                "pageLength": 5000,
                buttons: [

                    /*{
                        text: 'กลับไปหน้าหลัก',
                        className: 'btn btn-primary btn-margin',
                        action: function(e, dt, button, config) {
                            window.location = '/wealththaiinsurance/report/cordinator';
                        }
                    },*/

                    /* {
                        extend: 'copyHtml5',
                        className:'btn btn-default btn-margin',
                        bom: true,
                        title: file_name
                    },
                        text: 'Export CSV',
                        extend: 'csvHtml5',
                        className: 'btn btn-default btn-margin',
                        bom: true,
                        title: file_name,
                        footer: true

                    },
                    {
                        text: 'Export PDF',
                        extend: 'pdfHtml5',
                        className: 'btn btn-default btn-margin',
                        bom: true,
                        title: file_name,
                        footer: true,
                        charset: "utf-8",
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    },*/

                ],

            });
        }

    });
</script>
@endpush
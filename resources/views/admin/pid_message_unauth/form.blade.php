@extends('admin.pid_message_unauth.base')
@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add PID Message Unauthorize</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('pidunauthorizemsg.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Message Category</label>
                            <div class="col-md-6">
                                <select class="form-control selectsearch" name="messageCategory" required>
                                    <option>โปรดเลือก</option>
                                    @foreach($messageCategory as $data)
                                        @if(empty($pidMessageUnauth))
                                            <option value="{{$data->id}}">{{$data->name}}</option>
                                        @else
                                            <option value="{{$data->id}}" {{$data->id == $pidMessageUnauth->msg_category ? 'selected' : ''}}>{{$data->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Message Channel</label>
                            <div class="col-md-6">
                                <select class="form-control selectsearch" name="messageChannel" required>
                                    <option>โปรดเลือก</option>
                                        @if(empty($pidMessageUnauth))
                                            <option value="1">Email</option>
                                            <option value="2">Line</option>
                                        @else
                                            <option value="1" {{1 == $pidMessageUnauth->msg_channel ? 'selected' : ''}}>Email</option>
                                            <option value="2" {{2 == $pidMessageUnauth->msg_channel ? 'selected' : ''}}>Line</option>                                        
                                        @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Public ID</label>
                            <div class="col-md-6">
                                <select class="form-control" name="publicID" required>
                                    <option>โปรดเลือก</option>
                                    @foreach($publicID as $data)
                                        @if(empty($pidMessageUnauth))
                                            <option value="{{$data->id}}">{{$data->public_name}}</option>
                                        @else
                                            <option value="{{$data->id}}" {{$data->id == $pidMessageUnauth->pid ? 'selected' : ''}}>{{$data->public_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @if(empty($pidMessageUnauth))
                        @else
                            <input type="hidden" name="id" value="{{$pidMessageUnauth->id}}"/>
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

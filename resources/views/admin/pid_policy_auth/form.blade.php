@extends('admin.pid_policy_auth.base')
@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add PID Policy Authorization</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('pidpolicyauth.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Policy</label>
                            <div class="col-md-6">
                                <select class="form-control selectsearch" name="policy" required>
                                    <option value="">โปรดเลือก</option>
                                    @foreach($policy as $data)
                                        @if(empty($pidPolicyAuth))
                                            <option value="{{$data->id}}">{{$data->name}}</option>
                                        @else
                                            <option value="{{$data->id}}" {{$data->id == $pidPolicyAuth->policy_id ? 'selected' : ''}}>{{$data->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label ">Public ID</label>
                            <div class="col-md-6">
                                <select class="form-control selectsearch" name="publicID" required>
                                    <option value="">โปรดเลือก</option>
                                    @foreach($publicID as $data)
                                        @if(empty($pidPolicyAuth))
                                            <option value="{{$data->id}}">{{$data->public_name}}</option>
                                        @else
                                            <option value="{{$data->id}}" {{$data->id == $pidPolicyAuth->pid ? 'selected' : ''}}>{{$data->public_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        @if(empty($pidPolicyAuth))
                        @else
                            <input type="hidden" name="id" value="{{$pidPolicyAuth->id}}"/>
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

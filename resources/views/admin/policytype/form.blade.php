@extends('admin.policytype.base')

@section('action-content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Policy Type</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('policytype.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">

                            @if(empty($policyType))
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                            @else
                            <input type="hidden"  type="text" class="form-control" name="id" value="{{$policyType->id}}" >
                            <input id="name" type="text" class="form-control" name="name" value="{{$policyType->name}}" required autofocus>
                            @endif
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">

                            @if(empty($policyType))
                                <textarea id="description" type="text" class="form-control" name="description" ><{{ old('description') }}/textarea>
                            @else
                            <input type="hidden"  type="text" class="form-control" name="id" value="{{$policyType->id}}" >
                            <textarea id="description" type="text" class="form-control" name="description"  >{{$policyType->description}}</textarea>
                            @endif
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

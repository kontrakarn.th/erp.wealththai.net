<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PidMessageUnAuth extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table ='pid_message_unauth';
   
    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */

    public function messagecategory()
    {
    return $this->belongsTo('App\Message_cat','msg_category');
    }
    
    public function publicid()
    {
    return $this->belongsTo('App\match_id','pid');
    }

}

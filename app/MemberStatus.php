<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberStatus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table ='member_status';

   
    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */

}

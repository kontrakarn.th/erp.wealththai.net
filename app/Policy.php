<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $table = 'policy';
    protected $guarded = [];


public function policyType()
{
return $this->belongsTo('App\PolicyType','policy_type');
}

}



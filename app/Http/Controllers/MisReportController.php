<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Block;
use App\CaseChannel;
use App\Casemiddledata;
use App\Cases;
use App\Http\Controllers\DataController;
use App\Offer;
use App\Person;
use App\Portfolio;
use App\Structure;
use App\User;
use App\User_auth;
use App\View;
use App\Proposal;
use App\Asset_cat;
use App\Asset_type;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MisReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $currentID ;

    public function __construct()
    {
        $this->middleware('view');
        $this->datacontroller = New DataController;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function filtercasebyuser(Request $res)
    {
        if ($res->fromdate == '//' || $res->todate == '//' || $res->blockid == '') {
            return '';
        }
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromdate = $res->fromdate;
        $todate = $res->todate;
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findallunderblock($blockstartid);
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->whereIn('id', $underblock)->get();
            $array = [];
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereIn('service_user_block_id', $underblock)
                ->get();
            $fromdate = explode('/', $fromdate);
            $fromdate = $fromdate[2] . $fromdate[1] . $fromdate[0];
            $todate = explode('/', $todate);
            $todate = $todate[2] . $todate[1] . $todate[0];
            foreach ($casesfind as $ca) {

                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromdate && $dateindb <= $todate) {
                    array_push($array, $ca->id);
                }
            }
            $cases = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereIn('service_user_block_id', $array)
                ->pluck('id')->toArray();
            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person', 'cases.block', 'cases.block.structure'])->whereIn('case_id', $array)->get();
            return $casemiddle;
        } else {
            $array = [];
            $userblock = User_auth::where('user_id', $blockid)->pluck('block_id')->toArray();
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->where('service_user_block_id', $userblock)
                ->get();
            $fromdate = explode('/', $fromdate);
            $fromdate = $fromdate[2] . $fromdate[1] . $fromdate[0];
            $todate = explode('/', $todate);
            $todate = $todate[2] . $todate[1] . $todate[0];
            foreach ($casesfind as $ca) {
                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromdate && $dateindb <= $todate) {
                    array_push($array, $ca->id);
                }
            }
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->where('id', $blockid)->get();
            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person', 'cases.block', 'cases.block.structure'])->whereIn('case_id', $array)->get();
            return $casemiddle;
        }

    }
    public function getcolumnchart(Request $res)
    {
        if ($res->fromdate == '//' || $res->todate == '//' || $res->blockid == '') {
            return '';
        }
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromdate = $res->fromdate;
        $todate = $res->todate;
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $userid = $res->blockid;
            $underblock = $datacontoller->findallunderblock($userid);

            $tempStr = implode(',', $underblock);
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])
                ->whereIn('id', $underblock)
                ->orderByRaw(DB::raw("FIELD(id, $tempStr)"))
                ->get();
            $column = array();
            foreach ($block as $b) {
                if (!in_array($b->Structure->name, $column)) {
                    array_push($column, $b->Structure->name);
                }
            }
            return $column;
        } else {
            $cases = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->where('service_user_block_id', $res->blockid)->get();
            $userblock = User_auth::where('user_id', $blockid)->pluck('block_id')->toArray();
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->whereIn('id', $userblock)->get();
            $column = array();
            foreach ($block as $b) {
                if (!in_array($b->Structure->name, $column)) {
                    array_push($column, $b->Structure->name);
                }
            }
            return $column;
        }
    }

    public function filterblockbyuser($res)
    {

        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $this->getFromdate($res);
        $toDate = $this->getTodate($res);
        if($res->structureId == "0"){
                $blockstartid = $this->datacontroller->getblockid();
                $underblock = $this->datacontroller->findunderblock($blockstartid);
                $tempStr = implode(',', $underblock);
                $block = Block::with(['Cases', 'Structure', 'belongtoblock'])
                    ->whereIn('id', $underblock)
                    ->orderByRaw(DB::raw("FIELD(id, $tempStr)"))
                    ->get(['id','name','structure_id','under_block']);
                return $block;
        }
        else{
                if ($checkunderblock == 1) {
                $userid = $res->blockid;
                $underblock = $this->datacontroller->findallunderblock($userid);
                $tempStr = implode(',', $underblock);
                $block = Block::with(['Cases', 'Structure', 'belongtoblock'])
                    ->whereIn('id', $underblock)
                    ->orderByRaw(DB::raw("FIELD(id, $tempStr)"))
                    ->get(['id','name','structure_id','under_block']);
                return $block;
            } else {
                $userblock = User_auth::where('user_id', $blockid)->pluck('block_id')->toArray();
                $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->whereIn('id', $userblock)->get();
                return $block;
            }
    }
    }
    public function getliquidityasset($res)
    {
    $fromDate = $res->fromYear. '-' . $res->fromMonth . '-' . $res->fromDay ;
    $toDate = $res->toYear . '-' . $res->toMonth . '-' . $res->toDay;
    $blockid = $res->blockid;
    $checkunderblock = $res->underblock;
    $datacontoller = new DataController();
    if($res->structureId == "0"){
        $blockstartid = $datacontoller->getblockid();
        $underblock = $datacontoller->findunderblock($blockstartid);
        $array = [];
        $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])
        ->whereIn('service_user_block_id', $underblock)
        ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)    
        ->where('case_status', 2)
        ->pluck('id')->toArray();

        if(!empty($res->fromYearvalid) && !empty($res->fromMonthvalid) && !empty($res->fromDayvalid) && !empty($res->toYearvalid) && !empty($res->toMonthvalid) && !empty($res->toDayvalid)){
            
            $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $casesfind)->pluck('asset_id');
            if($res->assettypeid == "0"){
                $casemiddledata = Asset::whereIn('id',$casemiddledata)->pluck('id');
            }else{
                $casemiddledata = Asset::whereIn('id',$casemiddledata)->where('la_nla_type',$res->assettypeid)->pluck('id');
            }
            $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('asset_id', $casemiddledata)->get();
            $casemiddledatatID = [];
            $fromDatevalid = $res->fromYearvalid.$res->fromMonthvalid.$res->fromDayvalid;
            $toDatevalid = $res->toYearvalid.$res->toMonthvalid.$res->toDayvalid;
            foreach($casemiddledata as $index => $data){
                //Date Compare
                if(!empty($data->asset_id)){

                    $assetValidfrom = $data->asset->valid_from;
                    $assetValidfrom = explode('/',$assetValidfrom);
                    if(!empty($assetValidfrom[2]) && !empty($assetValidfrom[1]) && !empty($assetValidfrom[0])){
                        $assetValidfrom = $assetValidfrom[2].$assetValidfrom[1].$assetValidfrom[0];
                    }else{ 
                        $assetValidfrom = 0;
                    }
                    $assetValidto = $data->asset->valid_to;
                    $assetValidto = explode('/',$assetValidto);
                    if(!empty($assetValidto[2]) && !empty($assetValidto[1]) && !empty($assetValidto[0])){
                        $assetValidto = $assetValidto[2].$assetValidto[1].$assetValidto[0];
                    }else{
                        $assetValidto = 999999999;
                    }
                    //check input valid from date >= asset valid from date 
                    if($assetValidfrom >= $fromDatevalid && $assetValidto <= $toDatevalid){ 
                        $casemiddledatatID[$index] = $data->id;
                    }
                }

            }
            $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('id', $casemiddledatatID)->get();
        }else{
            $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $casesfind)->pluck('asset_id');
            if($res->assettypeid == "0"){
                $casemiddledata = Asset::whereIn('id',$casemiddledata)->pluck('id');
            }else{
                $casemiddledata = Asset::whereIn('id',$casemiddledata)->where('la_nla_type',$res->assettypeid)->pluck('id');
            }
            $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('asset_id', $casemiddledata)->get();
        }
        return $casemiddledata;
    }
    else{
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findunderblock($blockstartid);
            $array = [];
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])
            ->whereIn('service_user_block_id', $underblock)
            ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)    
            ->where('case_status', 2)
            ->pluck('id')->toArray();
    
            if(!empty($res->fromYearvalid) && !empty($res->fromMonthvalid) && !empty($res->fromDayvalid) && !empty($res->toYearvalid) && !empty($res->toMonthvalid) && !empty($res->toDayvalid)){
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $casesfind)->pluck('asset_id');
                if($res->assettypeid == "0"){
                    $casemiddledata = Asset::whereIn('id',$casemiddledata)->pluck('id');
                }else{
                    $casemiddledata = Asset::whereIn('id',$casemiddledata)->where('la_nla_type',$res->assettypeid)->pluck('id');
                }             
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('asset_id', $casemiddledata)->get();                $casemiddledatatID = [];
                $fromDatevalid = $res->fromYearvalid.$res->fromMonthvalid.$res->fromDayvalid;
                $toDatevalid = $res->toYearvalid.$res->toMonthvalid.$res->toDayvalid;
                foreach($casemiddledata as $index => $data){
                    //Date Compare
                    if(!empty($data->asset_id)){
    
                        $assetValidfrom = $data->asset->valid_from;
                        $assetValidfrom = explode('/',$assetValidfrom);
                        if(!empty($assetValidfrom[2]) && !empty($assetValidfrom[1]) && !empty($assetValidfrom[0])){
                            $assetValidfrom = $assetValidfrom[2].$assetValidfrom[1].$assetValidfrom[0];
                        }else{ 
                            $assetValidfrom = 0;
                        }
    
                        $assetValidto = $data->asset->valid_to;
                        $assetValidto = explode('/',$assetValidto);
                        if(!empty($assetValidto[2]) && !empty($assetValidto[1]) && !empty($assetValidto[0])){
                            $assetValidto = $assetValidto[2].$assetValidto[1].$assetValidto[0];
                        }else{
                            $assetValidto = 999999999;
                        }
    
                        //check input valid from date >= asset valid from date 
    
                        if($assetValidfrom >= $fromDatevalid && $assetValidto <= $toDatevalid){ 
                            $casemiddledatatID[$index] = $data->id;
                        }
                    }
    
                }
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('id', $casemiddledatatID)->get();
            }else{
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $casesfind)->pluck('asset_id');
                if($res->assettypeid == "0"){
                    $casemiddledata = Asset::whereIn('id',$casemiddledata)->pluck('id');
                }else{
                    $casemiddledata = Asset::whereIn('id',$casemiddledata)->where('la_nla_type',$res->assettypeid)->pluck('id');
                }
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('asset_id', $casemiddledata)->get();
            }
            return $casemiddledata;
        } else {
            $array = [];
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])
            ->where('service_user_block_id', $res->blockid)
            ->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)    
            ->where('case_status', 2)
            ->pluck('id')->toArray();
    
            if(!empty($res->fromYearvalid) && !empty($res->fromMonthvalid) && !empty($res->fromDayvalid) && !empty($res->toYearvalid) && !empty($res->toMonthvalid) && !empty($res->toDayvalid)){
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $casesfind)->pluck('asset_id');
                if($res->assettypeid == "0"){
                    $casemiddledata = Asset::whereIn('id',$casemiddledata)->pluck('id');
                }else{
                    $casemiddledata = Asset::whereIn('id',$casemiddledata)->where('la_nla_type',$res->assettypeid)->pluck('id');
                }
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('asset_id', $casemiddledata)->get();                
                $casemiddledatatID = [];
                $fromDatevalid = $res->fromYearvalid.$res->fromMonthvalid.$res->fromDayvalid;
                $toDatevalid = $res->toYearvalid.$res->toMonthvalid.$res->toDayvalid;
                foreach($casemiddledata as $index => $data){
                    //Date Compare
                    if(!empty($data->asset_id)){
    
                        $assetValidfrom = $data->asset->valid_from;
                        $assetValidfrom = explode('/',$assetValidfrom);
                        if(!empty($assetValidfrom[2]) && !empty($assetValidfrom[1]) && !empty($assetValidfrom[0])){
                            $assetValidfrom = $assetValidfrom[2].$assetValidfrom[1].$assetValidfrom[0];
                        }else{ 
                            $assetValidfrom = 0;
                        }
    
                        $assetValidto = $data->asset->valid_to;
                        $assetValidto = explode('/',$assetValidto);
                        if(!empty($assetValidto[2]) && !empty($assetValidto[1]) && !empty($assetValidto[0])){
                            $assetValidto = $assetValidto[2].$assetValidto[1].$assetValidto[0];
                        }else{
                            $assetValidto = 999999999;
                        }
    
                        //check input valid from date >= asset valid from date 
    
                        if($assetValidfrom >= $fromDatevalid && $assetValidto <= $toDatevalid){ 
                            $casemiddledatatID[$index] = $data->id;
                        }
                    }
    
                }
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('id', $casemiddledatatID)->get();
            }else{
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $casesfind)->pluck('asset_id');
                if($res->assettypeid == "0"){
                    $casemiddledata = Asset::whereIn('id',$casemiddledata)->pluck('id');
                }else{
                    $casemiddledata = Asset::whereIn('id',$casemiddledata)->where('la_nla_type',$res->assettypeid)->pluck('id');
                }
                $casemiddledata = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('asset_id', $casemiddledata)->get();
                }
            return $casemiddledata;
        }
    }

    }

    public function coordinatorfiltercase(Request $res)
    {
        ///*
        $coorId = $res->coorId;
        $checkunderblock = $res->underBlock;
        $fromDate = $res->fromDay . '/' . $res->fromMonth . '/' . $res->fromYear;
        $toDate = $res->toDay . '/' . $res->toMonth . '/' . $res->toYear;
        //*/
        /*
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $res->fromdate;
        $toDate = $res->todate;
         */
        $structure = $this->datacontroller->getstructureAuth();
        $coorDinatorid = User_auth::where('block_id', 72)->pluck('user_id')->toArray();
        $coorDinator = User::whereIn('id', $coorDinatorid)->get(['id', 'firstname', 'lastname']);
        if ($res->coorId == "all") {
            $array = [];
            $casesfind = Cases::whereIn('coordinate_user_block_id', $coorDinatorid)
                ->get(['finish_date', 'id']);
            $fromDate = explode('/', $fromDate);
            $fromDate = $fromDate[2] . $fromDate[1] . $fromDate[0];
            $toDate = explode('/', $toDate);
            $toDate = $toDate[2] . $toDate[1] . $toDate[0];
            foreach ($casesfind as $ca) {
                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromDate && $dateindb <= $toDate) {
                    array_push($array, $ca->id);
                }
            }
            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $array)->pluck('case_id')->toArray();
            $findmember = Cases::whereIn('id', $casemiddle)->pluck('member_case_owner')->toArray();
            $member = Person::whereIn('id', $findmember)->get();
            $offer = Cases::rightjoin('users', 'cases.coordinate_user_block_id', 'users.id')
                ->whereIn('cases.id', $array)
                ->rightjoin('case_middle_data', 'case_middle_data.case_id', 'cases.id')
                ->rightjoin('offer', 'case_middle_data.offer_id', 'offer.id')
                ->select([DB::raw("COUNT(coordinate_user_block_id) as total_case"), DB::raw("SUM(offer.offer_payment_value10) as total_fee"), DB::raw("SUM(offer.offer_payment_value9) as total_taxfee"), DB::raw("SUM(offer.offer_payment_value11) as total_otherfee"), DB::raw("SUM(offer.offer_payment_value21) as company_income"), 'users.id as user_id', 'users.firstname as user_name'])
                ->groupBy('users.id')->orderBy('total_case', 'DESC')
                ->get();
            $sumTotalcase = 0;
            $sumTotalfee = 0;

            foreach ($offer as $data) {
                $sumTotalcase += $data->total_case;
                $sumTotalfee += $data->total_fee;
            }
            //return $offer;
            $coorId = $res->coorId;
            $checkunderblock = $res->underBlock;
            $fromDay = $res->fromDay;
            $fromMonth = $res->fromMonth;
            $fromYear = $res->fromYear;
            $toDay = $res->toDay;
            $toMonth = $res->toMonth;
            $toYear = $res->toYear;
            $structureId = $res->structureId;
            return view('system-mgmt/insurance/misreport/coordinator', compact(['coorId', 'coorDinator', 'structure', 'offer', 'sumTotalcase', 'sumTotalfee', 'checkunderblock',
                'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear', 'structureId', 'block']));
        } else {
            $array = [];
            $casesfind = Cases::where('coordinate_user_block_id', $res->coorId)
                ->get(['finish_date', 'id']);
            $fromDate = explode('/', $fromDate);
            $fromDate = $fromDate[2] . $fromDate[1] . $fromDate[0];
            $toDate = explode('/', $toDate);
            $toDate = $toDate[2] . $toDate[1] . $toDate[0];
            foreach ($casesfind as $ca) {
                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromDate && $dateindb <= $toDate) {
                    array_push($array, $ca->id);
                }
            }

            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $array)->pluck('case_id')->toArray();
            $findmember = Cases::whereIn('id', $casemiddle)->pluck('member_case_owner')->toArray();
            $member = Person::whereIn('id', $findmember)->get();
            $offer = Cases::rightjoin('users', 'cases.coordinate_user_block_id', 'users.id')
                ->whereIn('cases.id', $array)
                ->rightjoin('case_middle_data', 'case_middle_data.case_id', 'cases.id')
                ->rightjoin('offer', 'case_middle_data.offer_id', 'offer.id')
                ->select([DB::raw("COUNT(coordinate_user_block_id) as total_case"), DB::raw("SUM(offer.offer_payment_value10) as total_fee"), DB::raw("SUM(offer.offer_payment_value9) as total_taxfee"), DB::raw("SUM(offer.offer_payment_value11) as total_otherfee"), DB::raw("SUM(offer.offer_payment_value21) as company_income"), 'users.id as user_id', 'users.firstname as user_name'])
                ->groupBy('users.id')->orderBy('total_case', 'DESC')
                ->get();
            $sumTotalcase = 0;
            $sumTotalfee = 0;

            foreach ($offer as $data) {
                $sumTotalcase += $data->total_case;
                $sumTotalfee += $data->total_fee;
            }
            //return $offer;
            $coorId = $res->coorId;
            $checkunderblock = $res->underBlock;
            $fromDay = $res->fromDay;
            $fromMonth = $res->fromMonth;
            $fromYear = $res->fromYear;
            $toDay = $res->toDay;
            $toMonth = $res->toMonth;
            $toYear = $res->toYear;
            $block = Block::where('id', $res->blockId)->first();
            $structureId = $res->structureId;
            $coorDinatorid = User_auth::where('block_id', 72)->pluck('user_id')->toArray();
            $coorDinator = User::whereIn('id', $coorDinatorid)->get(['id', 'firstname', 'lastname']);
            return view('system-mgmt/insurance/misreport/coordinator', compact(['coorId', 'coorDinator', 'structure', 'offer', 'sumTotalcase', 'sumTotalfee', 'checkunderblock',
                'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear', 'structureId', 'block']));
        }
    }
    public function s(Request $res)
    {
        if ($res->fromdate == '//' || $res->todate == '//') {
            return '';
        } else {
            $blockid = $res->blockid;
            $checkunderblock = $res->underblock;
            $fromdate = $res->fromdate;
            $todate = $res->todate;
            $array = [];
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereNotNull('coordinate_user_block_id')
                ->get();
            $fromdate = explode('/', $fromdate);
            $fromdate = $fromdate[2] . $fromdate[1] . $fromdate[0];
            $todate = explode('/', $todate);
            $todate = $todate[2] . $todate[1] . $todate[0];
            foreach ($casesfind as $ca) {
                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromdate && $dateindb <= $todate) {
                    array_push($array, $ca->id);
                }
            }
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->where('id', $blockid)->get();
            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $array)->pluck('case_id')->toArray();
            $findmember = Cases::whereIn('id', $casemiddle)->pluck('member_case_owner')->toArray();
            $member = Person::whereIn('id', $findmember)->get();
            /*$casemiddle = Offer::rightjoin('proposal','offer.proposal_id','proposal.id')
            ->rightjoin('cases','proposal.case_id','cases.id')
            ->rightjoin('case_middle_data','case_middle_data.case_id','cases.id')
            ->rightjoin('users','cases.coordinate_user_block_id','users.id')
            ->whereIn('cases.id',$array)
            ->select([DB::raw("SUM(offer_payment_value10) as total_case"),'users.id as user_id','users.firstname as user_name'])
            ->groupBy('users.id')->orderBy('total_case','DESC')
            ->get();
            return $casemiddle;*/
            $offer = Cases::rightjoin('users', 'cases.coordinate_user_block_id', 'users.id')

                ->whereIn('cases.id', $array)
                ->select([DB::raw("COUNT(coordinate_user_block_id) as total_case"), 'users.id as user_id', 'users.firstname as user_name'])
                ->groupBy('users.id')->orderBy('total_case', 'DESC')
                ->get();
            return $offer;
        }

    }
    public function getstructure()
    {
        return Structure::all();
    }

    public function teamperformance()
    {
        $structure = $this->datacontroller->getstructureAuth();
        $i = 0;
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        return view('system-mgmt/insurance/misreport/teamperformance', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'structureId', 'cases', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function teamperformancesearch(Request $request)
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];

        $blockList = $this->filterblock($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();

        $arrayBlockID = [];
        foreach ($blockList as $data) {
            array_push($arrayBlockID, $data->id);
        }
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);

        $findCase = Cases::whereIn('service_user_block_id', $arrayBlockID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
        $findProposal = Proposal::whereIn('case_id', $findCase)->pluck('id')->toArray();
        $sumTotalfee = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value19');
        $sumTotalcase = 1;
        $underBlock = $request['underblock'];

        return view('system-mgmt/insurance/misreport/teamperformance', compact('fromDate','toDate','underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'cases', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }
    
    private function getFromdate($request){
        $fromDate = $request->fromYear. '-' . $request->fromMonth . '-' . $request->fromDay ;
        return $fromDate;
    }

    private function getTodate($request){
        $toDate = $request->toYear. '-' . $request->toMonth . '-' . $request->toDay ;
        return $toDate;
    }
    public function loadcase()
    {
        return Cases::all();
    }
    public function getblock()
    {
        return Block::all();
    }
    public function getuserinstructure()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = explode("?filterstructure", $url);
        $url = $url[1];
        $userauths = User_auth::where('structure_id', $url)->pluck('user_id')->toArray();
        array_unique($userauths);
        return User::whereIn('id', $userauths)->get();
    }
    public function filterblock($res)
    {

        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $datacontoller = new DataController();
        if($res->structureId == "0"){
            $blockstartid = $datacontoller->getblockid();
            $underblock = $datacontoller->findunderblock($blockstartid);

            $tempStr = implode(',', $underblock);
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])
                    ->whereIn('id', $underblock)
                    ->orderByRaw(DB::raw("FIELD(id, $tempStr)"))
                    ->get();
            return $block;
        }else{
            if ($checkunderblock == 1) {
                    $blockstartid = $res->blockid;
                    $underblock = $datacontoller->findunderblock($blockstartid);
                    $tempStr = implode(',', $underblock);
                    $block = Block::with(['Cases', 'Structure', 'belongtoblock'])
                             ->whereIn('id', $underblock)
                             ->orderByRaw(DB::raw("FIELD(id, $tempStr)"))
                             ->get();
                return $block;
            } else {

                    $cases = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->where('service_user_block_id', $res->blockid)->get();
                    $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->where('id', $blockid)->get();

                return $block;
            }
        }

    }
    public function filtercase(Request $res)
    {
        if ($res->fromdate == '//' || $res->todate == '//' || $res->blockid == '') {
            return '';
        }
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $res->fromDay . '/' . $res->fromMonth . '/' . $res->fromYear;
        $toDate = $res->toDay . '/' . $res->toMonth . '/' . $res->toYear;
        $fromDate = explode('/', $fromDate);
        $fromDate = $fromDate[2] . $fromDate[1] . $fromDate[0];
        $toDate = explode('/', $toDate);
        $toDate = $toDate[2] . $toDate[1] . $toDate[0];
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findunderblock($blockstartid);
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->whereIn('id', $underblock)->get();
            $array = [];
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereIn('service_user_block_id', $underblock)
                ->get();
        
            foreach ($casesfind as $ca) {

                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromDate && $dateindb <= $toDate) {
                    array_push($array, $ca->id);
                }
            }
            $cases = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereIn('service_user_block_id', $array)
                ->pluck('id')->toArray();
            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $array)->get();
            return $casemiddle;
        } else {
            $array = [];
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->where('service_user_block_id', $res->blockid)
                ->get();
            
            foreach ($casesfind as $ca) {
                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromDate && $dateindb <= $toDate) {
                    array_push($array, $ca->id);
                }
            }

            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->where('id', $blockid)->get();
            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $array)->get();
            return $casemiddle;
        }

    }
    public function filteruser($res)
    {
        if ($res->fromdate == '//' || $res->todate == '//' || $res->blockid == '') {
            return '';
        }
        
        $datacontoller = new DataController();
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $this->getFromdate($res);
        $toDate = $this->getTodate($res);
        if($res->structureId == "0"){
            $blockstartid = $datacontoller->getblockid();
            $underblock = $datacontoller->findunderblock($blockstartid);
            $casemiddle = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                ->whereIn('cases.service_user_block_id',$underblock)
                ->rightjoin('case_middle_data', 'case_middle_data.case_id', 'cases.id')
                ->rightjoin('block', 'cases.service_user_block_id', 'block.id')
                ->rightjoin('structure', 'block.structure_id', 'structure.id')
                ->rightjoin('block as block_under', 'block.under_block', 'block_under.id')
                ->select([DB::raw("SUM(offer_payment_value19) as total_premium"), 'structure.name as structure_name', 'block.id as block_id', 'block.name', 'block_under.name as under_block_name'])
                ->groupBy('block.id')->orderBy('total_premium', 'DESC')
                ->get();
            return $casemiddle;
        }else{
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findunderblock($blockstartid);
            $casemiddle = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                ->whereIn('cases.service_user_block_id',$underblock)
                ->rightjoin('case_middle_data', 'case_middle_data.case_id', 'cases.id')
                ->rightjoin('block', 'cases.service_user_block_id', 'block.id')
                ->rightjoin('structure', 'block.structure_id', 'structure.id')
                ->rightjoin('block as block_under', 'block.under_block', 'block_under.id')
                ->select([DB::raw("SUM(offer_payment_value19) as total_premium"), 'structure.name as structure_name', 'block.id as block_id', 'block.name', 'block_under.name as under_block_name'])
                ->groupBy('block.id')->orderBy('total_premium', 'DESC')
                ->get();
            return $casemiddle;
        } else {
            $casemiddle = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                ->where('cases.service_user_block_id',$res->blockid)
                ->rightjoin('case_middle_data', 'case_middle_data.case_id', 'cases.id')
                ->rightjoin('block', 'cases.service_user_block_id', 'block.id')
                ->rightjoin('structure', 'block.structure_id', 'structure.id')
                ->rightjoin('block as block_under', 'block.under_block', 'block_under.id')
                ->select([DB::raw("SUM(offer_payment_value19) as total_premium"), 'structure.name as structure_name', 'block.id as block_id', 'block.name', 'block_under.name as under_block_name'])
                ->groupBy('block.id')->orderBy('total_premium', 'DESC')
                ->get();
            return $casemiddle;
        }
    }

    }
    public function filterreturncase(Request $res)
    {
        if ($res->fromdate == '//' || $res->todate == '//' || $res->blockid == '') {
            return '';
        }
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromdate = $res->fromdate;
        $todate = $res->todate;
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findunderblock($blockstartid);
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->whereIn('id', $underblock)->get();
            $array = [];
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereIn('service_user_block_id', $underblock)
                ->get();
            $fromdate = explode('/', $fromdate);
            $fromdate = $fromdate[2] . $fromdate[1] . $fromdate[0];
            $todate = explode('/', $todate);
            $todate = $todate[2] . $todate[1] . $todate[0];
            foreach ($casesfind as $ca) {

                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromdate && $dateindb <= $todate) {
                    array_push($array, $ca->id);
                }
            }
            $cases = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereIn('service_user_block_id', $array)
                ->pluck('id')->toArray();
            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $array)->pluck('case_id')->toArray();
            array_unique($casemiddle);
            $case = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereIn('id', $casemiddle)->get();
            return $case;
        } else {
            $array = [];
            $casesfind = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->where('service_user_block_id', $res->blockid)
                ->get();
            $fromdate = explode('/', $fromdate);
            $fromdate = $fromdate[2] . $fromdate[1] . $fromdate[0];
            $todate = explode('/', $todate);
            $todate = $todate[2] . $todate[1] . $todate[0];
            foreach ($casesfind as $ca) {
                $dateindb = explode('/', $ca->finish_date);
                if ($ca->finish_date == null || $ca->finish_date == '' || $ca->finish_date == 0 || $ca->finish_date == '//') {
                    $dateindb = 99999999999999999;
                } else {
                    $dateindb = $dateindb[2] . $dateindb[1] . $dateindb[0];
                }
                if ($dateindb >= $fromdate && $dateindb <= $todate) {
                    array_push($array, $ca->id);
                }
            }

            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->where('id', $blockid)->get();
            $casemiddle = Casemiddledata::with(['offer', 'cases', 'cases.person'])->whereIn('case_id', $array)->pluck('case_id')->toArray();
            array_unique($casemiddle);
            $case = Cases::with(['Person', 'Stage', 'Cases', 'Block.belongtoblock', 'Block.structure', 'Partner_block', 'CaseType', 'CaseSubType', 'Asset', 'match_id', 'CaseStatus', 'coordiantor', 'CaseChannel'])->whereIn('id', $casemiddle)->get();
            return $case;
        }

    }
    public function filtercustomer($res)
    {

        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $this->getFromdate($res);
        $toDate = $this->getTodate($res);
            if($res->structureId == "0"){
                $blockstartid = $this->datacontroller->getblockid();
                $underblock = $this->datacontroller->findunderblock($blockstartid);

                $offer = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)
                ->whereIn('cases.service_user_block_id',$underblock)
                ->rightjoin('case_middle_data', 'case_middle_data.offer_id', 'offer.id')
                ->rightjoin('portfolio', 'portfolio.id', 'case_middle_data.port_id')
                ->rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                ->select([DB::raw("SUM(offer_payment_value1) as total_premium"), 'offer.*', 'proposal.*', 'persons.name', 'persons.lname'])
                ->groupBy('cases.member_case_owner')->orderBy('total_premium', 'DESC')
                ->get();
            return $offer;
        }else{

            if ($checkunderblock == 1) {
            $blockstartid = $res->blockid;
            $underblock = $this->datacontroller->findunderblock($blockstartid);
            $block = Block::with(['Cases', 'Structure', 'belongtoblock'])->whereIn('id', $underblock)->get();
            $offer = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                ->rightjoin('case_middle_data', 'case_middle_data.offer_id', 'offer.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)
                ->whereIn('cases.service_user_block_id',$underblock)
                ->rightjoin('portfolio', 'portfolio.id', 'case_middle_data.port_id')
                ->rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                ->select([DB::raw("SUM(offer_payment_value1) as total_premium"), 'offer.*', 'proposal.*', 'persons.name', 'persons.lname'])
                ->groupBy('cases.member_case_owner')->orderBy('total_premium', 'DESC')
                ->get();
            return $offer;
        } else {
            
            $offer = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)
                ->where('cases.service_user_block_id',$res->blockid)
                ->rightjoin('case_middle_data', 'case_middle_data.offer_id', 'offer.id')
                ->rightjoin('portfolio', 'portfolio.id', 'case_middle_data.port_id')
                ->rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                ->select([DB::raw("SUM(offer_payment_value1) as total_premium"), 'offer.*', 'proposal.*', 'persons.name', 'persons.lname'])
                ->groupBy('cases.member_case_owner')->orderBy('total_premium', 'DESC')
                ->get();

            return $offer;
        }
    }


    }

    public function filterport($res)
    {
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $this->getFromdate($res);
        $toDate = $this->getTodate($res);
            if($res->structureId == "0"){
                $blockstartid = $this->datacontroller->getblockid();
                $underblock = $this->datacontroller->findunderblock($blockstartid);
                $offer = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                ->whereIn('cases.service_user_block_id',$underblock)
                ->rightjoin('case_middle_data', 'case_middle_data.offer_id', 'offer.id')
                ->rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                ->rightjoin('portfolio', 'portfolio.id', 'case_middle_data.port_id')
                ->rightjoin('block', 'portfolio.block_id', 'block.id')
                ->select([DB::raw("SUM(offer_payment_value1) as total_premium"), 'persons.name', 'persons.lname', 'portfolio.type as port_name', 'portfolio.id as port_id', 'block.name as blockportname'])
                ->groupBy('portfolio.id')->orderBy('total_premium', 'DESC')->get();
            return $offer;
        }else {
            if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findunderblock($blockstartid);
            $offer = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                ->whereIn('cases.service_user_block_id',$underblock)
                ->rightjoin('case_middle_data', 'case_middle_data.offer_id', 'offer.id')
                ->rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                ->rightjoin('portfolio', 'portfolio.id', 'case_middle_data.port_id')
                ->rightjoin('block', 'portfolio.block_id', 'block.id')
                ->select([DB::raw("SUM(offer_payment_value1) as total_premium"), 'persons.name', 'persons.lname', 'portfolio.type as port_name', 'portfolio.id as port_id', 'block.name as blockportname'])
                ->groupBy('portfolio.id')->orderBy('total_premium', 'DESC')->get();
            return $offer;
            } else {
                $offer = Offer::rightjoin('proposal', 'offer.proposal_id', 'proposal.id')
                    ->rightjoin('cases', 'proposal.case_id', 'cases.id')
                    ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                    ->where('cases.service_user_block_id',$res->blockid)
                    ->rightjoin('case_middle_data', 'case_middle_data.offer_id', 'offer.id')
                    ->rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                    ->rightjoin('portfolio', 'portfolio.id', 'case_middle_data.port_id')
                    ->rightjoin('block', 'portfolio.block_id', 'block.id')
                    ->select([DB::raw("SUM(offer_payment_value1) as total_premium"), 'persons.name', 'persons.lname', 'portfolio.type as port_name', 'portfolio.id as port_id', 'block.name as blockportname'])
                    ->groupBy('portfolio.id')->orderBy('total_premium', 'DESC')->get();
                return $offer;
            }
        }
    }
    public function filterportcase(Request $res)
    {

        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $this->getFromdate($res);
        $toDate = $this->getTodate($res);
            if($res->structureId == "0"){
                $blockstartid = $this->datacontroller->getblockid();
                $underblock = $this->datacontroller->findunderblock($blockstartid);
                $offer = Cases::rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                ->rightjoin('portfolio', 'portfolio.member_id', 'persons.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                ->whereIn('cases.service_user_block_id',$underblock)
                ->rightjoin('block', 'portfolio.block_id', 'block.id')
                ->rightjoin('portfolio', 'portfolio.member_id', 'persons.id')
                ->select([DB::raw("COUNT(member_case_owner) as total_case"), 'block.name as block_port_name', 'portfolio.id as port_id', 'portfolio.type as port_name', 'persons.name', 'persons.lname'])
                ->groupBy('portfolio.id')->orderBy('total_case', 'DESC')
                ->get();
            return $offer;
        }else{
                if ($checkunderblock == 1) {
                    $datacontoller = new DataController();
                    $blockstartid = $res->blockid;
                    $underblock = $datacontoller->findunderblock($blockstartid);
                    $offer = Cases::rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                            ->rightjoin('portfolio', 'portfolio.member_id', 'persons.id')
                            ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                            ->whereIn('cases.service_user_block_id',$underblock)
                            ->rightjoin('block', 'portfolio.block_id', 'block.id')
                            ->rightjoin('portfolio', 'portfolio.id', 'case_middle_data.port_id')
                            ->select([DB::raw("COUNT(member_case_owner) as total_case"), 'block.name as block_port_name', 'portfolio.id as port_id', 'portfolio.type as port_name', 'persons.name', 'persons.lname'])
                            ->groupBy('portfolio.id')->orderBy('total_case', 'DESC')
                            ->get();
                    return $offer;
                } else {
                    $offer = Cases::rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                            ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                            ->where('cases.service_user_block_id',$res->blockid)
                            ->rightjoin('portfolio', 'portfolio.id', 'case_middle_data.port_id')
                            ->rightjoin('block', 'portfolio.block_id', 'block.id')
                            ->select([DB::raw("COUNT(member_case_owner) as total_case"), 'block.name as block_port_name', 'portfolio.id as port_id', 'portfolio.type as port_name', 'persons.name', 'persons.lname'])
                            ->groupBy('portfolio.id')->orderBy('total_case', 'DESC')
                            ->get();
                    return $offer;
                }
        }
    }
    public function filtercustomercase(Request $res)
    {
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $this->getFromdate($res);
        $toDate = $this->getTodate($res);
            if($res->structureId == "0"){
                $blockstartid = $this->datacontroller->getblockid();
                $underblock = $this->datacontroller->findunderblock($blockstartid);
                $offer = Cases::rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)
                ->whereIn('cases.service_user_block_id',$underblock)
                ->select([DB::raw("COUNT(member_case_owner) as total_case"), 'persons.name', 'persons.lname'])
                ->groupBy('persons.id')->orderBy('total_case', 'DESC')
                ->get();
            return $offer;
        }else{
        if ($checkunderblock == 1) {
            $blockstartid = $res->blockid;
            $underblock = $this->datacontroller->findunderblock($blockstartid);
            $offer = Cases::rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                    ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)
                    ->whereIn('cases.service_user_block_id',$underblock)
                    ->select([DB::raw("COUNT(member_case_owner) as total_case"), 'persons.name', 'persons.lname'])
                    ->groupBy('persons.id')->orderBy('total_case', 'DESC')
                    ->get();
            return $offer;
        } else {
            $offer = Cases::rightjoin('persons', 'cases.member_case_owner', 'persons.id')
                    ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)
                    ->where('cases.service_user_block_id',$res->blockid)
                    ->select([DB::raw("COUNT(member_case_owner) as total_case"), 'persons.name', 'persons.lname'])
                    ->groupBy('persons.id')->orderBy('total_case', 'DESC')
                    ->get();
            return $offer;
        }
    }
    }
    public function filterusercase($res)
    {

        $datacontoller = new DataController();
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        $fromDate = $this->getFromdate($res);
        $toDate = $this->getTodate($res);
        if($res->structureId == "0"){
            $blockstartid = $datacontoller->getblockid();
             $underblock = $datacontoller->findunderblock($blockstartid);
            $offer = Cases::rightjoin('block', 'cases.service_user_block_id', 'block.id')
            ->rightjoin('structure', 'block.structure_id', 'structure.id')
            ->rightjoin('block as block_under', 'block.under_block', 'block_under.id')
            ->whereIn('cases.service_user_block_id',$underblock)
            ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
            ->select([DB::raw("COUNT(service_user_block_id) as total_case"), 'structure.name as structure_name', 'block.id as block_id', 'block.name', 'block_under.name as block_under_name'])
            ->groupBy('block.id')->orderBy('total_case', 'DESC')
            ->get();
        return $offer;
        }else{
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findunderblock($blockstartid);
            $offer = Cases::rightjoin('block', 'cases.service_user_block_id', 'block.id')
                ->whereIn('cases.service_user_block_id',$underblock)
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                ->rightjoin('structure', 'block.structure_id', 'structure.id')
                ->rightjoin('block as block_under', 'block.under_block', 'block_under.id')
                ->select([DB::raw("COUNT(service_user_block_id) as total_case"), 'structure.name as structure_name', 'block.id as block_id', 'block.name', 'block_under.name as block_under_name'])
                ->groupBy('block.id')->orderBy('total_case', 'DESC')
                ->get();
            return $offer;
        } else {

            $offer = Cases::rightjoin('block', 'cases.service_user_block_id', 'block.id')
                ->where('cases.service_user_block_id',$res->blockid)
                ->whereDate('cases.created_at','>=',$fromDate)->whereDate('cases.created_at','<=',$toDate)->where('cases.case_status', 2)    
                ->rightjoin('structure', 'block.structure_id', 'structure.id')
                ->rightjoin('block as block_under', 'block.under_block', 'block_under.id')
                ->select([DB::raw("COUNT(service_user_block_id) as total_case"), 'structure.name as structure_name', 'block.id as block_id', 'block.name', 'block_under.name as block_under_name'])
                ->groupBy('block.id')->orderBy('total_case', 'DESC')
                ->get();
            return $offer;
        }
    }
    }
    public function customer(Request $res)
    {
        if ($res->blockid == '') {
            return '';
        }
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findunderblock($blockstartid);
            $userauth = User_auth::with(['block', 'user'])->whereIn('block_id', $underblock)->get();
            return $userauth;
        } else {
            $userauth = User_auth::with(['block', 'user'])->where('block_id', $blockid)->get();
            return $userauth;
        }
    }
    public function userauth(Request $res)
    {
        if ($res->blockid == '') {
            return '';
        }
        $blockid = $res->blockid;
        $checkunderblock = $res->underblock;
        if ($checkunderblock == 1) {
            $datacontoller = new DataController();
            $blockstartid = $res->blockid;
            $underblock = $datacontoller->findunderblock($blockstartid);
            $userauth = User_auth::with(['block', 'user'])->whereIn('block_id', $underblock)->get();
            return $userauth;
        } else {
            $userauth = User_auth::with(['block', 'user'])->where('block_id', $blockid)->get();
            return $userauth;
        }
    }

    public function customerranking()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        return view('system-mgmt/insurance/misreport/customerranking', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }
    public function customerrankingsearch(Request $request)
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $customerList = $this->filtercustomer($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();

        $arrayUserID = [];
        foreach ($customerList as $data) {
            array_push($arrayUserID, $data->id);
        }
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $findCase = Cases::whereIn('service_user_block_id', $arrayUserID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
        $findConfirmOffer = Casemiddledata::whereIn('case_id', $findCase)->pluck('offer_id')->toArray();
        $sumTotalfee = Offer::whereIn('id', $findConfirmOffer)->sum('offer_payment_value1');
        $sumTotalcase = 1;
        $underBlock = $request['underblock'];
        $sumTotalfee = $customerList->sum('total_premium');

        return view('system-mgmt/insurance/misreport/customerranking', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'customerList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }
    public function userranking()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        return view('system-mgmt/insurance/misreport/userranking', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function userrankingsearch(Request $request)
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $userList = $this->filteruser($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();
        $arrayUserID = [];
        foreach ($userList as $data) {
            array_push($arrayUserID, $data->id);
        }
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $findCase = Cases::whereIn('service_user_block_id', $arrayUserID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
        $findConfirmOffer = Casemiddledata::whereIn('case_id', $findCase)->pluck('offer_id')->toArray();
        $sumTotalfee = Offer::whereIn('id', $findConfirmOffer)->sum('offer_payment_value1');
        $sumTotalcase = 1;
        $underBlock = $request['underblock'];
        $sumTotalfee = $userList->sum('total_premium');

        return view('system-mgmt/insurance/misreport/userranking', compact('fromDate', 'toDate', 'underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'userList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }
    public function distributioninsight()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $caseChannel = [];
        $blockinDropdown = '';

        $underBlock = 0;
        return view('system-mgmt/insurance/misreport/distributioninsight', compact('underBlock', 'blockinDropdown', 'caseChannel', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function distributioninsightsearch(Request $request)
    {
        $i = 0;
        
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $underBlock = $request['underblock'];

        if($structureId == "0"){
            $blockstartid = $this->datacontroller->getblockid();
            $blockID = $this->datacontroller->findunderblock($blockstartid);
        }else{
            if($underBlock == "1"){
                $blockstartid = $request->blockid;
                $block = $datacontoller->findunderblock($blockstartid);
            }else{
                $blockID = [];
                $blockID[0] = $request->blockid;
            }
        }
        $caseChannel = CaseChannel::get(['id', 'name']);
        $blockinDropdown = Block::where('id', $request->blockid)->first();
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $sumTotalcase = Cases::whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->whereIn('service_user_block_id',$blockID)->count();

        return view('system-mgmt/insurance/misreport/distributioninsight', compact('sumTotalcase','blockID', 'fromDate', 'toDate', 'underBlock', 'blockinDropdown', 'caseChannel', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function customerport()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        return view('system-mgmt/insurance/misreport/customerport', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function customerportsearch(Request $request)
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $portList = $this->filterport($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();

        $arrayUserID = [];
        foreach ($portList as $data) {
            array_push($arrayUserID, $data->id);
        }
        
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $findCase = Cases::whereIn('service_user_block_id', $arrayUserID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
        $findConfirmOffer = Casemiddledata::whereIn('case_id', $findCase)->pluck('offer_id')->toArray();
        $sumTotalfee = Offer::whereIn('id', $findConfirmOffer)->sum('offer_payment_value1');
        $sumTotalcase = 1;
        $underBlock = $request['underblock'];
        $sumTotalfee = $portList->sum('total_premium');
        return view('system-mgmt/insurance/misreport/customerport', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'portList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function customerportcase()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;

        return view('system-mgmt/insurance/misreport/customerportcase', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'portList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }
    public function customerportcasesearch(Request $request)
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $underBlock = $request['underblock'];

            if($request->structureId == "0"){
                $blockstartid = $this->datacontroller->getblockid();
                $underblock = $this->datacontroller->findunderblock($blockstartid);
                $case = Cases::whereIn('service_user_block_id', $underblock)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();

            }else{
                if($underBlock == "1"){
                    $blockstartid = $request->blockid;
                    $underblock = $this->datacontroller->findunderblock($blockstartid);
                    $case = Cases::whereIn('service_user_block_id', $underblock)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
                }else{
                    $case = Cases::where('service_user_block_id', $request->blockid)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
                }

            }
        $casemiddledata = Casemiddledata::whereIn('case_id',$case)->pluck('port_id')->toArray();
        $portList = Portfolio::whereIn('id',array_unique($casemiddledata))->get();
       // $portfolio = array_unique($portfolio);
        //$portList = $this->filterportcase($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();

        $arrayUserID = [];
        $findCase = Cases::whereIn('service_user_block_id', $arrayUserID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
        $findConfirmOffer = Casemiddledata::whereIn('case_id', $findCase)->pluck('offer_id')->toArray();
        $sumTotalfee = Offer::whereIn('id', $findConfirmOffer)->sum('offer_payment_value1');
        $sumTotalcase = count($case);
        $sumTotalfee = $portList->sum('total_case');

        return view('system-mgmt/insurance/misreport/customerportcase', compact('fromDate', 'toDate', 'underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'portList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }
    public function customercase()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        return view('system-mgmt/insurance/misreport/customercase', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }
    public function customercasesearch(Request $request)
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $customerList = $this->filtercustomercase($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();

        $arrayUserID = [];
        foreach ($customerList as $data) {
            array_push($arrayUserID, $data->id);
        }
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $findCase = Cases::whereIn('service_user_block_id', $arrayUserID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
        $findConfirmOffer = Casemiddledata::whereIn('case_id', $findCase)->pluck('offer_id')->toArray();
        $underBlock = $request['underblock'];
        $sumTotalcase = $customerList->sum('total_case');

        return view('system-mgmt/insurance/misreport/customercase', compact('underBlock', 'sumTotalcase', 'blockinDropdown', 'customerList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }
    public function usercase()
    {
        $cases = Cases::all();
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        return view('system-mgmt/insurance/misreport/userrankingcase', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'structureId', 'cases', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function usercasesearch(Request $request)
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $userList = $this->filterusercase($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();

        $arrayUserID = [];
        foreach ($userList as $data) {
            array_push($arrayUserID, $data->id);
        }
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $findCase = Cases::whereIn('service_user_block_id', $arrayUserID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
        $findConfirmOffer = Casemiddledata::whereIn('case_id', $findCase)->pluck('offer_id')->toArray();
        $sumTotalfee = Offer::whereIn('id', $findConfirmOffer)->sum('offer_payment_value1');
        $sumTotalcase = 1;
        $underBlock = $request['underblock'];
        $sumTotalfee = $userList->sum('total_case');

        return view('system-mgmt/insurance/misreport/userrankingcase', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'userList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function customerperfomance()
    {
        $cases = Cases::all();
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        return view('system-mgmt/insurance/misreport/customerperfomance', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'structureId', 'cases', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function customerperfomancesearch(Request $request)
    {

        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $blockList = $this->filterblock($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();

        $arrayBlockID = [];
        foreach ($blockList as $data) {
            array_push($arrayBlockID, $data->id);
        }

        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $findCase = Cases::whereIn('service_user_block_id', $arrayBlockID)->where('case_status', 2)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->pluck('id')->toArray();
        $findConfirmOffer = Casemiddledata::whereIn('case_id', $findCase)->pluck('offer_id')->toArray();
        $sumTotalfee = Offer::whereIn('id', $findConfirmOffer)->sum('offer_payment_value1');
        $sumTotalcase = 1;
        $underBlock = $request['underblock'];

        return view('system-mgmt/insurance/misreport/customerperfomance', compact('fromDate', 'toDate', 'underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'cases', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function coordinator()
    {
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $block = '';
        $checkunderblock = 0;
        $coorId = 0;
        $coorDinatorid = User_auth::where('block_id', 72)->pluck('user_id')->toArray();
        $coorDinator = User::whereIn('id', $coorDinatorid)->get(['id', 'firstname', 'lastname']);
        return view('system-mgmt/insurance/misreport/coordinator', compact(['coorId', 'coorDinator', 'structure', 'offer', 'fromDay', 'fromMonth', 'checkunderblock', 'fromYear', 'toDay', 'toMonth', 'toYear', 'structureId', 'block']));
    }
    
    public function managementreport()
    {
        return view('system-mgmt/insurance/misreport/managementreport');
    }
    public function managementreportbycoor()
    {
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $block = '';
        $underBlock = 0;
        $coorId = 0;
        $coorDinatorid = User_auth::where('block_id', 72)->pluck('user_id')->toArray();
        $coorDinator = User::whereIn('id', $coorDinatorid)->get(['id', 'firstname', 'lastname']);
        $coordinateIndropdown = 0;

        return view('system-mgmt/insurance/misreport/managementreportbycoor', compact(['coordinateIndropdown','coorId', 'coorDinator', 'structure', 'offer', 'fromDay', 'fromMonth', 'underBlock', 'fromYear', 'toDay', 'toMonth', 'toYear', 'structureId', 'block']));
    }

    public function managementreportbycoorsearch(Request $request){
        
        $i = 0;
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        if($request->coodinatorID == 0){
            $coorID = User_auth::where('block_id', 72)->pluck('user_id')->toArray();
            $blockList = User::whereIn('id', $coorID)->get(['id', 'firstname', 'lastname']);
            $findCase = Cases::whereIn('coordinate_user_block_id', $coorID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();

        }else{
            $coorID = $request->coordinatorID;
            $blockList = User::where('id',$request->coordinatorID)->get();
            $findCase = Cases::where('coordinate_user_block_id', $coorID)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status', 2)->pluck('id')->toArray();
        }
        $arrayBlockID = [];
        foreach ($blockList as $data) {
            array_push($arrayBlockID, $data->id);
        }
        $coorDinatorid = User_auth::where('block_id', 72)->pluck('user_id')->toArray();
        $coorDinator = User::whereIn('id', $coorDinatorid)->get(['id', 'firstname', 'lastname']);

        $findProposal = Proposal::whereIn('case_id', $findCase)->pluck('id')->toArray();
        $sumTotalfee = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value19');
        $sumTotalcompanyIncome = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value21');
        $sumcoorDinatorFee = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value10');
        $sumTotalnetfee = $sumcoorDinatorFee;
        $sumTotalcase = count($findCase);
        $coordinateIndropdown = $request->coordinatorID;
        return view('system-mgmt/insurance/misreport/managementreportbycoor', compact('fromDate','toDate','coordinateIndropdown','coorDinator','sumcoorDinatorFee','sumTotalnetfee','sumTotalcompanyIncome', 'underBlock', 'sumTotalcase', 'sumTotalfee', 'blockList', 'i', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));

    }
    public function managementreportbyuser()
    {
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $block = '';
        $underBlock = 0;
        $coorId = 0;
        $coorDinatorid = User_auth::where('block_id', 72)->pluck('user_id')->toArray();
        $coorDinator = User::whereIn('id', $coorDinatorid)->get(['id', 'firstname', 'lastname']);
        return view('system-mgmt/insurance/misreport/managementreportbyuser', compact(['coorId', 'coorDinator', 'structure', 'offer', 'fromDay', 'fromMonth', 'underBlock', 'fromYear', 'toDay', 'toMonth', 'toYear', 'structureId', 'block']));
    }

    public function managementreportbyusersearch(Request $request){
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $blockList = $this->filterblock($request);
        $blockinDropdown = Block::where('id', $request->blockid)->first();
        $fromDate = $this->getFromdate($request);
        $toDate = $this->getTodate($request);
        $arrayBlockID = [];
        
        
        foreach ($blockList as $data) {
            array_push($arrayBlockID, $data->id);
        }
        $findCase = Cases::whereIn('service_user_block_id', $arrayBlockID)->where('case_status', 2)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->pluck('id')->toArray();
        $findProposal = Proposal::whereIn('case_id', $findCase)->pluck('id')->toArray();
        $sumTotalfee = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value19');
        $sumTotalcompanyIncome = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value21');
      
        $sumcoorDinatorFee = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value10');
        $sumtaxFee = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value9');
        $sumotherFee = Offer::whereIn('proposal_id', $findProposal)->sum('offer_payment_value11');
        $sumTotalnetfee = $sumcoorDinatorFee+$sumtaxFee+$sumotherFee;
        $sumTotalcase = count($findCase);
        $underBlock = $request['underblock'];
        /*foreach($offer as $data){
        $sumTotalcase += $data->total_case;
        $sumTotalfee += $data->total_fee;
        }*/
        return view('system-mgmt/insurance/misreport/managementreportbyuser', compact('fromDate','toDate','sumcoorDinatorFee','sumtaxFee','sumotherFee','sumTotalnetfee','sumTotalcompanyIncome', 'underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'cases', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));

    }

    public function casecancelreport()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;

        return view('system-mgmt/insurance/misreport/casecancelreport', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'portList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));

    }

    public function casecancelreportsearch(Request $request)
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $fromDate = $this->getFromdate($request);
        $underBlock = $request['underblock'];
        $toDate = $this->getTodate($request);
            if($request->structureId == "0"){
                $blockstartid = $this->datacontroller->getblockid();
                $underblock = $this->datacontroller->findunderblock($blockstartid);
                $caseList = Cases::whereIn('service_user_block_id',$underblock)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status',3)->get();

            }else{
                if($underBlock == "1"){
                    $blockstartid = $request->blockid;
                    $underblock = $this->datacontroller->findunderblock($blockstartid);
                    $caseList = Cases::whereIn('service_user_block_id',$underblock)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status',3)->get();
                }else{
                    $caseList = Cases::where('service_user_block_id',$request->blockid)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->where('case_status',3)->get();
                }

            }
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        return view('system-mgmt/insurance/misreport/casecancelreport', compact('underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'caseList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));

    }
    public function liquidityassetreport()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        $fromDayvalid = 0;
        $fromMonthvalid = 0;
        $fromYearvalid = 0;
        $toDayvalid = 0;
        $toMonthvalid = 0;
        $toYearvalid = 0;
        $assetCategory = Asset_cat::get(['id','name']);
        $assetcatID = 0;
        $assettypeID = 0;
        $assettypeinDropdown = '';
        $assetType = Asset_type::get(['id','la_nla_type','nla_sub_type']);

        return view('system-mgmt/insurance/misreport/liquidityasset', compact('assetType','assettypeID','assetcatID','fromDayvalid', 'fromMonthvalid', 'fromMonthvalid', 'fromYearvalid','toDayvalid', 'toMonthvalid', 'toMonthvalid', 'toYearvalid', 'assetCategory', 'underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function liquidityassetreportsearch(Request $request)
    {
        if(empty($request->fromDay) && empty($request->fromDayvalid)){
            return $this->liquidityassetreport();
        }
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = $request['fromDay'];
        $fromMonth = $request['fromMonth'];
        $fromYear = $request['fromYear'];
        $toDay = $request['toDay'];
        $toMonth = $request['toMonth'];
        $toYear = $request['toYear'];
        $structureId = $request['structureId'];
        $caseList = $this->getliquidityasset($request);
        $blockinDropdown = $request['blockid'];
        $assetcatID = $request['assetcatID'];
        $assettypeID = $request['assettypeid'];
        $fromDayvalid = $request['fromDayvalid'];
        $fromMonthvalid = $request['fromMonthvalid'];
        $fromYearvalid = $request['fromYearvalid'];
        $toDayvalid = $request['toDayvalid'];
        $toMonthvalid = $request['toMonthvalid'];
        $toYearvalid = $request['toYearvalid'];
        $assetCategory = Asset_cat::get(['id','name']);
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = $request['underblock'];
        $assetType = Asset_type::where('asset_cat',$assetcatID)->get(['id','la_nla_type','nla_sub_type']);
        return view('system-mgmt/insurance/misreport/liquidityasset', compact('assetType','caseList','assettypeID','assetcatID','fromDayvalid', 'fromMonthvalid', 'fromMonthvalid', 'fromYearvalid','toDayvalid', 'toMonthvalid', 'toMonthvalid', 'toYearvalid', 'assetCategory', 'underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));

    }

    public function userperfomancereport()
    {
        $i = 0;
        $structure = $this->datacontroller->getstructureAuth();
        $offer = [];
        $fromDay = 0;
        $fromMonth = 0;
        $fromYear = 0;
        $toDay = 0;
        $toMonth = 0;
        $toYear = 0;
        $structureId = 0;
        $blockList = [];
        $blockinDropdown = '';
        $sumTotalcase = 0;
        $sumTotalfee = 0;
        $underBlock = 0;
        $dateType = 0;
        $lengthDate = [];
        return view('system-mgmt/insurance/misreport/userperfomance', compact('lengthDate','dateType', 'underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'caseList', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear'));
    }

    public function userperfomancereportsearch(Request $request)
    {
        $dateType = $request->datetype;
        switch ($dateType) {
            case "1":
                $fromDate = $request->fromYear. '-' . $request->fromMonth . '-' . $request->fromDay ;
                $toDate = $request->toYear . '-' . $request->toMonth . '-' . $request->toDay;

            break;
            case "2":
              $fromDate = "1" . '-' . $request->fromMonth . '-' . $request->fromYear;
              //Check if month have 31
              if($request->toMonth == "2"){
                  $request->toDay = "28";
              }else if($request->toMonth == "4" || $request->toMonth == "6" || $request->toMonth == "9" || $request->toMonth == "11"){
                  $request->toDay = "30";
              }else{
                  $request->toDay = "31";
              }
              $fromDate = $request->fromYear. '-' . $request->fromMonth . '-' . $request->fromDay ;
              $toDate = $request->toYear . '-' . $request->toMonth . '-' . $request->toDay;
          
            break;
            case "3":

                if($request->fromMonth == "4"){
                    $request->fromDay = "01";
                    $request->fromMonth = "10";
                    $request->toDay = "31";
                    $request->toMonth = "12";
                } else if($request->fromMonth == "3"){
                    $request->fromDay = "01";
                    $request->fromMonth = "07";
                    $request->toDay = "30";
                    $request->toMonth = "09";
                }else if($request->fromMonth == "2"){
                    $request->fromDay = "01";
                    $request->fromMonth = "04";
                    $request->toDay = "30";
                    $request->toMonth = "06";
                }else if($request->fromMonth == "1"){
                    $request->fromDay = "01";
                    $request->fromMonth = "01";
                    $request->toDay = "31";
                    $request->toMonth = "03";
                }
              $fromDate = $request->fromYear. '-' . $request->fromMonth . '-' . $request->fromDay ;
              $toDate = $request->toYear . '-' . $request->toMonth . '-' . $request->toDay;
            break;
            case "4":
                if($request->fromMonth == "1"){
                    $request->fromDay = "01";
                    $request->fromMonth = "01";
                    $request->toDay = "31";
                    $request->toMonth = "06";
                }else if($request->fromMonth == "2"){
                    $request->fromDay = "01";
                    $request->fromMonth = "07";
                    $request->toDay = "31";
                    $request->toMonth = "12";
                }
                $fromDate = $request->fromYear. '-' . $request->fromMonth . '-' . $request->fromDay ;
                $toDate = $request->toYear . '-' . $request->toMonth . '-' . $request->toDay;
            break;
            case "5":
              $request->fromDay = "01";
              $request->fromMonth = "01";
              $request->toDay = "31";
              $request->toMonth = "12";
              $fromDate = $request->fromYear. '-' . $request->fromMonth . '-' . $request->fromDay ;
              $toDate = $request->toYear . '-' . $request->toMonth . '-' . $request->toDay;
            break;
          default:
          $lengthDate = [];
        }
        
         $structure = Structure::pluck('id')->toArray();
         $block = Block::whereIn('structure_id',$structure)->pluck('id')->toArray();
         $cases = Cases::whereIn('service_user_block_id',$block)->whereIn('id',['337','336'])->pluck('id')->toArray();
         $casemiddledata = Casemiddledata::whereIn('case_id',$cases)->pluck('offer_id')->toArray();
         $offer = Casemiddledata::whereIn('case_id',$cases)->pluck('offer_id')->toArray();
         $i = 0;
         $structure = $this->datacontroller->getstructureAuth();
         $offer = [];
         $fromDay = $request['fromDay'];
         $fromMonth = $request['fromMonth'];
         $fromYear = $request['fromYear'];
         $toDay = $request['toDay'];
         $toMonth = $request['toMonth'];
         $toYear = $request['toYear'];
         $structureId = $request['structureId'];
        //$blockList = $this->filterblockbyuser($request);
        $blockList = $this->filterblockbyuser($request);
        $blockinDropdown = User::where('id', $request->blockid)->first();
        $arrayBlockID = [];
        foreach ($blockList as $data) {
            array_push($arrayBlockID, $data->id);
        }
        $findCase = Cases::whereIn('service_user_block_id', $arrayBlockID)->where('case_status', 2)->whereDate('created_at','>=',$fromDate)->whereDate('created_at','<=',$toDate)->pluck('id')->toArray();
        $findConfirmOffer = Casemiddledata::whereIn('case_id', $findCase)->pluck('offer_id')->toArray();
        $sumTotalfee = Offer::whereIn('id', $findConfirmOffer)->sum('offer_payment_value19');
        $underBlock = $request['underblock'];
        /*foreach($offer as $data){
        $sumTotalcase += $data->total_case;
        $sumTotalfee += $data->total_fee;
        }*/
       
       // $header = [];
        
         /// return $lengthDate;
        return view('system-mgmt/insurance/misreport/userperfomance', compact('lengthDate','dateType','underBlock', 'sumTotalcase', 'sumTotalfee', 'blockinDropdown', 'blockList', 'cases', 'structureId', 'i', 'structure', 'offer', 'fromDay', 'fromMonth', 'fromYear', 'toDay', 'toMonth', 'toYear','fromDate','toDate'));

    }
    
    public function getallusercasesbyDaily($request){
        $alluserBlock = User_auth::where('user_id',$request->userID)->pluck('block_id')->toArray();
        $case = Cases::whereIn('service_user_block_id',$alluserBlock)->get();
        return $case;
    }
}

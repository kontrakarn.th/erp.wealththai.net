<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\View;
use App\Block;
use App\User_auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\SidebarController;
use App\Cases;
use App\Proposal;
use App\Offer;
use App\Asset;
use App\CaseType;

use App\OfferType;
use Storage;

use App\Http\Controllers\CaseCenterController;
use App\Portfolio;
use App\Case_condition;
use App\Casemiddledata;
use App\Http\Controllers\FileController;
use App\Case_Attacht;
use App\File;
use App\Asset_Attacht;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('view');
		        $this->datacenter = new DataController;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {			
				  /*   date_default_timezone_set('Asia/Bangkok');
		$array = array();
		      $case = Cases::with(['Person','Stage','Cases','Block','Partner_block','CaseType','CaseSubType','Asset','match_id','CaseStatus','coordiantor','CaseChannel'])->whereNotNull('auto_renew_date')->whereNull('renew_case_id')->where('auto_renew_date','!=','')->where('case_status',2)->get();
      foreach($case as $ca)
      {
		 
        $explode = explode('/',$ca->auto_renew_date);
		 if(count($explode) == 3 )
		 {
        $dayre = $explode[0];
        $monthre = $explode[1];
        $yearre = $explode[2];
		        $day = date("d");
      $month = date("m");
      $year = date("Y");
        $currentdate = $year."-".$month."-".$day;
        $renewdate = $yearre."-".$monthre."-".$dayre;
		$datetime1 = date_create($renewdate);
		$datetime2 = date_create($currentdate);
		$interval = date_diff($datetime2,$datetime1);
        if($interval->format('%R%a') <= $ca->CaseType->day_auto_renew)
        {
           $caseid = $ca->id;
			array_push($array,$caseid);
         //  dispatch(new AddRenewCaseJobs($caseid))->delay(now()->addMinutes(1));

        }
        else
        {
          //echo "Nothing to Run";
        }
			 }
	  }	  
	return $array;*/
	
		/*$explode = explode('/','01/06/2020');
        $dayre = $explode[0];
        $monthre = $explode[1];
        $yearre = $explode[2];
        $day = date("d");
     	$month = date("m");
    	$year = date("Y");
        $currentdate = $year."-".$month."-".$day;
        $renewdate = $yearre."-".$monthre."-".$dayre;
		$datetime1 = date_create($renewdate);
		$datetime2 = date_create($currentdate);
		$interval = date_diff($datetime2,$datetime1);
		if($interval->format('%R%a') <= 46)
		{
		return $interval->format('%R%a');
		}
		
		return $interval->format('%R%a days');
		
		$diff=date_diff($newformat1,$newformat2);
		return $diff;
		//return Storage::path('');*/
        return view('dashboard');
    }


}

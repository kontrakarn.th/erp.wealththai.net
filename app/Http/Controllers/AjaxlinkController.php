<?php

namespace App\Http\Controllers;

use App\Action;
use App\Asset_type;
use App\Block;
use App\Campaign;
use App\CaseType;
use App\Condition;
use App\District;
use App\FilePackagecat;
use App\FileSubCat;
use App\match_member_id;
use App\OfferType;
use App\Partner_block;
use App\Party;
use App\Path_condition_detail;
use App\Person;
use App\Portfolio;
use App\Port_cat;
use App\Port_type;
use App\Province;
use App\Subdistrict;
use App\ToolSet;
use App\User;
use App\User_auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class AjaxlinkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function AssetTypeIssuer(Request $request)
    {
        $findIssuerID = Asset_type::where('id', $request->id)->value('issuer_guild');
        $group = match_member_id::where('member_group_id', $findIssuerID)->pluck('member_id')->toArray();
        $issuer = Person::whereIn('id', $group)->get(['id', 'name']);
        return $issuer;
    }
    public function findAssetHeaderForm(Request $request)
    {

        $findAssetHeader = Asset_type::find($request->id);
        if (empty($findAssetHeader)) {
            $findAssetHeader = "nodata";
            return response()->json($findAssetHeader);
        }
        return response()->json($findAssetHeader);
    }
    public function findFileCat(Request $request)
    {
        $data = FilePackagecat::leftJoin('file_category', 'file_package_cat.cat_id', '=', 'file_category.id')
            ->select('file_package_cat.*', 'file_category.name as file_category_name', 'file_category.id as file_category_id')->where('package_id', $request->id)->get();
        return response()->json($data);
    }

    public function findFileCategory(Request $request)
    {
        $data = FileSubCat::select('*')->where('id', $request->id)->get();
        return response()->json($data);
    }

    public function findAssetLabel(Request $request)
    {
        $data = Asset_type::select('ref_info_head1', 'ref_info_head2', 'ref_info_head3', 'ref_info_head4', 'ref_info_head5', 'ref_info_head6', 'ref_info_head7', 'ref_info_head8')->where('id', $request->id)->get();
        return response()->json($data);
    }

    public function findAssetRef(Request $request)
    {
        $data = Asset_type::select('ref_info_head1', 'ref_info_head2', 'ref_info_head3', 'ref_info_head4', 'ref_info_head5', 'ref_info_head6', 'ref_info_head7', 'ref_info_head8')->where('id', $request->id)->get();
        return response()->json($data);
    }

    public function findPortnum(Request $request)
    {
        $data = Portfolio::select('id')->where('id', $request->id)->get();
        return response()->json($data);
    }

    public function findPortMember(Request $request)
    {
        $data = Portfolio::select('id', 'type', 'number')->where('member_id', $request->id)->get();
        return response()->json($data);
    }

    public function findPortAsset(Request $request)
    {
        $data = DB::table('asset')
            ->leftJoin('asset_type', 'asset.la_nla_type', '=', 'asset_type.id')
            ->where('asset_type.la_nla', '=', 'Non Liquidity Asset')
            ->where('asset.port_id', $request->id)
            ->select('asset.name', 'asset_type.ref_info_head1 as ref_head1', 'asset_type.ref_info_head2 as ref_head2', 'asset_type.ref_info_head3 as ref_head3', 'asset_type.ref_info_head4 as ref_head4', 'asset_type.ref_info_head5 as ref_head5', 'asset_type.ref_info_head6 as ref_head6', 'asset_type.ref_info_head7 as ref_head7', 'asset_type.ref_info_head8 as ref_head8')
            ->get();
        return response()->json($data);
    }
    public function findMemId(Request $request)
    {
        $data = Person::select('id', 'name', 'lname')->where('id', $request->id)->get();
        return response()->json($data);
    }
    public function findMemRefid(Request $request)
    {
        $data = Person::select('id', 'name', 'lname')->where('id', $request->id)->get();
        return response()->json($data);
    }
    public function findAssetType(Request $request)
    {
        $data = DB::table('asset')
            ->leftJoin('asset_type', 'asset.la_nla_type', '=', 'asset_type.id')
            ->where('asset_type.la_nla', '=', 'Non Liquidity Asset')
            ->where('asset.id', $request->id)
            ->select('asset.name', 'asset_type.ref_info_head1 as ref_head1', 'asset_type.ref_info_head2 as ref_head2', 'asset_type.ref_info_head3 as ref_head3', 'asset_type.ref_info_head4 as ref_head4', 'asset_type.ref_info_head5 as ref_head5', 'asset_type.ref_info_head6 as ref_head6', 'asset_type.ref_info_head7 as ref_head7', 'asset_type.ref_info_head8 as ref_head8')
            ->get();
        return response()->json($data);
    }

    public function findMemType(Request $request)
    {
        $data = DB::table('persons')
            ->where('type', $request->id)
            ->get();
        return response()->json($data);
    }

    public function findPartyName(Request $request)
    {
        $data = Party::select('name', 'id')->where('member_group_id', $request->id)->get();
        return response()->json($data);
    }

    public function findDistrict(Request $request)
    {
        $data = District::select('name_in_thai', 'name_in_english', 'id')->where('province_id', $request->id)->get();
        return response()->json($data);
    }
    public function findProvince(Request $request)
    {
        $data = Province::select('name_in_thai', 'name_in_english', 'id')->where('country_id', $request->id)->get();
        return response()->json($data);
    }
    public function findSubdistrict(Request $request)
    {
        $data = SubDistrict::select('name_in_thai', 'name_in_english', 'id')->where('district_id', $request->id)->get();
        return response()->json($data);
    }
    public function findToolSet(Request $request)
    {
        $data = ToolSet::select('*')->where('tool_id', $request->id)->get();
        return response()->json($data);
    }

    public function findpartnerblock(Request $request)
    {
        $data = Partner_block::select('*')->where('structure_id', $request->id)->get();
        return response()->json($data);
    }

    public function findCaseType(Request $request)
    {
        $data = CaseType::join('partner_block', 'case_type.default_partner_block_id', '=', 'partner_block.id')
            ->leftJoin('block as bu', 'case_type.default_user_block_id', '=', 'bu.id')
            ->leftJoin('case_sub_type', 'case_sub_type.case_type', '=', 'case_type.id')
            ->leftJoin('partner_group', 'case_type.default_partner_group', '=', 'partner_group.id')
            ->where('case_type.id', $request->id)->select('*', 'bu.id as bu_id', 'bu.name as bu_name', 'partner_group.name as partner_group_name', 'partner_block.name as partner_block_name', 'case_sub_type.id as case_sub_type_id', 'case_sub_type.name as case_sub_type_name')->get();
        return response()->json($data);
    }

    public function findCondition(Request $request)
    {
        $data = Condition::select('*')->where('id', $request->id)->get();
        return response()->json($data);
    }
    public function AssetTypeInfo(Request $request)
    {
        $data = Asset_type::select('*')->where('id', $request->id)->get();
        return response()->json($data);
    }

    public function findOfferType(Request $request)
    {
        $data = OfferType::select('*')->where('id', $request->id)->get();
        return response()->json($data);
    }

    public function findOfferTypeCampaign(Request $request)
    {
        $data = Campaign::select('*')->where('id', $request->id)->get();
        return response()->json($data);
    }

    public function findAction(Request $request)
    {
        $data = Action::select('*')->where('id', $request->id)->get();
        return response()->json($data);
    }
    public function pathcondetail(Request $request)
    {
        $data = Path_condition_detail::select('*')->get();
        return response()->json($data);
    }

    public function findPortType(Request $request)
    {
        $portCategory = Port_cat::select('*')->where('structure_id', $request->id)->pluck('id')->toArray();
        $data = Port_type::select('*')->whereIn('port_cat', $portCategory)->get();
        return response()->json($data);
    }

    public function findCaseTypeByCaseCat(Request $request)
    {
        $data = CaseType::select('name', 'id')->where('case_cat_id', $request->id)->get();
        return response()->json($data);
    }

    public function findDefaultUserBlockByCaseType(Request $request)
    {
        $defaultUserBlockid = CaseType::where('id', $request->id)->value('default_user_block_id');
        $data = Block::where('id', $defaultUserBlockid)->get(['id', 'name']);
        $anotherBlock = Block::where('id', '!=', $defaultUserBlockid)->get(['id', 'name']);
        if (empty($data)) {
            $data = Block::get(['id', 'name']);
        }
        $data = ["default_block" => $data,
            "another_block" => $anotherBlock];
        return response()->json($data);
    }

    public function findDefaultPartnerBlockByCaseType(Request $request)
    {
        $defaultPartnerBlockid = CaseType::where('id', $request->id)->value('default_partner_block_id');
        $data = Partner_block::where('id', $defaultPartnerBlockid)->get(['id', 'name']);
        $anotherBlock = Partner_block::where('id', '!=', $defaultPartnerBlockid)->get(['id', 'name']);
        if (empty($data)) {
            $data = Partner_block::get(['id', 'name']);
        }
        $data = ["default_block" => $data,
                "another_block" => $anotherBlock];
        return response()->json($data);
    }

    public function findDefaultCoorBlockByCaseType(Request $request)
    {
        $defaultCoorBlockid = CaseType::where('id', $request->id)->value('default_coor_block_id');
        $findUserinAuth = User_auth::where('block_id', $defaultCoorBlockid)->pluck('user_id')->toArray();
        $data = User::whereIn('id', $findUserinAuth)->get(['id', 'firstname', 'lastname']);
        if (empty($data)) {
            $data = User::get(['id', 'firstname', 'lastname']);
        }
        return response()->json($data);
    }

    public function findBlockbyStructure(Request $request)
    {
      $data=Block::select('name','id')->where('structure_id',$request->id)->get();
      return response()->json($data);
    }
    public function findUserbyStructure(Request $request)
    {
      $data = User_auth::where('structure_id',$request->id)->pluck('user_id')->toArray();
      $data=User::select('firstname','lastname','id')->whereIn('id',$data)->orderBy('firstname')->get();
      return response()->json($data);
    }

    public function findAssetTypeByAssetCategory(Request $request){

        $data= Asset_type::select('la_nla_type','nla_sub_type','id')->where('asset_cat',$request->id)->get();
        return response()->json($data);

    }
}

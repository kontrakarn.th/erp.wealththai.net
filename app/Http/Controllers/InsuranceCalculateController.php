<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Structure;
use Illuminate\Support\Facades\Auth;
use App\Block;
use App\View;
use App\Asset_cat;
use App\ActionCategory;
use App\User;
use App\User_auth;
use App\Partner;
use App\Partner_block;
use App\CaseChannel;
use App\Person;
use App\Member_type;
use App\Country;
use App\Subdistrict;
use App\District;
use App\Province;
use App\Asset_type;
use App\match_member_id;
use App\Portfolio;
use App\Asset;
use App\CaseCategory;
use App\CaseType;
use App\CaseSubType;
use App\Cases;
use App\match_id;
use App\Procedures_To_Process;
use App\Process;
use App\Member_group;
use App\Family;
use App\Pid_group;
use App\Partner_group;
use App\CaseAuth;
use App\File;
use App\Asset_Attacht;
use App\Case_Attacht;
use App\Offer;
use App\OfferType;
use App\Proposal;
use App\Case_proposal;
use App\Case_log;
use App\Casemiddledata;
use App\Casemiddledatatype;
use App\Offer_Attacht;
use App\Promotion;
use App\Case_condition;
use App\CaseAction;
use App\Stage;
use App\CaseStatus;
use PDF;
use App\Http\Controllers\CaseCenterController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\QuotationController;
use App\Http\Controllers\ConsildateQuotationController;
use App\Http\Controllers\QuotationCustomerController;
use App\Http\Controllers\ConsolidateQuotationCustomerController;
use App\Http\Controllers\FileController;

use FPDF;

use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\SidebarController;
use App\Http\Controllers\DataController;

class InsuranceCalculateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     protected $datacenter;

    public function __construct()
    {
        $this->middleware('view');
        $this->datacenter = new DataController;

    }
    public function calculate(){
        $customer_lv             =  10;
        $partner_lv              =  0;
        $user_lv                 =  80;
        $promotion               =  10;
        $tax_percent             =  20;
        $coor_fee_percent        =  25;
        $other_fee_percent       =  0;
        $special_discount        =  2000;
        $partner_discount        =  0;
        $user_discount           =  10000;
        $compensate_company      =  1000;


        $Net_premium             =  1000000;
        $Duty                    =  200000;
        $Vat                     =  50000;
        $Premium                 =  1000000;
        $Withholding             =  12000;
        $Gross_com               =  ($Net_premium+$Duty+$Vat)-$Premium;
        $tax                     =  50000;
        $coor_fee                =  50000;
        $other_fee               =  0;     
        
        $CATCO                   =  $Gross_com-$tax-$coor_fee-$other_fee;
        $Promotion_discount      =  $promotion*$Net_premium;
        $Cutomer_discount        =  6000 ;
        $Specialdiscount        =  $special_discount;

        $CONCLUDE_CUSTOMER_DIS   =  max($Promotion_discount,$Cutomer_discount)+$Specialdiscount;
        $NCM                     =  $CATCO-$CONCLUDE_CUSTOMER_DIS;
        
        $Partner_quota           =0;
        $partner_discount        = 0;

        $P_COM                   = '';
        $NCMP                    =$NCM-$Partner_quota;
        $User_quota              =38400;
        $User_discount           =10000;
        $U_Com                   =$User_quota-$User_discount;   
        $NCMPU                   =$NCMP-$User_quota;
        $Company_compensation    =1000;

        $Company_income          =$NCMPU-$Company_compensation;

        $CON_Premium_ticket                        = $Net_premium+$Duty+$Vat;
        $CON_Withholding                           = $Withholding;
        $CON_All_discount                          = $CONCLUDE_CUSTOMER_DIS+$partner_discount+$User_discount+$Company_compensation;
        $CON_Customer_Pay_BeforeWithholding        = $CON_Premium_ticket- $CON_All_discount;
        $CON_Customer_Pay_AfterWithholding         = $CON_Customer_Pay_BeforeWithholding-$CON_All_discount;
        $CON_Partner_Pay_To_Company                = $CON_Customer_Pay_AfterWithholding - ($Partner_quota-$partner_discount) ;
        $CON_User_Pay_To_Company                   = $CON_Customer_Pay_AfterWithholding-$U_Com;
        $CON_Company_Pay_to_InsuranceCompany       = $Premium - $Withholding;
    }

    public function calculatetotalpaiduser($data){
      /*  $ans = ($data->offer_payment_value15)+($data->offer_payment_value16)+($data->offer_payment_value18)+($data->offer_payment_value20);
        $ans2 = $data->offer_payment_value4 - $ans;
        $ans3 = $ans2 - $data->offer_payment_value5;
        $ans4 = $ans3 - $data->offer_payment_value19;*/

       

        $result = $data->offer_payment_value4;
        $result2 = $result-$data->offer_payment_value5;
        $result3 = $result2-$data->offer_payment_value19;
        $alldiscount = ($data->offer_payment_value15)+($data->offer_payment_value16)+($data->offer_payment_value18)+($data->offer_payment_value20);
        $result = $result3 - $alldiscount;
          return $result;

    }
}

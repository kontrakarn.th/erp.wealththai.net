<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\Policy;
use App\PolicyType;

use App\Http\Controllers\SidebarController;

class PolicyController extends Controller
{

    public function index()
    {
      $policy = Policy::get();
      return view('admin/policy/index',compact('policy'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
        $policyType = PolicyType::get(['id','name','description']);
         return view('admin/policy/create',compact('policyType'));
     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
        date_default_timezone_set('Asia/Bangkok');

          Policy::create([
            'name' => $request['name'],
             'policy' => $request['policy'],
             'policy_type' => $request['policytype'],


         ]);

         return redirect ('/admin/policy');
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */


     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
        $policyType = PolicyType::get(['id','name','description']);
         $policy = Policy::find($id);
         // Redirect to country list if updating country wasn't existed
         if ($policy == null) {
           $policy = Port_type::find($id);
           $data = array(
               'policy' => $policy
             );
             return redirect ('/admin/policy');
         }

         return view('admin/policy/edit',compact('policy','policyType'));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         $input = [
           'name' => $request['name'],
           'policy' => $request['policy'],
           'policy_type' => $request['policytype'],

         ];

         Policy::where('id', $id)
             ->update($input);

         return redirect ('/admin/policy');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         Policy::where('id', $id)->delete();
          return redirect()->back();
     }

     /**
      * Search country from database base on some specific constraints
      *
      * @param  \Illuminate\Http\Request  $request
      *  @return \Illuminate\Http\Response
      */


     public function search(Request $request) {



         $constraints = [
             'name' => $request['name']

             ];

        $policy = $this->doSearchingQuery($constraints);
        return view('admin/policy/index', ['policy' => $policy, 'searchingVals' => $constraints]);
     }

     private function doSearchingQuery($constraints) {
         $query = Policy::query();
         $fields = array_keys($constraints);
         $index = 0;
         foreach ($constraints as $constraint) {
             if ($constraint != null) {
                 $query = $query->where( $fields[$index], 'like', '%'.$constraint.'%');
             }

             $index++;
         }
         return $query->paginate(1000000000000);
     }

 }

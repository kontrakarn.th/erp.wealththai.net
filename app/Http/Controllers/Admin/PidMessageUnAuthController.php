<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\PidMessageUnAuth;
use App\Message_cat;
use App\match_id;

use App\Http\Controllers\Controller;

class PidMessageUnAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('view');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pidMessageUnauth = PidMessageUnAuth::all();
      return view('/admin/pid_message_unauth/index',compact('pidMessageUnauth'));
    }
    public function create()
    {
      $pidMessageUnauth = [];
      $messageCategory = Message_cat::get(['id','name']);
      $publicID = match_id::get(['id','public_name']);

      return view('/admin/pid_message_unauth/form',compact('pidMessageUnauth','messageCategory','publicID'));
    }

    public function store(Request $request)
    {
      date_default_timezone_set('Asia/Bangkok');

      if(empty($request->id)){
        $saveData = New PidMessageUnAuth;
      }else{
        $saveData = PidMessageUnAuth::find($request->id);
      }
      $saveData->msg_category = $request->messageCategory;
      $saveData->msg_channel = $request->messageChannel;
      $saveData->pid = $request->publicID;
      $saveData->save();
      return redirect('/admin/pidunauthorizemsg');
    }
    
    public function edit($id)
    {
      $pidMessageUnauth = PidMessageUnAuth::find($id);
      $messageCategory = Message_cat::get(['id','name']);
      $publicID = match_id::get(['id','public_name']);
      return view('/admin/pid_message_unauth/form',compact('pidMessageUnauth','messageCategory','publicID'));
    }

    public function destroy($id)
    {
      PidMessageUnAuth::where('id',$id)->delete();
      return redirect('/admin/pidunauthorizemsg');
    }
}

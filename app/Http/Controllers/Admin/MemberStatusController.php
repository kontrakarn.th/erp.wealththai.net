<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\MemberStatus;
use App\Person;

use App\Http\Controllers\Controller;

class MemberStatusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('view');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $person =  Person::whereNull('status')->get();
      foreach($person as $data){
         $saveData = Person::find($data->id);
        $saveData->status = 2;
        $saveData->save();
      }
      $memberStatus = MemberStatus::all();
      return view('/admin/memberstatus/index',compact('memberStatus'));
    }
    public function create()
    {
      $memberStatus = [];
      return view('/admin/memberstatus/form',compact('memberStatus'));
    }

    public function store(Request $request)
    {
      if(empty($request->id)){
        $saveData = New MemberStatus;
      }else{
        $saveData = MemberStatus::find($request->id);
      }
      $saveData->name = $request->name;
      $saveData->save();
      return redirect('/admin/memberstatus');
    }
    
    public function edit($id)
    {
      $memberStatus = MemberStatus::find($id);
      return view('/admin/memberstatus/form',compact('memberStatus'));
    }

    public function destroy($id)
    {
      MemberStatus::where('id',$id)->delete();
      return redirect('/admin/memberstatus');
    }
}

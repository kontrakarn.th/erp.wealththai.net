<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PolicyType;
use Illuminate\Http\Request;

class PolicyTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('view');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $policyType = PolicyType::get(['id','name','description']);
        return view('/admin/policytype/index', compact('policyType'));
    }

    public function create()
    {
        $policyType = [];
        return view('/admin/policytype/form', compact('policyType'));
    }

    public function store(Request $request)
    {
      date_default_timezone_set('Asia/Bangkok');
        if (empty($request->id)) {
            $saveData = new PolicyType;
        } else {
            $saveData = PolicyType::find($request->id);
        }
        $saveData->name = $request->name;
        $saveData->description = $request->description;

        $saveData->save();
        return redirect('/admin/policytype');
    }

    public function edit($id)
    {
        $policyType = PolicyType::find($id);
        return view('/admin/policytype/form', compact('policyType'));
    }

    public function destroy($id)
    {
        PolicyType::where('id', $id)->delete();
        return redirect('/admin/policytype');
    }
}

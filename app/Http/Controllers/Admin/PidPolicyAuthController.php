<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\PidPolicyAuth;
use App\Policy;
use App\match_id;

use App\Http\Controllers\Controller;

class PidPolicyAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('view');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pidPolicyAuth = PidPolicyAuth::all();
      return view('/admin/pid_policy_auth/index',compact('pidPolicyAuth'));
    }
    public function create()
    {
      $pidPolicyAuth = [];
      $policy = Policy::get(['id','name']);
      $publicID = match_id::get(['id','public_name']);

      return view('/admin/pid_policy_auth/form',compact('pidPolicyAuth','policy','publicID'));
    }

    public function store(Request $request)
    {
      date_default_timezone_set('Asia/Bangkok');

      if(empty($request->id)){
        $saveData = New PidPolicyAuth;
      }else{
        $saveData = PidPolicyAuth::find($request->id);
      }
      $saveData->policy_id = $request->policy;
      $saveData->pid = $request->publicID;
      $saveData->save();
      return redirect('/admin/pidpolicyauth');
    }
    
    public function edit($id)
    {
      $pidPolicyAuth = PidPolicyAuth::find($id);
      $policy = Policy::get(['id','name']);
      $publicID = match_id::get(['id','public_name']);
      return view('/admin/pid_policy_auth/form',compact('policy','pidPolicyAuth','publicID'));
    }

    public function destroy($id)
    {
      PidPolicyAuth::where('id',$id)->delete();
      return redirect('/admin/pidpolicyauth');
    }
}

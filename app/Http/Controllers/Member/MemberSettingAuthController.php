<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\PidMessageUnAuth;
use App\Message_cat;

use App\Http\Controllers\DataMemberController;

class MemberSettingAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('viewper');
        $this->datamember = New DataMemberController;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $memberPID = $this->datamember->getPID();
      $messageCategory = Message_cat::get(['id','name']);
      
      return view('/member/settingauthorize/index',compact('memberPID','messageCategory'));
    }
    public function create()
    {
      $pidMessageUnauth = [];
      $messageCategory = Message_cat::get(['id','name']);
      $publicID = match_id::get(['id','public_name']);

      return view('/admin/pid_message_unauth/form',compact('pidMessageUnauth','messageCategory','publicID'));
    }

    public function store(Request $request)
    {
      date_default_timezone_set('Asia/Bangkok');
      $memberPID = $this->datamember->getPID();
     if(!empty($request->email)){
       $emailUnauth = $request->email;
       PidMessageUnAuth::where('pid',$memberPID)->where('msg_channel',1)->whereNotIn('msg_category',$emailUnauth)->delete();
       for($i=0;$i<count($emailUnauth);$i++){
         $checkUnauth = PidMessageUnAuth::where('pid',$memberPID)->where('msg_channel',1)->where('msg_category',$emailUnauth[$i])->count();
         if($checkUnauth <= 0){
          $saveData = New PidMessageUnAuth;
          $saveData->msg_category = $emailUnauth[$i];
          $saveData->msg_channel = 1;
          $saveData->pid = $memberPID;
          $saveData->save();
         }
       }
     }else{
      $checkUnauth = PidMessageUnAuth::where('pid',$memberPID)->where('msg_channel',1)->delete();
     }
     if(!empty($request->line)){
      $lineUnauth = $request->line;
       PidMessageUnAuth::where('pid',$memberPID)->where('msg_channel',2)->whereNotIn('msg_category',$lineUnauth)->delete();
      for($i=0;$i<count($lineUnauth);$i++){
        $checkUnauth = PidMessageUnAuth::where('pid',$memberPID)->where('msg_channel',2)->where('msg_category',$lineUnauth[$i])->count();
        if($checkUnauth <= 0){
         $saveData = New PidMessageUnAuth;
         $saveData->msg_category = $lineUnauth[$i];
         $saveData->msg_channel = 2;
         $saveData->pid = $memberPID;
         $saveData->save();
        }
      }
    }else{
      $checkUnauth = PidMessageUnAuth::where('pid',$memberPID)->where('msg_channel',2)->delete();
    }
      $mesg = "บันทึกเรียบร้อย";
      $status = true;


      return response()->json([
          'status' => $status,
          'message' => $mesg,
          //'case_id' => $caseCreate->id,
      ]);
    }
    
    public function edit($id)
    {
      $pidMessageUnauth = PidMessageUnAuth::find($id);
      $messageCategory = Message_cat::get(['id','name']);
      $publicID = match_id::get(['id','public_name']);
      return view('/admin/pid_message_unauth/form',compact('pidMessageUnauth','messageCategory','publicID'));
    }

    public function destroy($id)
    {
      PidMessageUnAuth::where('id',$id)->delete();
      return redirect('/admin/pidunauthorizemsg');
    }
}

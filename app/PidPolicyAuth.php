<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PidPolicyAuth extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     protected $table ='pid_policy_auth';
   
    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */

    public function policy()
    {
    return $this->belongsTo('App\Policy','policy_id');
    }
    
    public function publicid()
    {
    return $this->belongsTo('App\match_id','pid');
    }

}
